<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vacantes', function (Blueprint $table) {
            $table->id();
            $table->string('cargo');
            $table->unsignedBigInteger('id_area');
            $table->unsignedBigInteger('id_puesto');
            $table->unsignedBigInteger('id_estado');
            $table->string('salario');
            $table->string('publicado');
            $table->longText('brief');
            $table->longText('describelo');
            $table->longText('requisitos');
            $table->longText('ofrecemos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vacantes');
    }
};
