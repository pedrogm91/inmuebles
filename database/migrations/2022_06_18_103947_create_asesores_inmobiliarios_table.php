<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asesores_inmobiliarios', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_usuario')->nullable();
            $table->string('nombres_apellidos');//
            $table->string('cedula');//
            $table->string('telefono');//
            $table->string('telefono_movil');//
            $table->string('correo');//
            $table->string('fecha_nacimiento');//
            $table->string('direccion');//
            $table->string('area_de_trabajo');//
            $table->string('tipo_de_contrato');//
            $table->string('vehiculo');//
            $table->string('lugar_de_trabajo');//
            $table->string('porcentaje_ganancia');//
            $table->string('total_percibido');//
            $table->string('recomendado');//
            $table->string('estatus');//
            $table->string('imagen_perfil')->nullable();//
            $table->string('experiencia');//
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asesores_inmobiliarios');
    }
};
