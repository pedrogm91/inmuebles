<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citas', function (Blueprint $table) {
            $table->id();
//          $table->unsignedBigInteger('id_prospect');
            $table->unsignedBigInteger('id_asesor');
            $table->unsignedBigInteger('id_propiedad');
            $table->datetime('fecha_cita');//
            $table->string('hora_cita');//

            $table->string('nombre_cliente')->nullable();//
            $table->string('telefono_cliente')->nullable();//
            $table->string('correo_cliente')->nullable();//
// 
//          $table->string('telefono_prospect');//
//          $table->string('correo_prospect');//
// 
            $table->enum('prioridad',['Baja','Media','Alta'])->default('Media');//
            $table->string('asunto');//
            $table->string('estatus');//
            $table->string('descripcion')->nullable();
            $table->string('observaciones')->nullable();
            $table->string('url')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('citas');
    }
};
