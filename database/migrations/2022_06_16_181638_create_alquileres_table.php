<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        // Schema::create('alquileres', function (Blueprint $table) {
        //     $table->id();
        //     $table->unsignedBigInteger('id_propiedad');
        //     $table->string('precio_final_venta');
        //     $table->string('oferta')->nullable();
        //     $table->string('porcentaje_asesor')->nullable();
        //     $table->string('fecha_corte_pago');
        //     $table->string('sistema_de_pago');
        //     $table->string('estatus');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('alquileres');
    }
};
