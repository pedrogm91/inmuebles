<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propiedades', function (Blueprint $table) {
           
            $table->id();
            $table->string('nombre_propiedad');
            $table->string('estatus_publicacion');
            $table->string('tipo_negocio');
            $table->string('id_categoria');
            $table->unsignedBigInteger('id_asesor_asignado');
            $table->boolean('destacado');
            $table->string('precio');
            $table->string('ubicacion');
            $table->string('id_estado');
            $table->string('id_municipio');
            $table->string('id_parroquia')->nullable();
            $table->string('id_ciudad')->nullable();
            $table->string('oferta')->nullable();
            $table->json('datos_propietario')->nullable();
            $table->json('datos_apoderado')->nullable();
            $table->json('instalaciones_cercanas')->nullable();
            $table->json('datos_construccion')->nullable();
            $table->json('servicios')->nullable();
            $table->longText('descripcion')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('propiedades');
    }
};
