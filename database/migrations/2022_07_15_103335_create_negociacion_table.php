<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('negociaciones', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_propiedad');
            $table->unsignedBigInteger('id_asesor');
            $table->string('nombre_cliente')->nullable();
            $table->string('tel_cliente')->nullable();
            $table->enum('tipo_negocio',['Venta','Alquiler','Reserva','Anula Reserva']);
            $table->date('fecha_oferta');
            $table->string('monto_mediacion');
            $table->string('porcentaje_honorarios');
            $table->string('sistema_de_pago');
            $table->string('fecha_inicial')->nullable();
            $table->string('fecha_final')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('negociaciones');
    }
};
