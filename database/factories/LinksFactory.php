<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Experience>
 */
class LinksFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_menu'  => $this->faker->numberBetween(1,2),
            'nombre'   => $this->faker->unique()->randomElement(['Comprar', 'Alquiler', 'Nosotros', 'Experiencias', 'Solicita una cita', 'Blog', 'Contáctanos']),
            'route'    => $this->faker->unique()->randomElement(['comprar', 'alquiler', 'nosotros', 'experiencias', 'solicita-una-cita', 'blog', 'contactanos']),
            'ordering' => $this->faker->unique()->randomElement(['1', '2', '3', '4', '5', '6', '7']),
            'status'   => $this->faker->randomElement(['Habilitado', 'Deshabilitado']),
        ];
    }
}
