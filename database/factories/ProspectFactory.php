<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Prospect>
 */
class ProspectFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nombres_apellidos' => $this->faker->name(),
            'cedula' => $this->faker->numberBetween($int1=500000,$int2=30000000),
            'telefono' => $this->faker->phoneNumber(),
            'telefono_movil' => $this->faker->phoneNumber(),
            'correo' => $this->faker->email(),
            'direccion' => $this->faker->address()
        ];
    }
}
