<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Experience>
 */
class ExperienceFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'fecha_experiencia' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'redes_sociales'    => $this->faker->randomElement(array('twitter: qwerty', 'facebook: qwerty', 'instagram: qwerty')),
            'horario'           => $this->faker->sentence(),
            'sitio_web'         => $this->faker->domainName(),
            'mapa'              => '{"twitter":"qwerty","facebook":"qwerty","instagram":"qwerty"}',
            'descripcion'       => $this->faker->text(200),
            'portada'           => $this->faker->randomElement(array('active', 'inactive')),
            'fecha_publicacion' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
        ];
    }
}
