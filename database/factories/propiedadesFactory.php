<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class propiedadesFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nombre_propiedad' => $this->faker->name(),
            'estatus_publicacion' => $this->faker->numberBetween(1,3),
            'tipo_negocio' => $this->faker->numberBetween(1,2),
            'id_categoria' => $this->faker->numberBetween(1,8),
            'id_asesor_asignado' => $this->faker->numberBetween(1,10),
            'destacado' => $this->faker->boolean(),
            'precio' => $this->faker->numberBetween(0,10000000) ,
            'ubicacion' => $this->faker->address(),
            'id_estado' => 1,
            'id_municipio' => 1,
            'id_parroquia' => 1,
            'id_ciudad' => 1,
            'datos_propietario' => '{"tel_propietario": "45678987", "email_propietario": "weqimenyd@example.org", "nombre_propietario": "Incidunt et ratione"}',
            'datos_apoderado' => '{"tel_apoderado": null, "email_apoderado": null, "nombre_apoderado": null}',
            'instalaciones_cercanas' => '{"autobus": "Si", "clinica": "Si", "colegios": "Si", "farmacia": "No", "supermercado": "No", "estacion_metro": "0", "centro_comercial": "No"}',
            'datos_construccion' => '{"tv": "0", "sala": "10", "wifi": "3", "banos": "1", "habit": "1", "aire_a": "0", "cocina": "8", "banos_s": "6", "habit_s": "4", "piscina": "3", "gimnasio": "1", "lavadero": "0", "maletero": "1", "antiguedad": "3", "bano_medio": "9", "parrillera": "2", "descripcion": "Ipsum maxime quod e", "area_terreno": "500", "estacionamiento": "0", "area_construccion": "450", "tipo_estacionamiento": "Techado"}',
            'descripcion' => $this->faker->text(),
        ];
    }
}
