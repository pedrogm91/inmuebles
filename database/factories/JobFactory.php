<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\State>
 */
class JobFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'cargo'      => $this->faker->sentence(),
            'id_area'    => $this->faker->numberBetween(1,6),
            'id_puesto'  => $this->faker->numberBetween(1,2),
            'id_estado'  => $this->faker->numberBetween(1,24),
            'salario'    => $this->faker->numberBetween(100,2000),
            'publicado'  => $this->faker->randomElement(['Si', 'No']),
            'brief'      => $this->faker->sentence(),
            'describelo' => $this->faker->sentence(),
            'requisitos' => $this->faker->sentence(),
            'ofrecemos'  => $this->faker->sentence(),
        ];
    }
}
