<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class LeadFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'prospect_id' => $this->faker->numberBetween(1,40),
            'nombres_apellidos' => $this->faker->name(),
            'interes' => $this->faker->randomElement(['compra','venta','cita','visita','registro']),
            'datos_adicionales' => null,
        ];
    }
}
