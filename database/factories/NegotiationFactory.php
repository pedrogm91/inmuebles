<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Negotiation>
 */
class NegotiationFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_propiedad' => $this->faker->numberBetween(1,100),
            'id_asesor' => $this->faker->numberBetween(1,10),
            'nombre_cliente' => $this->faker->name(),
            'tel_cliente' => $this->faker->phoneNumber(),
            'tipo_negocio' => $this->faker->randomElement(['Venta','Alquiler','Reserva','Anula Reserva']),
            'monto_mediacion' => $this->faker->numberBetween(2000,500000),
            'fecha_oferta' => $this->faker->dateTimebetween("-2 years",now()),
            'porcentaje_honorarios' => $this->faker->randomElement(['10','20','30','40','50']),
            'sistema_de_pago' => $this->faker->randomElement(['Divisa en Efectivo','Cryptomonedas','Zelle','Transferencia Internacional','Transferencia Nacional']),
            'fecha_inicial' => date('Y-m-d'),
            'fecha_final' => date('Y-m-d', strtotime(date('Y-m-d').' + 3 months'))
        ];
    }
}
