<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class ventasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_asesor' => $this->faker->numberBetween(1,15),
            'id_propiedad' => $this->faker->numberBetween(1,80),
            'precio_final_venta' => $this->faker->numberBetween(1000,100000),
            'oferta' => $this->faker->randomElement(['10%','5%','8%','50%','1%']),
            'porcentaje_asesor' => $this->faker->randomElement(['10%','5%','8%','20%','1%']),
            'sistema_de_pago' => $this->faker->randomElement(['contado','credito a 6 meses','credito a 12 meses','credito a 24 meses','credito a 36 meses']),
            'estatus' => $this->faker->randomElement(['activa','post-puesta','pausada','cancelada','concreatada']),
        ];
    }
}
