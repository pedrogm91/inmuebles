<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class respuestasCitasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'citas_id' => $this->faker->numberBetween(1,20),
            'asesores_inmobiliarios_id' => $this->faker->numberBetween(1,20),
            'texto' => $this->faker->sentence(8)
        ];
    }
}
