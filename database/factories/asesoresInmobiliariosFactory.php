<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class asesoresInmobiliariosFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_usuario' => $this->faker->numberBetween(1,10),
            'nombres_apellidos' => $this->faker->name(),
            'cedula' => $this->faker->numberBetween($int1= 100000, $int2= 30000000),
            'telefono' => $this->faker->phoneNumber(),
            'telefono_movil' => $this->faker->phoneNumber(),
            'correo' => $this->faker->email(),
            'fecha_nacimiento' => $this->faker->date(),
            'direccion' => $this->faker->address(),
            'area_de_trabajo' => $this->faker->numberBetween($int1=0,$int2=4),
            'tipo_de_contrato' => $this->faker->numberBetween($int1=1,$int2=3),
            'vehiculo' => $this->faker->boolean(),
            'lugar_de_trabajo' => $this->faker->sentence(),
            'porcentaje_ganancia' => $this->faker->numberBetween($int1=1,$int2=3),
            'total_percibido' => $this->faker->numberBetween($int1=10000,$int2=300000),
            'recomendado' => $this->faker->boolean(),
            'estatus' => $this->faker->randomElement(array('activo','suspendido')),
            'imagen_perfil' => $this->faker->imageUrl(),
            'experiencia' => $this->faker->randomElement(array('1 año','5 años',"3 años y medio","10 años", "6 meses", "7 años","12 años")),

        ];
    }
}
