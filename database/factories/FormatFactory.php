<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Experience>
 */
class FormatFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'name'   => $this->faker->sentence(),
            'route'  => $this->faker->file($sourceDir = 'public/storage/company', $targetDir = 'public/storage/formatos', false),
            'status' => $this->faker->randomElement(['Habilitado', 'Deshabilitado']),
        ];
    }
}
