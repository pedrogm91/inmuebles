<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class OfferFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'precio' => $this->faker->numberBetween(0,10000000),
            'fecha_oferta' => $this->faker->date($format = 'Y-m-d', $max = 'now'),
            'id_propiedad' => $this->faker->numberBetween(1, 100),
            'id_asesor' => $this->faker->numberBetween(1, 10),
            'fecha_caducidad' => $this->faker->date($format = 'Y-m-d', $max = '+30 days'),
            'margen_de_ganancia' => $this->faker->numberBetween(1, 100).'%'
        ];
    }
}
