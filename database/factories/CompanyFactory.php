<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Experience>
 */
class CompanyFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nombre' =>  'Habita Inmueble',
            'rif' =>  null,
            'telefono' =>  '(+58) 212 961 6062',
            'celular' =>  '(+58) 414 151 4151',
            'fax' =>  null,
            'direccion' =>  'Centro de Servicio Plaza La Boyera, Urbanización La Boyera, Municipio El Hatillo',
            'correo' =>  'atencion@habitainmueble.com',
            'horario' =>  'Lun - Vie 8:30 AM - 6:00 PM',
            'whatsapp' =>  '(+58) 414 151 4151',
            'twitter' =>  null,
            'facebook' =>  'https://www.facebook.com/habitainmueble/',
            'instagram' =>  'https://www.instagram.com/habitainmueble/',
            'youtube' =>  null,
            'linkedin' =>  null,
            'logo' => null
        ];
    }
}
