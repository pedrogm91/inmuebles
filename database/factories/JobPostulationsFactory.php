<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\State>
 */
class JobPostulationsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'id_vacante' => $this->faker->numberBetween(1,10),
            'nombre' => $this->faker->name(),
            'correo' => $this->faker->email(),
            'telefono' => $this->faker->phoneNumber(),
            'comentario' => $this->faker->sentence(),
            'adjunto'  => null,
        ];
    }
}
