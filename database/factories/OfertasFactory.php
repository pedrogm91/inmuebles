<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class OfertasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            
            'precio' => $this->faker->numberBetween(2000,500000),
            'fecha_oferta' => $this->faker->dateTimebetween("-2 years",now()),
            'id_propiedad' => $this->faker->numberBetween(1,100),
            'id_asesor' => $this->faker->numberBetween(1,15),
            'fecha_caducidad' => $this->faker->dateTimebetween("-2 years","1 year"),
            'margen_de_ganancia' => $this->faker->randomElement(['10%','20%','30%','5%','2%','1%','0,5%',])
        ];
    }
}
