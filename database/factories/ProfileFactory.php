<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Profile>
 */
class ProfileFactory extends Factory
{


    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'first_name' => $this->faker->name(),
            'second_name'=> $this->faker->name(),
            'surname' => $this->faker->lastName(),
            'second_surname' => $this->faker->lastName(),
            'dni' => $this->faker->randomNumber(),
            'date_birth' => $this->faker->date(),
            'phone' => $this->faker->phoneNumber(),
            'second_phone' => $this->faker->phoneNumber(),
            'percentage' => '50',
            'job' => $this->faker->randomElement(array('Gerente', 'Asesor')),
            'work_address' => $this->faker->address(),
            'house_address' => $this->faker->address(),
            'type_contract' => $this->faker->randomElement(array('Fijo', 'Contratado')),
            'years_experience' => '1 - 5',
            'referred' => 'No',
            'vehicle' => 'No',
            'email' => $this->faker->safeEmail(),
        ];
    }
}
