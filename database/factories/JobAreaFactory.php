<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\State>
 */
class JobAreaFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'nombre' => $this->faker->unique()->randomElement(array (
                'Administracion', 'Mantenimiento', 'Asesor Inmobiliario', 'Asesor Legal', 'Directora', 'Gerente General'
                )),
        ];
    }
}
