<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Citas>
 */
class CitasFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'fecha_cita' => $this->faker->dateTimebetween("-2 years","1 year"),
            'hora_cita' => $this->faker->randomElement(['08:00 am','07:00 am','09:00 am','06:30 am','10:15 am','12:00 m','01:00 pm','02:00 pm','01:450 pm','02:00 pm','03:00 pm']),
            'nombre_cliente' => 'Nombre del cliente',
            'id_asesor' => $this->faker->numberBetween(1,10),
            'id_propiedad' => $this->faker->numberBetween(1,100),
            'telefono_cliente' => $this->faker->phoneNumber(),
            'correo_cliente' => $this->faker->email(),
            'prioridad' => $this->faker->randomElement(['Baja','Media','Alta']),
            'asunto' => $this->faker->randomElement(['Visita lugar', 'Concretar venta','Explicar precio','proponer oferta','recibir papeles propiedad','gestionar documentos prospect','recibir catastro']),
            'descripcion' => $this->faker->sentence(10),
            'observaciones' => $this->faker->sentence(3),
            'estatus' => $this->faker->randomElement(['Por confirmar','Confirmada','Cancelada']),
            'url' => $this->faker->url(),
        ];
    }
}
