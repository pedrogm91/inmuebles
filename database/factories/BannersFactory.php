<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Experience>
 */
class BannersFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'slogan' =>  'Tu casa es nuestra prioridad, confía en los que saben.',
            'banner_principal' => null,
            'banner_mobile' => null
        ];
    }
}
