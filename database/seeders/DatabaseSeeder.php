<?php

namespace Database\Seeders;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Seeder;
use App\Models\asesoresInmobiliarios;
use App\Models\propiedades;
use App\Models\User;
use App\Models\State;
use App\Models\Municipality;
use App\Models\Parish;
use App\Models\Location;
use App\Models\CategoryProperty;
use App\Models\citas;
use App\Models\Ofertas;
use App\Models\respuestasCitas;
use App\Models\Offer;
use App\Models\Negotiation;
use App\Models\JobPlace;
use App\Models\JobArea;
use App\Models\Job;
use App\Models\JobPostulations;
use App\Models\Company;
use App\Models\Banners;
use App\Models\LeadActivity;
use App\Models\Links;
use App\Models\Menus;
use App\Models\Prospect;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        User::factory(10)->create();
        User::create([
            'name' => 'Pedro Elías',
            'email' => 'pedrogm91@gmail.com',
            'password' => Hash::make('Admin.2020'),
            'status' => 'active',
            'role' => 'admin'
        ]);
        AsesoresInmobiliarios::factory(10)->create();
        Prospect::factory(50)->create();
        
        // -- State::factory(24)->create();
        // -- Municipios::factory(20)->create();
        // -- Parish::factory(20)->create();
        // -- Location::factory(20)->create();

        Propiedades::factory(100)->create();
        CategoryProperty::factory(8)->create();
        citas::factory(40)->createQuietly();
        respuestasCitas::factory(200)->create();

        //--ventas::factory(100)->createQuietly();

        Ofertas::factory(100)->createQuietly();
        Offer::factory(2)->createQuietly();
        Negotiation::factory(20)->createQuietly();
        JobPlace::factory(2)->create();
        JobArea::factory(6)->create();
        Job::factory(20)->create();
        JobPostulations::factory(5)->create();

        // LeadActivity::factory(40)->create();
        Company::factory(1)->create();
        Banners::factory(1)->create();
        Menus::factory(2)->create();
        Links::factory(7)->create();

        // ESTADOS | MUNICIPIOS | CIUDADES


        State::insert([
            [
                'id' => 1,
                'nombre' => 'Amazonas',
                'capital' => 'Puerto Ayacucho',
            ],
            [
                'id' => 2,
                'nombre' => 'Anzoátegui',
                'capital' => 'Barcelona',
            ],
            [
                'id' => 3,
                'nombre' => 'Apure',
                'capital' => 'San Fernando de Apure',
            ],
            [
                'id' => 4,
                'nombre' => 'Aragua',
                'capital' => 'Maracay',
            ],
            [
                'id' => 5,
                'nombre' => 'Barinas',
                'capital' => 'Barinas',
            ],
            [
                'id' => 6,
                'nombre' => 'Bolívar',
                'capital' => 'Ciudad Bolívar',
            ],
            [
                'id' => 7,
                'nombre' => 'Carabobo',
                'capital' => 'Valencia',
            ],
            [
                'id' => 8,
                'nombre' => 'Cojedes',
                'capital' => 'San Carlos',
            ],
            [
                'id' => 9,
                'nombre' => 'Delta Amacuro',
                'capital' => 'Tucupita',
            ],
            [
                'id' => 10,
                'nombre' => 'Falcón',
                'capital' => 'Coro',
            ],
            [
                'id' => 11,
                'nombre' => 'Guárico',
                'capital' => 'San Juan de Los Morros',
            ],
            [
                'id' => 12,
                'nombre' => 'Lara',
                'capital' => 'Barquisimeto',
            ],
            [
                'id' => 13,
                'nombre' => 'Mérida',
                'capital' => 'Mérida',
            ],
            [
                'id' => 14,
                'nombre' => 'Miranda',
                'capital' => 'Los Teques',
            ],
            [
                'id' => 15,
                'nombre' => 'Monagas',
                'capital' => 'Maturín',
            ],
            [
                'id' => 16,
                'nombre' => 'Nueva Esparta',
                'capital' => 'La Asunción',
            ],
            [
                'id' => 17,
                'nombre' => 'Portuguesa',
                'capital' => 'Guanare',
            ],
            [
                'id' => 18,
                'nombre' => 'Sucre',
                'capital' => 'Cumaná',
            ],
            [
                'id' => 19,
                'nombre' => 'Táchira',
                'capital' => 'San Cristóbal',
            ],
            [
                'id' => 20,
                'nombre' => 'Trujillo',
                'capital' => 'Trujillo',
            ],
            [
                'id' => 21,
                'nombre' => 'Vargas',
                'capital' => 'La Guaira',
            ],
            [
                'id' => 22,
                'nombre' => 'Yaracuy',
                'capital' => 'San Felipe',
            ],
            [
                'id' => 23,
                'nombre' => 'Zulia',
                'capital' => 'Maracaibo',
            ],
            [
                'id' => 24,
                'nombre' => 'Distrito Capital',
                'capital' => 'Caracas',
            ],
        ]);

        // Amazonas | Municipios
        Municipality::insert([
            [
                'id' => 1,
                'id_estado' => 1,
                'nombre' => 'Alto Orinoco',
            ],
            [
                'id' => 2,
                'id_estado' => 1,
                'nombre' => 'Atabapo',
            ],
            [
                'id' => 3,
                'id_estado' => 1,
                'nombre' => 'Atures',
            ],
            [
                'id' => 4,
                'id_estado' => 1,
                'nombre' => 'Autana',
            ],
            [
                'id' => 5,
                'id_estado' => 1,
                'nombre' => 'Manapiare',
            ],
            [
                'id' => 6,
                'id_estado' => 1,
                'nombre' => 'Maroa',
            ],
            [
                'id' => 7,
                'id_estado' => 1,
                'nombre' => 'Río Negro',
            ]
        ]);

        // Amazonas | Ciudades
        Location::insert([
            [
                'id_estado' => 1,
                'nombre' => 'Maroa',
            ],
            [
                'id_estado' => 1,
                'nombre' => 'Puerto Ayacucho',
            ],
            [
                'id_estado' => 1,
                'nombre' => 'San Fernando de Atabapo',
            ],
        ]);

        // Anzoátegui | Municipios
        Municipality::insert([
            [
                'id_estado' => 2,
                'nombre' => 'Anaco',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Aragua',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Bolívar',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Bruzual',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Cajigal',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Carvajal',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Diego Bautista Urbaneja',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Freites',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Guanipa',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Guanta',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Independencia',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Libertad',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'McGregor',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Miranda',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Monagas',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Peñalver',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Píritu',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'San Juan de Capistrano',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Santa Ana',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Simón Rodríguez',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Sotillo',
            ],
        ]);

        // Anzoátegui | Ciudades
        Location::insert([
            [
                'id_estado' => 2,
                'nombre' => 'Anaco',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Aragua de Barcelona',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Barcelona',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Boca de Uchire',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Cantaura',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Clarines',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'El Chaparro',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'El Pao Anzoátegui',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'El Tigre',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'El Tigrito',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Guanape',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Guanta',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Lechería',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Onoto',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Pariaguán',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Píritu',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Puerto La Cruz',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Puerto Píritu',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Sabana de Uchire',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'San Mateo Anzoátegui',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'San Pablo Anzoátegui',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'San Tomé',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Santa Ana de Anzoátegui',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Santa Fe Anzoátegui',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Santa Rosa',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Soledad',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Urica',
            ],
            [
                'id_estado' => 2,
                'nombre' => 'Valle de Guanape',
            ],
        ]);

        // Apure | Municipios
        Municipality::insert([
            [
                'id_estado' => 3,
                'nombre' => 'Achaguas',
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Biruaca',
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Muñoz',
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Páez',
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Pedro Camejo',
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Rómulo Gallegos',
            ],
            [
                'id_estado' => 3,
                'nombre' => 'San Fernando',
            ]
        ]);

        // Apure | Ciudades
        Location::insert([
            [
                'id_estado' => 3,
                'nombre' => 'Achaguas'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Biruaca'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Bruzual'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'El Amparo'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'El Nula'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Elorza'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Guasdualito'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Mantecal'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'Puerto Páez'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'San Fernando de Apure'
            ],
            [
                'id_estado' => 3,
                'nombre' => 'San Juan de Payara'
            ],
        ]);

        // Aragua | Municipios
        Municipality::insert([
            [
                'id_estado' => 4,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Camatagua",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Francisco Linares Alcántara",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Girardot",
            ],
            [
                'id_estado' => 4,
                "nombre" => "José Ángel Lamas",
            ],
            [
                'id_estado' => 4,
                "nombre" => "José Félix Ribas",
            ],
            [
                'id_estado' => 4,
                "nombre" => "José Rafael Revenga",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Libertador",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Mario Briceño Iragorry",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Ocumare de la Costa de Oro",
            ],
            [
                'id_estado' => 4,
                "nombre" => "San Casimiro",
            ],
            [
                'id_estado' => 4,
                "nombre" => "San Sebastián",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Santiago Mariño",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Santos Michelena",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Tovar",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Urdaneta",
            ],
            [
                'id_estado' => 4,
                "nombre" => "Zamora",
            ]
        ]);

        // Aragua | Ciudades
        Location::insert([
            [
                'id_estado' => 4,
                "nombre" => "Barbacoas"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Cagua"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Camatagua"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Choroní"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Colonia Tovar"
            ],
            [
                'id_estado' => 4,
                "nombre" => "El Consejo"
            ],
            [
                'id_estado' => 4,
                "nombre" => "La Victoria"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Las Tejerías"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Magdaleno"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Maracay"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Ocumare de La Costa"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Palo Negro"
            ],
            [
                'id_estado' => 4,
                "nombre" => "San Casimiro"
            ],
            [
                'id_estado' => 4,
                "nombre" => "San Mateo"
            ],
            [
                'id_estado' => 4,
                "nombre" => "San Sebastián"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Santa Cruz de Aragua"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Tocorón"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Turmero"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Villa de Cura"
            ],
            [
                'id_estado' => 4,
                "nombre" => "Zuata"
            ]
        ]);

        // Barinas | Municipios
        Municipality::insert([
            [
                'id_estado' => 5,
                "nombre" => "Alberto Arvelo Torrealba",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Andrés Eloy Blanco",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Antonio José de Sucre",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Arismendi",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Barinas",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Cruz Paredes",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Ezequiel Zamora",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Obispos",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Pedraza",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Rojas",
            ],
            [
                'id_estado' => 5,
                "nombre" => "Sosa",
            ]
        ]);

        // Barinas | Ciudades
        Location::insert([
            [
                'id_estado' => 5,
                "nombre" => "Barinas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Barinitas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Barrancas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Calderas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Capitanejo"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Ciudad Bolivia"
            ],
            [
                'id_estado' => 5,
                "nombre" => "El Cantón"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Las Veguitas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Libertad de Barinas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Sabaneta"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Santa Bárbara de Barinas"
            ],
            [
                'id_estado' => 5,
                "nombre" => "Socopó"
            ]
        ]);

        // Bolívar | Municipios
        Municipality::insert([
            [
                'id_estado' => 6,
                "nombre" => "Angostura",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Caroní",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Cedeño",
            ],
            [
                'id_estado' => 6,
                "nombre" => "El Callao",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Gran Sabana",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Heres",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Píar",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Roscio",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Sifontes",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 6,
                "nombre" => "Padre Pedro Chien",
            ]
        ]);

        // Bolívar | Ciudades
        Location::insert([
            [
                'id_estado' => 6,
                "nombre" => "Caicara del Orinoco"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Canaima"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Ciudad Bolívar"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Ciudad Piar"
            ],
            [
                'id_estado' => 6,
                "nombre" => "El Callao"
            ],
            [
                'id_estado' => 6,
                "nombre" => "El Dorado"
            ],
            [
                'id_estado' => 6,
                "nombre" => "El Manteco"
            ],
            [
                'id_estado' => 6,
                "nombre" => "El Palmar"
            ],
            [
                'id_estado' => 6,
                "nombre" => "El Pao"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Guasipati"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Guri"
            ],
            [
                'id_estado' => 6,
                "nombre" => "La Paragua"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Matanzas"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Puerto Ordaz"
            ],
            [
                'id_estado' => 6,
                "nombre" => "San Félix"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Santa Elena de Uairén"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Tumeremo"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Unare"
            ],
            [
                'id_estado' => 6,
                "nombre" => "Upata"
            ]
        ]);

        // Carabobo | Municipios
        Municipality::insert([
            [
                'id_estado' => 7,
                "nombre" => "Bejuma",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Carlos Arvelo",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Diego Ibarra",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Guacara",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Mora",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Libertador",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Los Guayos",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Miranda",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Montalbán",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Naguanagua",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Puerto Cabello",
            ],
            [
                'id_estado' => 7,
                "nombre" => "San Diego",
            ],
            [
                'id_estado' => 7,
                "nombre" => "San Joaquín",
            ],
            [
                'id_estado' => 7,
                "nombre" => "Valencia",
            ]
        ]);

        // Carabobo | Ciudades
        Location::insert([
            [
                'id_estado' => 7,
                "nombre" => "Bejuma"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Belén"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Campo de Carabobo"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Canoabo"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Central Tacarigua"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Chirgua"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Ciudad Alianza"
            ],
            [
                'id_estado' => 7,
                "nombre" => "El Palito"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Guacara"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Guigue"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Las Trincheras"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Los Guayos"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Mariara"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Miranda"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Montalbán"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Morón"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Naguanagua"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Puerto Cabello"
            ],
            [
                'id_estado' => 7,
                "nombre" => "San Joaquín"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Tocuyito"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Urama"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Valencia"
            ],
            [
                'id_estado' => 7,
                "nombre" => "Vigirimita"
            ],
        ]);

        // Cojedes | Municipios
        Municipality::insert([
            [
                'id_estado' => 8,
                "nombre" => "Anzoátegui",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Tinaquillo",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Girardot",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Lima Blanco",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Pao de San Juan Bautista",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Ricaurte",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Rómulo Gallegos",
            ],
            [
                'id_estado' => 8,
                "nombre" => "San Carlos",
            ],
            [
                'id_estado' => 8,
                "nombre" => "Tinaco",
            ]
        ]);

        // Cojedes | Ciudades
        Location::insert([
            [
                'id_estado' => 8,
                "nombre" => "Aguirre"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Apartaderos Cojedes"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Arismendi"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Camuriquito"
            ],
            [
                'id_estado' => 8,
                "nombre" => "El Baúl"
            ],
            [
                'id_estado' => 8,
                "nombre" => "El Limón"
            ],
            [
                'id_estado' => 8,
                "nombre" => "El Pao Cojedes"
            ],
            [
                'id_estado' => 8,
                "nombre" => "El Socorro"
            ],
            [
                'id_estado' => 8,
                "nombre" => "La Aguadita"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Las Vegas"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Libertad de Cojedes"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Mapuey"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Piñedo"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Samancito"
            ],
            [
                'id_estado' => 8,
                "nombre" => "San Carlos"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Sucre"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Tinaco"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Tinaquillo"
            ],
            [
                'id_estado' => 8,
                "nombre" => "Vallecito"
            ]
        ]);

        // Delta Amacuro | Municipios
        Municipality::insert([
            [
                'id_estado' => 9,
                "nombre" => "Antonio Díaz",
            ],
            [
                'id_estado' => 9,
                "nombre" => "Casacoima",
            ],
            [
                'id_estado' => 9,
                "nombre" => "Pedernales",
            ],
            [
                'id_estado' => 9,
                "nombre" => "Tucupita",
            ]
        ]);

        // Delta Amacuro | Ciudades
        Location::insert([
            [
                'id_estado' => 9,
                "nombre" => "Tucupita",
            ]
        ]);

        // Falcón | Municipios
        Municipality::insert([
            [
                'id_estado' => 10,
                "nombre" => "Acosta",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Buchivacoa",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Carirubana",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Colina",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Dabajuro",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Democracia",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Falcón",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Federación",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Jacura",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Los Taques",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Manaure",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Mauroa",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Miranda",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Monseñor Iturriza",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Palmasola",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Petit",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Píritu",
            ],
            [
                'id_estado' => 10,
                "nombre" => "San Francisco",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Silva",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Tocópero",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Unión",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Urumaco",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Zamora",
            ]
        ]);

        // Falcón | Ciudades
        Location::insert([
            [
                'id_estado' => 10,
                "nombre" => "Adícora",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Boca de Aroa",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Cabure",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Capadare",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Capatárida",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Chichiriviche",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Churuguara",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Coro",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Cumarebo",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Dabajuro",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Judibana",
            ],
            [
                'id_estado' => 10,
                "nombre" => "La Cruz de Taratara",
            ],
            [
                'id_estado' => 10,
                "nombre" => "La Vela de Coro",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Los Taques",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Maparari",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Mene de Mauroa",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Mirimire",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Pedregal",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Píritu Falcón",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Pueblo Nuevo Falcón",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Puerto Cumarebo",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Punta Cardón",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Punto Fijo",
            ],
            [
                'id_estado' => 10,
                "nombre" => "San Juan de Los Cayos",
            ],
            [
                'id_estado' => 10,
                "nombre" => "San Luis",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Santa Ana Falcón",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Santa Cruz De Bucaral",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Tocopero",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Tocuyo de La Costa",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Tucacas",
            ],
            [
                'id_estado' => 10,
                "nombre" => "Yaracal",
            ]

        ]);

        // Guárico | Municipios
        Municipality::insert([
            [
                'id_estado' => 11,
                "nombre" => "Camaguán",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Chaguaramas",
            ],
            [
                'id_estado' => 11,
                "nombre" => "El Socorro",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Infante",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Las Mercedes",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Mellado",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Miranda",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Monagas",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Ortiz",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Ribas",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Roscio",
            ],
            [
                'id_estado' => 11,
                "nombre" => "San Gerónimo de Guayabal",
            ],
            [
                'id_estado' => 11,
                "nombre" => "San José de Guaribe",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Santa María de Ipire",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Zaraza",
            ]
        ]);

        // Guárico | Ciudades
        Location::insert([
            [
                'id_estado' => 11,
                "nombre" => "Altagracia de Orituco",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Cabruta",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Calabozo",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Camaguán",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Chaguaramas Guárico",
            ],
            [
                'id_estado' => 11,
                "nombre" => "El Socorro",
            ],
            [
                'id_estado' => 11,
                "nombre" => "El Sombrero",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Las Mercedes de Los Llanos",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Lezama",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Onoto",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Ortíz",
            ],
            [
                'id_estado' => 11,
                "nombre" => "San José de Guaribe",
            ],
            [
                'id_estado' => 11,
                "nombre" => "San Juan de Los Morros",
            ],
            [
                'id_estado' => 11,
                "nombre" => "San Rafael de Laya",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Santa María de Ipire",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Tucupido",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Valle de La Pascua",
            ],
            [
                'id_estado' => 11,
                "nombre" => "Zaraza"
            ],
        ]);

        // Lara | Municipios
        Municipality::insert([
            [
                'id_estado' => 12,
                "nombre" => "Andrés Eloy Blanco",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Crespo",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Iribarren",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Jiménez",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Morán",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Palavecino",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Simón Planas",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Torres",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Urdaneta",
            ]
        ]);

        // Lara | Ciudades
        Location::insert([
            [
                'id_estado' => 12,
                "nombre" => "Aguada Grande",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Atarigua",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Barquisimeto",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Bobare",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Cabudare",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Carora",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Cubiro",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Cují",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Duaca",
            ],
            [
                'id_estado' => 12,
                "nombre" => "El Manzano",
            ],
            [
                'id_estado' => 12,
                "nombre" => "El Tocuyo",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Guaríco",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Humocaro Alto",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Humocaro Bajo",
            ],
            [
                'id_estado' => 12,
                "nombre" => "La Miel",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Moroturo",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Quíbor",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Río Claro",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Sanare",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Santa Inés",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Sarare",
            ],
            [
                'id_estado' => 12,
                "nombre" => "Siquisique",
            ],
            [
                'id_estado' => 12,
                "nombre" => ",Tintorero",
            ]
        ]);

        // Mérida | Municipios
        Municipality::insert([
            [
                'id_estado' => 13,
                "nombre" => "Alberto Adriani",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Andrés Bello",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Antonio Pinto Salinas",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Aricagua",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Arzobispo Chacón",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Campo Elías",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Caracciolo Parra Olmedo",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Cardenal Quintero",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Guaraque",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Julio César Salas",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Justo Briceño",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Libertador",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Miranda",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Obispo Ramos de Lora",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Padre Noguera",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Pueblo Llano",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Rangel",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Rivas Dávila",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Santos Marquina",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Tovar",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Tulio Febres Cordero",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Zea",
            ]
        ]);

        // Mérida | Ciudades
        Location::insert([
            [
                'id_estado' => 13,
                "nombre" => "Apartaderos Mérida",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Arapuey",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Bailadores",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Caja Seca",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Canaguá",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Chachopo",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Chiguara",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Ejido",
            ],
            [
                'id_estado' => 13,
                "nombre" => "El Vigía",
            ],
            [
                'id_estado' => 13,
                "nombre" => "La Azulita",
            ],
            [
                'id_estado' => 13,
                "nombre" => "La Playa",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Lagunillas Mérida",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Mérida",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Mesa de Bolívar",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Mucuchíes",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Mucujepe",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Mucuruba",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Nueva Bolivia",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Palmarito",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Pueblo Llano",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Santa Cruz de Mora",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Santa Elena de Arenales",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Santo Domingo",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Tabáy",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Timotes",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Torondoy",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Tovar",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Tucani",
            ],
            [
                'id_estado' => 13,
                "nombre" => "Zea"
            ],
        ]);

        // Miranda | Municipios
        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Acevedo",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Aragüita'],
            ['id_municipio' =>  $id, 'nombre' => 'Arévalo González'],
            ['id_municipio' =>  $id, 'nombre' => 'Capaya'],
            ['id_municipio' =>  $id, 'nombre' => 'Caucagua'],
            ['id_municipio' =>  $id, 'nombre' => 'Panaquire'],
            ['id_municipio' =>  $id, 'nombre' => 'El Café'],
            ['id_municipio' =>  $id, 'nombre' => 'Marizapa'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Andrés Bello",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Cumbo'],
            ['id_municipio' =>  $id, 'nombre' => 'San José de Barlovento'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Baruta",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'El Cafetal'],
            ['id_municipio' =>  $id, 'nombre' => 'Las Minas'],
            ['id_municipio' =>  $id, 'nombre' => 'Nuestra Señora del Rosario'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Brión",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Higuerote'],
            ['id_municipio' =>  $id, 'nombre' => 'Curiepe'],
            ['id_municipio' =>  $id, 'nombre' => 'Tacarigua de Brión'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Buroz",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Mamporal'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Carrizal",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Carrizal'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Chacao",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Chacao'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Cristóbal Rojas",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Charallave'],
            ['id_municipio' =>  $id, 'nombre' => 'Las Brisas'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "El Hatillo",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'El Hatillo'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Guaicaipuro",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Altagracia de la Montaña'],
            ['id_municipio' =>  $id, 'nombre' => 'Cecilio Acosta'],
            ['id_municipio' =>  $id, 'nombre' => 'Los Teques'],
            ['id_municipio' =>  $id, 'nombre' => 'El Jarillo'],
            ['id_municipio' =>  $id, 'nombre' => 'San Pedro'],
            ['id_municipio' =>  $id, 'nombre' => 'Tácata'],
            ['id_municipio' =>  $id, 'nombre' => 'Paracotos'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Independencia",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Cartanal'],
            ['id_municipio' =>  $id, 'nombre' => 'Santa Teresa del Tuy'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Lander",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'La Democracia'],
            ['id_municipio' =>  $id, 'nombre' => 'Ocumare del Tuy'],
            ['id_municipio' =>  $id, 'nombre' => 'Santa Bárbara'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Los Salias",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'San Antonio de los Altos'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Páez",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Río Chico'],
            ['id_municipio' =>  $id, 'nombre' => 'El Guapo'],
            ['id_municipio' =>  $id, 'nombre' => 'Tacarigua de la Laguna'],
            ['id_municipio' =>  $id, 'nombre' => 'Paparo'],
            ['id_municipio' =>  $id, 'nombre' => 'San Fernando del Guapo'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Paz Castillo",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Santa Lucía del Tuy'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Pedro Gual",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Cúpira'],
            ['id_municipio' =>  $id, 'nombre' => 'Machurucuto'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Plaza",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Guarenas'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Simón Bolívar",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'San Antonio de Yare'],
            ['id_municipio' =>  $id, 'nombre' => 'San Francisco de Yare'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Sucre",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Leoncio Martínez'],
            ['id_municipio' =>  $id, 'nombre' => 'Petare'],
            ['id_municipio' =>  $id, 'nombre' => 'Caucagüita'],
            ['id_municipio' =>  $id, 'nombre' => 'Filas de Mariche'],
            ['id_municipio' =>  $id, 'nombre' => 'La Dolorita'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Urdaneta",
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Cúa'],
            ['id_municipio' =>  $id, 'nombre' => 'Nueva Cúa'],
        ]);

        $id = Municipality::insertGetId([
            'id_estado' => 14,
            "nombre" => "Zamora"
        ]);
        Parish::insert([
            ['id_municipio' =>  $id, 'nombre' => 'Guatire'],
            ['id_municipio' =>  $id, 'nombre' => 'Bolívar'],
        ]);

        // Miranda | Ciudades
        Location::insert([
            [
                'id_estado' => 14,
                "nombre" => "Araguita",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Carrizal",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Caucagua",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Chaguaramas Miranda",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Charallave",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Chirimena",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Chuspa",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Cúa",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Cupira",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Curiepe",
            ],
            [
                'id_estado' => 14,
                "nombre" => "El Guapo",
            ],
            [
                'id_estado' => 14,
                "nombre" => "El Jarillo",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Filas de Mariche",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Guarenas",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Guatire",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Higuerote",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Los Anaucos",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Los Teques",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Ocumare del Tuy",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Panaquire",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Paracotos",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Río Chico",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San Antonio de Los Altos",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San Diego de Los Altos",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San Fernando del Guapo",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San Francisco de Yare",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San José de Los Altos",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San José de Río Chico",
            ],
            [
                'id_estado' => 14,
                "nombre" => "San Pedro de Los Altos",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Santa Lucía",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Santa Teresa",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Tacarigua de La Laguna",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Tacarigua de Mamporal",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Tácata",
            ],
            [
                'id_estado' => 14,
                "nombre" => "Turumo"
            ],
        ]);

        // Monagas | Municipios
        Municipality::insert([
            [
                'id_estado' => 15,
                "nombre" => "Acosta",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Aguasay",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Caripe",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Cedeño",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Ezequiel Zamora",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Libertador",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Maturín",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Piar",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Punceres",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Santa Bárbara",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Sotillo",
            ],
            [
                'id_estado' => 15,
                "nombre" => "Uracoa",
            ]
        ]);

        // Monagas | Ciudades
        Location::insert([
            [
                'id_estado' => 15,
                "nombre" => "Aguasay"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Aragua de Maturín"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Barrancas del Orinoco"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Caicara de Maturín"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Caripe"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Caripito"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Chaguaramal"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Chaguaramas Monagas"
            ],
            [
                'id_estado' => 15,
                "nombre" => "El Furrial"
            ],
            [
                'id_estado' => 15,
                "nombre" => "El Tejero"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Jusepín"
            ],
            [
                'id_estado' => 15,
                "nombre" => "La Toscana"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Maturín"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Miraflores"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Punta de Mata"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Quiriquire"
            ],
            [
                'id_estado' => 15,
                "nombre" => "San Antonio de Maturín"
            ],
            [
                'id_estado' => 15,
                "nombre" => "San Vicente Monagas"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Santa Bárbara"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Temblador"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Teresen"
            ],
            [
                'id_estado' => 15,
                "nombre" => "Uracoa"
            ],
        ]);

        // Nueva Esparta | Municipios
        Municipality::insert([
            [
                'id_estado' => 16,
                "nombre" => "Antolín del Campo",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Arismendi",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Díaz",
            ],
            [
                'id_estado' => 16,
                "nombre" => "García",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Gómez",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Maneiro",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Marcano",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Mariño",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Península de Macanao",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Tubores",
            ],
            [
                'id_estado' => 16,
                "nombre" => "Villalba",
            ]
        ]);

        // Nueva Esparta | Ciudades
        Location::insert([
            [
                'id_estado' => 16,
                "nombre" => "Altagracia"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Boca de Pozo"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Boca de Río"
            ],
            [
                'id_estado' => 16,
                "nombre" => "El Espinal"
            ],
            [
                'id_estado' => 16,
                "nombre" => "El Valle del Espíritu Santo"
            ],
            [
                'id_estado' => 16,
                "nombre" => "El Yaque"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Juangriego"
            ],
            [
                'id_estado' => 16,
                "nombre" => "La Asunción"
            ],
            [
                'id_estado' => 16,
                "nombre" => "La Guardia"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Pampatar"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Porlamar"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Puerto Fermín"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Punta de Piedras"
            ],
            [
                'id_estado' => 16,
                "nombre" => "San Francisco de Macanao"
            ],
            [
                'id_estado' => 16,
                "nombre" => "San Juan Bautista"
            ],
            [
                'id_estado' => 16,
                "nombre" => "San Pedro de Coche"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Santa Ana de Nueva Esparta"
            ],
            [
                'id_estado' => 16,
                "nombre" => "Villa Rosa"
            ],
        ]);

        // Portuguesa #17 | Municipios
        Municipality::insert([
            [
                'id_estado' => 17,
                "nombre" => "Agua Blanca",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Araure",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Esteller",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Guanare",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Guanarito",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Monseñor José Vicente de Unda",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Ospino",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Páez",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Papelón",
            ],
            [
                'id_estado' => 17,
                "nombre" => "San Genaro de Boconoíto",
            ],
            [
                'id_estado' => 17,
                "nombre" => "San Rafael de Onoto",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Santa Rosalía",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 17,
                "nombre" => "Turén",
            ]
        ]);

        // Portuguesa #17 | Ciudades
        Location::insert([
            [
                'id_estado' => 17,
                "nombre" => "Acarigua"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Agua Blanca"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Araure"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Biscucuy"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Boconoito"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Campo Elías"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Chabasquén"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Guanare"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Guanarito"
            ],
            [
                'id_estado' => 17,
                "nombre" => "La Aparición"
            ],
            [
                'id_estado' => 17,
                "nombre" => "La Misión"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Mesa de Cavacas"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Ospino"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Papelón"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Payara"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Pimpinela"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Píritu de Portuguesa"
            ],
            [
                'id_estado' => 17,
                "nombre" => "San Rafael de Onoto"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Santa Rosalía"
            ],
            [
                'id_estado' => 17,
                "nombre" => "Turén"
            ]
        ]);

        // Sucre #18 | Municipios
        Municipality::insert([
            [
                'id_estado' => 18,
                "nombre" => "Andrés Eloy Blanco",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Andrés Mata",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Arismendi",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Benítez",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Bermúdez",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Cajigal",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Cruz Salmerón Acosta",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Libertador",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Mariño",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Mejía",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Montes",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Ribero",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 18,
                "nombre" => "Valdez",
            ]
        ]);

        // Sucre #18 | Ciudades
        Location::insert([
            [
                'id_estado' => 18,
                "nombre" => "Altos de Sucre"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Araya"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Cariaco"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Carúpano"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Casanay"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Cumaná"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Cumanacoa"
            ],
            [
                'id_estado' => 18,
                "nombre" => "El Morro Puerto Santo"
            ],
            [
                'id_estado' => 18,
                "nombre" => "El Pilar"
            ],
            [
                'id_estado' => 18,
                "nombre" => "El Poblado"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Guaca"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Guiria"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Irapa"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Manicuare"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Mariguitar"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Río Caribe"
            ],
            [
                'id_estado' => 18,
                "nombre" => "San Antonio del Golfo"
            ],
            [
                'id_estado' => 18,
                "nombre" => "San José de Aerocuar"
            ],
            [
                'id_estado' => 18,
                "nombre" => "San Vicente de Sucre"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Santa Fe de Sucre"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Tunapuy"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Yaguaraparo"
            ],
            [
                'id_estado' => 18,
                "nombre" => "Yoco"
            ]
        ]);

        // Táchira #19 | Municipios
        Municipality::insert([
            [
                'id_estado' => 19,
                "nombre" => "Andrés Bello",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Antonio Rómulo Costa",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Ayacucho",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Cárdenas",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Córdoba",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Fernández Feo",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Francisco de Miranda",
            ],
            [
                'id_estado' => 19,
                "nombre" => "García de Hevia",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Guásimos",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Independencia",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Jáuregui",
            ],
            [
                'id_estado' => 19,
                "nombre" => "José María Vargas",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Junín",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Libertad",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Libertador",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Lobatera",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Michelena",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Panamericano",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Pedro María Ureña",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Rafael Urdaneta",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Samuel Darío Maldonado",
            ],
            [
                'id_estado' => 19,
                "nombre" => "San Cristóbal",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Seboruco",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Simón Rodríguez",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Torbes",
            ],
            [
                'id_estado' => 19,
                "nombre" => "Uribante",
            ],
            [
                'id_estado' => 19,
                "nombre" => "San Judas Tadeo",
            ]
        ]);

        // Táchira #19 | Ciudades
        Location::insert([
            [
                'id_estado' => 19,
                "nombre" => "Abejales"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Borota"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Bramon"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Capacho"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Colón"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Coloncito"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Cordero"
            ],
            [
                'id_estado' => 19,
                "nombre" => "El Cobre"
            ],
            [
                'id_estado' => 19,
                "nombre" => "El Pinal"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Independencia"
            ],
            [
                'id_estado' => 19,
                "nombre" => "La Fría"
            ],
            [
                'id_estado' => 19,
                "nombre" => "La Grita"
            ],
            [
                'id_estado' => 19,
                "nombre" => "La Pedrera"
            ],
            [
                'id_estado' => 19,
                "nombre" => "La Tendida"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Las Delicias"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Las Hernández"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Lobatera"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Michelena"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Palmira"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Pregonero"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Queniquea"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Rubio"
            ],
            [
                'id_estado' => 19,
                "nombre" => "San Antonio del Tachira"
            ],
            [
                'id_estado' => 19,
                "nombre" => "San Cristobal"
            ],
            [
                'id_estado' => 19,
                "nombre" => "San José de Bolívar"
            ],
            [
                'id_estado' => 19,
                "nombre" => "San Josecito"
            ],
            [
                'id_estado' => 19,
                "nombre" => "San Pedro del Río"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Santa Ana Táchira"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Seboruco"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Táriba"
            ],
            [
                'id_estado' => 19,
                "nombre" => "Umuquena"
            ]
        ]);

        // Trujillo #20 | Municipios
        Municipality::insert([
            [
                'id_estado' => 20,
                "nombre" => "Andrés Bello",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Boconó",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Candelaria",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Carache",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Escuque",
            ],
            [
                'id_estado' => 20,
                "nombre" => "José Felipe Márquez Cañizalez",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Juan Vicente Campos Elías",
            ],
            [
                'id_estado' => 20,
                "nombre" => "La Ceiba",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Miranda",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Monte Carmelo",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Motatán",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Pampán",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Pampanito",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Rafael Rangel",
            ],
            [
                'id_estado' => 20,
                "nombre" => "San Rafael de Carvajal",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Trujillo",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Urdaneta",
            ],
            [
                'id_estado' => 20,
                "nombre" => "Valera",
            ]
        ]);

        // Trujillo #20 | Ciudades
        Location::insert([
            [
                'id_estado' => 20,
                "nombre" => "Batatal"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Betijoque"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Boconó"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Carache"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Chejende"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Cuicas"
            ],
            [
                'id_estado' => 20,
                "nombre" => "El Dividive"
            ],
            [
                'id_estado' => 20,
                "nombre" => "El Jaguito"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Escuque"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Isnotú"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Jajó"
            ],
            [
                'id_estado' => 20,
                "nombre" => "La Ceiba"
            ],
            [
                'id_estado' => 20,
                "nombre" => "La Concepción de Trujllo"
            ],
            [
                'id_estado' => 20,
                "nombre" => "La Mesa de Esnujaque"
            ],
            [
                'id_estado' => 20,
                "nombre" => "La Puerta"
            ],
            [
                'id_estado' => 20,
                "nombre" => "La Quebrada"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Mendoza Fría"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Meseta de Chimpire"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Monay"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Motatán"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Pampán"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Pampanito"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Sabana de Mendoza"
            ],
            [
                'id_estado' => 20,
                "nombre" => "San Lázaro"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Santa Ana de Trujillo"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Tostós"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Trujillo"
            ],
            [
                'id_estado' => 20,
                "nombre" => "Valera"
            ]
        ]);

        // Vargas #21 | Municipios
        Municipality::insert([
            [
                'id_estado' => 21,
                "nombre" => "Vargas",
            ],
        ]);

        // Vargas #21 | Ciudades
        Location::insert([
            [
                'id_estado' => 21,
                "nombre" => "Carayaca",
            ],
            [
                'id_estado' => 21,
                "nombre" => "Litoral",
            ],
        ]);

        // Yaracuy #22 | Municipios
        Municipality::insert([
            [
                'id_estado' => 22,
                "nombre" => "Arístides Bastidas",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Bolívar",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Bruzual",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Cocorote",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Independencia",
            ],
            [
                'id_estado' => 22,
                "nombre" => "José Antonio Páez",
            ],
            [
                'id_estado' => 22,
                "nombre" => "La Trinidad",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Manuel Monge",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Nirgua",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Peña",
            ],
            [
                'id_estado' => 22,
                "nombre" => "San Felipe",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Urachiche",
            ],
            [
                'id_estado' => 22,
                "nombre" => "Veroes",
            ]
        ]);

        // Yaracuy #22 | Ciudades
        Location::insert([
            [
                'id_estado' => 22,
                "nombre" => "Aroa"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Boraure"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Campo Elías de Yaracuy"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Chivacoa"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Cocorote"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Farriar"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Guama"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Marín"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Nirgua"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Sabana de Parra"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Salom"
            ],
            [
                'id_estado' => 22,
                "nombre" => "San Felipe"
            ],
            [
                'id_estado' => 22,
                "nombre" => "San Pablo de Yaracuy"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Urachiche"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Yaritagua"
            ],
            [
                'id_estado' => 22,
                "nombre" => "Yumare"
            ]
        ]);

        // Zulia #23 | Municipios
        Municipality::insert([
            [
                'id_estado' => 23,
                "nombre" => "Almirante Padilla",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Baralt",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Cabimas",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Catatumbo",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Colón",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Francisco Javier Pulgar",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Páez",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Jesús Enrique Lossada",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Jesús María Semprún",
            ],
            [
                'id_estado' => 23,
                "nombre" => "La Cañada de Urdaneta",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Lagunillas",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Machiques de Perijá",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Mara",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Maracaibo",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Miranda",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Rosario de Perijá",
            ],
            [
                'id_estado' => 23,
                "nombre" => "San Francisco",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Santa Rita",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Simón Bolívar",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Sucre",
            ],
            [
                'id_estado' => 23,
                "nombre" => "Valmore Rodríguez",
            ]
        ]);

        // Zulia #23 | Ciudades
        Location::insert([
            [
                'id_estado' => 23,
                "nombre" => "Bachaquero"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Bobures"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Cabimas"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Campo Concepción"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Campo Mara"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Campo Rojo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Carrasquero"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Casigua"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Chiquinquirá"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Ciudad Ojeda"
            ],
            [
                'id_estado' => 23,
                "nombre" => "El Batey"
            ],
            [
                'id_estado' => 23,
                "nombre" => "El Carmelo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "El Chivo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "El Guayabo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "El Mene"
            ],
            [
                'id_estado' => 23,
                "nombre" => "El Venado"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Encontrados"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Gibraltar"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Isla de Toas"
            ],
            [
                'id_estado' => 23,
                "nombre" => "La Concepción"
            ],
            [
                'id_estado' => 23,
                "nombre" => "La Paz"
            ],
            [
                'id_estado' => 23,
                "nombre" => "La Sierrita"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Lagunillas"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Las Piedras"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Los Cortijos"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Machiques"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Maracaibo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Mene Grande"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Palmarejo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Paraguaipoa"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Potrerito"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Pueblo Nuevo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Puertos de Altagracia"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Punta Gorda"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Sabaneta de Palma"
            ],
            [
                'id_estado' => 23,
                "nombre" => "San Francisco"
            ],
            [
                'id_estado' => 23,
                "nombre" => "San José de Perijá"
            ],
            [
                'id_estado' => 23,
                "nombre" => "San Rafael del Moján"
            ],
            [
                'id_estado' => 23,
                "nombre" => "San Timoteo"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Santa Bárbara del Zulia"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Santa Cruz de Mara"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Santa Cruz del Zulia"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Santa Rita"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Sinamaica"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Tamare"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Tía Juana"
            ],
            [
                'id_estado' => 23,
                "nombre" => "Villa Rosario"
            ]
        ]);

        // Distrito Capital #24 | Municipios
        Municipality::insert([
            [
                'id_estado' => 24,
                "nombre" => "Libertador"
            ],
        ]);

        // Distrito Capital #24 | Ciudades
        Location::insert([
            [
                'id_estado' => 24,
                "nombre" => "23 de enero"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Altagracia"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Antímano"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Caricuao"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Catedral"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Coche"
            ],
            [
                'id_estado' => 24,
                "nombre" => "El Junquito"
            ],
            [
                'id_estado' => 24,
                "nombre" => "El Paraíso"
            ],
            [
                'id_estado' => 24,
                "nombre" => "El Recreo"
            ],
            [
                'id_estado' => 24,
                "nombre" => "El Valle"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Candelaria"
            ],
            [
                'id_estado' => 24,
                "nombre" => "La Pastora"
            ],
            [
                'id_estado' => 24,
                "nombre" => "La Vega"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Macarao"
            ],
            [
                'id_estado' => 24,
                "nombre" => "San Agustín"
            ],
            [
                'id_estado' => 24,
                "nombre" => "San Bernardino"
            ],
            [
                'id_estado' => 24,
                "nombre" => "San José"
            ],
            [
                'id_estado' => 24,
                "nombre" => "San Juan"
            ],
            [
                'id_estado' => 24,
                "nombre" => "San Pedro"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Santa Rosalía"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Santa Teresa"
            ],
            [
                'id_estado' => 24,
                "nombre" => "Sucre (Catia)"
            ]
        ]);
    }
}
