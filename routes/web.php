<?php

use App\Http\Controllers\asesoresInmobiliariosController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\PropertiesController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\MunicipalitiesController;
use App\Http\Controllers\ParishesController;
use App\Http\Controllers\LocationsController;
use App\Http\Controllers\CitasController;
use App\Http\Controllers\respuestasCitasController;
use App\Http\Controllers\OffersController;
use App\Http\Controllers\NegotiationController;
use App\Http\Controllers\NewsletterController;
use App\Http\Controllers\ReportsController;
use App\Http\Controllers\CategoryPropertyController;
use App\Http\Controllers\ProspectController;
use App\Http\Controllers\PropertiesFrontController;
use App\Http\Controllers\ContentsController;
use App\Http\Controllers\JobController;
use App\Http\Controllers\JobPostulationsController;
use App\Http\Controllers\pages\JobsFrontController;
use App\Http\Controllers\LeadActivityController;
use App\Http\Controllers\FormatsController;
use App\Http\Controllers\FavoriteController;
use App\Http\Controllers\HomeController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('welcome');

// require __DIR__.'/auth.php';

Auth::routes();
Route::get('/dashboard', [HomeController::class, 'index'])->name('home');

## RUTAS USUARIOS ##
    ## Usuarios con priviledios limitados ##
    Route::middleware(['checkRoles:director-admin-adviser'])->group(function(){
        //meetings
        Route::get('/meetings',[CitasController::class,'index'])->name('meetings.index');
        Route::get('/meetings/calendar',[CitasController::class,'calendar_view'])->name('meetings.calendar');
        Route::get('meetings/calendardata',[CitasController::class,'calendardata'])->name('meetings.calendarData');
        Route::get('/meetings/create',[CitasController::class,'create'])->name('meetings.create');
        Route::POST('/meetings',[CitasController::class,'store'])->name('meetings.store');
        Route::get('/meetings/{id}',[CitasController::class,'show'])->name('meetings.show');
        Route::get('/meetings/{id}/edit',[CitasController::class,'edit'])->name('meetings.edit');
        Route::put('/meetings/{id}',[CitasController::class,'update'])->name('meetings.update');

        //reply meetings
        Route::post('/respuestasCitas',[respuestasCitasController::class,'store']);

        Route::get('/prospect',[ProspectController::class,'index'])->name('prospect.index');
        Route::get('/prospect/create',[ProspectController::class,'create'])->name('prospect.create');
        Route::post('/prospect',[ProspectController::class,'store'])->name('prospect.store');
        Route::get('/prospect/{prospect}',[ProspectController::class,'show'])->name('prospect.show');
        Route::get('/prospect/{prospect}/edit',[ProspectController::class,'edit'])->name('prospect.edit');
        Route::put('/prospect/{prospect}',[ProspectController::class,'update'])->name('prospect.update');

        //LeadActivity
        Route::get('/leadactivity', [LeadActivityController::class,'index'])->name('leadactivity.index');
        Route::get('/leadactivity/create', [LeadActivityController::class,'create'])->name('leadactivity.create');

        Route::get('/leadactivity/{leadactivity}', [LeadActivityController::class,'show'])->name('leadactivity.show');
        Route::get('/leadactivity/{leadactivity}/edit', [LeadActivityController::class,'edit'])->name('leadactivity.edit');
        Route::put('/leadactivity/{leadactivity}', [LeadActivityController::class,'update'])->name('leadactivity.update');

        //Properties
        Route::get('/properties', [PropertiesController::class,'index'])->name('properties.index');
        Route::get('/properties/create', [PropertiesController::class,'create'])->name('properties.create');
        Route::post('/properties', [PropertiesController::class,'store'])->name('properties.store');
        Route::get('/properties/{property}', [PropertiesController::class,'show'])->name('properties.show');
        Route::get('/properties/{property}/edit', [PropertiesController::class,'edit'])->name('properties.edit');
        Route::put('/properties/{property}', [PropertiesController::class,'update'])->name('properties.update');

        Route::get('/properties/{property}/documents', [PropertiesController::class,'documents'])->name('properties.documents');
        Route::get('/properties/{property}/photos', [PropertiesController::class,'photos'])->name('properties.photos');
        Route::post('/properties/upload_documents', [PropertiesController::class,'upload_documents'])->name('properties.upload_documents');
        Route::get('/properties/{id}/destroy_documents', [PropertiesController::class,'destroy_documents'])->name('properties.destroy_documents');
        Route::post('/properties/order_documents', [PropertiesController::class,'order_documents'])->name('properties.order_documents');

        //timeline{Properties}
        Route::get('/properties/{id}/timeline',[PropertiesController::class,'timeline'])->name('property.timeline');

        //Offers
        Route::get('/offers', [OffersController::class,'index'])->name('offers.index');
        Route::get('/offers/create', [OffersController::class,'create'])->name('offers.create');
        Route::post('/offers', [OffersController::class,'store'])->name('offers.store');
        Route::get('/offers/{id}', [OffersController::class,'show'])->name('offers.show');
        Route::get('/offers/{id}/edit', [OffersController::class,'edit'])->name('offers.edit');
        Route::put('/offers/{id}', [OffersController::class,'update'])->name('offers.update');

        //Negotiations
        Route::get('/negotiations', [NegotiationController::class,'index'])->name('negotiations.index');
        Route::get('/negotiations/create', [NegotiationController::class,'create'])->name('negotiations.create');
        Route::post('/negotiations', [NegotiationController::class,'store'])->name('negotiations.store');
        Route::get('/negotiations/{negotiation}', [NegotiationController::class,'show'])->name('negotiations.show');
        Route::get('/negotiations/{negotiation}/edit', [NegotiationController::class,'edit'])->name('negotiations.edit');
        Route::put('/negotiations/{negotiation}', [NegotiationController::class,'update'])->name('negotiations.update');

        //Reports
        Route::get('reports/profitability', [ReportsController::class,'profitability'])->name('reports.profitability');
        Route::post('reports/profitability', [ReportsController::class,'queryProfitability'])->name('reports.queryProfitability');
        Route::get('reports/profitability-advisers', [ReportsController::class,'profitability_advisers'])->name('reports.profitability_advisers');
        Route::post('reports/profitability-advisers', [ReportsController::class,'queryProfitability'])->name('reports.queryProfitabilityAdvisers');

        //TypeProperty
        Route::get('/types-property', [CategoryPropertyController::class,'index'])->name('types-property.index');
        Route::get('/types-property/create', [CategoryPropertyController::class,'create'])->name('types-property.create');
        Route::post('/types-property', [CategoryPropertyController::class,'store'])->name('types-property.store');
        Route::get('/types-property/{category}', [CategoryPropertyController::class,'show'])->name('types-property.show');
        Route::get('/types-property/{category}/edit', [CategoryPropertyController::class,'edit'])->name('types-property.edit');
        Route::put('/types-property/{category}', [CategoryPropertyController::class,'update'])->name('types-property.update');

        //Job
        Route::get('jobs', [JobController::class,'index'])->name('jobs.index');
        Route::get('/jobs/create', [JobController::class,'create'])->name('jobs.create');
        Route::post('/jobs', [JobController::class,'store'])->name('jobs.store');
        Route::get('/jobs/{job}', [JobController::class,'show'])->name('jobs.show');
        Route::get('/jobs/{job}/edit', [JobController::class,'edit'])->name('jobs.edit');
        Route::put('/jobs/{job}', [JobController::class,'update'])->name('jobs.update');

        //JobPostulations
        Route::get('postulations', [JobPostulationsController::class,'index'])->name('postulations.index');
        Route::get('/postulations/{postulation}', [JobPostulationsController::class,'show'])->name('postulations.show');

    });

    ## Usuarios Con privilegios de Admin ##
    Route::middleware(['checkRoles:admin'])->group(function(){
        Route::delete('/users/{id}', [UserController::class,'destroy'])->name('users.destroy');
        Route::delete('prospect/{id}/destroy', [ProspectController::class,'destroy'])->name('prospect.destroy');
        Route::delete('leadactivity/{id}/destroy', [leadactivityController::class,'destroy'])->name('leadactivity.destroy');
        Route::delete('/advisers/{id}',[asesoresInmobiliariosController::class,'destroy'])->name('advisers.destroy');
        Route::delete('/properties/{property}',[PropertiesController::class,'destroy'])->name('properties.destroy');
        Route::delete('types-property/{category}',[CategoryPropertyController::class,'destroy'])->name('types-property.destroy');
        Route::delete('/meetings/{id}',[CitasController::class,'destroy'])->name('meetings.destroy');
        Route::delete('/states/{state}',[StatesController::class,'destroy'])->name('states.destroy');
        Route::delete('/municipalities/{municipality}',[MunicipalitiesController::class,'destroy'])->name('municipalities.destroy');
        Route::delete('/parishes/{parish}',[ParishesController::class,'destroy'])->name('parishes.destroy');
        Route::delete('/locations/{location}',[LocationsController::class,'destroy'])->name('locations.destroy');
        Route::delete('/offers/{id}', [OffersController::class,'destroy'])->name('offers.destroy');
        Route::delete('/negotiations/{negotiation}', [NegotiationController::class,'destroy'])->name('negotiations.destroy');
        Route::delete('jobs/{job}',[JobController::class,'destroy'])->name('jobs.destroy');
        Route::delete('contents/menus/{menu}/destroy', [ContentsController::class,'destroy_menus'])->name('menus.destroy');
        Route::delete('contents/links/{link}/destroy', [ContentsController::class,'destroy_links'])->name('links.destroy');
        Route::delete('formats/{format}/destroy', [FormatsController::class,'destroy'])->name('formats.destroy');

        //Users
        Route::get('/users', [UserController::class,'index'])->name('users.index');
        Route::get('/users/create', [UserController::class,'create'])->name('users.create');
        Route::post('/users', [UserController::class,'store'])->name('users.store');
        Route::get('/users/{id}', [UserController::class,'show'])->name('users.show');
        Route::get('/users/{id}/edit', [UserController::class,'edit'])->name('users.edit');
        Route::put('/users/{id}', [UserController::class,'update'])->name('users.update');
        //Advisers
        Route::get('/advisers', [asesoresInmobiliariosController::class,'index'])->name('advisers.index');
        Route::get('/advisers/create/{id?}', [asesoresInmobiliariosController::class,'create'])->name('advisers.create');
        Route::post('/advisers', [asesoresInmobiliariosController::class,'store'])->name('advisers.store');
        Route::get('/advisers/{id}', [asesoresInmobiliariosController::class,'show'])->name('advisers.show');
        Route::get('/advisers/{id}/edit', [asesoresInmobiliariosController::class,'edit'])->name('advisers.edit');
        Route::put('/advisers/{id}', [asesoresInmobiliariosController::class,'update'])->name('advisers.update');

        //Contents
        Route::get('contents/{opt?}', [ContentsController::class,'index'])->name('contents.index');
        Route::post('contents/company', [ContentsController::class,'update_company'])->name('company.update');
        Route::post('contents/logos', [ContentsController::class,'update_logos'])->name('logos.update');
        Route::post('contents/banners', [ContentsController::class,'update_banners'])->name('banners.update');

        //Menus
        Route::get('contents/menus/create', [ContentsController::class,'create_menus'])->name('menus.create');
        Route::post('contents/menus/create', [ContentsController::class,'store_menus'])->name('menus.store');
        Route::get('contents/menus/{menu}/edit', [ContentsController::class,'edit_menus'])->name('menus.edit');
        Route::get('contents/menus/{menu}', [ContentsController::class,'show_menus'])->name('menus.show');
        Route::put('contents/menus/{menu}/update', [ContentsController::class,'update_menus'])->name('menus.update');

        //Links
        Route::get('contents/links/create', [ContentsController::class,'create_links'])->name('links.create');
        Route::post('contents/links/create', [ContentsController::class,'store_links'])->name('links.store');
        Route::get('contents/links/{link}/edit', [ContentsController::class,'edit_links'])->name('links.edit');
        Route::get('contents/links/{link}', [ContentsController::class,'show_links'])->name('links.show');
        Route::put('contents/links/{link}/update', [ContentsController::class,'update_links'])->name('links.update');

        //States
        Route::get('states', [StatesController::class,'index'])->name('states.index');
        Route::get('/states/create', [StatesController::class,'create'])->name('states.create');
        Route::post('/states', [StatesController::class,'store'])->name('states.store');
        Route::get('/states/municipalities/{state}', [StatesController::class,'municipalities'])->name('states.municipalities');
        Route::get('/states/cities/{state}', [StatesController::class,'state_cities'])->name('states.cities');
        Route::get('/states/{state}/edit', [StatesController::class,'edit'])->name('states.edit');
        Route::put('/states/{state}', [StatesController::class,'update'])->name('states.update');
        //Municipality
        Route::get('municipalities', [MunicipalitiesController::class,'index'])->name('municipalities.index');
        Route::get('/municipalities/{state}/create', [MunicipalitiesController::class,'create'])->name('municipalities.create');
        Route::post('/municipalities', [MunicipalitiesController::class,'store'])->name('municipalities.store');
        Route::get('/municipalities/{municipality}', [MunicipalitiesController::class,'show'])->name('municipalities.show');
        Route::get('/municipalities/{municipality}/edit', [MunicipalitiesController::class,'edit'])->name('municipalities.edit');
        Route::put('/municipalities/{municipality}', [MunicipalitiesController::class,'update'])->name('municipalities.update');
        //Parish
        Route::get('parishes', [ParishesController::class,'index'])->name('parishes.index');
        Route::get('/parishes/{municipality}/create', [ParishesController::class,'create'])->name('parishes.create');
        Route::post('/parishes', [ParishesController::class,'store'])->name('parishes.store');
        Route::get('/parishes/{parish}', [ParishesController::class,'show'])->name('parishes.show');
        Route::get('/parishes/{parish}/edit', [ParishesController::class,'edit'])->name('parishes.edit');
        Route::put('/parishes/{parish}', [ParishesController::class,'update'])->name('parishes.update');
        //City
        Route::get('locations', [LocationsController::class,'index'])->name('locations.index');
        Route::get('/locations/{state}/create', [LocationsController::class,'create'])->name('locations.create');
        Route::post('/locations', [LocationsController::class,'store'])->name('locations.store');
        Route::get('/locations/{location}', [LocationsController::class,'show'])->name('locations.show');
        Route::get('/locations/{location}/edit', [LocationsController::class,'edit'])->name('locations.edit');
        Route::put('/locations/{location}', [LocationsController::class,'update'])->name('locations.update');

        //Formats
        Route::get('formats', [FormatsController::class,'index'])->name('formats.index');
        Route::get('formats-list', [FormatsController::class,'list'])->name('formats.list');
        Route::get('/formats/create', [FormatsController::class,'create'])->name('formats.create');
        Route::post('/formats', [FormatsController::class,'store'])->name('formats.store');
        Route::get('/formats/{format}', [FormatsController::class,'show'])->name('formats.show');
        Route::get('/formats/{format}/edit', [FormatsController::class,'edit'])->name('formats.edit');
        Route::put('/formats/{format}', [FormatsController::class,'update'])->name('formats.update');
    });

    ## Usuarios Autenticados, pero sin privilegios ##
    Route::middleware(['auth'])->group(function(){
        Route::get('/perfil', [UserController::class,'perfil'])->name('users.perfil');
        Route::put('/perfil/{id}', [UserController::class,'perfil_update'])->name('users.perfil_update');
        Route::post('/favorite', [FavoriteController::class,'store'])->name('favorite.store');
        Route::delete('favorite/{favorite}/destroy', [FavoriteController::class,'destroy'])->name('favorite.destroy');
        //NewsLetter
        Route::post('/newsletter',[NewsletterController::class,'store'])->name('newsletter.store');
    });

    ### JSON PARA FILTROS ###
    Route::get('/municipalities/{state}/json', [MunicipalitiesController::class,'json'])->name('municipalities.json');
    Route::get('/parishes/{municipality}/json', [ParishesController::class,'json'])->name('parishes.json');
    Route::get('/locations/{parish}/json', [LocationsController::class,'json'])->name('locations.json');

    Route::post('/leadactivity', [LeadActivityController::class,'store'])->name('leadactivity.store');

    ###FRONT
    Route::get('/', [PropertiesFrontController::class,'index'])->name('front.inicio');
    //Route::get('/inicio', [PropertiesFrontController::class,'index'])->name('front.inicio');
    Route::get('property/{id}',[PropertiesFrontController::class,'show'])->name('front.show');
    Route::get('/propertylist',[PropertiesFrontController::class,'showProperty'])->name('front.showProperty');
    Route::get('/trabajos', [JobsFrontController::class,'index'])->name('page.postulations.index');
    Route::get('/trabajos/{job}', [JobsFrontController::class,'show'])->name('page.postulations.show');

    Route::get('/thankyou',function(){
        return view('/page/thankyou');
    });
    Route::get('/nosotros',[PropertiesFrontController::class,'about'])->name('page.about');
