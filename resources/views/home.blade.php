@extends('layouts.dashboard')

@section('content')
    @if (auth()->user()->role != 'user')
        @include('layouts.includes.dashboard.card_admin')
    @endif
@endsection
