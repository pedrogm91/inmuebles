<div class="row  text-center">
    <div class="col-xl-12">
        <label for="">Registrate al NewsLetter</label>
    </div>
    <div class="col-xl-12">
        <form action="/newsletter" method="POST">
            @csrf
            <div class="row">
                <div class="col-md-5"><input type="text" name="nombre" class="form-control" placeholder="Dinos tu Nombre"></div>
                <div class="col-md-5"><input type="text" name ="correo_cliente" class="form-control" placeholder="Solo Ingresa tu Email"></div>
                <div class="col-md-2"><input type="submit" value="Enviar" class="btn btn-primary"></div>
            </div>
        </form>
    </div>
</div>