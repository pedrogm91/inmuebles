@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 1px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Editar Inmueble</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2 mb-3">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Inmueble</a>
                                <a class="nav-link" id="v-pills-construccion-tab" data-toggle="pill" href="#v-pills-construccion" role="tab" aria-controls="v-pills-construccion" aria-selected="false">Construcción</a>
                                <a class="nav-link" id="v-pills-instalaciones-tab" data-toggle="pill" href="#v-pills-instalaciones" role="tab" aria-controls="v-pills-instalaciones" aria-selected="false">Instalaciones</a>
                                <a class="nav-link" id="v-pills-propietario-tab" data-toggle="pill" href="#v-pills-propietario" role="tab" aria-controls="v-pills-propietario" aria-selected="false">Propietario</a>
                                <a class="nav-link" id="v-pills-apoderado-tab" data-toggle="pill" href="#v-pills-apoderado" role="tab" aria-controls="v-pills-apoderado" aria-selected="false">Apoderado</a>
                                <a class="nav-link" id="v-pills-asesor-tab" data-toggle="pill" href="#v-pills-asesor" role="tab" aria-controls="v-pills-asesor" aria-selected="false">Asesor</a>
                                <a class="nav-link" id="v-pills-description-tab" data-toggle="pill" href="#v-pills-description" role="tab" aria-controls="v-pills-description" aria-selected="false">Descripción</a>
                            </div>
                        </div>

                        <div class="col-sm-10">
                            <form action="/properties/{{$property->id}}" method="POST" class="needs-validation">
                                @csrf
                                @method('PUT')
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="nombre_propiedad">Nombre <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="nombre_propiedad" class="form-control {{ $errors->has('nombre_propiedad') ? 'is-invalid' : '' }}" value="{{ old('nombre_propiedad') ? old('nombre_propiedad') : $property->nombre_propiedad }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('nombre_propiedad') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="id_categoria">Categoría <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('id_categoria') ? 'is-invalid' : '' }}" name="id_categoria" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5">
                                                        @foreach($categories as $category)
                                                            <option value="{{$category->id}}" {{$property->id_categoria==$category->id?'selected':''}}>{{$category->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_categoria') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="estatus_publicacion">Estatus de Publicación <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('estatus_publicacion') ? 'is-invalid' : '' }}" name="estatus_publicacion"  aria-label="floating label select example" title="Seleccione" data-live-search="true">
                                                        <option value="1" {{ $property->estatus_publicacion==1?'selected':''}}>Publicada</option>
                                                        <option value="2" {{ $property->estatus_publicacion==2?'selected':''}}>Terminada</option>
                                                        <option value="3" {{ $property->estatus_publicacion==3?'selected':''}}>Pausada</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('estatus_publicacion') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="destacado">Destacado <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('destacado') ? 'is-invalid' : '' }}" name="destacado"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="1" {{ $property->destacado==1?'selected':'' }}>Si</option>
                                                        <option value="0" {{ $property->destacado==0?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('destacado') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="tipo_negocio">Tipo de Negocio <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('tipo_negocio') ? 'is-invalid' : '' }}" name="tipo_negocio"  aria-label="floating label select example" title="Seleccione" data-live-search="true">
                                                        <option value="1" {{$property->tipo_negocio==1?'selected':''}}>Alquiler</option>
                                                        <option value="2" {{$property->tipo_negocio==2?'selected':''}}>Venta</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('tipo_negocio') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="precio">Precio <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="precio" class="form-control only-number {{ $errors->has('precio') ? 'is-invalid' : '' }}" value="{{ $property->precio }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('precio') }}
                                                    </div>
                                                </div>
                                            </div>
                                           
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="id_estado">Estado <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('id_estado') ? 'is-invalid' : '' }}" name="id_estado"  aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5" onchange="getMunicipalities(this.value)">
                                                        @foreach($states as $state)
                                                            <option value="{{$state->id}}" {{$property->id_estado==$state->id?'selected':''}}>{{$state->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_estado') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="id_municipio">Municipio <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('id_municipio') ? 'is-invalid' : '' }}" name="id_municipio" id="id_municipio" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5" onchange="getParishes(this.value)">
                                                        @foreach($municipalities as $municipality)
                                                            <option value="{{$municipality->id}}" {{$property->id_municipio==$municipality->id?'selected':''}}>{{$municipality->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_municipio') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="id_parroquia">Parroquia o Urbanización<span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('id_parroquia') ? 'is-invalid' : '' }}" name="id_parroquia" id="id_parroquia" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5" >
                                                        @foreach($parishes as $parish)
                                                            <option value="{{$parish->id}}" {{$property->id_parroquia==$parish->id?'selected':''}}>{{$parish->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_parroquia') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <label for="id_ciudad">Ciudad <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('id_ciudad') ? 'is-invalid' : '' }}" name="id_ciudad" id="id_ciudad" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5">
                                                        @foreach($locations as $location)
                                                            <option value="{{$location->id}}" {{$property->id_ciudad==$location->id?'selected':''}}>{{$location->nombre}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_ciudad') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <label for="ubicacion">Dirección</label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="ubicacion" class="form-control {{ $errors->has('ubicacion') ? 'is-invalid' : '' }}" placeholder="" value="{{ $property->ubicacion }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('ubicacion') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-construccion" role="tabpanel" aria-labelledby="v-pills-construccion-tab">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="area_terreno">Área Terreno <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="area_terreno" class="form-control {{ $errors->has('area_terreno') ? 'is-invalid' : '' }}" placeholder="" min="0" value="{{ $property->datos_construccion['area_terreno'] }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('area_terreno') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="area_construccion">Área Construcción <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="area_construccion" class="form-control {{ $errors->has('area_construccion') ? 'is-invalid' : '' }}" placeholder="" min="0" value="{{ $property->datos_construccion['area_construccion'] }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('area_construccion') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="antiguedad">Antiguedad <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="antiguedad" class="form-control {{ $errors->has('antiguedad') ? 'is-invalid' : '' }}" placeholder="" min="0" value="{{ $property->datos_construccion['antiguedad'] }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('antiguedad') }}
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-sm-2">
                                                <label for="cocina">Cocina <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('cocina') ? 'is-invalid' : '' }}" name="cocina"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['cocina']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('cocina') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="sala">Sala <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('sala') ? 'is-invalid' : '' }}" name="sala"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['sala']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('sala') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="habit">Habit/Cubi <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('habit') ? 'is-invalid' : '' }}" name="habit"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['habit']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('habit') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="habit_s">Habit. S <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('habit_s') ? 'is-invalid' : '' }}" name="habit_s"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['habit_s']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('habit_s') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="banos">Baños <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('banos') ? 'is-invalid' : '' }}" name="banos"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['banos']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('banos') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="banos_s">Baño S <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('banos_s') ? 'is-invalid' : '' }}" name="banos_s"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['banos_s']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('banos_s') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="bano_medio">1/2 Baño <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('bano_medio') ? 'is-invalid' : '' }}" name="bano_medio"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['bano_medio']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('bano_medio') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="aire_a">A/A <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('aire_a') ? 'is-invalid' : '' }}" name="aire_a"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['aire_a']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('aire_a') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="tv">TV Cable <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('tv') ? 'is-invalid' : '' }}" name="tv"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['tv']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('tv') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="wifi">Internet <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('wifi') ? 'is-invalid' : '' }}" name="wifi"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['wifi']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('wifi') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="parrillera">Parrillera <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('parrillera') ? 'is-invalid' : '' }}" name="parrillera"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['parrillera']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('parrillera') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="piscina">Piscina <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('piscina') ? 'is-invalid' : '' }}" name="piscina"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['piscina']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('piscina') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="maletero">Maletero <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('maletero') ? 'is-invalid' : '' }}" name="maletero"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['maletero']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('maletero') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="lavadero">Lavadero <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('lavadero') ? 'is-invalid' : '' }}" name="lavadero"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['lavadero']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('lavadero') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="gimnasio">Gimnasio <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('gimnasio') ? 'is-invalid' : '' }}" name="gimnasio"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['gimnasio']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('gimnasio') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-2">
                                                <label for="estacionamiento">Estac. <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('estacionamiento') ? 'is-invalid' : '' }}" name="estacionamiento"  aria-label="floating label select example" title="" data-live-search="true">
                                                        @for ($i = 0; $i <= 200; $i++)
                                                        <option value="{{ $i }}" {{$property->datos_construccion['estacionamiento']==$i?'selected':''}}>{{ $i }}</option>
                                                        @endfor
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('estacionamiento') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="tipo_estacionamiento">Tipo Estac <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('tipo_estacionamiento') ? 'is-invalid' : '' }}" name="tipo_estacionamiento"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Techado" {{ $property->datos_construccion['tipo_estacionamiento']=='Techado'?'selected':'' }}>Techado</option>
                                                        <option value="Aire libre" {{ $property->datos_construccion['tipo_estacionamiento']=='Aire libre'?'selected':'' }}>Aire libre</option>
                                                        <option value="Sin Estacionamiento" {{ $property->datos_construccion['tipo_estacionamiento']=='Sin Estacionamiento'?'selected':'' }}>Sin Estacionamiento</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('tipo_estacionamiento') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-instalaciones" role="tabpanel" aria-labelledby="v-pills-instalaciones-tab">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="supermercado">Supermercado <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('supermercado') ? 'is-invalid' : '' }}" name="supermercado"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['supermercado']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['supermercado']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('supermercado') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="clinica">Clínica/Hospital <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('clinica') ? 'is-invalid' : '' }}" name="clinica"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['clinica']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['clinica']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('clinica') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="farmacia">Farmacia <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('farmacia') ? 'is-invalid' : '' }}" name="farmacia"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['farmacia']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['farmacia']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('farmacia') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="colegios">Colegios <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('colegios') ? 'is-invalid' : '' }}" name="colegios"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['colegios']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['colegios']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('colegios') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="estacion_metro">Estación Metro <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('estacion_metro') ? 'is-invalid' : '' }}" name="estacion_metro"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['estacion_metro']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['estacion_metro']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('estacion_metro') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="autobus">Parada Autobus <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('autobus') ? 'is-invalid' : '' }}" name="autobus"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['autobus']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['autobus']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('autobus') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="centro_comercial">Centro Comercial <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('centro_comercial') ? 'is-invalid' : '' }}" name="centro_comercial"  aria-label="floating label select example" title="Seleccione">
                                                        <option value="Si" {{ $property->instalaciones_cercanas['centro_comercial']=='Si'?'selected':'' }}>Si</option>
                                                        <option value="No" {{ $property->instalaciones_cercanas['centro_comercial']=='No'?'selected':'' }}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('centro_comercial') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-propietario" role="tabpanel" aria-labelledby="v-pills-propietario-tab">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="nombre_propietario">Nombre Completo del Propietario <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="nombre_propietario" class="form-control {{ $errors->has('nombre_propietario') ? 'is-invalid' : '' }}" value="{{ $property->datos_propietario['nombre_propietario']}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('nombre_propietario') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="tel_propietario">Teléfono del Propietario <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="tel_propietario" class="form-control {{ $errors->has('tel_propietario') ? 'is-invalid' : '' }}" value="{{ $property->datos_propietario['tel_propietario']}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('tel_propietario') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="email_propietario">Correo del Propietario <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="email" name="email_propietario" class="form-control {{ $errors->has('email_propietario') ? 'is-invalid' : '' }}" value="{{ $property->datos_propietario['email_propietario']}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('email_propietario') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-apoderado" role="tabpanel" aria-labelledby="v-pills-apoderado-tab">
                                        <div class="row">
                                            <div class="col-sm-9">
                                                <label for="nombre_apoderado">Nombre Completo del Apoderado <small>(opcional)</small></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="nombre_apoderado" class="form-control {{ $errors->has('nombre_apoderado') ? 'is-invalid' : '' }}" value="{{ $property->datos_apoderado['nombre_apoderado']}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('nombre_apoderado') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="tel_apoderado">Teléfono del Apoderado <small>(opcional)</small></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="tel_apoderado" class="form-control {{ $errors->has('tel_apoderado') ? 'is-invalid' : '' }}" value="{{ $property->datos_apoderado['tel_apoderado']}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('tel_apoderado') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="email_apoderado">Correo del Apoderado <small>(opcional)</small></label>
                                                <div class="form-floating mb-3">
                                                    <input type="email" name="email_apoderado" class="form-control {{ $errors->has('email_apoderado') ? 'is-invalid' : '' }}" value="{{ $property->datos_apoderado['email_apoderado']}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('email_apoderado') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-asesor" role="tabpanel" aria-labelledby="v-pills-asesor-tab">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <label for="id_asesor_asignado">Asesor <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('id_asesor_asignado') ? 'is-invalid' : '' }}" name="id_asesor_asignado" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5">
                                                        @foreach($advisers as $adviser)
                                                            <option value="{{$adviser->id}}" {{ $property->id_asesor_asignado==$adviser->id?'selected':'' }}>{{$adviser->nombres_apellidos}}</option>
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_asesor_asignado') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-description" role="tabpanel" aria-labelledby="v-pills-description-tab">
                                        <div class="col-sm-12">
                                            <label for="descripcion">Descripción <span class="text-danger">*</span></label>
                                            <div class="form-floating mb-3">
                                                <textarea class="form-control {{ $errors->has('descripcion') ? 'is-invalid' : '' }}" rows="6" name="descripcion">{{ trim($property->datos_construccion['descripcion']) }}</textarea>
                                                <div class="invalid-feedback">
                                                    {{ $errors->first('descripcion') }}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <small>Los campos marcados con <span class="text-danger">*</span> son obligatorios</small>
                            </div>

                            <div class="col-sm-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-fw fa-save"></i>
                                    </span>
                                    <span class="text">Guardar Registro</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')

    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });

        function getMunicipalities(id){
            $.ajax({
                url: '/municipalities/'+id+'/json',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                method: 'GET',
                success: function(response) {
                    $("#id_municipio").empty();
                    $.each(response.municipalities, function(index, value) {
                        $("#id_municipio").append(`<option value=` + value.id + `>` + value.nombre + `</option>`);
                    });
                    $("#id_municipio").selectpicker('refresh');
                }
            })

            $.ajax({
                url: '/locations/'+id+'/json',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                method: 'GET',
                success: function(response) {
                    $("#id_ciudad").empty();
                    $.each(response.locations, function(index, value) {
                        $("#id_ciudad").append(`<option value=` + value.id + `>` + value.nombre + `</option>`)
                    });
                    $("#id_ciudad").selectpicker('refresh');
                }
            })
        }

        function getParishes(id){
            $.ajax({
                url: '/parishes/'+id+'/json',
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                method: 'GET',
                success: function(response) {
                    $("#id_parroquia").empty();
                    $.each(response.parishes, function(index, value) {
                        $("#id_parroquia").append(`<option value=` + value.id + `>` + value.nombre + `</option>`)
                    });
                    $("#id_parroquia").selectpicker('refresh');
                }
            })
        }

        // function getLocations(id){
        //     $.ajax({
        //         url: '/locations/'+id+'/json',
        //         headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
        //         method: 'GET',
        //         success: function(response) {
        //             $("#id_ciudad").empty();
        //             $.each(response.locations, function(index, value) {
        //                 $("#id_ciudad").append(`<option value=` + value.id + `>` + value.nombre + `</option>`)
        //             });
        //             $("#id_ciudad").selectpicker('refresh');
        //         }
        //     })
        // }
    </script>
@endsection