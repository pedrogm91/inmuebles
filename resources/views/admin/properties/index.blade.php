@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 1px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Inmuebles</h6>
                    <div class="float-right">
                        <a href="{{ route('properties.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Inmueble</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered display nowrap" style="width:100%">
                            <thead class="bg-slate-50">
                                <tr>
                                    <th scope="col" class="">
                                        Nombre
                                    </th>
                                    <th scope="col" class="">
                                        Categoría
                                    </th>
                                    <th scope="col" class="">
                                        Negocio
                                    </th>
                                    <th scope="col" class="">
                                        Estado
                                    </th>
                                    <th scope="col" class="">
                                        Asesor
                                    </th>
                                    <th scope="col" class="">
                                        <span class="sr-only">Acciones</span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($properties as $property)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        <a href="{{ route('properties.show', [$property->id]) }}" id="title_{{ $property->id }}">{{$property->nombre_propiedad}}</a>
                                    </th>
                                    <td class="">
                                        {{$property->category->nombre}}
                                    </td>
                                    <td class="">
                                        {{$property->negocio()}}
                                    </td>
                                    <td class="">
                                        {{$property->state->nombre}}
                                    </td>
                                    <td class="">
                                        @isset($property->adviser)
                                            <a href="{{ route('advisers.show', [$property->id_asesor_asignado]) }}">{{$property->adviser->nombres_apellidos}}</a>
                                        @endisset
                                    </td>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}
                                        {{-- @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->id==$property->id_asesor_asignado) --}}
                                        <a href="{{ route('properties.show', [$property->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Inmueble"><i class="fas fa-fw fa-eye"></i></a>
                                        {{-- @endif --}}
                                        {{-- edit --}}
                                        
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->asesoresInmobiliarios->id == $property->id_asesor_asignado)
                                        <a href="{{ route('properties.edit', [$property->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Inmueble"><i class="fas fa-fw fa-edit"></i></a>
                                        @endif
                                        {{-- documents --}}
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->asesoresInmobiliarios->id == $property->id_asesor_asignado)
                                        <a href="{{ route('properties.documents', [$property->id]) }}" class="btn btn-sm mr-1 btn-circle btn-warning" data-toggle="tooltip" data-placement="top" title="Cargar Documentación"><i class="far fa-fw fa-file-alt"></i></a>
                                        @endif
                                        {{-- photos --}}
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->asesoresInmobiliarios->id == $property->id_asesor_asignado)
                                        <a href="{{ route('properties.photos', [$property->id]) }}" class="btn btn-sm mr-1 btn-circle btn-secondary" data-toggle="tooltip" data-placement="top" title="Cargar Fotografías"><i class="far fa-fw fa-images"></i></a>
                                        @endif
                                        {{-- negocio --}}
                                        @if($property->isUsed() == 0)
                                            @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                            <a href="javascript:0" onclick="getProperty({{ $property->id }})" class="btn btn-sm mr-1 btn-circle btn-success" data-toggle="tooltip" data-placement="top" title="Negociar"><i class="fas fa-fw fa-hand-holding-usd"></i></a>
                                            @endif
                                        @endif
                                        {{-- timeline --}}
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                        <a href="{{ route('property.timeline',  $property->id) }}" class="btn btn-sm mr-1 btn-circle btn-dark" data-toggle="tooltip" data-placement="top" title="Linea de tiempo"><i class="fas fa-hourglass-half"></i></a>
                                        @endif
                                        {{-- delete --}}
                                        @if($property->isUsed() == 0)
                                            @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                                <form action="{{ route('properties.destroy', [$property->id]) }}" method="post" id="eliminar-{{$property->id}}">
                                                    @method('delete')
                                                    @csrf
                                                </form>
                                                <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Inmueble" onclick="confirmar('eliminar-{{$property->id}}', '¿Desea eliminar este inmueble?', 'Se borrara de forma permanente');"><i class="fas fa-fw fa-trash"></i></button>
                                            @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $properties->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

    {{-- NEGOCIO --}}
    <div class="modal fade" id="modalNegocio" tabindex="-1" role="dialog" aria-labelledby="modalNegocioLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalNegocioLabel">Negociación de Inmueble</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="/negotiations" method="POST" class="needs-validation">
                        @csrf
                        <input type="hidden" name="id_propiedad" id="id_propiedad" class="form-control">
                        <div class="row">
                            <div class="col-md-12 text-center font-weight-bold">
                                <p id="title_modal"></p>
                            </div>
                            <div class="col-md-7">
                                <label for="nombre_cliente">Nombre del Cliente</label>
                                <div class="form-floating mb-3">
                                    <input class="form-control" type="text" name="nombre_cliente" id="nombre_cliente">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('nombre_cliente') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label for="tel_cliente">Nro Telefónico</label>
                                <div class="form-floating mb-3">
                                    <input class="form-control" type="text" name="tel_cliente" id="tel_cliente">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('tel_cliente') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <label for="tipo_negocio">Tipo de Negociación <span class="text-danger">*</span></label>
                                <div class="form-floating mb-3">
                                    <select class="form-control {{ $errors->has('tipo_negocio') ? 'is-invalid' : '' }}" name="tipo_negocio" id="tipo_negocio" aria-label="floating label select example" title="Seleccione" data-live-search="true" required>
                                        <option value="Venta" {{old('tipo_negocio')=='Venta'?'selected':''}}>Venta</option>
                                        <option value="Alquiler" {{old('tipo_negocio')=='Alquiler'?'selected':''}}>Alquiler</option>
                                        <option value="Reserva" {{old('tipo_negocio')=='Reserva'?'selected':''}}>Reserva</option>
                                        <option value="Anula Reserva" {{old('tipo_negocio')=='Anula Reserva'?'selected':''}}>Anula Reserva</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('tipo_negocio') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-none" id="div_fecha_inicial">
                                <label for="fecha_inicial">Fecha Inicial</label>
                                <div class="form-floating mb-3">
                                    <input class="form-control" type="date" name="fecha_inicial" id="fecha_inicial" min="<?= date('Y-m-d'); ?>">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('fecha_inicial') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 d-none" id="div_fecha_final">
                                <label for="fecha_final">Fecha Final</label>
                                <div class="form-floating mb-3">
                                    <input class="form-control" type="date" name="fecha_final" id="fecha_final" min="<?= date('Y-m-d'); ?>">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('fecha_final') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="sistema_de_pago">Sistema de pago <span class="text-danger">*</span></label>
                                <div class="form-floating mb-3">
                                    <input class="form-control" type="text" name="sistema_de_pago" id="sistema_de_pago" required>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('sistema_de_pago') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="monto_mediacion">Monto de la mediación <span class="text-danger">*</span></label>
                                <div class="form-floating mb-3">
                                    <input class="form-control only-number" type="number" name="monto_mediacion" id="monto_mediacion" required>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('monto_mediacion') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="id_asesor">Asesor <span class="text-danger">*</span></label>
                                <div class="form-floating mb-3">
                                    <select class="form-control {{ $errors->has('id_asesor') ? 'is-invalid' : '' }}" name="id_asesor" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                        @foreach($advisers as $adviser)
                                            <option value="{{$adviser->id}}">{{$adviser->nombres_apellidos}} - {{$adviser->cedula}}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('id_asesor') }}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <label for="porcentaje_honorarios">% de honorarios profesionales <span class="text-danger">*</span></label>
                                <div class="form-floating mb-3">
                                    <input class="form-control only-number" type="number" name="porcentaje_honorarios" id="porcentaje_honorarios" min="0" max="100" value="0" required>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('porcentaje_honorarios') }}
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
                <div class="modal-footer">
                    <button type="button" data-dismiss="modal" class="btn btn-danger btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                            <i class="fas fa-fw fa-times-circle"></i>
                        </span>
                        <span class="text">Cancelar</span>
                    </button>
                    <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                        <span class="icon text-white-50">
                            <i class="fas fa-fw fa-save"></i>
                        </span>
                        <span class="text">Guardar</span>
                    </button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });

        $('#tipo_negocio').on('change', function() {
            if(this.value == 'Alquiler'){
                $("#div_fecha_inicial, #div_fecha_final").removeClass('d-none');
                $("#fecha_inicial, #fecha_final").val('');
            }else{
                $("#div_fecha_inicial, #div_fecha_final").addClass('d-none');
                $("#fecha_inicial, #fecha_final").val('');
            }
        });

        function getProperty(id){
            $("#title_modal").text('').text($("#title_"+id).text());
            $("#id_propiedad").val(id);
            $("#modalNegocio").modal('show');
        }
    </script>
@endsection