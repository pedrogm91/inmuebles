<div class="row">
    @if(count($photos)>0)
    <form method="POST" action="{{ route('properties.order_documents') }}">
        @csrf
        <div class="col-sm-12 mb-3 text-center">
            <ul id="sortable">
                @foreach($photos as $photo)
                    <li class="ui-state-default">
                        <input type="hidden" name="order[]" value="{{$photo->id}}">
                        <div class="col-sm-12">
                            <div class="card mb-4">
                                <div class="card-body">
                                    <img src="{{ asset('storage/'.$photo->documento_url) }}" class="img-thumbnail mb-2">
                                    <a href="{{ asset('storage/'.$photo->documento_url) }}" target="_blank" class="btn btn-sm btn-success btn-block">Descargar</a>
                                    @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->asesoresInmobiliarios->id==$property->id_asesor_asignado)
                                                                      <a href="{{ route('properties.destroy_documents', $photo->id) }}" class="btn btn-sm btn-danger btn-block">Eliminar</a>
                                    @endif

                                </div>
                            </div>
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
        <div class="col-sm-12 mb-3 text-right">
            <hr>
            <small>Arrastre las fotografías para ordenar. La primera será tomada como principal.</small>
            <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                <span class="icon text-white-50">
                    <i class="fas fa-fw fa-save"></i>
                </span>
                <span class="text">Guardar Orden</span>
            </button>
        </div>
    </form>
    @else
    <div class="col-lg-12 mb-3 text-center text-danger">
        <h5>La propiedad no posee fotografías asociadas</h5>
    </div>
    @endif
</div>
