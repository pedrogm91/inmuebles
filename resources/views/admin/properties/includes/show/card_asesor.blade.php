<div class="card shadow mb-4">
    <div class="card-header bg-light py-3">
        <h6 class="font-weight-bold mb-0 text-center">Asesor</h6>
    </div>
    <a href="{{ route('advisers.show', $property->adviser->id) }}">
    <img src="{{ Storage::url($property->adviser->imagen_perfil) }}" class="img-fluid" alt="{{ $property->adviser->nombres_apellidos }}">
    </a>
    <div class="card-body text-center">
        <a href="{{ route('advisers.show', $property->adviser->id) }}" class="card-link">{{ $property->adviser->nombres_apellidos }}</a>
    </div>
</div>