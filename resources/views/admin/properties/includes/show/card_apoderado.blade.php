<div class="card shadow mb-4">
    <div class="card-header bg-light py-3">
        <h6 class="font-weight-bold mb-0">Datos del apoderado</h6>
    </div>
    <ul class="list-group list-group-flush">
        @if ($property->datos_apoderado['nombre_apoderado'])
        <li class="list-group-item">{{ $property->datos_apoderado['nombre_apoderado']}}</li>    
        @endif
        @if ($property->datos_apoderado['tel_apoderado'])
        <li class="list-group-item">Telf: {{ $property->datos_apoderado['tel_apoderado']}}</li>
        @endif
        @if ($property->datos_apoderado['email_apoderado'])
        <li class="list-group-item">Email: {{ $property->datos_apoderado['email_apoderado']}}</li>
        @endif
    </ul>
</div>