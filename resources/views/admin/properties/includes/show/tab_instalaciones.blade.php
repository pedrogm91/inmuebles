<div class="row">
    <div class="col-lg-3">
        <label for="supermercado">Supermercado</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('supermercado') ? 'is-invalid' : '' }}" name="supermercado"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['supermercado']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['supermercado']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('supermercado') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="clinica">Clínica/Hospital</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('clinica') ? 'is-invalid' : '' }}" name="clinica"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['clinica']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['clinica']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('clinica') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="farmacia">Farmacia</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('farmacia') ? 'is-invalid' : '' }}" name="farmacia"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['farmacia']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['farmacia']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('farmacia') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="colegios">Colegios</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('colegios') ? 'is-invalid' : '' }}" name="colegios"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['colegios']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['colegios']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('colegios') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="estacion_metro">Estacio Metro</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('estacion_metro') ? 'is-invalid' : '' }}" name="estacion_metro"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['estacion_metro']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['estacion_metro']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('estacion_metro') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="autobus">Parada Autobus</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('autobus') ? 'is-invalid' : '' }}" name="autobus"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['autobus']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['autobus']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('autobus') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="centro_comercial">Centro Comercial</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('centro_comercial') ? 'is-invalid' : '' }}" name="centro_comercial"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Si" {{ $property->instalaciones_cercanas['centro_comercial']=='Si'?'selected':'' }}>Si</option>
                <option value="No" {{ $property->instalaciones_cercanas['centro_comercial']=='No'?'selected':'' }}>No</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('centro_comercial') }}
            </div>
        </div>
    </div>
</div>