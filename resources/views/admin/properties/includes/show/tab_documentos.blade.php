<div class="row">
    @if(count($documents)>0)
    @foreach($documents as $document)
    <div class="col-lg-3">
        <div class="card mb-4">
            <div class="card-header">
                <h6 class="m-0 font-weight-bold text-primary">{{ $document->nombre_documento }}</h6>
            </div>
            <div class="card-body">
                <a href="{{ asset('storage/'.$document->documento_url) }}" target="_blank" class="btn btn-success btn-block btn-sm">Descargar</a>
            @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->asesoresInmobiliarios->id==$property->id_asesor_asignado)
                <a href="{{ route('properties.destroy_documents', $document->id) }}" class="btn btn-danger btn-block btn-sm">Eliminar</a>
            @endif
            </div>
        </div>
    </div>
    @endforeach
    @else
    <div class="col-lg-12 mb-3 text-center text-danger">
        <h5>La propiedad no posee documentos asociados</h5>
    </div>
    @endif
</div>
