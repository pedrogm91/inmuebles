<div class="row">
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body">
                {{-- <b class="card-text">Nombre de la propiedad</b> --}}
                <h5 class="card-title text-primary mb-0"><b>{{ $property->nombre_propiedad }}</b></h5>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Categoría: <b>{{ $property->category->nombre }}</b></li>
                <li class="list-group-item">Tipo de Negocio: <b>{{$property->tipo_negocio==1?'Alquiler':'Venta'}}</b></li>
                <li class="list-group-item">Precio: <b>{{ $property->precio }}</b></li>
                <li class="list-group-item">Estado:
                    @switch($property->estatus_publicacion)
                        @case(1)
                            <b class="text-success">Publicada</b>
                            @break
                        @case(2)
                         <b class="text-danger">Terminada</b>
                            @break
                        @case(3)
                            <b class="text-warning">Pausada</b>
                            @break
                    @endswitch 
                </li>
                <li class="list-group-item"><b>Publicación:</b>
                    @switch($property->destacado)
                        @case(1)
                            <b class="text-info">Destacada</b>
                            @break
                        @case(2)
                            <b class="text-info">Simple</b>
                            @break
                    @endswitch 
                </li>
            </ul>
        </div>
    </div>
    <div class="col-lg-6">
        <div class="card">
            <div class="card-body"> 
              <p class="card-text"><b>Dirección:</b> {{ $property->ubicacion }}</p>
            </div>
            <ul class="list-group list-group-flush">
              <li class="list-group-item"><b>Estado:</b> {{ $states->nombre ?? ''}}</li>
              <li class="list-group-item"><b>Municipio:</b> {{ $municipalities->nombre ?? ''}}</li>
              <li class="list-group-item"><b>Parroquia o Urbanización:</b> {{ $parishes->nombre ?? '' }} </li>
              <li class="list-group-item"><b>Ciudad:</b> {{ $locations->nombre ?? '' }} </li>
              <li class="list-group-item">
                  <a href="/states" class="card-link"><i class="fas fa-eye"></i> Ubicaciones en sistema</a>
              </li>
            </ul>
        </div>
    </div>
</div>
<div class="row border-top pt-3 mt-3">
    <div class="col-lg-12">
        <label for="descripcion">Descripción</label>
        <div class="form-floating mb-3 overflow-auto" style="max-height: 400px;">
            {{ trim($property->datos_construccion['descripcion']) }}
        </div>
    </div>
</div>