<div class="card shadow mb-4">
    <div class="card-header bg-light py-3">
        <h6 class="font-weight-bold mb-0">Datos del propietario</h6>
    </div>
    <ul class="list-group list-group-flush">
        @if ($property->datos_propietario['nombre_propietario'])<li class="list-group-item">{{ $property->datos_propietario['nombre_propietario']}}</li>@endif
        @if ($property->datos_propietario['tel_propietario'])<li class="list-group-item">Telf: {{ $property->datos_propietario['tel_propietario']}}</li>@endif
        @if ($property->datos_propietario['email_propietario'])<li class="list-group-item">Email: {{ $property->datos_propietario['email_propietario']}}</li>@endif
    </ul>
</div>