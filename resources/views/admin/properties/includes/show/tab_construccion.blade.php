<div class="row">
    <div class="col-lg-3">
        <label for="area_terreno">Área Terreno</label>
        <div class="form-floating mb-3">
            <input type="text" name="area_terreno" class="form-control {{ $errors->has('area_terreno') ? 'is-invalid' : '' }}" placeholder="" min="0" value="{{ $property->datos_construccion['area_terreno'] }}" readonly disabled>
            <div class="invalid-feedback">
                {{ $errors->first('area_terreno') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="area_construccion">Área Construcción</label>
        <div class="form-floating mb-3">
            <input type="text" name="area_construccion" class="form-control {{ $errors->has('area_construccion') ? 'is-invalid' : '' }}" placeholder="" min="0" value="{{ $property->datos_construccion['area_construccion'] }}" readonly disabled>
            <div class="invalid-feedback">
                {{ $errors->first('area_construccion') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="antiguedad">Antiguedad</label>
        <div class="form-floating mb-3">
            <input type="text" name="antiguedad" class="form-control {{ $errors->has('antiguedad') ? 'is-invalid' : '' }}" placeholder="" min="0" value="{{ $property->datos_construccion['antiguedad'] }}" readonly disabled>
            <div class="invalid-feedback">
                {{ $errors->first('antiguedad') }}
            </div>
        </div>
    </div>

    <div class="col-lg-2">
        <label for="cocina">Cocina</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('cocina') ? 'is-invalid' : '' }}" name="cocina"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['cocina']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('cocina') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="sala">Sala</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('sala') ? 'is-invalid' : '' }}" name="sala"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['sala']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('sala') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="habit">Habit/Cubi</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('habit') ? 'is-invalid' : '' }}" name="habit"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['habit']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('habit') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="habit_s">Habit. S</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('habit_s') ? 'is-invalid' : '' }}" name="habit_s"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['habit_s']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('habit_s') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="banos">Baños</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('banos') ? 'is-invalid' : '' }}" name="banos"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['banos']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('banos') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="banos_s">Baño S</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('banos_s') ? 'is-invalid' : '' }}" name="banos_s"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['banos_s']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('banos_s') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="bano_medio">1/2 Baño</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('bano_medio') ? 'is-invalid' : '' }}" name="bano_medio"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['bano_medio']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('bano_medio') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="aire_a">A/A</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('aire_a') ? 'is-invalid' : '' }}" name="aire_a"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['aire_a']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('aire_a') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="tv">TV Cable</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('tv') ? 'is-invalid' : '' }}" name="tv"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['tv']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('tv') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="wifi">Internet</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('wifi') ? 'is-invalid' : '' }}" name="wifi"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['wifi']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('wifi') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="parrillera">Parrillera</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('parrillera') ? 'is-invalid' : '' }}" name="parrillera"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['parrillera']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('parrillera') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="piscina">Piscina</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('piscina') ? 'is-invalid' : '' }}" name="piscina"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['piscina']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('piscina') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="maletero">Maletero</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('maletero') ? 'is-invalid' : '' }}" name="maletero"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['maletero']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('maletero') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="lavadero">Lavadero</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('lavadero') ? 'is-invalid' : '' }}" name="lavadero"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['lavadero']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('lavadero') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="gimnasio">Gimnasio</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('gimnasio') ? 'is-invalid' : '' }}" name="gimnasio"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 3; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['gimnasio']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('gimnasio') }}
            </div>
        </div>
    </div>
    <div class="col-lg-2">
        <label for="estacionamiento">Estac.</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('estacionamiento') ? 'is-invalid' : '' }}" name="estacionamiento"  aria-label="floating label select example" title="" data-live-search="true" readonly disabled>
                @for ($i = 0; $i <= 10; $i++)
                <option value="{{ $i }}" {{$property->datos_construccion['estacionamiento']==$i?'selected':''}}>{{ $i }}</option>
                @endfor
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('estacionamiento') }}
            </div>
        </div>
    </div>
    <div class="col-lg-3">
        <label for="tipo_estacionamiento">Tipo Estac</label>
        <div class="form-floating mb-3">
            <select class="form-control {{ $errors->has('tipo_estacionamiento') ? 'is-invalid' : '' }}" name="tipo_estacionamiento"  aria-label="floating label select example" title="Seleccione" readonly disabled>
                <option value="Techado" {{ $property->datos_construccion['tipo_estacionamiento']=='Techado'?'selected':'' }}>Techado</option>
                <option value="Aire libre" {{ $property->datos_construccion['tipo_estacionamiento']=='Aire libre'?'selected':'' }}>Aire libre</option>
                <option value="Sin Estacionamiento" {{ $property->datos_construccion['tipo_estacionamiento']=='Sin Estacionamiento'?'selected':'' }}>Sin Estacionamiento</option>
            </select>
            <div class="invalid-feedback">
                {{ $errors->first('tipo_estacionamiento') }}
            </div>
        </div>
    </div>
    
</div>