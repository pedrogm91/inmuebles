@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 1px;
        }
        .btn-light.disabled, .btn-light:disabled {
            color: #3a3b45;
            background-color: #ffffff;
            border: solid #d1d3e2 1px;
        }
        .dropdown-toggle::after {
            border-top: 0;
            border-right: 0;
            border-bottom: 0;
            border-left: 0;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
        }
        #sortable { list-style-type: none; margin: 0; padding: 0; min-height: 500px; overflow-y: scroll; }
        #sortable li { margin: 0; padding: 0; float: left; width: 24%; }
        .ui-state-default, .ui-widget-content .ui-state-default, .ui-widget-header .ui-state-default, .ui-button, html .ui-button.ui-state-disabled:hover, html .ui-button.ui-state-disabled:active {
            border: 0px;
            background: transparent;
        }
        .ui-state-default a, .ui-state-default a:link, .ui-state-default a:visited, a.ui-button, a:link.ui-button, a:visited.ui-button, .ui-button {
            color: #fff;
            text-decoration: none;
        }
    </style>
@endsection

@section('content')
    
    <div class="row justify-content-center">
        {{-- Column left --}}
        <div class="col-lg-9">
            <div class="card shadow mb-4">
                <div class="card-header bg-light py-3">
                    <h4 class="font-weight-bold float-left mb-0">#{{$property->id}}</h4>
                    <div class="float-right">
                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->asesoresInmobiliarios->id==$property->id_asesor_asignado)
                        <a href="{{ route('properties.edit', [$property->id]) }}" class="btn btn-primary btn-icon-split btn-sm" style="border: none;">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-edit"></i>
                            </span>
                            <span class="text">Editar Propiedad</span>
                        </a>
                        <a href="{{ route('properties.documents', [$property->id]) }}" class="btn btn-primary btn-icon-split btn-sm" style="border: none;">
                            <span class="icon text-white-50">
                                <i class="far fa-fw fa-file-alt"></i>
                            </span>
                            <span class="text">Cargar Documentación</span>
                        </a>
                        <a href="{{ route('properties.photos', [$property->id]) }}" class="btn btn-primary btn-icon-split btn-sm" style="border: none;">
                            <span class="icon text-white-50">
                                <i class="far fa-fw fa-images"></i>
                            </span>
                            <span class="text">Cargar Fotogafías</span>
                        </a>
                        @endif
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="nav nav-tabs mb-4" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Inmueble</a>
                            <a class="nav-link" id="v-pills-construccion-tab" data-toggle="pill" href="#v-pills-construccion" role="tab" aria-controls="v-pills-construccion" aria-selected="false">Construcción</a>
                            <a class="nav-link" id="v-pills-instalaciones-tab" data-toggle="pill" href="#v-pills-instalaciones" role="tab" aria-controls="v-pills-instalaciones" aria-selected="false">Instalaciones</a>
                            <a class="nav-link" id="v-pills-documentos-tab" data-toggle="pill" href="#v-pills-documentos" role="tab" aria-controls="v-pills-documentos" aria-selected="false">Documentos</a>
                            <a class="nav-link" id="v-pills-fotografias-tab" data-toggle="pill" href="#v-pills-fotografias" role="tab" aria-controls="v-pills-fotografias" aria-selected="false">Fotografías</a>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    @include('admin.properties.includes.show.tab_home')
                                </div>
                                <div class="tab-pane fade" id="v-pills-construccion" role="tabpanel" aria-labelledby="v-pills-construccion-tab">
                                    @include('admin.properties.includes.show.tab_construccion')
                                </div>
                                <div class="tab-pane fade" id="v-pills-instalaciones" role="tabpanel" aria-labelledby="v-pills-instalaciones-tab">
                                    @include('admin.properties.includes.show.tab_instalaciones')
                                </div>
                                <div class="tab-pane fade" id="v-pills-documentos" role="tabpanel" aria-labelledby="v-pills-documentos-tab">
                                    @include('admin.properties.includes.show.tab_documentos')
                                </div>
                                <div class="tab-pane fade" id="v-pills-fotografias" role="tabpanel" aria-labelledby="v-pills-fotografias-tab">
                                    @include('admin.properties.includes.show.tab_fotografias')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- Column right --}}
        <div class="col-lg-3">
            @include('admin.properties.includes.show.card_asesor')
            @include('admin.properties.includes.show.card_propietario')
            @include('admin.properties.includes.show.card_apoderado')
        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@endsection