@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 1px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Carga de Documentación</h6>
                    <div class="float-right">
                        <a href="{{ route('properties.show', $property->id) }}" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-arrow-circle-left"></i>
                            </span>
                            <span class="text">Ir atrás</span>
                        </a>

                        <a href="#" onclick="addDocumet()" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Agregar más Documentos</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-12 mb-3 text-center">
                            <h5>#{{$property->id}} | {{$property->nombre_propiedad}}</h5>
                        </div>

                        <div class="col-sm-12">
                            <form action="/properties/upload_documents" method="POST" class="needs-validation" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" class="form-control" value="{{ $property->id }}" name="id_propiedad">
                                <input type="hidden" class="form-control" value="{{ $property->id_asesor_asignado }}" name="id_asesor">
                                <input type="hidden" class="form-control" value="documentos" name="tipo_documento">

                                <div class="row" id="div_documentos">
                                    <div class="col-sm-4 mb-4">
                                        <input type="file" name="documento[]" class="dropify" accept="image/png, image/gif, image/jpeg, image/jpg, application/pdf" data-max-file-size="5M" data-allowed-file-extensions="png jpg jpeg gif pdf" data-default-file="">
                                        <input type="text" class="form-control mt-2" name="nombre[]" placeholder="Nombre Documento">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 mt-3 text-center">
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-upload"></i>
                                            </span>
                                            <span class="text">Guardar Documentos Cargados</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz clic.<br><h6>El tamaño máximo es 5 megabytes</h6>',
                'replace': 'Arrastra y suelta o haz clic para reemplazar.<br><h6>El tamaño máximo es 5 megabytes</h6>',
                'remove': 'Eliminar',
                'error': 'Vaya, sucedió algo malo.'
            },
            error: {
                'fileSize': 'El tamaño del archivo es demasiado grande.<br><h6>El tamaño máximo es 5 megabytes</h6>',
                'minWidth': 'El ancho de la imagen es demasiado pequeño.',
                'maxWidth': 'El ancho de la imagen es demasiado grande.',
                'minHeight': 'La altura de la imagen es demasiado pequeña',
                'maxHeight': 'La altura de la imagen es demasiado grande',
            }
        });

        function addDocumet(){
            $("#div_documentos").append('<div class="col-sm-4 mb-4"><input type="file" name="documento[]" class="dropify" accept="image/png, image/gif, image/jpeg, image/jpg, application/pdf" data-max-file-size="5M" data-allowed-file-extensions="png jpg jpeg gif pdf" data-default-file=""><input type="text" class="form-control mt-2" name="nombre[]" placeholder="Nombre Documento"></div>');

            $('.dropify').dropify({
                messages: {
                    'default': 'Arrastra y suelta un archivo aquí o haz clic.<br><h6>El tamaño máximo es 5 megabytes</h6>',
                    'replace': 'Arrastra y suelta o haz clic para reemplazar.<br><h6>El tamaño máximo es 5 megabytes</h6>',
                    'remove': 'Eliminar',
                    'error': 'Vaya, sucedió algo malo.'
                },
                error: {
                    'fileSize': 'El tamaño del archivo es demasiado grande.<br><h6>El tamaño máximo es 5 megabytes</h6>',
                    'minWidth': 'El ancho de la imagen es demasiado pequeño. ',
                    'maxWidth': 'El ancho de la imagen es demasiado grande.',
                    'minHeight': 'La altura de la imagen es demasiado pequeña',
                    'maxHeight': 'La altura de la imagen es demasiado grande',
                }
            });
        }
    </script>
@endsection