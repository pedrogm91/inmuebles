@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Postulaciones</h6>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                    <th scope="col" class="">
                                        Nombre
                                    </th>
                                    <th scope="col" class="">
                                        Teléfono
                                    </th>
                                    <th scope="col" class="">
                                        Correo
                                    </th>
                                    <th scope="col" class="">
                                        Vacante
                                    </th>
                                    <th scope="col" class="">
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($postulations as $postulation)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td>
                                        {{ $postulation->nombre}}
                                    </td>
                                    <td>
                                        {{ $postulation->telefono}}
                                    </td>
                                    <td>
                                        {{ $postulation->correo}}
                                    </td>
                                    <td>
                                        {{ $postulation->vacante->cargo}}
                                    </td>
                                    <td>
                                        {{-- show --}}
                                        <a href="{{ route('postulations.show', [$postulation->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fas fa-eye"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $postulations->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
    