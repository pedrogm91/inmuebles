@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 0px;
        }
        .btn-light.disabled, .btn-light:disabled {
            color: #3a3b45;
            background-color: #ffffff;
            border: solid #d1d3e2 0px;
        }
        .dropdown-toggle::after {
            border-top: 0;
            border-right: 0;
            border-bottom: 0;
            border-left: 0;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
            border: solid transparent 0px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Detalles Postulación</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-4">
                            <label for="nombre">Nombre</label>
                            <div class="form-floating mb-3">
                                <input type="text" name="nombre" class="form-control" value="{{ $postulation->nombre }}" readonly disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="nombre">Teléfono</label>
                            <div class="form-floating mb-3">
                                <input type="text" name="nombre" class="form-control" value="{{ $postulation->telefono }}" readonly disabled>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="nombre">Correo</label>
                            <div class="form-floating mb-3">
                                <input type="text" name="nombre" class="form-control" value="{{ $postulation->correo }}" readonly disabled>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <label for="nombre">Vacante</label>
                            <div class="form-floating mb-3">
                                <input type="text" name="nombre" class="form-control" value="{{ $postulation->vacante->cargo }}" readonly disabled>
                            </div>
                        </div>
                        @if($postulation->adjunto)
                        <div class="col-sm-6">
                            <label for="nombre">Archivo Adjunto</label>
                            <div class="form-floating mb-3">
                                <a href="{{ asset('storage/'.$postulation->adjunto) }}" target="_blank">Descargar</a>
                            </div>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>

    </script>
@endsection