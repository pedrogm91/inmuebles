@extends('layouts.dashboard')
<script>
    var check = function() {
    if (document.getElementById('password1').value ==
      document.getElementById('password2').value) {
      document.getElementById('message').style.color = 'green';
      document.getElementById('message').innerHTML = 'Las Conraseñas coinciden';
    } else {
      document.getElementById('message').style.color = 'red';
      document.getElementById('message').innerHTML = 'Contraseñas diferentes';
    }
  }
</script>
@section('content')
<div class="container">
    <div class="row justify-content-center">
                    <div class="col-md-9">
                        <div class="card mb-4">
                            <div class="card-header text-center">Registro de Usuario</div>
                            <div class="card-body">  
                            <form action="/users" method="POST" class="needs-validation">
                                @csrf
                                <div class="row ">
                                    <div class="col-sm-5 ">
                                        <label for="name">Nombre</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" placeholder="nombre">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5"><label for="email">Correo Electronico</label>
                                        <div class="form-floating mb-3">
                                            <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' : '' }}" placeholder="correo">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row ">
                                    <div class="col-sm-5 "><label for="password">Contraseña</label>
                                        <div class="form-floating mb-3">
                                            <input type="password" name="password" id="password1" onkeyup='check();' class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="contraseña">
                                            <span id='message'></span>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('password') }}
                                    </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5 "><label for="password">Confirmar Contraseña</label>
                                        <div class="form-floating mb-3">
                                            <input type="password" name="password" id="password2" onkeyup='check();' class="form-control {{ $errors->has('password') ? 'is-invalid' : '' }}" placeholder="contraseña">
                                            <span id='message'></span>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('password') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row text-center">
                                    <div class="col-sm-5"><label for="status">Estatus Cuenta</label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status"  aria-label="Selecciona el status">
                                                <option selected value="active">Activo</option>
                                                <option value="inactive">Inactivo</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('status') }}
                                        </div>
                                        </div>
                                    </div>
                                    <div class="col-sm-5"><label for="role">Rol Desempeñado</label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('role') ? 'is-invalid' : '' }}" name="role"  aria-label="Floating label select example">
                                                <option selected value="admin">Administrador</option>
                                                <option value="director">Director</option>
                                                <option value="adviser">Asesor</option>
                                                <option value="user">Usuario</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('role') }}
                                    </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row text-center">
                                    <div class="col-sm-12">
                                            <button type="submit" class="btn btn-primary btn-lg">Registrar Usuario</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                
            
        
    </div>
</div>
@endsection