@extends('layouts.dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-9">
            <div class="card mb-4">
                <div class="card-header text-center text-white">Editar Datos de la Cuenta</div>
                <div class="card-body">  
                <form action="/users/{{$user->id}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row ">
                        <div class="col-sm-5 ">Nombre</div>
                        <div class="col-sm-7 text-secondary">
                            <div class="form-floating mb-3">
                                
                                <input type="text" name="name"  @class(['is-invalid' => $errors->get('name'), 'form-control' => true]) id="" placeholder="{{$user->name}}" value="{{old('name', $user->name)}}">
                                {{ $errors->has('name') ? 'Nombre no puede estar vacio' :"" }}
                                <div class="invalid-feedback">
                                    {{ $errors->first('name') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-5">Correo Electronico</div>
                        <div class="col-sm-7 text-secondary">
                            <div class="form-floating mb-3">
                                <input type="email" name="email" @class(['is-invalid' => $errors->get('email'), 'form-control' => true]) id="" placeholder="{{$user->email}}" value="{{old('email', $user->email)}}">
                                {{ $errors->has('email') ? 'El formato no corresponde a un correo' :""}}
                                <div class="invalid-feedback">
                                    {{ $errors->first('email') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-5">Estatus</div>
                        <div class="col-sm-7 text-secondary">
                            <div class="form-floating mb-3">
                                <select class="form-control" name="status" id="status" aria-label="Floating label select example">
                                    @if (old('status', $user->status) === 'active')
                                        <option selected value="active">Activo</option>
                                        <option value="inactive">Inactivo</option>
                                    @else
                                        <option value="active">Activo</option>
                                        <option selected value="inactive">Inactivo</option>
                                    @endif
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('status') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-5">Rol de usuario</div>
                        <div class="col-sm-7 text-secondary">
                            <div class="form-floating mb-3">
                                <select class="form-control" name="role" id="role" aria-label="{{ $user->role }}">
                                        <option {{ $user->role == 'admin' ? 'selected' : '' }} value="admin">Administrador</option>
                                        <option {{ $user->role == 'director' ? 'selected' : '' }} value="director">Director</option>
                                        <option {{ $user->role == 'adviser' ? 'selected' : '' }} value="adviser">Asesor</option>
                                        <option {{ $user->role == 'user' ? 'selected' : '' }} value="user">Usuario</option>
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('role') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row text-center">
                        <div class="col-sm-12">
                                <button type="submit" class="btn btn-primary btn-lg">Actualizar</button>
                        </div>
                    </div>
                </form>
                </div>
            </div>
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-header text-white">Cambiar contraseña</div>
                <div class="card-body">
                  <h5 class="card-title">Detalles</h5>
                  <p class="card-text">Para cambiar tu contraseña por seguridad enviaremos al correo asociado a la cuenta un email de recuperación de contraseña.</p>
                  <a href="/password/reset" class="btn btn-warning">Cambiar</a>
                </div>
              </div>

        </div>
    </div>
</div>
@endsection