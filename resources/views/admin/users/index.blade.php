@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Usuarios </h6>
                    <div class="float-right">
                        <a href="{{ route('users.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Usuario</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Nombre
                                </th>
                                <th scope="col" class="">
                                Email
                                </th>
                                <th scope="col" class="">
                                Rol
                                </th>
                                <th scope="col" class="">
                                Estado
                                </th>
                                <th scope="col" class="">
                                    <span class="sr-only">Acciones</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        <a href="/users/{{$user->id}}">{{$user->name}}</a>
                                    </th>
                                    <td class="">
                                    {{$user->email}}
                                    </td>
                                    <td class="">
                                    {{$user->role}}
                                    </td>
                                    <td class="">
                                    {{$user->status}}
                                    </td>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}
                                        <a href="{{ route('users.show', [$user->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Usuario"><i class="fas fa-eye"></i></a>
                                        {{-- edit --}}
                                        <a href="{{ route('users.edit', [$user->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Usuario"><i class="fas fa-edit"></i></a>
                                        {{-- adviser --}}
                                        @if(!$user->asesoresInmobiliarios)
                                            <a href="/advisers/create/{{$user->id}}" class="btn btn-sm mr-1 btn-circle btn-success" data-toggle="tooltip" data-placement="top" title="Convertir en Asesor"><i class="fas fa-user"></i></a>
                                        @else
                                            <a href="{{ route('advisers.show', $user->asesoresInmobiliarios->id) }}" class="btn btn-sm mr-1 btn-circle btn-warning" data-toggle="tooltip" data-placement="top" title="Ver perfil del Asesor"><i class="fas fa-user"></i></a>
                                        @endif
                                        {{-- delete --}}
                                        @if(Auth::user()->id!=$user->id)
                                        <form action="{{ route('users.destroy', [$user->id]) }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Usuario"><i class="fas fa-trash"></i></button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $users->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

    