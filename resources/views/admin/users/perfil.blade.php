@extends('layouts.dashboard')

@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-9">

            <div class="card text-center">
                <div class="card-header bg-light">
                    <ul class="nav nav-tabs card-header-tabs" id="myTab">
                        <li class="nav-item" role="presentation">
                          <a href="#" class="nav-link active" id="home-tab" data-toggle="tab" data-target="#home" role="tab" aria-controls="home" aria-selected="true">Cuenta de usuario</a>
                        </li>
                        <li class="nav-item" role="presentation">
                          <a href="#" class="nav-link" id="profile-tab" data-toggle="tab" data-target="#profile" role="tab" aria-controls="profile" aria-selected="false">Favoritos</a>
                        </li>
                    </ul>
                </div>
                <div class="card-body">
                    
                    {{-- Form edit user --}}
                      <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <form action="/perfil/{{$user->id}}" method="POST">
                                @csrf
                                @method('PUT')
                                
                                <div class="row">
                                    <div class="col">
                                        <div class="form-form-group mb-3 text-left">
                                            <label for="">Nombre del usuario</label>
                                            <input type="text" name="name"  @class(['is-invalid' => $errors->get('name'), 'form-control' => true]) id="" placeholder="{{$user->name}}" value="{{old('name', $user->name)}}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        </div>
                                        <div class="form-form-group mb-3 text-left">
                                            <label for="">Email del nombre</label>
                                            <input type="email" name="email" @class(['is-invalid' => $errors->get('email'), 'form-control' => true]) id="" placeholder="{{$user->email}}" value="{{old('email', $user->email)}}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('email') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="d-grid">
                                    <button type="submit" class="btn btn-primary btn-block">Actualizar</button>
                                </div>
                            </form>
                        </div>

                        {{-- Favoritos --}}
                        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                            <div class="table-responsive">
                                <table class="table text-left">
                                    <caption>Propiedades favoritas</caption>
                                    <thead>
                                        <tr>
                                        <th scope="col">Propiedad</th>
                                        <th scope="col">Acciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($favorites as $favorite)
                                        <tr>
                                            <td>{{ $favorite->property->nombre_propiedad }}</td>
                                            <td>
                                                {{-- delete --}}
                                                <form action="{{ route('favorite.destroy', $favorite) }}" method="post">
                                                    @method('delete')
                                                    @csrf
                                                    <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Quitar de favoritos"><i class="fas fa-trash"></i></button>
                                                </form>
                                            </td>
                                        </tr>
                                            
                                        @endforeach
                                    </tbody>
                                </table>
                              </div>
                        </div>
                      </div>
                </div>
              </div>

            
        </div>

        <div class="col-md-3">
            <div class="card">
                <div class="card-header bg-light">Cambiar contraseña</div>
                <div class="card-body">
                  <h5 class="card-title">Detalles</h5>
                  <p class="card-text">Para cambiar tu contraseña por seguridad utilizaremos el metodo de verificación de correo electronico.</p>
                  <a href="/password/reset" class="btn btn-info">Actualizar Contraseña</a>
                </div>
              </div>
        </div>
    </div>
</div>
@endsection