@extends('layouts.dashboard')

@section('content')
<div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-4 text-center">
                <!-- Profile picture card-->
                <div class="card mb-4 mb-xl-0">
                    <div class="card-header text-white">Perfil Asesor</div>
                    <div class="card-body text-center">
                            @if ($user->adviser)
                            <!-- Profile picture image-->
                            <img class="img-account-profile rounded-circle mb-2" style="object-fit: cover;" width="100" height="100" 
                            src="{{ Storage::url($user->adviser->imagen_perfil) }}" height="175">
                            <!-- Profile picture help block-->
                            <div class="small font-italic text-muted mb-4"></div>
                            <!-- Profile picture upload button-->
                            <!--este boton va a cambiar en base a si se tiene un perfil de usuario o no -->
                            <a  href="" class="btn btn-primary" type="button">Ir al perfil de asesor</a>
                            @else
                            <a href="/advisers/create/{{$user->id}}" class="btn btn-sm mr-1 btn-success" data-toggle="tooltip" data-placement="top" title="Convertir en Asesor">
                                Convertir en asesor
                            </a>
                            @endif
                        </div>
                    </div>
                </div>
            <div class="col-xl-8">
                <!-- Account details card-->
                <div class="card mb-4">
                    <div class="card-header text-center text-white">Detalles de la Cuenta</div>
                    <div class="card-body">
                        <div class="row ">
                            <div class="col-sm-5 ">Nombre</div>
                            <div class="col-sm-7 text-secondary">{{$user->name}}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-5">Correo Electrónico</div>
                            <div class="col-sm-7 text-secondary">{{$user->email}}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-5">Estatus de la Cuenta</div>
                            <div class="col-sm-7 text-secondary">{{$user->status}}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-5">Rol Desempeñado</div>
                            <div class="col-sm-7 text-secondary">{{$user->role}}</div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-sm-12">
                                <a href="/users/{{$user->id}}/edit" class="btn btn-primary">Editar perfil</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection