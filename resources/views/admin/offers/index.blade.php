@extends('layouts.dashboard')

@section('style')
    <style>

    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Ofertas</h6>
                    <div class="float-right">
                        <a href="{{ route('offers.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Oferta</span>
                        </a>
                    </div>

                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Propiedad
                                </th>
                                <th scope="col" class="">
                                Asesor
                                </th>
                                <th scope="col" class="">
                                Fecha Oferta
                                </th>
                                <th scope="col" class="">
                                Fecha Caducidad
                                </th>
                                <th scope="col" class="">
                                Negocio
                                </th>
                                <th scope="col" class="">
                                Ganancia
                                </th>
                                <th scope="col" class="">
                                    <span class="sr-only">Acciones</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($offers as $offer)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        <a href="{{ route('properties.show', [$offer->id_propiedad]) }}">{{$offer->property->category->nombre}} en {{$offer->property->negocio()}} {{ $offer->property->datos_construccion["area_construccion"] }}m2. {{$offer->property->location->nombre}}, Edo. {{$offer->property->state->nombre}}</a>
                                    </th>
                                    <td scope="row" class="">
                                    	<a href="{{ route('advisers.show', [$offer->id_asesor]) }}">{{$offer->adviser->nombres_apellidos}}</a>
                                    </td>
                                    <td scope="row" class="">
                                        {{date('d-m-Y', strtotime($offer->fecha_oferta))}}
                                    </td>
                                    <td scope="row" class="">
                                        {{date('d-m-Y', strtotime($offer->fecha_caducidad))}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$offer->property->negocio()}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$offer->margen_de_ganancia}}
                                    </td>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}
                                        <a href="{{ route('offers.show', [$offer->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Oferta"><i class="fas fa-eye"></i></a>
                                        {{-- edit --}}
                                        <a href="{{ route('offers.edit', [$offer->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Oferta"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        @if(Auth::user()->id!=$offer->id_asesor)
                                        <form action="{{ route('offers.destroy', [$offer->id]) }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Oferta"><i class="fas fa-trash"></i></button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{-- $offers->links() --}}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection