@extends('layouts.dashboard')

@section('style')
    <style>

    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Negociaciones</h6>
                    <div class="float-right">
                        <a href="{{ route('negotiations.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Negociación</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Propiedad
                                </th>
                                <th scope="col" class="">
                                Asesor
                                </th>
                                <th scope="col" class="">
                                Fecha Negociacion
                                </th>
                                <th scope="col" class="">
                                Negocio
                                </th>
                                <th scope="col" class="">
                                Monto Mediación
                                </th>
                                <th scope="col" class="">
                                Honorarios
                                </th>
                                <th scope="col" class="">
                                    <span class="sr-only">Acciones</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($negotiations as $negotiation)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td scope="row" class="">
                                        {{$negotiation->property->nombre_propiedad}}
                                    </td>
                                    <td scope="row" class="">
                                    	{{$negotiation->adviser->nombres_apellidos}}
                                    </td>
                                    <td scope="row" class="">
                                        {{date('d-m-Y', strtotime($negotiation->fecha_oferta))}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$negotiation->tipo_negocio}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$negotiation->monto_mediacion}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$negotiation->porcentaje_honorarios}}%
                                    </td>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}
                                        <a href="{{ route('negotiations.show', [$negotiation->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Negociacion"><i class="fas fa-eye"></i></a>
                                        {{-- edit --}}
                                        <a href="{{ route('negotiations.edit', [$negotiation->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Negociacion"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->id==$negotiation->id_asesor)
                                        <form action="{{ route('negotiations.destroy', [$negotiation->id]) }}" method="post" id="eliminar-{{$negotiation->id}}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                        <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Negociacion" onclick="confirmar('eliminar-{{$negotiation->id}}', '¿Desea eliminar esta negociación?', 'Se borrara de forma permanente');"><i class="fas fa-trash"></i></button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $negotiations->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection