@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 1px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Registro de Negociación</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="/negotiations" method="POST" class="needs-validation">
                                @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="tipo_negocio">Tipo de Negociación <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('tipo_negocio') ? 'is-invalid' : '' }}" name="tipo_negocio" id="tipo_negocio" aria-label="floating label select example" title="Seleccione" data-live-search="true" required>
                                                <option value="Venta" {{old('tipo_negocio')=='Venta'?'selected':''}}>Venta</option>
                                                <option value="Alquiler" {{old('tipo_negocio')=='Alquiler'?'selected':''}}>Alquiler</option>
                                                <option value="Reserva" {{old('tipo_negocio')=='Reserva'?'selected':''}}>Reserva</option>
                                                <option value="Anula Reserva" {{old('tipo_negocio')=='Anula Reserva'?'selected':''}}>Anula Reserva</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('tipo_negocio') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 d-none" id="div_fecha_inicial">
                                        <label for="fecha_inicial">Fecha Inicial</label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control" type="date" name="fecha_inicial" id="fecha_inicial" min="<?= date('Y-m-d'); ?>">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('fecha_inicial') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 d-none" id="div_fecha_final">
                                        <label for="fecha_final">Fecha Final</label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control" type="date" name="fecha_final" id="fecha_final" min="<?= date('Y-m-d'); ?>">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('fecha_final') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-8">
                                        <label for="id_propiedad">Propiedad <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('id_propiedad') ? 'is-invalid' : '' }}" name="id_propiedad" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                @foreach($properties as $property)
                                                    <option value="{{$property->id}}">{{$property->nombre_propiedad}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('id_propiedad') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="nombre_cliente">Nombre Cliente</label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control" type="text" name="nombre_cliente" id="nombre_cliente">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('nombre_cliente') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="tel_cliente">Nro Telefónico</label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control" type="text" name="tel_cliente" id="tel_cliente">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('tel_cliente') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="sistema_de_pago">Sistema de pago <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control" type="text" name="sistema_de_pago" id="sistema_de_pago" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('sistema_de_pago') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="monto_mediacion">Monto de la mediación <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control only-number" type="number" name="monto_mediacion" id="monto_mediacion" min="0" value="0" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('monto_mediacion') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="id_asesor">Asesor <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('id_asesor') ? 'is-invalid' : '' }}" name="id_asesor" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                @foreach($advisers as $adviser)
                                                    <option value="{{$adviser->id}}">{{$adviser->nombres_apellidos}} - {{$adviser->cedula}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('id_asesor') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="porcentaje_honorarios">Porcentaje de honorarios profesionales <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input class="form-control only-number" type="number" name="porcentaje_honorarios" id="porcentaje_honorarios" min="0" max="100" value="0" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('porcentaje_honorarios') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <small>Los campos marcados con <span class="text-danger">*</span> son obligatorios</small>
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('negotiations.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>

                                <div class="">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });

        $('#tipo_negocio').on('change', function() {
            if(this.value == 'Alquiler'){
                $("#div_fecha_inicial, #div_fecha_final").removeClass('d-none');
                $("#fecha_inicial, #fecha_final").val('');
            }else{
                $("#div_fecha_inicial, #div_fecha_final").addClass('d-none');
                $("#fecha_inicial, #fecha_final").val('');
            }
        });
    </script>
@endsection