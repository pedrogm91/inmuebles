@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid transparent 1px;
        }
        .btn-light.disabled, .btn-light:disabled {
            color: #3a3b45;
            background-color: #ffffff;
            border: solid transparent 1px;
        }
        .dropdown-toggle::after {
            border-top: 0;
            border-right: 0;
            border-bottom: 0;
            border-left: 0;
        }
        .form-control:disabled, .form-control[readonly] {
            background-color: #ffffff;
            opacity: 1;
        }
        .form-control:disabled, .form-control[readonly] {
            border: solid transparent 1px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Detalles de Negociación</h6>
                    <div class="float-right">
                        <a href="{{ route('negotiations.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-arrow-circle-left"></i>
                            </span>
                            <span class="text">Regresar</span>
                        </a>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="tipo_negocio">Tipo de Negociación</label>
                                    <div class="form-floating mb-3">
                                        <select class="form-control {{ $errors->has('tipo_negocio') ? 'is-invalid' : '' }}" name="tipo_negocio" aria-label="floating label select example" title="Seleccione" data-live-search="true" disabled="" readonly>
                                            <option value="Venta" {{$negotiation->tipo_negocio=='Venta'?'selected':''}}>Venta</option>
                                            <option value="Alquiler" {{$negotiation->tipo_negocio=='Alquiler'?'selected':''}}>Alquiler</option>
                                            <option value="Reserva" {{$negotiation->tipo_negocio=='Reserva'?'selected':''}}>Reserva</option>
                                            <option value="Anula Reserva" {{$negotiation->tipo_negocio=='Anula Reserva'?'selected':''}}>Anula Reserva</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('tipo_negocio') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 {{$negotiation->tipo_negocio=='Alquiler'?'':'d-none'}}">
                                    <label for="fecha_inicial">Fecha Inicial</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" name="fecha_inicial" id="fecha_inicial" value="{{date('d-m-Y', strtotime($negotiation->fecha_inicial))}}" disabled="" readonly>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('fecha_inicial') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 {{$negotiation->tipo_negocio=='Alquiler'?'':'d-none'}}">
                                    <label for="fecha_final">Fecha Final</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" name="fecha_final" id="fecha_final" value="{{date('d-m-Y', strtotime($negotiation->fecha_final))}}" disabled="" readonly>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('fecha_final') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <label for="id_propiedad">Propiedad</label>
                                    <div class="form-floating mb-3">
                                        <select class="form-control {{ $errors->has('id_propiedad') ? 'is-invalid' : '' }}" name="id_propiedad" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" disabled="" readonly>
                                            @foreach($properties as $property)
                                                <option value="{{$property->id}}" {{ $negotiation->id_propiedad == $property->id ?'selected':''}}>{{$property->nombre_propiedad}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('id_propiedad') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 {{ $negotiation->nombre_cliente ? '':'d-none' }} ">
                                    <label for="nombre_cliente">Nombre Cliente</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" name="nombre_cliente" id="nombre_cliente" disabled="" readonly value="{{ $negotiation->nombre_cliente }}">
                                        <div class="invalid-feedback">
                                            {{ $errors->first('nombre_cliente') }}
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 {{ $negotiation->tel_cliente ? '':'d-none' }} ">
                                    <label for="tel_cliente">Nro Telefónico</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" name="tel_cliente" id="tel_cliente" disabled="" readonly value="{{ $negotiation->tel_cliente }}">
                                        <div class="invalid-feedback">
                                            {{ $errors->first('tel_cliente') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="sistema_de_pago">Sistema de pago</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control" type="text" name="sistema_de_pago" id="sistema_de_pago" value="{{ $negotiation->sistema_de_pago }}" readonly="" disabled="">
                                        <div class="invalid-feedback">
                                            {{ $errors->first('sistema_de_pago') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="monto_mediacion">Monto de la mediación</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control only-number" type="number" name="monto_mediacion" id="monto_mediacion" min="0" value="{{ $negotiation->monto_mediacion }}" disabled readonly>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('monto_mediacion') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="id_asesor">Asesor</label>
                                    <div class="form-floating mb-3">
                                        <select class="form-control {{ $errors->has('id_asesor') ? 'is-invalid' : '' }}" name="id_asesor" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" disabled="" readonly>
                                            @foreach($advisers as $adviser)
                                                <option value="{{$adviser->id}}" {{ $negotiation->id_asesor == $adviser->id ?'selected':'' }}>{{$adviser->nombres_apellidos}} - {{$adviser->cedula}}</option>
                                            @endforeach
                                        </select>
                                        <div class="invalid-feedback">
                                            {{ $errors->first('id_asesor') }}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <label for="porcentaje_honorarios">Porcentaje de honorarios profesionales</label>
                                    <div class="form-floating mb-3">
                                        <input class="form-control only-number" type="number" name="porcentaje_honorarios" id="porcentaje_honorarios" min="0" max="100" value="{{ $negotiation->porcentaje_honorarios }}" readonly="" disabled="">
                                        <div class="invalid-feedback">
                                            {{ $errors->first('porcentaje_honorarios') }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });
    </script>
@endsection