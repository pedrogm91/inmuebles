@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Perfiles | Habita Inmueble</h6>
                </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Nombre
                                </th>
                                <th scope="col" class="">
                                    Acciones
                                </th>
                                <th scope="col" class="">
                                    Ficha de Perfil
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($profiles as $profile)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        {{ $profile->first_name}} {{ $profile->surname }}
                                    </th>
                                    <td class="d-flex gap-2">
                                        <a href="{{ route('profiles.show', [$profile->id]) }}" class="btn btn-sm btn-success" data-bs-toggle="tooltip" data-bs-placement="top" title="Ver ficha">
                                            <i class="bi bi-person-lines-fill"></i>
                                        </a>
                                        <a href="{{ route('profiles.edit', [$profile->id]) }}" class="btn btn-sm btn-primary" data-bs-toggle="tooltip" data-bs-placement="top" title="Editar">
                                            <i class="bi bi-pencil-square"></i>
                                        </a>
                                        <form action="{{ route('profiles.destroy', [$profile->id]) }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-sm btn-danger" type="submit" data-bs-toggle="tooltip" data-bs-placement="top" title="Eliminar">
                                                <i class="bi bi-dash-lg"></i>
                                            </button>
                                        </form>
                                    </td>
                                    <td class="">
                                        
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $profiles->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
    