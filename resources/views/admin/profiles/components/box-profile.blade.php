<div class="row text-center">
    <div class="col">
        <h6 class="mb-1">{{ $profile->properties->count() }}</h6>
        <p class="mb-0">Propiedades</p>
    </div>
    <div class="col border-start">
        <h6 class="mb-1">{{ $profile->percentage }}%</h6>
        <p class="mb-0">Porcentaje</p>
    </div>
    <div class="col border-start">
        <h6 class="mb-1">{{ $profile->type_contract }}</h6>
        <p class="mb-0">Tipo de contrato</p>
    </div>
</div>