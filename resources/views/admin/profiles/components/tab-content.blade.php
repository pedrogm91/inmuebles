<div class="tab-content" id="user-set-tabContent">
    <div class="tab-pane fade active show" id="user-set-profile" role="tabpanel" aria-labelledby="user-set-profile-tab">
        <div class="card">
            <div class="card-header">
                <h5>
                    <i class="bi bi-person-fill"></i> <span class="p-l-5">Datos adicionales</span>
                </h5>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col">

                    </div>
                    
                </div>
                <div class="card border-primary mb-3 w-50">
                    <div class="card-header">Dirección Casa</div>
                    <div class="card-body text-primary">
                      <p class="card-text">{{ $profile->house_address }}</p>
                    </div>
                </div>
                <div class="card border-primary mb-3 w-50">
                    <div class="card-header">Dirección Trabajo</div>
                    <div class="card-body text-primary">
                      <p class="card-text">{{ $profile->work_address }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="user-set-information" role="tabpanel" aria-labelledby="user-set-information-tab">
        <div class="card">
            
            id user-set-information
                
        </div>
    </div>
    <div class="tab-pane fade" id="user-set-account" role="tabpanel" aria-labelledby="user-set-account-tab">
        <div class="card">
            id user-set-account
        </div>
    </div>
    <div class="tab-pane fade" id="user-set-passwort" role="tabpanel" aria-labelledby="user-set-passwort-tab">
        
    </div>
    <div class="tab-pane fade" id="user-set-email" role="tabpanel" aria-labelledby="user-set-email-tab">
        <div class="card">
            id user-set-email
        </div>
    </div>
</div>