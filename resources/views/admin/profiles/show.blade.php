@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <!-- [ sample-page ] start -->
        <div class="col-lg-4">
            <div class="card user-card user-card-1">

                {{-- info primary --}}
                <div class="card-body pb-0">
                    <div class="float-end">
                        <span class="badge bg-primary">Pro</span>
                    </div>
                    <div class="media user-about-block d-flex align-items-center mt-0 mb-3">
                        <div class="d-inline-block">
                            <img class="img-radius img-fluid" width="80" src="https://picsum.photos/200" alt="User image">
                            <div class="certificated-badge">
                                <i class="fas fa-certificate text-primary bg-icon"></i>
                                <i class="fas fa-check front-icon text-white"></i>
                            </div>
                        </div>
                        <div class="media-body ms-3">
                            <h6 class="mb-1">{{ $profile->first_name }}  {{ $profile->surname }}</h6>
                            <p class="mb-0 text-muted">DNI {{ $profile->dni }}</p>
                        </div>
                    </div>
                </div>

                {{-- info basic --}}
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-mail m-r-10"></i>Nombres</span>
                        <span class="float-end text-body">{{ $profile->first_name }}  {{ $profile->second_name }}</span>
                    </li>
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-mail m-r-10"></i>Apellidos</span>
                        <span class="float-end text-body">{{ $profile->surname }}  {{ $profile->second_surname }}</span>
                    </li>
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-mail m-r-10"></i>Email</span>
                        <a href="mailto:{{$profile->email}}" class="float-end text-body">{{$profile->email}}</a>
                    </li>
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-phone-call m-r-10"></i>Teléfono 1</span>
                        <a href="tel:{{$profile->phone}}" class="float-end text-body">{{$profile->phone}}</a>
                    </li>
                    <li class="list-group-item">
                        <span class="f-w-500"><i class="feather icon-phone-call m-r-10"></i>Teléfono 2</span>
                        <a href="tel:{{$profile->second_phone}}" class="float-end text-body">{{$profile->second_phone}}</a>
                    </li>
                    <li class="list-group-item border-bottom-0">
                        <span class="f-w-500"><i class="feather icon-map-pin m-r-10"></i>Fecha de nacimiento</span>
                        <span class="float-end">{{$profile->date_birth}}</span>
                    </li>
                </ul>
                {{-- box profile --}}
                <div class="card-body">
                    @include('admin.profiles.components.box-profile')
                </div>
                
                {{-- button tabs --}}
                <div class="nav flex-column nav-pills list-group list-group-flush list-pills" id="user-set-tab" role="tablist" aria-orientation="vertical">
                    <a class="nav-link list-group-item list-group-item-action active" id="user-set-profile-tab" data-bs-toggle="pill" href="#user-set-profile" role="tab" aria-controls="user-set-profile" aria-selected="true">
                        <span class="f-w-500"><i class="feather icon-user m-r-10 h5 "></i>Información adicional</span>
                        <span class="float-end"><i class="feather icon-chevron-right"></i></span>
                    </a>
                    <a class="nav-link list-group-item list-group-item-action" id="user-set-information-tab" data-bs-toggle="pill" href="#user-set-information" role="tab" aria-controls="user-set-information" aria-selected="false">
                        <span class="f-w-500"><i class="feather icon-file-text m-r-10 h5 "></i>Personal Information</span>
                        <span class="float-end"><i class="feather icon-chevron-right"></i></span>
                    </a>
                    <a class="nav-link list-group-item list-group-item-action" id="user-set-account-tab" data-bs-toggle="pill" href="#user-set-account" role="tab" aria-controls="user-set-account" aria-selected="false">
                        <span class="f-w-500"><i class="feather icon-book m-r-10 h5 "></i>Account Information</span>
                        <span class="float-end"><i class="feather icon-chevron-right"></i></span>
                    </a>
                    <a class="nav-link list-group-item list-group-item-action" id="user-set-passwort-tab" data-bs-toggle="pill" href="#user-set-passwort" role="tab" aria-controls="user-set-passwort" aria-selected="false">
                        <span class="f-w-500"><i class="feather icon-shield m-r-10 h5 "></i>Change Password</span>
                        <span class="float-end"><i class="feather icon-chevron-right"></i></span>
                    </a>
                    <a class="nav-link list-group-item list-group-item-action" id="user-set-email-tab" data-bs-toggle="pill" href="#user-set-email" role="tab" aria-controls="user-set-email" aria-selected="false">
                        <span class="f-w-500"><i class="feather icon-mail m-r-10 h5 "></i>Email settings</span>
                        <span class="float-end"><i class="feather icon-chevron-right"></i></span>
                    </a>
                </div>

            </div>
            {{-- footer content profile --}}
            <div class="card">
                
                <div class="card-body">
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit sed do eiusmod tempor incididunt
                    </p>
                </div>
            </div>
        </div>

        {{-- tab content --}}
        <div class="col-lg-8">
            @include('admin.profiles.components.tab-content')
        </div>
        <!-- [ sample-page ] end -->
    </div>
</div>
@endsection