@extends('layouts.dashboard')

@section('style')

@endsection

@section('content')
<script>
    $('.only-number').on('input', function () {
        this.value = this.value.replace(/[^0-9]/g,'');
    });

    $('.only-letter').on('input', function () {
        this.value = this.value.replace(/[^a-zA-Z ]/g,'');
    });
</script>
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Editar Estado</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="/states/{{$state->id}}" method="POST" class="needs-validation">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-4">
                                        <label for="nombre">Nombre del Estado</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="nombre" class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" placeholder="" value="{{ old('nombre') }}{{ $state->nombre }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('nombre') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="nombre">Capital del Estado</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="capital" class="form-control {{ $errors->has('capital') ? 'is-invalid' : '' }}" placeholder="" value="{{ old('capital') }}{{ $state->capital }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('capital') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('states.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>

                                <div class="">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
   
@endsection