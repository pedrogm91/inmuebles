@extends('layouts.dashboard')

@section('style')
    <style>

    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Vacantes</h6>
                    <div class="float-right">
                        <a href="{{ route('jobs.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Vacante</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="row">
                            <div class="col-sm-5">
                                <div class="form-floating mb-3">
                                    <input type="text" name="search" class="form-control form-control-sm" id="search" placeholder="Cargo">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating mb-3">
                                    <select class="form-control form-control-sm" name="id_area" id="id_area" aria-label="floating label select example" title="Área" data-live-search="true">
                                        @foreach($areas as $area)
                                        <option value="{{$area->id}}">{{$area->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="form-floating mb-3">
                                    <select class="form-control form-control-sm" name="id_puesto" id="id_puesto" aria-label="floating label select example" title="Tipo de Puesto" data-live-search="true">
                                        @foreach($puestos as $puesto)
                                        <option value="{{$puesto->id}}">{{$puesto->nombre}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-1">
                                <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-fw fa-search"></i>
                                    </span>
                                </button>
                            </div>

                            <div class="col-12">
                                <hr>
                            </div>

                            <div class="col-12 text-right">

                            </div>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Cargo
                                </th>
                                <th scope="col" class="">
                                Área
                                </th>
                                <th scope="col" class="">
                                Puesto
                                </th>
                                <th scope="col" class="">
                                Publicado
                                </th>
                                <th scope="col" class="">
                                    Acciones
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($jobs as $job)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td scope="row" class="">
                                        {{$job->cargo}}
                                    </td>
                                    <td scope="row" class="">
                                    	{{$job->area->nombre}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$job->puesto->nombre}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$job->publicado}}
                                    </td>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}
                                        <a href="{{ route('jobs.show', [$job->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver"><i class="fas fa-eye"></i></a>
                                        {{-- edit --}}
                                        <a href="{{ route('jobs.edit', [$job->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        @if($job->isUsed() == 0)
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                        <form action="{{ route('jobs.destroy', [$job->id]) }}" method="post" id="eliminar-{{$job->id}}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                        <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="confirmar('eliminar-{{$job->id}}', '¿Desea eliminar esta vacante?', 'Se borrara de forma permanente');"><i class="fas fa-trash"></i></button>
                                        @endif
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $jobs->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('scripts')
    <script>

    </script>
@endsection