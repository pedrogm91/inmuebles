@extends('layouts.dashboard')

@section('style')
    <style>
        .btn:not(:disabled):not(.disabled) {
            border: solid #d1d3e2 1px;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Edición de Vacante</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="/jobs/{{$job->id}}" method="POST" class="needs-validation">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-md-4">
                                        <label for="cargo">Cargo <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="cargo" class="form-control {{ $errors->has('cargo') ? 'is-invalid' : '' }}" value="{{old('cargo')?old('cargo'):$job->cargo}}" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('cargo') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="id_area">Área <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('id_area') ? 'is-invalid' : '' }}" name="id_area" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                @foreach($areas as $area)
                                                    <option value="{{$area->id}}" {{ old('id_area')==$area->id?'selected':'' }} {{ $job->id_area==$area->id?'selected':'' }}>{{$area->nombre}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('id_area') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="id_puesto">Tipo de Puesto <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('id_puesto') ? 'is-invalid' : '' }}" name="id_puesto" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                @foreach($puestos as $puesto)
                                                    <option value="{{$puesto->id}}" {{ old('id_puesto')==$puesto->id?'selected':'' }} {{ $job->id_puesto==$puesto->id?'selected':'' }}>{{$puesto->nombre}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('id_puesto') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <label for="id_estado">Estado <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('id_estado') ? 'is-invalid' : '' }}" name="id_estado" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                @foreach($states as $state)
                                                    <option value="{{$state->id}}" {{ old('id_estado')==$state->id?'selected':'' }} {{ $job->id_estado==$state->id?'selected':'' }}>{{$state->nombre}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('id_estado') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="salario">Salario <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="salario" class="form-control {{ $errors->has('salario') ? 'is-invalid' : '' }} only-number" value="{{old('salario')?old('salario'):$job->salario}}" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('salario') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label for="publicado">¿Publicar? <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('publicado') ? 'is-invalid' : '' }}" name="publicado" aria-label="floating label select example" title="Seleccione" required>
                                                <option value="Si" {{ old('publicado')=='Si'?'selected':'' }} {{ $job->publicado=='Si'?'selected':'' }}>Si</option>
                                                <option value="No" {{ old('publicado')=='No'?'selected':'' }} {{ $job->publicado=='No'?'selected':'' }}>No</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('publicado') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-6">
                                        <label for="brief">Brief <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <textarea rows="5" required id="brief" name="brief" class="form-control">{{old('brief')?old('brief'):$job->brief}}</textarea>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('brief') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="describelo">Descripción del Cargo <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <textarea rows="5" required id="describelo" name="describelo" class="form-control">{{old('describelo')?old('describelo'):$job->describelo}}</textarea>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('describelo') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="requisitos">Requisitos <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <textarea rows="5" required id="requisitos" name="requisitos" class="form-control">{{old('requisitos')?old('requisitos'):$job->requisitos}}</textarea>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('requisitos') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="ofrecemos">¿Qué ofrecemos? <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <textarea rows="5" required id="ofrecemos" name="ofrecemos" class="form-control">{{old('ofrecemos')?old('ofrecemos'):$job->ofrecemos}}</textarea>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('ofrecemos') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-12">
                                        <small>Los campos marcados con <span class="text-danger">*</span> son obligatorios</small>
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('jobs.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });
    </script>
@endsection