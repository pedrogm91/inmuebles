@extends('layouts.dashboard')

@section('style')
    <style>
        .nav-link {
            padding: 0.5rem 0.8rem;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-sm-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Registre los datos publicos del Asesor</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                                <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Info. Personal <span class="text-danger">*</span></a>
                                <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Info. Laboral <span class="text-danger">*</span></a>
                                <a class="nav-link" id="v-pills-image-tab" data-toggle="pill" href="#v-pills-image" role="tab" aria-controls="v-pills-image" aria-selected="false">Imagen Perfl <span class="text-danger">*</span></a>
                            </div>
                            <p class="mt-2">
                                Observación: debes llenar todos los campos requeridos <i class="text-danger">(*)</i> para poder registrar los datos del asesor.
                            </p>
                        </div>

                        <div class="col-sm-10">
                            <form action="/advisers" method="POST" class="needs-validation" enctype="multipart/form-data">
                                @csrf
                                <div class="tab-content" id="v-pills-tabContent">
                                    <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                        <div class="row">
                                            <div class="col-sm-3 {{ isset($user) ? 'd-none':'' }}">
                                                <label for="id_usuario">Usuario <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control selectpicker" name="id_usuario" id="id_usuario" title="Seleccione" required="">
                                                        @foreach($users as $usr)
                                                            @if(isset($user))
                                                                <option value="{{$usr->id}}" {{ $user->id == $usr->id ? 'selected' : '' }}>{{$usr->name}}</option>
                                                            @else
                                                                <option value="{{$usr->id}}">{{$usr->name}}</option>
                                                            @endif
                                                        @endforeach
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('id_usuario') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-9">
                                                <label for="nombres_apellidos">Nombres y Apellidos <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="nombres_apellidos" class="form-control {{ $errors->has('nombres_apellidos') ? 'is-invalid' : '' }}" value="{{ isset($user) ? $user->name:'' }}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('nombres_apellidos') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="cedula">Cédula de Identidad <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="cedula" class="form-control {{ $errors->has('cedula') ? 'is-invalid' : '' }} only-number" value="{{old('cedula')}}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('cedula') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="telefono">Teléfono <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="telefono" class="form-control {{ $errors->has('telefono') ? 'is-invalid' : '' }} only-number" value="{{old('telefono')}}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('telefono') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="telefono_movil">Celular <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="telefono_movil" class="form-control {{ $errors->has('telefono_movil') ? 'is-invalid' : '' }} only-number" value="{{old('telefono_movil')}}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('telefono_movil') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="correo">Correo Electronico <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="email" name="correo" class="form-control {{ $errors->has('correo') ? 'is-invalid' : '' }}" value="{{ isset($user) ? $user->email : old('correo') }}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('correo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="fecha_nacimiento">Fecha de Nacimiento</label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="fecha_nacimiento" required class="form-control {{ $errors->has('fecha_nacimiento') ? 'is-invalid' : '' }} datepicker" value="{{old('fecha_nacimiento')}}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('fecha_nacimiento') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-12">
                                                <label for="direccion">Dirección <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="direccion" class="form-control {{ $errors->has('direccion') ? 'is-invalid' : '' }}" value="{{old('direccion')}}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('direccion') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                        <div class="row">
                                            <div class="col-sm-3">
                                                <label for="area_de_trabajo">Área de Trabajo <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('area_de_trabajo') ? 'is-invalid' : '' }}" name="area_de_trabajo"  aria-label="Floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                        <option value="1" {{old('area_de_trabajo')==1?'selected':''}}>Administración</option>
                                                        <option value="2" {{old('area_de_trabajo')==2?'selected':''}}>Mantenimiento</option>
                                                        <option value="3" {{old('area_de_trabajo')==3?'selected':''}}>Asesor Inmobiliario</option>
                                                        <option value="4" {{old('area_de_trabajo')==4?'selected':''}}>Asesor Legal</option>
                                                        <option value="5" {{old('area_de_trabajo')==5?'selected':''}}>Directora</option>
                                                        <option value="6" {{old('area_de_trabajo')==6?'selected':''}}>Gerente General</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('area_de_trabajo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="tipo_de_contrato">Tipo de Contrato <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('tipo_de_contrato') ? 'is-invalid' : '' }}" name="tipo_de_contrato"  aria-label="Floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                        <option value="1" {{old('tipo_de_contrato')==1?'selected':''}}>Fijo</option>
                                                        <option value="2" {{old('tipo_de_contrato')==2?'selected':''}}>Por Contrato</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('tipo_de_contrato') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="vehiculo">Vehículo <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('vehiculo') ? 'is-invalid' : '' }}" name="vehiculo"  aria-label="Floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                        <option value="1" {{old('vehiculo')==1?'selected':''}}>Si</option>
                                                        <option value="0" {{old('vehiculo')==0?'selected':''}}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('vehiculo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="lugar_de_trabajo">Lugar de Trabajo <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="lugar_de_trabajo" class="form-control {{ $errors->has('lugar_de_trabajo') ? 'is-invalid' : '' }}" value="{{ old('lugar_de_trabajo') }}" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('lugar_de_trabajo') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="porcentaje_ganancia">Porcentaje de Ganancia <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="porcentaje_ganancia" class="form-control {{ $errors->has('porcentaje_ganancia') ? 'is-invalid' : '' }} only-number" value="{{ old('porcentaje_ganancia') }}" min="0" max="100" required maxlength="2">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('porcentaje_ganancia') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="total_percibido">Total Percibido <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="text" name="total_percibido" class="form-control {{ $errors->has('total_percibido') ? 'is-invalid' : '' }} only-number" value="{{ old('total_percibido') }}" min="0" required>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('total_percibido') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="recomendado">Recomendado <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('recomendado') ? 'is-invalid' : '' }}" name="recomendado"  aria-label="Floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                        <option value="1"{{old('recomendado')==1?'selected':''}}>Si</option>
                                                        <option value="0"{{old('recomendado')==0?'selected':''}}>No</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('recomendado') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="estatus">Estatus <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('estatus') ? 'is-invalid' : '' }}" name="estatus"  aria-label="Floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                        <option value="activo" {{old('estatus')=='activo'?'selected':''}}>Activo</option>
                                                        <option value="suspendido" {{old('estatus')=='suspendido'?'selected':''}}>Suspendido</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('estatus') }}
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-3">
                                                <label for="experiencia">Experiencia <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <select class="form-control {{ $errors->has('experiencia') ? 'is-invalid' : '' }}" name="experiencia"  aria-label="Floating label select example" title="Seleccione" data-live-search="true" data-size="6" required>
                                                        <option value="1 a 3" {{old('experiencia')=='1 a 3'?'selected':''}}>1 a 3 años</option>
                                                        <option value="4 a 7" {{old('experiencia')=='4 a 7'?'selected':''}}>4 a 7 años</option>
                                                        <option value="8 a 10" {{old('experiencia')=='8 a 10'?'selected':''}}>8 a 10 años</option>
                                                        <option value="Mas 10" {{old('experiencia')=='Mas 10'?'selected':''}}>Más de 10 años</option>
                                                    </select>
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('experiencia') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="v-pills-image" role="tabpanel" aria-labelledby="v-pills-image-tab">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <label for="imagen_perfil">Imagen de Perfil <span class="text-danger">*</span></label>
                                                <div class="form-floating mb-3">
                                                    <input type="file" name="imagen_perfil" id="imagen_perfil" class="dropify" accept="image/png, image/gif, image/jpeg, image/jpg" data-max-file-size="2M" data-allowed-file-extensions="png jpg jpeg gif" data-default-file="{{ old('imagen_perfil') }}">
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('imagen_perfil') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <small>Los campos marcados con <span class="text-danger">*</span> son obligatorios</small>
                            </div>

                            <div class="col-sm-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-fw fa-save"></i>
                                    </span>
                                    <span class="text">Guardar Registro</span>
                                </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });

        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz clic',
                'replace': 'Arrastra y suelta o haz clic para reemplazar',
                'remove': 'Eliminar',
                'error': 'Vaya, sucedió algo malo.'
            },
            error: {
                'fileSize': 'El tamaño del archivo es demasiado grande.',
                'minWidth': 'El ancho de la imagen es demasiado pequeño. ',
                'maxWidth': 'El ancho de la imagen es demasiado grande.',
                'minHeight': 'La altura de la imagen es demasiado pequeña',
                'maxHeight': 'La altura de la imagen es demasiado grande',
            }
        });

        $(document).on('change','input[type="file"]',function(){
            var fileName = this.files[0].name;
            var fileSize = this.files[0].size;

            if(fileSize > 2000000){
                this.value = '';
                Swal.fire({
                    title: 'La documentación adjuntada no puede exceder 2MB',
                    text: 'Intente nuevamente',
                    icon: 'error',
                    showCancelButton: false,
                    showConfirmButton: false,
                    cancelButtonColor: '#d33',
                    cancelButtonText: 'Cancelar',
                    timer: 10000
                });
            }else{
                var ext = fileName.split('.').pop();
                switch (ext) {
                    case 'jpg':
                    case 'png':
                    case 'gif':
                    case 'JPG':
                    case 'PNG':
                    case 'GIF':
                        break;
                    default:
                        this.value = '';
                        Swal.fire({
                            title: 'La documentación adjuntada debe poseer una extensión apropiada. Sólo se aceptan archivos jpg, png o gif',
                            text: 'Intente nuevamente',
                            icon: 'error',
                            showCancelButton: false,
                            showConfirmButton: false,
                            cancelButtonColor: '#d33',
                            cancelButtonText: 'Cancelar',
                            timer: 10000
                        });
                }
            }
        });
    </script>
@endsection