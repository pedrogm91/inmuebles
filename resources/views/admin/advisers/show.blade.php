@extends('layouts.dashboard')

@section('style')
    <style>
    	.list-group-item {
    		padding: 0.25rem 1.25rem;
    		border: 0;
    	}
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-lg-9">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary text-center align-items-center">Datos del Asesor</h6>
                </div>
                <div class="card-body">
                	<ul class="list-group list-group-flush">
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Nombre</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->nombres_apellidos}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Cédula de Identidad</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->cedula}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Fecha de Nacimiento</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->fecha_nacimiento}}</div>
                			</div>
                		</li>

                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Teléfono</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->telefono}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Teléfono Celular</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->telefono_movil}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Correo</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->correo}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Dirección</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->direccion}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Área de Trabajo</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->area_trabajo()}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Tipo de Contrato</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->tipo_de_contrato==1?'Fijo':'Por Contrato'}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Vehículo</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->vehiculo==1?'Si':'No'}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Lugar de Trabajo</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->lugar_de_trabajo}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Porcentaje de Ganancia</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->porcentaje_ganancia}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Total Percibido</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->total_percibido}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Recomendado</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->recomendado==1?'Si':'No'}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Experiencia</div>
                				<div class="col-sm-8 text-secondary">{{$asesor->experiencia}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-4 font-weight-bold">Estatus</div>
                				<div class="col-sm-8 text-secondary">{{ucfirst($asesor->estatus)}}</div>
                			</div>
                		</li>
                		<li class="list-group-item">
                			<div class="row">
                				<div class="col-sm-12 text-center">
                					<a href="/advisers/{{$asesor->id}}/edit" class="btn btn-primary btn-icon-split btn-sm mt-2">
                						<span class="icon text-white-50">
                							<i class="fas fa-fw fa-edit"></i>
                						</span>
                						<span class="text">Editar Datos del Asesor</span>
                					</a>
                				</div>
                			</div>
                		</li>
                	</ul>
                	<div class="row text-right">
                		<div class="col-sm-8">

                		</div>
                	</div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary text-center align-items-center">Perfil Asesor</h6>
                </div>
                <div class="card-body text-center">
                	<img src="{{ asset('storage/'.$asesor->imagen_perfil) }}" alt="thumbnail" class="img-thumbnail">
                </div>
            </div>
            <div class="card shadow mb-4">
            	<a href="#collapsePropiedades" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapsePropiedades">
            		<h6 class="m-0 font-weight-bold text-primary">Propiedades</h6>
            	</a>
            	<div class="collapse" id="collapsePropiedades">
            		<div class="card-body p-0">
                        <ul class="list-group">
                            @foreach($asesor->propiedades as $property)
                            <li class="list-group-item px-2">
                                <a href="{{ route('properties.show', [$property->id]) }}">
                                    {{$property->nombre_propiedad}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
            		</div>
            	</div>
            </div>
            <div class="card shadow mb-4">
            	<a href="#collapseCitas" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCitas">
            		<h6 class="m-0 font-weight-bold text-primary">Citas</h6>
            	</a>
            	<div class="collapse" id="collapseCitas">
            		<div class="card-body p-0">
                        <ul class="list-group">
                            @foreach($asesor->citas as $cita)
                            <li class="list-group-item px-2">
                                <a href="{{ route('meetings.show', [$cita->id]) }}">
                                    {{$cita->asunto}}
                                </a>
                            </li>
                            @endforeach
                        </ul>
            		</div>
            	</div>
            </div>
        </div>
    </div>
@endsection