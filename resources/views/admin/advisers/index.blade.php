@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Lista de Asesores</h6>
                    <div class="float-right">
                        <a href="{{ route('advisers.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Asesor</span>
                        </a>
                    </div>

                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Nombre
                                </th>
                                <th scope="col" class="">
                                Área
                                </th>
                                <th scope="col" class="">
                                Vehículo
                                </th>
                                <th scope="col" class="">
                                Estado
                                </th>
                                <th scope="col" class="">
                                    <span class="sr-only">Acciones</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($asesores as $asesor)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        <a href="/advisers/{{$asesor->id}}">{{$asesor->nombres_apellidos}}</a>
                                    </th>
                                    <td class="">
                                    {{$asesor->area_trabajo()}}
                                    </td>
                                    <td class="">
                                    {{ $asesor->vehiculo == 0 ? "No":"Si" }}
                                    </td>
                                    <td class="">
                                    {{ucfirst($asesor->estatus)}}
                                    </td>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}
                                        <a href="{{ route('advisers.show', [$asesor->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Asesor"><i class="fas fa-eye"></i></a>
                                        {{-- edit --}}
                                        <a href="{{ route('advisers.edit', [$asesor->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Asesor"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        {{-- @if(Auth::user()->role=='admin') --}}
                                        <form action="{{ route('advisers.destroy', [$asesor->id]) }}" method="post" id="eliminar-{{$asesor->id}}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                        <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="confirmar('eliminar-{{$asesor->id}}', '¿Desea eliminar este asesor?', 'Se borrara de forma permanente');"><i class="fas fa-fw fa-trash"></i></button>
                                        {{-- @endif --}}
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $asesores->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection