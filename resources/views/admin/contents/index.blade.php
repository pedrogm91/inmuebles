@extends('layouts.dashboard')

@section('style')
    <style>
        a:hover {
            text-decoration: none;
        }
        .border-left-primary:hover{
            border: .15rem solid #4e73df!important;
        }
        .border-left-info:hover{
            border: .15rem solid #36b9cc!important;
        }
        .border-left-success:hover{
            border: .15rem solid #1cc88a!important;
        }
        .border-left-warning:hover{
            border: .15rem solid #f6c23e!important;
        }
    </style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Gestor de Contenido</h6>
                </div>
                <div class="card-body row">
                    <div class="col-md-3 mb-4">
                        <a href="{{ route('contents.index', 'company') }}">
                            <div class="card border-left-warning shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 font-weight-bold text-primary mb-1">
                                                Empresa</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="far fa-building fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 mb-4">
                        <a href="{{ route('contents.index', 'logos') }}">
                            <div class="card border-left-primary shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 font-weight-bold text-primary mb-1">
                                                Logos</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="far fa-image fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 mb-4">
                        <a href="{{ route('contents.index', 'banners') }}">
                            <div class="card border-left-info shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 font-weight-bold text-primary mb-1">
                                                Banners</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-photo-video fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col-md-3 mb-4">
                        <a href="{{ route('contents.index', 'menus') }}">
                            <div class="card border-left-success shadow h-100 py-2">
                                <div class="card-body">
                                    <div class="row no-gutters align-items-center">
                                        <div class="col mr-2">
                                            <div class="h5 font-weight-bold text-primary mb-1">
                                                Menús</div>
                                        </div>
                                        <div class="col-auto">
                                            <i class="fas fa-link fa-2x text-gray-300"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection