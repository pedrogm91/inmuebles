@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Menú {{ $menu->nombre }}</h6>
                    <div class="float-right">
                        <a href="{{ route('contents.index', 'menus') }}" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-arrow-circle-left"></i>
                            </span>
                            <span class="text">Listado de Menús</span>
                        </a>
                        <a href="{{ route('links.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Enlace</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                    <th scope="col" class="">
                                        Orden
                                    </th>
                                    <th scope="col" class="">
                                        Nombre
                                    </th>
                                    <th scope="col" class="">
                                        Ruta
                                    </th>
                                    <th scope="col" class="">
                                        Menú
                                    </th>
                                    <th scope="col" class="">
                                        Estado
                                    </th>
                                    <th scope="col" class="">
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($links as $link)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <td scope="row" class="">
                                        {{$link->ordering}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$link->nombre}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$link->route}}
                                    </td>
                                    <td scope="row" class="">
                                        {{$link->menu->nombre}}
                                    </td>
                                    <th scope="row" class="{{$link->status=='Habilitado'?'text-success':'text-danger'}}">
                                        {{$link->status}}
                                    </th>
                                    <td class="d-flex gap-2">
                                        {{-- edit --}}
                                        <a href="{{ route('links.edit', [$link->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        <form action="{{ route('links.destroy', [$link->id]) }}" method="post" id="eliminar-{{$link->id}}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                        <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="confirmar('eliminar-{{$link->id}}', '¿Desea eliminar este enlace?', 'Se borrara de forma permanente');"><i class="fas fa-trash"></i></button>
                                        </form>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{-- {{ $links->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

