@extends('layouts.dashboard')

@section('style')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Logo de la Empresa</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('logos.update') }}" method="POST" class="needs-validation" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="id" class="form-control" value="{{ $company->id }}">
                                <div class="row">
                                    <div class="col-6 offset-3">
                                        <label for="nombre">Logo de la Empresa <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="file" name="logo" class="dropify" accept="image/png, image/gif, image/jpeg, image/jpg" data-max-file-size="5M" data-allowed-file-extensions="png jpg jpeg gif" data-default-file="{{ $company->logo ? asset('storage/'.$company->logo) : '' }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('nombre') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('contents.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });

        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz clic',
                'replace': 'Arrastra y suelta o haz clic para reemplazar',
                'remove': 'Eliminar',
                'error': 'Vaya, sucedió algo malo.'
            },
            error: {
                'fileSize': 'El tamaño del archivo es demasiado grande.',
                'minWidth': 'El ancho de la imagen es demasiado pequeño. ',
                'maxWidth': 'El ancho de la imagen es demasiado grande.',
                'minHeight': 'La altura de la imagen es demasiado pequeña',
                'maxHeight': 'La altura de la imagen es demasiado grande',
            }
        });
    </script>
@endsection