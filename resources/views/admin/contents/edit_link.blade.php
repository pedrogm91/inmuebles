@extends('layouts.dashboard')

@section('style')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Editar Enlace</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('links.update', $link->id) }}" method="POST" class="needs-validation">
                                @csrf
                                @method('PUT')
                                <div class="row">
                                    <div class="col-4">
                                        <label for="id_menu">Menú Asociado <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('id_menu') ? 'is-invalid' : '' }}" name="id_menu"  aria-label="floating label select example" title="Seleccione" required>
                                                @foreach($menus as $men)
                                                    <option value="{{$men->id}}" {{ $link->id_menu == $men->id ? 'selected' : '' }}>{{$men->nombre}}</option>
                                                @endforeach
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('id_menu') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="nombre">Nombre del Enlace <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="nombre" class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" value="{{ $link->nombre }}" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('nombre') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="route">Ruta del Enlace <span class="text-danger">*</span>
                                            <span class="" data-toggle="tooltip" data-placement="top" title="http://google.com o perfil/">
                                                <i class="fas fa-question-circle"></i>
                                            </span>
                                        </label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="route" class="form-control {{ $errors->has('route') ? 'is-invalid' : '' }}" value="{{ $link->route }}" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('route') }}
                                            </div> 
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="ordering">Orden del Enlace <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="number" name="ordering" class="form-control {{ $errors->has('ordering') ? 'is-invalid' : '' }}" value="{{ $link->ordering }}" required min="1">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('ordering') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <label for="status">Estado <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status"  aria-label="floating label select example" title="Seleccione" required>
                                                <option value="Habilitado" {{ $link->status=='Habilitado'?'selected':''}}>Habilitado</option>
                                                <option value="Deshabilitado" {{ $link->status=='Deshabilitado'?'selected':''}}>Deshabilitado</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('status') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ url()->previous() }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });
    </script>
@endsection