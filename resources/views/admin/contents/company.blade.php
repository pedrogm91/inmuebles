@extends('layouts.dashboard')

@section('style')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Información de la Empresa</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('company.update') }}" method="POST" class="needs-validation">
                                @csrf
                                <input type="hidden" name="id" class="form-control" value="{{ $company->id }}">
                                <div class="row">
                                    <div class="col-6">
                                        <label for="nombre">Nombre de la Empresa <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="nombre" class="form-control {{ $errors->has('nombre') ? 'is-invalid' : '' }}" value="{{ old('nombre') ? old('nombre') : $company->nombre }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('nombre') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="rif">RIF</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="rif" class="form-control {{ $errors->has('rif') ? 'is-invalid' : '' }}" value="{{ old('rif') ? old('rif') : $company->rif }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('rif') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="correo">Correo</label>
                                        <div class="form-floating mb-3">
                                            <input type="email" name="correo" class="form-control {{ $errors->has('correo') ? 'is-invalid' : '' }}" value="{{ old('correo') ? old('correo') : $company->correo }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('correo') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="telefono">Teléfono</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="telefono" class="form-control {{ $errors->has('telefono') ? 'is-invalid' : '' }}" value="{{ old('telefono') ? old('telefono') : $company->telefono }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('telefono') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="celular">Celular</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="celular" class="form-control {{ $errors->has('celular') ? 'is-invalid' : '' }}" value="{{ old('celular') ? old('celular') : $company->celular }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('celular') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="fax">Fax</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="fax" class="form-control {{ $errors->has('fax') ? 'is-invalid' : '' }}" value="{{ old('fax') ? old('fax') : $company->fax }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('fax') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="horario">Horario</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="horario" class="form-control {{ $errors->has('horario') ? 'is-invalid' : '' }}" value="{{ old('horario') ? old('horario') : $company->horario }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('horario') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-9">
                                        <label for="direccion">Dirección</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="direccion" class="form-control {{ $errors->has('direccion') ? 'is-invalid' : '' }}" value="{{ old('direccion') ? old('direccion') : $company->direccion }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('direccion') }}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-3">
                                        <label for="whatsapp">Whatsapp</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="whatsapp" class="form-control {{ $errors->has('whatsapp') ? 'is-invalid' : '' }}" value="{{ old('whatsapp') ? old('whatsapp') : $company->whatsapp }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('whatsapp') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="twitter">Twitter</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="twitter" class="form-control" value="{{ old('twitter') ? old('twitter') : $company->twitter }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('twitter') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="facebook">Facebook</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="facebook" class="form-control" value="{{ old('facebook') ? old('facebook') : $company->facebook }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('facebook') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="instagram">Instagram</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="instagram" class="form-control" value="{{ old('instagram') ? old('instagram') : $company->instagram }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('instagram') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="youtube">YouTube</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="youtube" class="form-control" value="{{ old('youtube') ? old('youtube') : $company->youtube }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('youtube') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-3">
                                        <label for="linkedin">LinkedIn</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="linkedin" class="form-control" value="{{ old('linkedin') ? old('linkedin') : $company->linkedin }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('linkedin') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('contents.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });
    </script>
@endsection