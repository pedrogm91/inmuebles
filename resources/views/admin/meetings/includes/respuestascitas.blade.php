<div class="modal fade" id="respuestaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
    aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Mensajeria:</h5>
                <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body tex">

                <form action="/respuestasCitas" method="POST">
                    <input type="hidden" name="citas_id" value="{{$datos_cita->id}}">
                    <input type="hidden" name="asesores_inmobiliarios_id" value="{{$current_adviser}}">
                    @csrf
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="card">
                                <div class="card-header text-small bg-light">
                                    Último mensaje
                                </div>
                                    <ul id="list-messages" class="list-group list-group-flush overflow-auto" style="max-height: 340px">
                                        @foreach ($datos_cita->respuestasCitas as $respuesta)
                                        <li class="list-group-item">
                                            <label for="">Fecha: {{$respuesta['created_at']}}</label> <br>
                                            @if ($respuesta->asesores)
                                            Autor: <b>{{$respuesta->asesores->nombres_apellidos}}</b> <br>
                                            @endif
                                            Mensaje:  {{$respuesta['texto']}}
                                        </li>
                                        @endforeach
                                    </ul>
                                
                                <div class="card-body">
                                    @if ($current_adviser)
                                        <textarea name="texto" class="form-control mb-3" id="" cols="30" rows="5"></textarea>
                                        <button class="btn btn-primary btn-block" type="submit">Enviar Mensaje</button>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button class="btn btn-danger" type="button" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">Cancelar</span>
                </button>
            </div>
        </div>
    </div>
</div>