@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Citas Programadas</h6>
                    <div class="float-right">
                        <a href="{{route('meetings.create')}}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Cita</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive" style="font-size:14px">
                        <table class="table table-striped table-hover table-bordered text-center">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Fecha
                                </th>
                                <th>
                                    Hora
                                </th>
                                <th>
                                    Asunto
                                </th>
                                @if(auth()->user()->role=="admin"||auth()->user()->role=="director")
                                    <th scope="col" class="">
                                        Responsable
                                    </th>
                                @endif
                                <th scope="col" class="">
                                Estatus
                                </th>
                                <th scope="col" class="">
                                    Prioridad
                                </th>
                                <th scope="col" class="">
                                Acciones
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($citas_programadas as $cita)
                                <tr>
                                    <td @class(['text-danger' => $cita->fecha_cita < now(),])>
                                        {{ \Carbon\Carbon::parse($cita->fecha_cita)->format('d/m/y') }}
                                    </td>
                                    <td>
                                        {{$cita->hora_cita}}
                                    </td>
                                    <td class="text-capitalize">
                                        <b>{{$cita->asunto}}</b>
                                    </td>
                                    @if(auth()->user()->role=="admin"||auth()->user()->role=="director")
                                        <td>
                                            <a href="/advisers/{{$cita->id_asesor}} ">{{$cita->nombres_apellidos}}</a>
                                        </td>
                                    @endif

                                    <td class="text-capitalize">
                                        {{$cita->estatus}}
                                    </td>
                                    <td class="text-capitalize">
                                        <span @class([
                                            'badge',
                                            'badge-secondary' => $cita->prioridad == 'Baja',
                                            'badge badge-success' => $cita->prioridad == 'Media',
                                            'badge badge-danger' => $cita->prioridad == 'Alta',
                                        ])>{{$cita->prioridad}}</span>
                                    </td>
                                    <td class="d-flex justify-content-center">
                                        {{-- show --}}
                                        <a href="{{ route('meetings.show', [$cita->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Cita"><i class="fas fa-eye"></i></a>
                                        {{-- edit --}}
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director' || Auth::user()->id==$cita->id_asesor)
                                           <a href="{{ route('meetings.edit', [$cita->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Cita"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        <form action="{{ route('meetings.destroy', [$cita->id]) }}" method="post">
                                            @method('delete')
                                            @csrf
                                            <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Cita"><i class="fas fa-trash"></i></button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            {{ $citas_programadas->links() }}
        </div>
    </div>
@endsection
