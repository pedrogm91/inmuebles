@extends('layouts.dashboard')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-4 text-center">
                <div class="card">
                    <div class="card-header bg-light">Asesor Asignado</div>
                    <div class="card-body"> 
                        <img class="img-fluid" src="{{ Storage::url($datos_cita->asesor->imagen_perfil) }}" alt="">
                           
                    </div>
                    <div class="card-footer">
                        <a href="{{ route('advisers.show', $datos_cita->asesor->id)}}"> {{$datos_cita->asesor->nombres_apellidos}}</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-8 justify">
                <div class="card ">
                    <div class="card-header bg-light">
                        Datos Cita
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4">Fecha Cita</div>
                            <div class="col-xl-8">
                                {{\Carbon\Carbon::parse($datos_cita->fecha_cita)->format('d/m/y')}} a las {{$datos_cita->hora_cita}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">Prospecto</div>
                            <div class="col-xl-8">
                                {{$datos_cita->nombre_cliente}}

                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">
                                Teléfono:
                            </div>
                            <div class="col-xl-8">
                                {{$datos_cita->telefono_cliente}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">
                                Correo:
                            </div>
                            <div class="col-xl-8">
                                {{$datos_cita->correo_cliente}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">Propiedad Relacionada</div>
                            <div class="col-xl-8">
                                <a href="{{route('properties.show',[$datos_cita->property->id])}}">
                                    {{ $datos_cita->property->nombre_propiedad }}
                                </a>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">Asunto</div>
                            <div class="col-xl-8">
                                {{$datos_cita->asunto}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">Breve Descripcion</div>
                            <div class="col-xl-8">
                                {{$datos_cita->descripcion}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">Estatus</div>
                            <div class="col-xl-8">
                                {{$datos_cita->estatus}}
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-xl-4">Observaciones</div>
                            <div class="col-xl-8">
                                {{$datos_cita->observaciones}}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-12 text-center">
                            <div class="card-footer">
                                <a class="btn  mr-1  btn-info" href="#" data-toggle="modal" data-target="#respuestaModal">
                                    <i class="fas fa-comment-dots"></i> Comentarios
                                </a>
                                <a href="{{route('meetings.edit',[$datos_cita->id])}}" class="btn btn-success"><i class="fas fa-edit"></i> Editar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@include('admin.meetings.includes.respuestascitas')

@section('scripts')

@endsection