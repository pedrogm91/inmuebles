@extends('layouts.dashboard')
@section('content')

<div class='container'>
    <div class="row">
        <div class="col-xl-12 text-center">
            <form action="/meetings" method="POST" >
                @csrf
                <div class="card">
                    <div class="card-header bg-light">
                        Programar Cita
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6">
                                <label for="fecha_cita">Fecha:</label>
                                <input type="date" class="form-control" name='fecha_cita'>

                            </div>
                            <div class="col-xl-6">
                                <label for="hora_cita">Hora:</label>

                                <input type="time" name="hora_cita" class="form-control" >
                            </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-light">Datos del Cliente</li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4">
                                <label for="id_cliente">Nombre Completo</label>
                                <input type="text" name='nombre_cliente' class="form-control" value=''>
                                {{-- <select class="form-control selectpicker" name='id_cliente' data-live-search="true">
                                    @foreach ($clientes as $cliente)
                                        <option value="{{$cliente->id}}">{{$cliente->name}}</option>
                                    @endforeach
                                </select> --}}
                            </div>
                            <div class="col-xl-4">
                                <label for="telefono_cliente">Telefono:</label>
                                <input type="text" name='telefono_cliente' class="form-control" value=''>
                            </div>
                            <div class="col-xl-4">
                                <label for="correo_cliente">Correo: </label>
                                <input type="text" name='correo_cliente' class="form-control" value=''>
                            </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-light">Asesor y Propiedad</li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6"><label for="id_asesor">Asesor</label></div>
                            <div class="col-xl-6">
                                <select name="id_asesor" id="asesorSeleccionado" class="form-control selectpicker" data-live-search="true">
                                    @if (auth()->user()->role=="adviser")
                                    <option value="{{auth()->user()->asesoresInmobiliarios->id}}">{{auth()->user()->asesoresInmobiliarios->nombres_apellidos}}</option>
                                    @endif
                                    @if(auth()->user()->role=='admin' || auth()->user()->role=='director')
                                        @foreach ($asesores as $asesor)
                                        <option value="{{$asesor->id}}">{{$asesor->nombres_apellidos}}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6"><label for="propiedades">Propiedad</label></div>
                            <div class="col-xl-6">
                                <select name="id_propiedad" id="propiedades" class="form-control selectpicker" data-live-search="true">
                                    @foreach ($propiedades as $propiedad)
                                    <option value="{{$propiedad->id}}">{{$propiedad->nombre_propiedad}}</option>
                                @endforeach
                                </select>
                            </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-light">Detalles</li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4">
                                <label for="asunto">Asunto</label>
                                <input type="text" name="asunto" class="form-control">
                            </div>

                            <div class="col-xl-8">
                                <label for="descripcion">Descripcion Breve</label>
                                <input type="text" name="descripcion" class="form-control">
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6"><label for="id_asesor">Estatus</label>
                                <select name="estatus" id="" class="form-control">
                                    <option value="por confirmar">Por Confirmar</option>
                                    <option value="Confirmada">Confirmada</option>
                                    <option value="Cancelada">Cancelada</option>
                                </select>
                            </div>
                            <div class="col-lg-6"><label for="id_asesor">Prioridad</label>
                                <select name="prioridad" id="" class="form-control">
                                    <option value="Media">Media</option>
                                    <option value="Baja">Baja</option>
                                    <option value="Alta">Alta</option>
                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12"><label for="observaciones">Observaciones</label>
                                <textarea name='observaciones' class="form-control" id="" cols="10" rows="3"></textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <button type="submit" class="btn btn-primary">Registrar Cita</button>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection
