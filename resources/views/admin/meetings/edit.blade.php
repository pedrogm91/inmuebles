@extends('layouts.dashboard')
@section('content')

<div class='container'>
    <div class="row">
        <div class="col-xl-12 text-center">
            <form action="/meetings/{{$datos_cita->id}}" method="POST" class="needs-validation" >
                @csrf
                @method('PUT')
                <div class="card">
                    <div class="card-header bg-light">
                        Editar datos de la Cita
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6">
                                <label for="fecha_cita">Fecha:</label>
                                <input type="date" name="fecha_cita" class="form-control {{ $errors->has('fecha_cita') ? 'is-invalid' : '' }}" 
                                value="{{ \Carbon\Carbon::parse($datos_cita->fecha_cita)->format('Y-m-d') }}">
                               
                                
                                <div class="invalid-feedback">
                                    {{ $errors->first('fecha_cita') }}
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <label for="hora_cita">Hora:</label>
                                <input type="text" name="hora_cita" class="form-control {{ $errors->has('hora_cita') ? 'is-invalid' : '' }}" value="{{ $datos_cita->hora_cita }}">
                                <div class="invalid-feedback">
                                    {{ $errors->first('hora_cita') }}
                                </div>
                            </div>
                        </div>
                    </div>
            
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-light">Datos del Cliente</li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4">
                                <label for="id_cliente">Nombre Completo</label>
                                <input type="text" name='nombre_cliente' class="form-control {{ $errors->has('nombre_cliente') ? 'is-invalid' : '' }}" value='{{ $datos_cita->nombre_cliente }}'>
                                <div class="invalid-feedback">
                                    {{ $errors->first('nombre_cliente') }}
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <label for="telefono_cliente">Telefono:</label>
                                <input type="text" name="telefono_cliente" class="form-control {{ $errors->has('telefono_cliente') ? 'is-invalid' : '' }}"  value="{{$datos_cita->telefono_cliente}}">
                                    <div class="invalid-feedback">
                                        {{ $errors->first('telefono_prospect') }}
                                    </div>
                            </div>
                            <div class="col-xl-4">
                                <label for="correo_cliente">Correo: </label>
                                <input type="text" name="correo_cliente" class="form-control {{ $errors->has('correo_cliente') ? 'is-invalid' : '' }}"  value="{{$datos_cita->correo_cliente}}">
                                <div class="invalid-feedback">
                                    {{ $errors->first('correo_cliente') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-light">Asesor y Propiedad</li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6"><label for="id_asesor">Asesor</label></div>
                            <div class="col-xl-6">
                                <select  title="Seleccione" data-live-search="true" data-size="5" name="id_asesor" id="asesorSeleccionado" class="form-control  selectpicker {{ $errors->has('id_asesor') ? 'is-invalid' : '' }}">
                                       
                                    @foreach ($datos_asesores as $asesor)
                                        @if ($datos_cita->id_asesor == $asesor->id)
                                        <option value="{{$asesor->id}}" selected>{{$asesor->nombres_apellidos}}</option>
                                        @endif
                                        <option value="{{$asesor->id}}">{{$asesor->nombres_apellidos}}</option>
                                    @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('id_asesor') }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6"><label for="propiedades">Propiedad</label></div>
                            <div class="col-xl-6">
                                <select  title="Seleccione" data-live-search="true" data-size="5" name="id_propiedad" id="propiedades" class="form-control  selectpicker {{ $errors->has('id_propiedad') ? 'is-invalid' : '' }}">
                                    @foreach ($datos_propiedades as $propiedad)
                                    @if ($datos_cita->id_propiedad == $propiedad->id )
                                    <option value="{{$propiedad->id}}" selected>{{$propiedad->nombre_propiedad}}</option>
                                    @endif
                                    <option value="{{$propiedad->id}}">{{$propiedad->nombre_propiedad}}</option>
                                @endforeach
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('id_propiedad') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <ul class="list-group list-group-flush">
                        <li class="list-group-item bg-light">Detalles</li>
                    </ul>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-4">
                                <label for="asunto">Asunto</label>
                                <input type="text" name="asunto" class="form-control {{ $errors->has('asunto') ? 'is-invalid' : '' }}"  value={{$datos_cita->asunto}}>
                                <div class="invalid-feedback">
                                    {{ $errors->first('asunto') }}
                                </div>
                            </div>
                        
                            <div class="col-xl-8">
                                <label for="descripcion">Descripcion Breve</label>
                                <input type="text" name="descripcion" class="form-control {{ $errors->has('descripcion') ? 'is-invalid' : '' }}"  value={{$datos_cita->descripcion}}>
                                <div class="invalid-feedback">
                                    {{ $errors->first('asunto') }}
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-lg-6"><label for="id_asesor">Estatus</label>
                                <select name="estatus" id="" class="form-control">
                                    <option value="por confirmar"> {{$datos_cita->estatus == "por confirmar" ? 'selected': ''}} Por Confirmar</option>
                                    <option value="Confirmada" {{$datos_cita->estatus == "por confirmar" ? 'selected': ''}} >Confirmada</option>
                                    <option value="Cancelada" {{$datos_cita->estatus == "por confirmar" ? 'selected': ''}} >Cancelada</option>
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('estatus') }}
                                </div>
                            </div>
                            <div class="col-lg-6"><label for="id_asesor">Prioridad</label>
                                <select name="prioridad" id="" class="form-control">
                                    <option value="Media" {{$datos_cita->estatus == "Media" ? 'selected': ''}} >Media</option>
                                    <option value="Baja" {{$datos_cita->estatus == "Baja" ? 'selected': ''}} >Baja</option>
                                    <option value="Alta" {{$datos_cita->estatus == "Alta" ? 'selected': ''}} >Alta</option>
                                </select>
                                <div class="invalid-feedback">
                                    {{ $errors->first('prioridad') }}
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12"><label for="observaciones">Observaciones</label>
                                <textarea name='observaciones' class="form-control" id="" cols="10" rows="3">{{$datos_cita->observaciones}}</textarea>
                            </div>
                        </div>
                    </div>

                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-12">
                                <button type="submit" class="btn btn-success">Actualizar Cita</button>
                            </div>
                        </div>
                    </div>       
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('scripts')

@endsection