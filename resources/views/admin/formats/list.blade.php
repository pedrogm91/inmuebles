@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Formatos</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        @foreach ($formats as $format)
                        <div class="col-sm-3">
                            <div class="card mb-4 border-dark">
                                <div class="card-body">
                                    <h6 class="font-weight-bold text-center">{{$format->name}}</h6>
                                    <a href="{{ asset('storage/'.$format->route) }}" target="_blank" class="btn btn-success btn-block btn-sm"><i class="fas fa-download"></i> Descargar</a>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

