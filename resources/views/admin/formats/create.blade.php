@extends('layouts.dashboard')

@section('style')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Carga de Formato</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('formats.store') }}" method="POST" class="needs-validation" enctype="multipart/form-data">
                                @csrf
                                <div class="row">
                                    <div class="col-6">
                                        <label for="name">Nombre del Formato <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' : '' }}" value="{{ old('name') }}" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('name') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-6">
                                        <label for="status">Estado <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <select class="form-control {{ $errors->has('status') ? 'is-invalid' : '' }}" name="status" title="Seleccione" required>
                                                <option value="Habilitado" {{old('status')=='Habilitado'?'selected':''}}>Habilitado</option>
                                                <option value="Deshabilitado" {{old('status')=='Deshabilitado'?'selected':''}}>Deshabilitado</option>
                                            </select>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('status') }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-6">
                                        <label for="route">Documento <span class="text-danger">*</span></label>
                                        <div class="form-floating mb-3">
                                            <input type="file" name="route" class="dropify" data-max-file-size="5M" data-allowed-file-extensions="*" data-default-file="{{ old('route') }}" data-show-remove="false" required>
                                            <div class="invalid-feedback">
                                                {{ $errors->first('route') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('formats.index') }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });

        $('.dropify').dropify({
            messages: {
                'default': 'Arrastra y suelta un archivo aquí o haz clic',
                'replace': 'Arrastra y suelta o haz clic para reemplazar',
                'remove': 'Eliminar',
                'error': 'Vaya, sucedió algo malo.'
            },
            error: {
                'fileSize': 'El tamaño del archivo es demasiado grande.',
                'minWidth': 'El ancho de la imagen es demasiado pequeño. ',
                'maxWidth': 'El ancho de la imagen es demasiado grande.',
                'minHeight': 'La altura de la imagen es demasiado pequeña',
                'maxHeight': 'La altura de la imagen es demasiado grande',
            }
        });
    </script>
@endsection