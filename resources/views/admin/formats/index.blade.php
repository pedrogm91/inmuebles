@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Formatos</h6>
                    <div class="float-right">
                        <a href="{{ route('formats.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Formato</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                    <th scope="col" class="">
                                        Nombre
                                    </th>
                                    <th scope="col" class="">
                                        Estado
                                    </th>
                                    <th scope="col" class="">
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($formats as $format)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        {{$format->name}}
                                    </th>
                                    <th scope="row" class="{{$format->status=='Habilitado'?'text-success':'text-danger'}}">
                                        {{$format->status}}
                                    </th>
                                    <td class="d-flex gap-2">
                                        {{-- download --}}
                                        <a href="{{ asset('storage/'.$format->route) }}" target="_blank" class="btn btn-sm mr-1 btn-circle btn-success" data-toggle="tooltip" data-placement="top" title="Descargar"><i class="fas fa-download"></i></a>
                                        @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                        {{-- edit --}}
                                        <a href="{{ route('formats.edit', [$format->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        <form action="{{ route('formats.destroy', [$format->id]) }}" method="post" id="eliminar-{{$format->id}}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                        <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="confirmar('eliminar-{{$format->id}}', '¿Desea eliminar este documento?', 'Se borrara de forma permanente');"><i class="fas fa-trash"></i></button>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{-- {{ $formats->links() }} --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

