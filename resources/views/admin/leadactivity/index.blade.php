@extends('layouts.dashboard')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Actividades de leads</h6>
                    <div class="float-right">
                        <a href="{{ route('leadactivity.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Registrar actividad</span>
                        </a>
                    </div>
    
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>
    
                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead >
                                <tr>
                                    <th>Cliente ID</th>
                                    <th>Nombres Completo</th>
                                    <th>Interese</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($leads as $lead)
                                    <tr>
                                        <td>{{$lead->prospects_id}}</td>
                                        <td>{{$lead->nombres_apellidos}}</td>
                                        <td>{{$lead->interes}}</td>
                                        <td class="d-flex gap-2">
                                            {{-- show --}}
                                            <a href="{{ route('leadactivity.show', [$lead->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver LEAD"><i class="fas fa-eye"></i></a>
                                            {{-- edit --}}
                                            <a href="{{ route('leadactivity.edit', [$lead->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar LEAD"><i class="fas fa-edit"></i></a>
                                            {{-- delete --}}
                                            @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                            <form action="{{ route('leadactivity.destroy', [$lead->id]) }}" method="post" id="eliminar-{{$lead->id}}">
                                                @method('delete')
                                                @csrf
                                            </form>
                                            <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar LEAD" onclick="confirmar('eliminar-{{$lead->id}}', '¿Desea eliminar este LEAD?', 'Se borrara de forma permanente');"><i class="fas fa-fw fa-trash"></i></button>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
    
                    <div>
                        {{ $leads->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection