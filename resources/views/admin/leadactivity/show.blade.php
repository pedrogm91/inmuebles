@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow md-4">
                    <div class="card-header py-3">
                        <h6 class="font-weight-bold text-primary float-left align-items-center">Detalles</h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-xl-6">
                                <label for="Telefono">Prospecto: </label>
                                <div class="form-floating mb-3">
                                    {{ $prospect->nombres_apellidos }}
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <label for="Correo">Interes: </label>
                                <div class="form-floating mb-3">
                                    {{ $leadactivity->interes }}
                                </div>
                            </div>
                            <div class="col-xl-12">
                                <label for="comen">Datos Adicionales</label>
                                <div class="form-floating mb-3">
                                    <textarea name="datos_adicionales" class="form-control" cols="10" rows="5" readonly>{{ $leadactivity->datos_adicionales }}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 text-right">
                                <hr>
                                <a href="{{ route('leadactivity.edit', $leadactivity->id) }}"
                                    class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50"><i class="fas fa-fw fa-save"></i></span>
                                    <span class="text">Actualizar registro</span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
