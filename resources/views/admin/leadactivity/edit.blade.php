@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow md-4">
                    <div class="card-header py-3">
                        <h6 class="font-weight-bold text-primary float-left align-items-center">Registro de actividad</h6>
                    </div>
                    <div class="card-body">
                        <form action="/leadactivity">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-xl-6">
                                    <label for="Telefono">Prospecto</label>
                                    <select title="Seleccione" data-live-search="true" data-size="5" name="prospects_id"
                                        id="prospectSelected"
                                        class="form-control  selectpicker {{ $errors->has('prospects_id') ? 'is-invalid' : '' }}">
                                        @foreach ($prospects as $prospect)
                                            @if ($leadactivity->prospects_id == $prospect->id)
                                                <option value="{{ $prospect->id }}" selected>
                                                    {{ $prospect->nombres_apellidos }}</option>
                                            @endif
                                            <option value="{{ $prospect->id }}">{{ $prospect->nombres_apellidos }}</option>
                                        @endforeach
                                    </select>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('prospects_id') }}
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                    <label for="Correo">Interes</label>
                                    <div class="form-floating mb-3">
                                        <select name="interes" id="interesSelected">
                                            <option  value="comprar" @if ($leadactivity->interes=='comprar')
                                                selected
                                            @endif>comprar</option>
                                            <option  value="vender" @if ($leadactivity->interes=='vender')
                                                selected
                                            @endif>vender</option>
                                            <option  value="alquilar" @if ($leadactivity->interes=='alquilar')
                                                selected
                                            @endif>alquilar</option>
                                            <option  value="cita" @if ($leadactivity->interes=='cita')
                                                selected
                                            @endif>cita</option>
                                            <option  value="contacto" @if ($leadactivity->interes=='contacto')
                                                selected
                                            @endif>contacto</option>
                                        </select>
                                    </div>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('interes') }}
                                    </div>
                                </div>
                                <div class="col-xl-12">
                                    <label for="comen">Datos Adicionales</label>
                                    <div class="form-floating mb-3">
                                        <textarea name="datos_adicionales" class="form-control" cols="10" rows="5">{{ $leadactivity->datos_adicionales }}</textarea>
                                    </div>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('datos_adicionales') }}
                                    </div>
                                </div>
                                <div class="col-sm-12 text-right">
                                    <hr>
                                    <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                        <span class="icon text-white-50"><i class="fas fa-fw fa-save"></i></span>
                                        <span class="text">Actualizar actividad</span>
                                    </button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
