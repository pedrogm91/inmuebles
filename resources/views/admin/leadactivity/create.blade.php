@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-12">
             <div class="card shadow md-4">
                 <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Registro de actividad</h6>
                 </div>
                 <div class="card-body">
                     <form action="/leadactivity" method="POST" >
                         @csrf
                         <div class="row">
                             <div class="col-xl-8">
                                 <label for="nombres_apellidos">Nombre Completo</label>
                                <div class="form-floating mb-3"> <input type="text" class="form-control" name="nombres_apellidos"></div>
                             </div>
                             <div class="col-xl-4">
                                <label for="Telefono">Telefono</label>
                                <div class="form-floating mb-3">
                                  <input type="text" class="form-control" name="telefono">
                                </div>
                            </div>
                             <div class="col-xl-4">
                                <label for="Correo">Correo</label>
                                <div class="form-floating mb-3">
                                  <input type="text" class="form-control" name="correo">
                                </div>
                            </div>
                            <div class="col-xl-4">
                                <label for="Correo">Interes</label>
                                <div class="form-floating mb-3">
                                  <input type="text" class="form-control" name="interes">
                                </div>
                            </div>
                             <div class="col-xl-12">
                                 <label for="comen">Datos Adicionales</label>
                                 <div class="form-floating mb-3">
                                    <textarea name="datos_adicionales" class="form-control" id="" cols="10" rows="5"></textarea>
                                 </div>
                             </div>
                             <div class="col-sm-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50"><i class="fas fa-fw fa-save"></i></span>
                                    <span class="text">Registrar</span>
                                </button>
                             </div>
                     </form>
                 </div>
             </div>
        </div>
    </div>
</div>
@endsection