@extends('layouts.dashboard')

@section('style')
<style>
    :not(.input-group)>.bootstrap-select.form-control:not([class*=col-]) {
        width: 25%;
        border: solid 1px #d8dbe4;
    }
</style>
@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">{{ $title }}</h6>
                    <div class="float-right">
                        <a href="{{ route('reports.profitability') }}" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-arrow-circle-left"></i>
                            </span>
                            <span class="text">Regresar</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive mt-4">
                        <table class="table table-striped table-hover table-bordered DataTable display nowrap" style="width:100%">
                            <thead class="bg-slate-50">
                                <tr>
                                    <th class="text-center">Nro.</th>
                                    <th>Asesor</th>
                                    <th>Título</th>
                                    <th class="text-center">Precio Publicado ($)</th>
                                    <th class="text-center">Precio Negociado ($)</th>
                                    <th class="text-center">% Honorarios profesionales</th>
                                    <th class="text-center">Honorarios profesionales ($)</th>
                                    <th class="text-center">% Comisión del asesor</th>
                                    <th class="text-center">Ganancia del asesor ($)</th>
                                    <th class="text-center">Monto Neto a la inmobiliaria ($)</th>
                                    <th class="text-center">Fecha</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php $i = 1; $t_publicado = 0; $t_negociado = 0; $t_honorarios = 0; @endphp
                                @foreach($reports as $report)
                                <tr>
                                    <td class="text-center">{{ $i }}</td>
                                    <td>{{ $report->adviser->nombres_apellidos }}</td>
                                    <td>{{ $report->property->category->nombre}} de {{ $report->property->datos_construccion["area_construccion"] }}m2. {{$report->property->location->nombre}}, Edo. {{$report->property->state->nombre}}</td>
                                    <td class="text-center">{{App\Models\Reports::Parsear($report->property->precio)}}</td>
                                    <td class="text-center">{{App\Models\Reports::Parsear($report->monto_mediacion)}}</td>
                                    <td class="text-center">{{$report->porcentaje_honorarios}}</td>
                                    <td class="text-center">{{App\Models\Reports::Parsear(($report->monto_mediacion*$report->porcentaje_honorarios)/100)}}</td>
                                    <td class="text-center">{{$report->adviser->porcentaje_ganancia}}</td>
                                    <td class="text-center">{{App\Models\Reports::Parsear(((($report->monto_mediacion*$report->porcentaje_honorarios)/100)*$report->adviser->porcentaje_ganancia)/100)}}</td>
                                    <td class="text-center">{{App\Models\Reports::Parsear(((((($report->monto_mediacion*$report->porcentaje_honorarios)/100)*$report->adviser->porcentaje_ganancia)/100)-($report->monto_mediacion*$report->porcentaje_honorarios)/100)*-1)}}</td>
                                    <td class="text-center">{{date('d-m-Y', strtotime($report->fecha_oferta))}}</td>
                                </tr>
                                @php $i++; $t_publicado += $report->property->precio; $t_negociado = $report->monto_mediacion; $t_honorarios += ($report->monto_mediacion*$report->porcentaje_honorarios)/100; @endphp
                                @endforeach
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th class="text-right">TOTALES</th>
                                    <th class="text-center">{{ App\Models\Reports::Parsear($t_publicado) }}</th>
                                    <th class="text-center">{{ App\Models\Reports::Parsear($t_negociado) }}</th>
                                    <th></th>
                                    <th class="text-center">{{ App\Models\Reports::Parsear($t_honorarios) }}</th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>

</script>
@endsection
