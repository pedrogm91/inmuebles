@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Reporte de Rentabilidad por Asesor</h6>
                </div>
                <div class="card-body">
                    <form action="" method="POST" class="needs-validation">
                        @csrf
                        <div class="row">
                            <div class="col-sm-3">
                                <label for="desde">Fecha desde</label>
                                <div class="form-floating mb-3">
                                    <input type="text" name="desde" class="form-control" id="desde" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="hasta">Fecha hasta</label>
                                <div class="form-floating mb-3">
                                    <input type="text" name="hasta" class="form-control" id="hasta" placeholder="">
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="tipo_negocio">Tipo de Negocio</label>
                                <div class="form-floating mb-3">
                                    <select class="form-control" name="tipo_negocio" id="tipo_negocio" aria-label="floating label select example" title="Seleccione" data-live-search="true" required>
                                        <option value="Venta">Venta</option>
                                        <option value="Alquiler">Alquiler</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <label for="asesor">Asesor</label>
                                <div class="form-floating mb-3">
                                    <select class="form-control" name="asesor" aria-label="floating label select example" title="Seleccione" data-live-search="true" data-size="5" required>
                                        @foreach($advisers as $adviser)
                                        <option value="{{$adviser->id}}">{{$adviser->nombres_apellidos}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="col-12">
                                <hr>
                            </div>

                            <div class="col-12 text-right">
                                <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50">
                                        <i class="fas fa-fw fa-search"></i>
                                    </span>
                                    <span class="text">Consultar</span>
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="table-responsive d-none">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Nombre
                                </th>
                                <th scope="col" class="">
                                Área
                                </th>
                                <th scope="col" class="">
                                Vehículo
                                </th>
                                <th scope="col" class="">
                                Estado
                                </th>
                                <th scope="col" class="">
                                    <span class="sr-only">Acciones</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(function () {
        $('#desde').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            locale: 'es-es',
            uiLibrary: 'bootstrap4',
            format: 'dd-mm-yyyy',
            maxDate: function () {
                return $('#hasta').val();
            }
        });
        $('#hasta').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            locale: 'es-es',
            uiLibrary: 'bootstrap4',
            format: 'dd-mm-yyyy',
            minDate: function () {
                return $('#desde').val();
            }
        });
    })
</script>
@endsection
