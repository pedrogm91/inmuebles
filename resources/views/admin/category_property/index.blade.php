@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Tipos de Inmuebles</h6>
                    <div class="float-right">
                        <a href="{{ route('types-property.create') }}" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Tipo de Inmueble</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                    <th scope="col" class="">
                                        Nombre
                                    </th>
                                    <th scope="col" class="">
                                        Acciones
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($categories as $category)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        {{$category->nombre}}
                                    </th>
                                    <td class="d-flex gap-2">
                                        {{-- edit --}}
                                        <a href="{{ route('types-property.edit', [$category->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fas fa-edit"></i></a>
                                        {{-- delete --}}
                                        @if($category->isUsed() == 0)
                                        <form action="{{ route('types-property.destroy', [$category->id]) }}" method="post" id="eliminar-{{$category->id}}">
                                            @method('delete')
                                            @csrf
                                        </form>
                                        <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar" onclick="confirmar('eliminar-{{$category->id}}', '¿Desea eliminar este tipo de inmueble?', 'Se borrara de forma permanente');"><i class="fas fa-trash"></i></button>
                                        </form>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>
                        {{ $categories->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

