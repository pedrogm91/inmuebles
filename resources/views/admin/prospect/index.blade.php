@extends('layouts.dashboard')
@section('content')
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Prospectos</h6>
                <div class="float-right">
                    <a href="{{ route('prospect.create') }}" class="btn btn-success btn-icon-split btn-sm">
                        <span class="icon text-white-50">
                            <i class="fas fa-fw fa-plus-circle"></i>
                        </span>
                        <span class="text">Registrar Prospecto</span>
                    </a>
                </div>

            </div>
            <div class="card-body">
                <form action="" class="mb-2">
                    <div class="input-group">
                        <input type="text" name="search" aria-label="searchQuery" class="form-control">
                        <button class="btn btn-outline-primary" type="submit">Buscar</button>
                    </div>
                </form>

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-bordered">
                        <thead >
                            <tr>
                                <th>Cedula</th>
                                <th>Nombre Completo</th>
                                <th>Telefono</th>
                                <th>Correo</th>
                                <th>Interes</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($prospects as $prospect)
                                    <tr>   
                                        <td>{{$prospect->cedula}}</td>
                                        <td>{{$prospect->nombres_apellidos}}</td>
                                        <td>{{$prospect->telefono}}</td>
                                        <td>{{$prospect->correo}}</td>
                                        <td> {{$prospect->LeadActivity->pluck('interes')->join(' | ') }}</td>
                                        <td class="d-flex gap-2">
                                            {{-- show --}}
                                            <a href="{{ route('prospect.show', [$prospect->id]) }}" class="btn btn-sm mr-1 btn-circle btn-info" data-toggle="tooltip" data-placement="top" title="Ver Asesor"><i class="fas fa-eye"></i></a>
                                            {{-- edit --}}
                                            <a href="{{ route('prospect.edit', [$prospect->id]) }}" class="btn btn-sm mr-1 btn-circle btn-primary" data-toggle="tooltip" data-placement="top" title="Editar Asesor"><i class="fas fa-edit"></i></a>
                                            {{-- delete --}}
                                            @if(Auth::user()->role=='admin' || Auth::user()->role=='director')
                                            <form action="{{ route('prospect.destroy', [$prospect->id]) }}" method="post" id="eliminar-{{$prospect->id}}">
                                                @method('delete')
                                                @csrf
                                            </form>
                                            <button class="btn btn-sm mr-1 btn-danger btn-circle" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Inmueble" onclick="confirmar('eliminar-{{$prospect->id}}', '¿Desea eliminar este Prospecto?', 'Se borrara de forma permanente');"><i class="fas fa-fw fa-trash"></i></button>
                                            @endif
                                        </td>
                                    </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>

                <div>
                    {{ $prospects->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection