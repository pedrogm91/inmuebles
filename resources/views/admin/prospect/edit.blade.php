@extends('layouts.dashboard')
@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-sm-12">
                <div class="card shadow">
                    <div class="card-header py-3">
                        <h6 class="font-weight-bold text-primary float-left align-items-center">Editar Prospecto</h6>
                    </div>
                    <div class="card-body">
                        <form action="/prospect/{{$prospect->id}}" method="POST" class="needs-validation">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col-xl-8">
                                    <label for="id_prospecte">Nombre Completo</label>
                                   <div class="form-floating mb-3"> <input type="text"  name="nombres_apellidos" class="form-control {{ $errors->has('nombres_apellidos') ? 'is-invalid' : '' }} only-letter" value="{{ old('nombres_apellidos') ? old('nombres_apellidos') : $prospect->nombres_apellidos }}"></div>
                                   <div class="invalid-feedback">
                                    {{ $errors->first('nombres_apellidos') }}
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                   <label for="id_prospecte">Cedula:</label>
                                   <div class="form-floating mb-3">
                                       <input type="text"  name="cedula" class="form-control {{ $errors->has('cedula') ? 'is-invalid' : '' }} only-letter" value="{{ old('cedula') ? old('cedula') : $prospect->cedula }}"></div>
                                   <div class="invalid-feedback">
                                    {{ $errors->first('cedula') }}
                                </div>
                               </div>
                               
                                <div class="col-xl-6">
                                    <label for="telefono_prospecte">Telefono:</label>
                                    <div class="form-floating mb-3"><input type="text" name='telefono' class="form-control {{ $errors->has('telefono') ? 'is-invalid' : '' }} only-letter" value="{{ old('telefono') ? old('telefono') : $prospect->telefono }}"></div>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('telefono') }}
                                    </div>
                                </div>
                                <div class="col-xl-6">
                                   <label for="telefono_prospecte">Telefono Movil:</label>
                                   <div class="form-floating mb-3"><input type="text" name='telefono_movil' class="form-control {{ $errors->has('telefono_movil') ? 'is-invalid' : '' }} only-letter" value="{{ old('telefono_movil') ? old('telefono_movil') : $prospect->telefono_movil }}"></div>
                                   <div class="invalid-feedback">
                                    {{ $errors->first('telefono_movil') }}
                                    </div>
                                </div>
                                <div class="col-xl-8">
                                    <label for="correo_prospecte">Direccion: </label>
                                    <div class="form-floating mb-3"><input type="text" name='direccion' class="form-control {{ $errors->has('direccion') ? 'is-invalid' : '' }} only-letter" value="{{ old('direccion') ? old('direccion') : $prospect->direccion }}"></div>
                                    <div class="invalid-feedback">
                                        {{ $errors->first('direccion') }}
                                    </div>
                                </div>
                                <div class="col-xl-4">
                                   <label for="correo_prospecte">Correo: </label>
                                   <div class="form-floating mb-3"><input type="text" name='correo' class="form-control {{ $errors->has('correo') ? 'is-invalid' : 'correo' }} only-letter" value="{{ old('correo') ? old('correo') : $prospect->correo }}"></div>
                                   <div class="invalid-feedback">
                                    {{ $errors->first('correo') }}
                                </div>
                                </div>
                            </div>
                                <div class="col-sm-12 text-right">
                                   <hr>
                                   <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                       <span class="icon text-white-50"><i class="fas fa-fw fa-save"></i></span>
                                       <span class="text">Actualizar Prospecto</span>
                                   </button>
                                </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection