@extends('layouts.dashboard')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-sm-12">
             <div class="card shadow md-4">
                 <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Registro de Prospecto</h6>
                 </div>
                 <div class="card-body">
                     <form action="/prospect" method="POST" >
                         @csrf
                         <div class="row">
                             <div class="col-xl-8">
                               
                                 <label for="id_cliente">Nombre Completo</label>
                                <div class="form-floating mb-3"> <input type="text" class="form-control" name="nombres_apellidos"></div>
                             </div>
                             <div class="col-xl-4">
                                <label for="id_cliente">Cedula:</label>
                                <div class="form-floating mb-3">
                                    <input type="text" class="form-control" name="cedula">
                                </div>
                            </div>
                            
                             <div class="col-xl-6">
                                 <label for="telefono_cliente">Telefono:</label>
                                 <div class="form-floating mb-3"><input type="text" name='telefono' class="form-control" value=''></div>
                             </div>
                             <div class="col-xl-6">
                                <label for="telefono_cliente">Telefono Movil:</label>
                                <div class="form-floating mb-3"><input type="text" name='telefono_movil' class="form-control" value=''></div>
                            </div>
                             <div class="col-xl-8">
                                 <label for="correo_cliente">Direccion: </label>
                                 <div class="form-floating mb-3"><input type="text" name='direccion' class="form-control" value=''></div>
                             </div>
                             <div class="col-xl-4">
                                <label for="correo_cliente">Correo: </label>
                                <div class="form-floating mb-3"><input type="text" name='correo' class="form-control" value=''></div>
                            </div>
                         </div>
                             <div class="col-sm-12 text-right">
                                <hr>
                                <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                    <span class="icon text-white-50"><i class="fas fa-fw fa-save"></i></span>
                                    <span class="text">Registrar Prospecto</span>
                                </button>
                             </div>
                     </form>
                 </div>
             </div>
        </div>
    </div>
</div>
@endsection