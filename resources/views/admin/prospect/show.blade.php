@extends('layouts.dashboard')
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-xl-12 text-center">
                <div class="card">
                    <div class="card-header bg-light">Detalles Personales Prospecto</div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label for="">Nombre Completo:</label>
                                {{ $prospect->nombres_apellidos }}
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <label for="">Documento de Identidad:</label>
                                @if ($prospect->cedula == '')
                                    {No Registrado}
                                @else
                                    {{ $prospect->telefono_movil }}
                                @endif
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <label for="">Telefono:</label>
                                {{ $prospect->telefono }}
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <label for="">Telefono Movil</label>
                                @if ($prospect->telefono_movil == '')
                                    {No Registrado}
                                @else
                                    {{ $prospect->telefono_movil }}
                                @endif
                                <hr>
                            </div>
                            <div class="col-md-6">
                                <label for="">Correo</label>
                                @if ($prospect->correo == '')
                                    {No Registrado}
                                @else
                                    {{ $prospect->correo }}
                                @endif
                                <hr>
                            </div>
                            <div class="col-md-4">
                                <label for="">Direccion</label>
                                @if ($prospect->direccion == '')
                                    {No Registrada}
                                @else
                                    {{ $prospect->direccion }}
                                @endif
                                <hr>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <a href="{{ route('prospect.edit', $prospect->id) }}" class="btn btn-success">Actualizar Datos
                            Personales</a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="col-xl-12 justify">
                <div class="card ">
                    <div class="card-header bg-light">
                        LEADS Registrados
                    </div>
                    <div class="card-body">
                        <div class="row">
                            @foreach ($prospect->LeadActivity as $lead)
                                <!-- Earnings (Monthly) Card Example -->
                                <div class="col-xl-3 col-md-6 mb-4">
                                    @switch($lead->interes)
                                        @case('compra')
                                            <div class="card border-left-primary shadow h-100 py-2">
                                            @break
                                        @case('vender')
                                            <div class="card border-left-success shadow h-100 py-2">
                                            @break
                                        @case('alquilar')
                                            <div class="card border-left-info shadow h-100 py-2">
                                            @break
                                        @case('contacto')
                                        <div class="card border-left-warning shadow h-100 py-2">
                                        @break
                                        @default
                                            <div class="card border-left-dark shadow h-100 py-2"> 
                                    @endswitch
                                        <div class="card-body">
                                            <div class="row no-gutters align-items-center">
                                                <div class="col mr-2">
                                                    <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">
                                                        {{ \Carbon\Carbon::parse($lead->created_at)->format('Y-m-d') }}</div>
                                                    <div class="h5 mb-0 font-weight-bold text-gray-800"><a href="{{route('leadactivity.show',$lead->id)}}">{{$lead->interes}}</a></div>
                                                </div>
                                                <div class="col-auto">
                                                    <i class="fas fa-calendar fa-2x text-gray-300"></i>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xl-12 text-center">
                        <div class="card-footer">
                            <a class="btn  mr-1  btn-info" href="{{url('/leadactivity/create')}}">
                                <i class="fas fa-comment-dots"></i>Agregar actividad
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
