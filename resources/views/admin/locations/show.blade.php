@extends('layouts.dashboard')

@section('content')
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="mt-1 font-weight-bold text-primary float-left align-items-center">Ciudades o Urbanizaciones de la Parroquia {{ $parish->nombre }}</h6>
                    <div class="float-right">
                        <a href="{{ route('municipalities.show', $parish->id_municipio) }}" class="btn btn-danger btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-arrow-circle-left"></i>
                            </span>
                            <span class="text">Listado de Parroquias o Urbanizaciones</span>
                        </a>
                        <a href="" class="btn btn-success btn-icon-split btn-sm">
                            <span class="icon text-white-50">
                                <i class="fas fa-fw fa-plus-circle"></i>
                            </span>
                            <span class="text">Crear Ciudad</span>
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <form action="" class="mb-2">
                        <div class="input-group">
                            <input type="text" name="search" aria-label="searchQuery" class="form-control">
                            <button class="btn btn-outline-primary" type="submit">Buscar</button>
                        </div>
                    </form>

                    <div class="table-responsive">
                        <table class="table table-striped table-hover table-bordered">
                            <thead class="bg-slate-50">
                                <tr>
                                <th scope="col" class="">
                                Nombre
                                </th>
                                <th scope="col" class="">
                                    <span class="sr-only">Acciones</span>
                                </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($parish->cities as $city)
                                <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700">
                                    <th scope="row" class="">
                                        <a href="/cities/{{$city->id}}">{{$city->nombre}}</a>
                                    </th>
                                    <td class="d-flex gap-2">
                                        {{-- show --}}

                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

