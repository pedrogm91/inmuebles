@extends('layouts.dashboard')

@section('style')

@endsection

@section('content')
    <div class="row justify-content-center">
        <div class="col-12">
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="font-weight-bold text-primary float-left align-items-center">Editar Parroquia o Urbanización</h6>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-12">
                            <form action="{{ route('locations.update', $location->id) }}" method="POST" class="needs-validation">
                                @csrf
                                @method('PUT')
                                <input type="hidden" name="id_estado" class="form-control" value="{{ $location->id_estado }}">
                                <div class="row">
                                    <div class="col-4">
                                        <label for="nombre">Nombre de la ciudad</label>
                                        <div class="form-floating mb-3">
                                            <input type="text" name="nombre" class="form-control" value="{{ old('nombre') }}{{ $location->nombre }}">
                                            <div class="invalid-feedback">
                                                {{ $errors->first('nombre') }}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12">
                                        <hr>
                                    </div>
                                    <div class="col-12 text-right">
                                        <a href="{{ route('states.cities', $location->id_estado) }}" class="btn btn-danger btn-icon-split btn-sm">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-times-circle"></i>
                                            </span>
                                            <span class="text">Cancelar</span>
                                        </a>
                                        <button type="submit" class="btn btn-success btn-sm btn-icon-split">
                                            <span class="icon text-white-50">
                                                <i class="fas fa-fw fa-save"></i>
                                            </span>
                                            <span class="text">Guardar</span>
                                        </button>
                                    </div>
                                </div>

                                <div class="">

                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $('.only-number').on('input', function () {
            this.value = this.value.replace(/[^0-9]/g,'');
        });

        $('.only-letter').on('input', function () {
            this.value = this.value.replace(/[^a-zA-Z ]/g,'');
        });
    </script>
@endsection