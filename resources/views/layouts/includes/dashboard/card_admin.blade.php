<div class="row justify-content-center">
    <div class="col-md-3 mb-4">
        <div class="card border-left-dark shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 font-weight-bold text-primary mb-1">
                            {{$properties}}
                        </div>
                        Propiedades
                    </div>
                    <div class="col-auto">
                        <i class="far fa-building fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 mb-4">
        <div class="card border-left-dark shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 font-weight-bold text-primary mb-1">
                            {{$advisers}}
                        </div>
                        Asesores
                    </div>
                    <div class="col-auto">
                        <i class="far fa-id-badge fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 mb-4">
        <div class="card border-left-dark shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 font-weight-bold text-primary mb-1">
                            {{$negotiation}} / més
                        </div>
                        Negociaciones
                    </div>
                    <div class="col-auto">
                        <i class="far fa-2x fa-handshake text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 mb-4">
        <div class="card border-left-dark shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="h5 font-weight-bold text-primary mb-1">
                            {{$meeting}} / més
                        </div>
                        Citas
                    </div>
                    <div class="col-auto">
                        <i class="far fa-calendar-check fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <canvas id="myChart" width="800" height="200"></canvas>
    </div>
</div>
@push('scripts')
<script src="{{ asset('libs/chart.js/chart.min.js') }}" type="text/javascript"></script>
<script>
const labels = [
    @foreach ($total_month_negotiation as $item)
        'Més {{ $item["month"] }}',
    @endforeach
];

const data = {
    labels: labels,
    datasets: [
        {
            label: 'Total',
            backgroundColor: 'rgb(80, 80, 80)',
            borderColor: 'rgb(0, 0, 0)',
            data: [
                @foreach ($total_month_negotiation as $item)
                    '{{ $item["total"] }}',
                @endforeach
            ],
        },
        
    ],
};

const config = {
    type: 'line',
    data: data,
    options: {
        locale: 'en-IN',
        scales: {
            yAxes: [{
            ticks: {
                beginAtZero: true,
                callback: function(value, index, values) {
                if(parseInt(value) >= 1000){
                    return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
                } else {
                    return '$' + value;
                }
                }
            }
            }]
      }
    }
};

new Chart(
    document.getElementById('myChart'),
    config
);
</script>
@endpush