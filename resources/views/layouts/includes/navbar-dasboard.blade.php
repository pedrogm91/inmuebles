<ul class="navbar-nav bg-gradient-silver sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ url('dashboard') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">{{ $company->nombre ?? 'Sin título' }}</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0 d-none">

    <!-- Nav Item - Dashboard -->
    @if (auth()->user()->role != 'user')
        <li class="nav-item active">
            <a class="nav-link" href="{{ url('dashboard') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
    @else
        <li class="nav-item active">
            <a class="nav-link" href="{{ route('users.perfil') }}">
                <i class="fas fa-user-circle"></i>
                <span>Mi cuenta</span></a>
        </li>
    @endif


    <!-- Divider -->
    <hr class="sidebar-divider">

    <!-- Nav Item - Pages Collapse Menu -->

    @if (auth()->user()->role != 'user')
        <li class="nav-item" id="menu_inmuebles">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#subseccion_inmuebles"
                aria-expanded="true" aria-controls="subseccion_inmuebles" id="seccion_inmuebles">
                <i class="fas fa-fw fa-hotel"></i>
                <span>Inmuebles</span>
            </a>
            <div id="subseccion_inmuebles" class="collapse" aria-labelledby="headingOne"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('properties.index') }}" id="link_inmuebles">Ver
                        Inmuebles</a>
                    {{-- <a class="collapse-item" href="{{ route('offers.index') }}" id="link_offers">Ver Ofertas</a> --}}
                    @if (auth()->user()->role == 'admin' || auth()->user()->role == 'director')
                        <a class="collapse-item" href="{{ route('negotiations.index') }}" id="link_negotiations">Ver
                            Negociaciones</a>
                    @endif
                    <a class="collapse-item" href="{{ route('meetings.index') }}" id="link_meetings">Ver Citas</a>
                    <a class="collapse-item" href="{{ route('types-property.index') }}" id="link_types_property">Tipos
                        de Inmuebles</a>
                </div>
            </div>
        </li>
    @endif
    @if (auth()->user()->role != 'user')
        <li class="nav-item" id="menu_asesores">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#subseccion_asesores"
                aria-expanded="true" aria-controls="subseccion_asesores" id="seccion_asesores">
                <i class="fas fa-fw fa-users"></i>
                <span>Asesores</span>
            </a>
            <div id="subseccion_asesores" class="collapse" aria-labelledby="headingNine"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('advisers.index') }}" id="link_asesores">Ver Asesores</a>
                    <a class="collapse-item d-none" href="{{ route('formats.list') }}"
                        id="link_formats_asesores">Formatos</a>

                    @foreach ($formats as $format)
                        <a class="collapse-item" href="{{ asset('storage/' . $format->route) }}">{{ $format->name }}</a>
                    @endforeach
                </div>
            </div>
        </li>
    @endif
    @if (auth()->user()->role != 'user')
        <li class="nav-item" id="menu_prospectos">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#subseccion_prospectos"
                aria-expanded="true" aria-controls="subseccion_prospectos" id="seccion_prospectos">
                <i class="fas fa-fw fa-address-book"></i>
                <span>Prospectos</span>
            </a>
            <div id="subseccion_prospectos" class="collapse" aria-labelledby="headingTen"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('prospect.index') }}" id="link_prospectos">Ver
                        Prospectos</a>
                    <a class="collapse-item" href="{{ route('leadactivity.index') }}" id="link_leadactivity">Ver
                        Actividades</a>
                </div>
            </div>
        </li>
    @endif
    @if (auth()->user()->role != 'user')
        <li class="nav-item">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo"
                aria-expanded="true" aria-controls="collapseTwo">
                <i class="far fa-fw fa-calendar-alt"></i>
                <span>Agenda Calendario</span>
            </a>
            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('meetings.calendar') }}">Ver Actividades</a>
                </div>
            </div>
        </li>
    @endif

    @if (auth()->user()->role == 'admin' || auth()->user()->role == 'director')
        <li class="nav-item" id="menu_reportes">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#subseccion_reportes"
                aria-expanded="true" aria-controls="subseccion_reportes" id="seccion_reportes">
                <i class="fas fa-fw fa-file-alt"></i>
                <span>Reportes</span>
            </a>
            <div id="subseccion_reportes" class="collapse" aria-labelledby="headingFour"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('reports.profitability') }}"
                        id="link_profitability">Rentabilidad</a>
                    <a class="collapse-item" href="{{ route('reports.profitability_advisers') }}"
                        id="link_profitability_advisers">Rentabilidad por Asesor</a>
                </div>
            </div>
        </li>
    @endif

    @if (auth()->user()->role == 'admin' || auth()->user()->role == 'director')
        <li class="nav-item" id="menu_vacantes">
            <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#subseccion_vacantes"
                aria-expanded="true" aria-controls="subseccion_vacantes" id="seccion_vacantes">
                <i class="fas fa-fw fa-briefcase"></i>
                <span>Ofertas Laborales</span>
            </a>
            <div id="subseccion_vacantes" class="collapse" aria-labelledby="headingFive"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    <a class="collapse-item" href="{{ route('jobs.index') }}" id="link_vacantes">Agregar
                        Vacantes</a>
                    <a class="collapse-item" href="{{ route('postulations.index') }}" id="link_postulations">Ver
                        Postulaciones</a>
                </div>
            </div>
        </li>
    @endif

    @if (auth()->user()->role == 'admin' || auth()->user()->role == 'director')
        <li class="nav-item" id="menu_administracion">
            <a class="nav-link collapsed" href="#" data-toggle="collapse"
                data-target="#subseccion_administracion" aria-expanded="true"
                aria-controls="subseccion_administracion" id="seccion_administracion">
                <i class="fas fa-fw fa-cogs"></i>
                <span>Administración</span>
            </a>
            <div id="subseccion_administracion" class="collapse" aria-labelledby="headingEight"
                data-parent="#accordionSidebar">
                <div class="bg-white py-2 collapse-inner rounded">
                    @if (auth()->user()->role == 'admin' || auth()->user()->role == 'director')
                        <a class="collapse-item" href="{{ route('users.index') }}" id="link_usuarios">Usuarios</a>
                        <a class="collapse-item" href="{{ route('contents.index') }}" id="link_contents">Configuración</a>
                        <a class="collapse-item" href="{{ route('formats.index') }}" id="link_formats">Formatos</a>
                    @endif
                    <a class="collapse-item" href="{{ route('states.index') }}" id="link_localidades">Localidades</a>
                </div>
            </div>
        </li>
        <hr class="sidebar-divider d-none d-md-block">
    @endif

    <!-- Divider -->

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
