<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="" />
    <meta name="author" content="">
    <meta name="keyword" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>{{$company->nombre ?? 'Sin título'}}</title>
    <!-- Custom fonts for this template-->
    <link href="{{ asset('libs/fontawesome/css/all.min.css') }}" rel="stylesheet" type="text/css">
    <!-- Fonts Google -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('libs/bootstrap-select/css/bootstrap-select.min.css') }}">
    <!-- gijgo Datepicker -->
    <link href="{{ asset('libs/bootstrap-datepicker/css/gijgo.min.css') }}" rel="stylesheet" type="text/css" />
    <!-- Custom styles for this template-->
    <link href="{{ asset('libs/sbadmin/css/sb-admin-2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/jquery/jquery-ui.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/sweetalert2/css/sweetalert2.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/dropify/css/dropify.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/datatables/responsive.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('libs/datatables/dataTables.bootstrap4.min.css') }}" rel="stylesheet">
    <link href="{{ asset('lib_calendar/main.css') }}" rel="stylesheet" >
    <link href="{{ asset('css/main.css') }}" rel="stylesheet">



    @yield('style')
    <style>
        .sidebar .nav-item .collapse .collapse-inner .collapse-item, .sidebar .nav-item .collapsing .collapse-inner .collapse-item
        {
            white-space: normal;
        }
    </style>
</head>

<body id="page-top">

    <!-- Page Wrapper -->
    <div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.includes.navbar-dasboard')
        
        <!-- End of Sidebar -->

        <!-- Content Wrapper -->
        <div id="content-wrapper" class="d-flex flex-column">

            <!-- Main Content -->
            <div id="content" >

                <!-- Topbar -->
                <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

                    <!-- Sidebar Toggle (Topbar) -->
                    <button id="sidebarToggleTop" class="btn btn-link d-md-none rounded-circle mr-3">
                        <i class="fa fa-bars"></i>
                    </button>

                    <!-- Topbar Navbar -->
                    <ul class="navbar-nav ml-auto">

                        <!-- Nav Item - User Information -->
                        <li class="nav-item dropdown no-arrow">
                            <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                                data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="mr-2 d-none d-lg-inline text-gray-600 small">{{ Auth::user()->name }}</span>
                                @if (Auth::user()->asesoresInmobiliarios()->first()==null)
                                <img class="img-profile rounded-circle"
                                src="https://picsum.photos/200">
                                @elseif(Auth::user()->asesoresInmobiliarios()->first()->imagen_perfil!=null)
                                <img class="img-profile rounded-circle"
                                src="{{asset('storage/'.Auth::user()->asesoresInmobiliarios()->first()->imagen_perfil)}}">
                                @else
                                <img class="img-profile rounded-circle"
                                src="https://picsum.photos/200">
                                @endif
                            </a>
                            <!-- Dropdown - User Information -->
                            <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                                aria-labelledby="userDropdown">
                                <a class="dropdown-item" href="/">
                                    <i class="fas fa-home fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Ir a la web
                                </a>
                                <a class="dropdown-item" href="{{ route('users.perfil') }}">
                                    <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Perfil de Usuario
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                                    <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                                    Salir
                                </a>
                            </div>
                        </li>

                    </ul>

                </nav>
                <!-- End of Topbar -->

                <!-- Begin Page Content -->
                <div class="container-fluid pt-3 pb-5" style="min-height: 100vh;">
                    {{-- start flash messages --}}
                    @include('admin.partials.flash-messages')
                    {{-- end flash messages  --}}

                    @yield('content')

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

            <!-- Footer -->
            <footer class="sticky-footer bg-white">
                <div class="container my-auto">
                    <div class="copyright text-center my-auto">
                        <span>&copy; Todos los derechos reservados. {{ config('app.name') }} {{ date('Y') }}</span>
                    </div>
                </div>
            </footer>
            <!-- End of Footer -->

        </div>
        <!-- End of Content Wrapper -->

    </div>
    <!-- End of Page Wrapper -->

    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
        <i class="fas fa-angle-up"></i>
    </a>

    <!-- Logout Modal-->
    <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
        aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">¿Listo para salir?</h5>
                    <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span>
                    </button>
                </div>
                <div class="modal-body">Seleccione "{{ __('Logout') }}" a continuación si está listo para finalizar su sesión actual.</div>
                <div class="modal-footer">
                    <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
                    <a class="btn btn-danger" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">{{ __('Logout') }}</a>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </div>

    <input type="hidden" id="menu" value="menu_{{ $modulo }}">
    <input type="hidden" id="seccion" value="seccion_{{ $modulo }}">
    <input type="hidden" id="subseccion" value="subseccion_{{ $modulo }}">
    <input type="hidden" id="link" value="link_{{ $seccion }}">


    <!-- Bootstrap core JavaScript-->
    <script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('libs/jquery/jquery-ui.js') }}"></script>
    <script src="{{ asset('libs/bootstrap/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <!-- Core plugin JavaScript-->
    <script src="{{ asset('libs/jquery-easing/jquery.easing.min.js') }}" type="text/javascript"></script>
    <!-- Custom scripts for all pages-->
    <script src="{{ asset('libs/sbadmin/js/sb-admin-2.min.js') }}" type="text/javascript"></script>
    <!-- Page level plugins -->
    <script src="{{ asset('libs/chart.js/Chart.min.js') }}" type="text/javascript"></script>
    <!-- sweetalert2 -->
    <script src="{{ asset('libs/sweetalert2/js/sweetalert2.min.js') }}" type="text/javascript"></script>
    <!-- Latest compiled and minified JavaScript -->
    <script src="{{ asset('libs/bootstrap-select/js/bootstrap-select.min.js') }}" type="text/javascript"></script>
    <!-- (Optional) Latest compiled and minified JavaScript translation files -->
    <script src="{{ asset('libs/bootstrap-select/js/defaults-es_ES.min.js') }}" type="text/javascript"></script>
    <!-- gijgo Datepicker -->
    <script src="{{ asset('libs/bootstrap-datepicker/js/gijgo.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('libs/bootstrap-datepicker/js/messages/messages.es-es.js') }}" type="text/javascript"></script>
    <script src="{{ asset('libs/dropify/js/dropify.js') }}" type="text/javascript"></script>
    <script src="{{ asset('libs/datatables/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('libs/datatables/dataTables.responsive.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('libs/datatables/dataTables.bootstrap4.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('lib_calendar/main.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    @yield('scripts')
    @stack('scripts')
</body>
</html>