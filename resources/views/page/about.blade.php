@extends('page.layout.themes')
@section('content')

<style>
  .topimage {
    background-image: url(assets/images/aboutus/banner-nosotros.jpg );
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    width: 100%;
    min-height: 200px;
    display: flex;
    flex-wrap: wrap;
    justify-content: center;
    align-items: center;


  }

  .about {

    background-image: url(assets/images/aboutus/banner-avila.jpg );
    background-repeat: no-repeat;
    background-position: center;
    background-size: cover;
    width: 100%;
    height: auto;
  }
</style>
<div class="topimage">
  <div class="col-12 col-lg-8 mx-auto py-3 text-center">
    <h2 class="text-primary fw-bold">Bienvenidos a Habita Inmueble</h2>
    <p class=" text-light lead">
      Personalización, destreza y confiabilidad es la clave de nuestro éxito.
    </p>
  </div>
</div>
</div>
<section class="bg-white-light py-5">
  <div class="container pt-3 pb-5">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto py-3">
        <p class="text-center lead">
          Nuestra trayectoria de más de 10 años en el sector inmobiliario de Venezuela, nos permite trabajar con el
          conocimiento necesario para encontrar las opciones más adecuadas a sus requerimientos de forma eficiente y
          segura.
        </p>
      </div>
    </div>

  </div>
  @include('page.imports.aboutDetails')
  <div class="container-fluid about text-light py-5">
    <div class="col-12 col-lg-7 text-center mx-auto">

      <h4>"Para muchos, obtener una vivienda es el sueño dorado, por eso lo más importante es encontrar un
        inmueble que
        haga sentir al cliente confortable en su nuevo hogar."</h4>
      <p class="text-primary lead">Magda Azouz</p>
      <p class="fst-italic lead">Directora General y fundadora</p>
    </div>
  </div>
  @include('page.imports.aboutFacility')
  @include('page.imports.aboutTeam')
</section>
@endsection