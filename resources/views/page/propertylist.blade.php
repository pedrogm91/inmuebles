@extends('page.layout.themes')

@section('content')

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-3  mb-5">
            @include('page.imports.propertyListFilter')
        </div>
        <div class="col-12 col-lg-9">
            <div class="row">
                @include('page.imports.recentProperties',['column' =>'col-lg-6'])
            </div>


        </div>
    </div>
</div>

@endsection

@section('scripts')
<script>
    function getJsonRelationState(id){
        // Get Municipaloties
        $.ajax({
            url: '/municipalities/'+id+'/json',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method: 'GET',
            success: function(response) {
                $("#id_municipio").empty();
                $("#id_municipio").append(`<option selected disabled>Municipio</option>`);
                $.each(response.municipalities, function(index, value) {
                    $("#id_municipio").append(`<option value=` + value.id + `>` + value.nombre + `</option>`);
                });
            }
        });
    }
</script>
@endsection