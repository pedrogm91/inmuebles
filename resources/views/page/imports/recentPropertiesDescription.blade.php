<div class="card-body">
  <div class="card-top d-flex align-items-center">
    <!-- title -->
    <div class="card-title col-9">
      <h5 class="text-truncate">
        <a href="/property/{{ $property->id }}" class="text-black fw-bold text-decoration-none"
          title="
       {{$property->category->nombre}} en {{$property->negocio()}} {{$property->location->nombre ?? ''}}, Edo. {{$property->state->nombre}}">
          {{-- Oficina / Local en Alquiler 1.490m2 en La Boyera --}}

          {{$property->nombre_propiedad}} en
          {{$property->negocio()}}
        </a>
      </h5>
    </div>
    <!-- save & favorite -->
    <div class="card-tools text-end col-3 mb-3">

      @if(auth()->user())
      <form action="{{ route('favorite.store', ['user_id' => Auth::user()->id, 'property_id' => $property->id]) }}"
        method="post">
        @csrf
        <button class="btn btn-link" type="submit" data-toggle="tooltip" data-placement="top" title="Eliminar Usuario">
          <img src="{{ asset('assets/icons/favorite.svg') }}" class="pl-1" alt="favorite" />
        </button>
      </form>
      @endif
    </div>
  </div>
  <!-- Info -->
  <div class="card-info d-flex flex-wrap mb-1 align-items-center">
    <div class="card-location col-12">
      <p class="text-truncate">
        <img class="align-baseline" src="{{ asset('assets/icons/location.svg') }}" alt="location" />
          @isset($property->parish)
              {{$property->parish->nombre}}, Edo
          @endisset
      @isset($property->state->nombre)
          {{$property->state->nombre}}
      @endisset

      </p>
    </div>
    <div class="card-type col-6">
      <h6 class="text-gray">
        @isset($property->category->nombre)
        {{$property->category->nombre}}
        @endisset

      </h6>
    </div>
    <div class="card-price col-6 text-end">
      <h5 class="title-secondary fw-bold">
        @isset($property->precio)
        Ref {{ number_format($property->precio, 0, ',', '.') }}
        @endisset

      </h5>
    </div>
  </div>
  <hr />
  <!-- features -->
  <div class="card-features d-flex justify-content-between">
    <div class="">
      <img src="{{ asset('assets/icons/mts2.svg') }}" alt="mts2" />
      <span class="d-inline font-weight-bold">{{$property->datos_construccion["area_construccion"]}}
        M²</span>
    </div>
    <div class="">
      @if ($property->category->nombre=='Local' || $property->category->nombre=='Oficina' )
      <img src="{{ asset('assets/icons/air-conditioner.svg') }}" width="23px" height="24px" alt="rooms" />
      <span class="d-inline font-weight-bold">{{$property->datos_construccion["aire_a"]}}</span>
      @else
      <img src="{{asset('assets/icons/rooms.png') }}" width="23px" height="24px" alt="rooms" />
      <span class="d-inline font-weight-bold">{{$property->datos_construccion["habit"]}}</span>
      @endif
    </div>
    <div class="">
      <img src="{{asset('assets/icons/bathroom.svg')}}" alt="bathroom" />
      <span class="d-inline font-weight-bold">{{$property->datos_construccion["banos"]}}</span>
    </div>
    <div class="">
      <img src="{{asset('assets/icons/parking.svg')}}" alt="parking" />
      <span class="d-inline font-weight-bold">{{$property->datos_construccion["estacionamiento"]}}</span>
    </div>
  </div>
</div>