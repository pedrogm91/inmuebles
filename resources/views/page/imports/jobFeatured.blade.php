<section>
  <div class="container py-5">
    <div class="row">
      <div class="col-12">
        <h2 class="fw-bold text-center mb-5">
          Empleos destacados
        </h2>
      </div>
    </div>
    <div class="row  row-cols-1 row-cols-md-2 row-cols-lg-3 g-4">
      @forelse ($jobs as $job)
      <div class="col">
        <div class="card">
          <div class="card-header">
            <h4 class="fw-bold mb-0">{{$job->cargo}}</h4>
          </div>
          <div class="card-body">

            <p class="card-text">{{$job->describelo}}</p>
            <p>Área: {{$job->area->nombre}}</p>

          </div>
          <div class="card-footer d-flex justify-content-between text-muted ">

            <a href="#"
              class="btn btn-light btn-sm lead">{{Carbon\Carbon::parse($job->updated_at)->diffForHumans()}}</a>
            <a href="{{route('page.postulations.show',$job)}}" class="btn btn-primary btn-sm text-light">Leer más</a>
          </div>
        </div>
      </div>
      @empty
      <center>No existen vacantes publicadas.</center>
      @endforelse
    </div>

    <div class="py-5">
      {{ $jobs->links() }}
    </div>
  </div>
</section>