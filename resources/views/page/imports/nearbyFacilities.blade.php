<div class="card-body">
  <p class="fw-bold mb-2">
    Instalaciones cercanas
  </p>
  <div class="row justify-content g-3">
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{
                    asset('assets/icons/supermarket.svg')
                }}" width="28px" alt="Supermercado" />
      <span class="fs-5">
        Supermercado:

        {{ $property_details->instalaciones_cercanas['supermercado'] }}

      </span>
    </div>
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{ asset('assets/icons/school.svg') }}" width="28px" alt="colegio" />
      <span class="fs-5">
        Colegios:

        {{ $property_details->instalaciones_cercanas['colegios'] }}

      </span>
    </div>
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{ asset('assets/icons/shop.svg') }}" width="28px" alt="centro comercial" />
      <span class="fs-5">

        Centro comercial:
        {{ $property_details->instalaciones_cercanas['centro_comercial'] }}

      </span>
    </div>
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{ asset('assets/icons/hospital.svg') }}" width="28px" alt="hospital" />
      <span class="fs-5">
        Hospital:

        {{ $property_details->instalaciones_cercanas['clinica'] }}

      </span>
    </div>
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{ asset('assets/icons/medication.svg') }}" width="28px" alt="Farmacia" />
      <span class="fs-5">
        Farmacia:

        {{ $property_details->instalaciones_cercanas['farmacia'] }}

      </span>
    </div>
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{
                    asset('assets/icons/public-transport.svg')
                }}" width="28px" alt="transporte publico" />
      <span class="fs-5">
        Parada de Autobús:

        {{ $property_details->instalaciones_cercanas['autobus'] }}

      </span>
    </div>
    <div class="col-6 col-lg-4">
      <img class="list-grid-ico" src="{{ asset('assets/icons/train.svg') }}" width="28px" alt="metro" />
      <span class="fs-5">
        Metros:

        {{ $property_details->instalaciones_cercanas['estacion_metro'] }}

      </span>
    </div>
  </div>
</div>