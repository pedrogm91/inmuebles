<div class="card">
  <a href="/property/{{ $property->id }}" class="">
    <div style="height: 230px;">
      @if(count($property->documents())>0)
      <img src="{{ asset('storage/'.$property->documents()[0]->documento_url) }}" class="card-img-top"
        style="height: 230px; object-fit:cover;"
        alt=" {{$property->category->nombre}} en {{$property->negocio()}} {{$property->location->nombre}}" />
      @else
      <img src="" class="card-img-top" style="height: 230px; object-fit:cover;"
        alt=" {{$property->category->nombre}} en {{$property->negocio()}} {{$property->location->nombre}}" />
      @endif
    </div>
  </a>

  <div class="container d-flex flex-row-reverse position-absolute justify-content-between p-3 top-0 start-0 ">
    <a class="btn btn-primary text-light fw-bold" href="javascript:void(0)" role="button">{{$property->negocio()}}</a>
  </div>
  <div
    class="card-counter-photecontainer d-flex flex-row-reverse position-absolute justify-content-between p-3 bottom-0 start-0 ">
    <span class="d-flex text-white"><img src="{{ asset('assets/icons/camera-white.svg') }}" class="pr-2"
        alt="camara" />{{ count($property->documents()) }}</span>
  </div>
</div>