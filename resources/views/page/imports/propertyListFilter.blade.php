<div class="card sticky-lg-top">
  <div class="card-body">
    <h4 class="fw-bold">Busca tu inmueble</h4>
    <hr>
    <div class="row">
      <div class="d-grid">
        <button class="btn btn-primary fw-bold text-light Filter-toggler" type="button"
          data-bs-toggle="collapse" data-bs-target="#collapseFilter" aria-controls="collapseFilter"
          aria-expanded="false" aria-label="Toggle navigation">

          <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-sliders"
            viewBox="0 0 16 16">
            <path fill-rule="evenodd"
              d="M11.5 2a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM9.05 3a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0V3h9.05zM4.5 7a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zM2.05 8a2.5 2.5 0 0 1 4.9 0H16v1H6.95a2.5 2.5 0 0 1-4.9 0H0V8h2.05zm9.45 4a1.5 1.5 0 1 0 0 3 1.5 1.5 0 0 0 0-3zm-2.45 1a2.5 2.5 0 0 1 4.9 0H16v1h-2.05a2.5 2.5 0 0 1-4.9 0H0v-1h9.05z" />
          </svg>
          Filtro de búsqueda
        </button>
      </div>
      <div class="Filter-collapse collapse mt-4 show" id="collapseFilter" style="">
        <form class="g-3" action="{{ route('front.showProperty') }}" method="get">

          <input class="form-control form-control-sm mb-3" type="text" name="nombre_propiedad" id="nombre_propiedad"
            placeholder="Nombre Propiedad" value="{{ $request['nombre_propiedad'] ?? false }}">
          <select id="tipo_negocio" name="tipo_negocio" class="form-select form-select-sm mb-3">
            <option disabled selected="">Negociación</option>
            @if(isset($request['tipo_negocio']))
            <option value="2" {{ $request['tipo_negocio']==2?'selected':'' }}>Venta</option>
            <option value="1" {{ $request['tipo_negocio']==1?'selected':'' }}>Alquiler</option>
            @else
            <option value="2">Venta</option>
            <option value="1">Alquiler</option>
            @endif
          </select>
          <select id="id_categoria" name="id_categoria" class="form-select form-select-sm mb-3">
            <option disabled selected="">Tipología</option>
            @foreach($cats as $cat)
            @if(isset($request['id_categoria']))
            <option value="{{$cat->id}}" {{ $request['id_categoria']==$cat->id?'selected':'' }}>
              {{$cat->nombre}}</option>
            @else
            <option value="{{$cat->id}}">{{$cat->nombre}}</option>
            @endif
            @endforeach
          </select>
          <input class="form-control form-control-sm mb-3" type="number" placeholder="Precio Máximo" aria-label=""
            name="precio" id="precio" min="1" value="{{ $request['precio'] ?? false }}">
          <select id="id_estado" name="id_estado" class="form-select form-select-sm mb-3"
            onchange="getJsonRelationState(this.value);">
            <option disabled selected="">Estado</option>
            @foreach($ests as $est)
            @if(isset($request['id_estado']))
            <option value="{{$est->id}}" {{ $request['id_estado']==$est->id?'selected':'' }}>
              {{$est->nombre}}</option>
            @else
            <option value="{{$est->id}}">{{$est->nombre}}</option>
            @endif
            @endforeach
          </select>

          @if(isset($request['id_estado']))
          <select id="id_ciudad" name="id_ciudad" class="form-select form-select-sm mb-3"
            onchange="getJsonRelationState(this.value);">
            <option disabled selected="">Ciudad</option>
            @foreach($cius as $ciu)
            @if($ciu->id_estado == $request['id_estado'])
            @if(isset($request['id_ciudad']))
            <option value="{{$ciu->id}}" {{ $request['id_ciudad']==$ciu->id?'selected':'' }}>
              {{$ciu->nombre}}</option>
            @else
            <option value="{{$ciu->id}}">{{$ciu->nombre}}</option>
            @endif
            @endif
            @endforeach
          </select>
          @endif
          <select id="id_municipio" name="id_municipio" class="form-select form-select-sm mb-3">
            <option disabled selected="">Municipio</option>
          </select>

          <div class="d-grid gap-2">
            <button class="btn btn-primary fw-bold text-light" type="submit">BUSCAR</button>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
