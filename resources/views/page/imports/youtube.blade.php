<section class="video py-5 bg-dark">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="text-primary fw-bold text-center">
            Nuestro canal de Youtube
          </h2>
        </div>
        <div class="col-12 col-lg-8 mx-auto">
          <div class="ratio ratio-16x9">
            <iframe src="https://www.youtube.com/embed/YH_yKF34KcU"  title="YouTube video" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen=""></iframe>
          </div>
         
        </div>
        <div class="col-12 text-center mt-5">
          <a href="#" class="btn btn-primary text-light fw-bold">IR AL CANAL</a>
        </div>
      </div>
    </div>
  </section>