<section id="jobs" class="jobs bg-dark py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto mb-lg-5 mb-3">
        <h2 class="fw-bold text-primary text-center">
          Reclutamiento y Selección
        </h2>
        <h6 class="text-light text-center mt-4">¡TU FUTURO ESTÁ AQUÍ!</h6>
      </div>
    </div>
    <div class="row  text-primary">
      <!-- 1 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4 ">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/step1.svg')}}" alt=" Completa el formulario">

        </figure>
        <h4 class="fw-bold text-center">
          Completa el formulario
        </h4>
        <p class="text-white text-center  fs-5">
          Comparte con nosotros todos tus datos personales, incluyendo tu experiencia laboral. </p>
      </div>
      <!-- 2 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/step2.svg')}}" alt="Entrevista Presencial">

        </figure>
        <h4 class="fw-bold text-center">
          Entrevista Presencial
        </h4>
        <p class="text-white text-center  fs-5">
          Un requisito fundamental para nuestro reclutamiento es conocerte personalmente.
        </p>
      </div>
      <!-- 3 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/step3.svg')}}" alt="Beneficios">
        </figure>
        <h4 class="fw-bold text-center">
          Beneficios
        </h4>
        <p class="text-white text-center  fs-5">
          Tenemos los mejores honorarios conforme al mercado actual y su economía
        </p>
      </div>
      <!-- 4 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/step4.svg')}}" alt="Bienvenido">
        </figure>
        <h4 class="fw-bold text-center">
          Bienvenido
        </h4>
        <p class="text-white text-center fs-5">
          Si cumples con todos los requisitos que buscamos ten por seguro que ya estás dentro de nuestro equipo.
        </p>
      </div>
    </div>
  </div>
</section>