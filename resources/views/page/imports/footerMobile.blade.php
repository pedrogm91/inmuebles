<div class="d-block d-lg-none p-4">
  <div class="row">
    <div class="col-12 mb-3 fs-5">
      <a href="index.php" class="d-block mb-2">
        <img class="fluid" src="{{ asset('storage/'.$company->logo) }}" alt="habitainmueble" width="240px">
      </a>
      <span class="text-white font-weight-bold">
        Llámanos ahora:
      </span>
      <h5 class="title-secondary text-white">
        <a href="https://wa.me/{{$company->celular}}" class="text-white">{{$company->celular}}</a><br>
        <a href="https://wa.me/{{$company->celular}}" class="text-white">{{$company->celular}}</a>
      </h5>
      <a href="mailto:{{$company->correo}}" class="text-white">
        <img src="{{asset('assets/icons/email.svg')}}" alt="email">&nbsp;
        correo: {{$company->correo}}
      </a>
    </div>

    <div class="col-12 my-1">
      <p>
        <a class="btn text-primary fw-bold fs-4" data-bs-toggle="collapse" href="#collapseVentas" role="button"
          aria-expanded="false" aria-controls="collapseVentas">
          Ventas
        </a>
        <img src="{{asset('assets/icons/arrow.svg')}} " alt="arrow">
      </p>

      <div class="collapse" id="collapseVentas">
        <ul class="list-group">
          @foreach ($enlaces['links_footer'] as $link)
          @if($link->id_menu==2)
          <li><a href="#{{$link->route}}" class="list-group-item fw-bold bg-dark text-light">{{$link->nombre}}</a>
          </li>
          @endif
          @endforeach
        </ul>
      </div>
    </div>

    <div class="col-12 my-1">
      <p>
        <a class="btn text-primary fw-bold fs-4" data-bs-toggle="collapse" href="#collapseAlquiler" role="button"
          aria-expanded="false" aria-controls="collapseAlquiler">
          Compras
        </a>
        <img src="{{asset('assets/icons/arrow.svg')}} " alt="arrow">
      </p>

      <div class="collapse" id="collapseAlquiler">
        <ul class="list-group">
          {{-- <li class="list-group-item bg-dark text-light ">Casas</li> --}}
          @foreach ($enlaces['links_footer'] as $link)
          @if($link->id_menu==2)
          <li><a href="#{{$link->route}}" class="list-group-item fw-bold bg-dark text-light">{{$link->nombre}}</a>
          </li>
          @endif
          @endforeach
        </ul>
      </div>
    </div>

    <div class=" col-12 my-1">
      <p>
        <a class="btn text-primary fw-bold fs-4" data-bs-toggle="collapse" href="#collapseInteres" role="button"
          aria-expanded="false" aria-controls="collapseInteres">
          Enlaces de interes
        </a>
        <img src="{{asset('assets/icons/arrow.svg')}} " alt="arrow">
      </p>

      <div class="collapse" id="collapseInteres">
        <ul class="list-group">

          @foreach ($enlaces['links_footer'] as $link)
          @if($link->id_menu==1)
          <li><a href="#{{$link->route}}" class="list-group-item fw-bold bg-dark text-light">{{$link->nombre}}</a>
          </li>
          @endif
          @endforeach
        </ul>
      </div>
    </div>

    <div class="col-12 mt-3 fs-5">
      <h4 class="text-white title-footer mb-3">{{$company->nombre}}</h4>
      <p class="text-white small">
        <img src="{{asset('assets/icons/alarm_24px.svg')}} " alt="hora">&nbsp;
        {{$company->horario}}
      </p>
      <p class="small">
        <a href="https://goo.gl/maps/7t68yqiUFMfKQFTK8" class="text-white">
          <img src="{{asset('assets/icons/map-2.svg')}} " alt="direccion">&nbsp;
          {{$company->direccion}}
        </a>
      </p>
      <span class="rrss">

        <a href="{{$company->instagram}}"><img src="{{asset('assets/icons/instagram.svg')}} " alt="instagram"></a>
        <a href="{{$company->facebook}}/"><img src="{{asset('assets/icons/facebook.svg')}} " alt="facebook"></a>

      </span>

    </div>
  </div>
</div>