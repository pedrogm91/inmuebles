<div>
  {{-- <h1>hero section</h1> --}}

  <style>
    #intro {
      background-image:url('{{ $banners->banner_principal }}');
      height: 100vh;
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center center;

    }

    #intro-mobile {
      background-image:url('{{ $banners->banner_mobile }}');

      height: 100vh;
      background-repeat: no-repeat;
      background-size: cover;
      background-position: center center;

    }
  </style>

  @include('page.imports.heroMobile')

  @include('page.imports.heroDesktop')
</div>