<footer class="bg-dark  pt-5 py-2">
  <div class="container">
    @include('page.imports.footerDesktop')

    @include('page.imports.footerMobile')
  </div>

  <aside class="copyright text-white mt-3 pt-2 border-top">
    <div class="container">
      <div class="row flex-column flex-lg-row">
        <div class="col">
          <p class="mb-0 small">
            © Todos los derechos reservados {{$company->nombre}}. Creado por: <a href="https://gianchinnici.site/" target="_blank" rel="noopener noreferrer">Gianchinnici</a>
          </p>
        </div>
      </div>
    </div>
  </aside>
</footer>