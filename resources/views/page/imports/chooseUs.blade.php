<section id="about" class="whychoose bg-dark py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto mb-lg-5 mb-3">
        <h2 class="fw-bold text-primary text-center">
          ¿Por qué elegirnos?
        </h2>
      </div>
    </div>
    <div class="row  text-primary">
      <!-- 1 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4 ">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/home.svg')}}" alt="Conducta ética">

        </figure>
        <h4 class="fw-bold text-center">
          Conducta ética
        </h4>
        <p class="text-white text-center  fs-5">
          Contamos con un equipo de profesionales capacitados y orientados al beneficio de nuestros clientes.
        </p>
      </div>
      <!-- 2 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/desktop.svg')}}" alt="Innovación">

        </figure>
        <h4 class="fw-bold text-center">
          Innovación
        </h4>
        <p class="text-white text-center  fs-5">
          Hacemos la diferencia porque constantemente capacitamos a nuestro personal con nuevas técnicas de venta de
          inmuebles.
        </p>
      </div>
      <!-- 3 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/manos.svg')}}" alt="Confianza">
        </figure>
        <h4 class="fw-bold text-center">
          Confianza
        </h4>
        <p class="text-white text-center  fs-5">
          Nuestra larga trayectoria en el mercado inmobiliario nos hace expertos en la materia. Deja tu requerimiento en
          nuestras manos.
        </p>
      </div>
      <!-- 4 -->
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <figure class="text-center p-1">
          <img src="{{asset('assets/icons/suport.svg')}}" alt="Asesoria Personalizada">
        </figure>
        <h4 class="fw-bold text-center">
          Asesoria Personalizada
        </h4>
        <p class="text-white text-center fs-5">
          Te asignamos un asesor inmobiliario que se dará a la tarea de ayudarte en la compra, alquiler o venta de un
          inmueble.
        </p>
      </div>
    </div>
  </div>
</section>