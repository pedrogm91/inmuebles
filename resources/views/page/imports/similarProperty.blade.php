@forelse($properties as $property)
<div class="{{$column ?$column : col-lg-4 }}">
  <article class="card mb-4 shadow rounded">
    <!-- image -->
    @include('page.imports.similarPropertyImage')
    @include('page.imports.similarPropertyDescription')
  </article>
</div>
@empty
<p>No hay propiedades</p>
@endforeach
