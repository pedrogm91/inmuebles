<div class="card-header d-flex justify-content-between bg-transparent mb-4">
  <span>Código: {{($property_details->id)}}</span>
  <div class="iconos-save-like">
    @if(auth()->user())
    <form action="{{ route('favorite.store', ['user_id' => Auth::user()->id, 'property_id' => $property_details->id]) }}"
        method="post">
        @csrf
        <button class="btn btn-link" type="submit" data-toggle="tooltip" data-placement="top"
        title="Eliminar Usuario">
        <img src="{{ asset('assets/icons/favorite.svg') }}" class="pl-1" alt="favorite" />
        </button>
    </form>
@endif

  </div>
</div>
<div class="card-body">
  <h4>
    {{$property_details->nombre_propiedad}}

  </h4>
  <h5>
    {{($property_details->category->nombre)}}
    {{($property_details->datos_construccion['area_terreno'])}} metros cuadrados
  </h5>
  <p class="fw-bold fs-5 mt-4">Descripción</p>
  <p class="fs-6">
    {{($property_details->descripcion)}}
  </p>
  {{-- <div class="text-center">
    <button id="readmore" class="btn btn-primary btn-sm fw-bold fs-5 text-light">
      Ver más
    </button>
    <button id="readless" class="btn btn-primary btn-sm" style="display: none;">
      Ver menos
    </button>
  </div> --}}
</div>