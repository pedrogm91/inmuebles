<section id="citas" class="appointment bg-white-light">
  <div class="container-fluid">
    <div class="row">
      <div class="col-12 col-lg-5 offset-lg-1 py-5">
          <h2 class="fw-bold text-primary">Agenda una cita</h2>
          <p class="fs-5">
            Completa el formulario para agendar una cita y ayudarte a
            conseguir o vender el inmueble o propiedad que necesitas.
          </p>
          <form id="demo-form" class="row" action="{{route('leadactivity.store')}}" method="POST">
            @csrf
            <div class="col-6 form-group py-2">
              <input id="name" name="nombres_apellidos" type="text" class="form-control"
                placeholder="Nombre y Apellido" required="">
            </div>
            <div class="col-6 form-group py-2">
              <input id="email" name="correo" type="text" class="form-control" placeholder="Correo" required="">
            </div>
            <div class="col-6 form-group py-2">
              <input id="phone" name="telefono" type="text" class="form-control" placeholder="Teléfono"
                required="">
            </div>
            <div class="col-6 form-group py-2">
              <select id="motivo" name="interes" class="form-select" required="">
                <option value="">Quiero...</option>
                <option value="Comprar">Comprar</option>
                <option value="Vender">Vender</option>
                <option value="Alquilar">Alquilar</option>
              </select>
            </div>
            <div class="col-6 form-group py-2">
              <select class="form-select" id="tipinm" name="tipinm" required="">
                <option value="">Tipo de Inmueble</option>
                <option value=""></option>
                <option value="Anexos">Anexos</option>
                <option value="Apartamentos">Apartamentos</option>
                <option value="Casas">Casas</option>
                <option value="Galpones">Galpones</option>
                <option value="Local">Local</option>
                <option value="Local Comercial">Local comercial</option>
                <option value="Locales">Locales</option>
                <option value="Oficina">Oficina</option>
                <option value="Oficinas">Oficinas</option>
                <option value="Proyecto">Proyecto</option>
                <option value="Terrenos">Terrenos</option>
              </select>
            </div>
            <div class="col-6 form-group py-2">
              <select class="form-select" id="zona" name="zona" required="">
                <option value="">Estado</option>
                <option value="CARACAS">Caracas</option>
                <option value="DISTRITO CAPITAL">Distrito capital</option>
                <option value="MIRANDA">Miranda</option>
                <option value="NUEVA ESPARTA">Nueva esparta</option>
                <option value="VARGAS">Vargas</option>
              </select>
            </div>
            <div class="col-6 form-group py-2">
              <select class="form-select" id="cuartos" name="cuartos" required="">
                <option value="">Rango de precio ($)</option>
                <option value="200 a 500">200 a 500</option>
                <option value="501 a 1,000">501 a 1,000</option>
                <option value="1,001 a 10,000">1,001 a 10,000</option>
                <option value="10,001 a 15,000">10,001 a 15,000</option>
                <option value="15,001 a 20,000">15,001 a 20,000</option>
                <option value="20,001 a 25,000">20,001 a 25,000</option>
                <option value="25,001 a 35,000">25,001 a 35,000</option>
                <option value="35,001 a 45,000">35,001 a 45,000</option>
                <option value="45,001 a 65,000">45,001 a 65,000</option>
                <option value="65,001 a 88,000">65,001 a 88,000</option>
                <option value="88,001 a 100,000">88,001 a 100,000</option>
                <option value="100,001 o más">100,001 o más</option>
              </select>
            </div>
            <div class="col-6 form-group py-2">
              <select class="form-select" id="metros" name="metros" required="">
                <option value="">Seleccione un área (M²)</option>
                <option value="Hasta 60">Hasta 60 </option>
                <option value="61 a 100">61 a 100</option>
                <option value="101 a 200">101 a 200</option>
                <option value="201 a 300">201 a 300</option>
                <option value="301 a 400">301 a 400</option>
                <option value="401 a 500">401 a 500</option>
                <option value="501 a 600">501 a 600</option>
                <option value="601 a 700">601 a 700</option>
                <option value="701 a 800">701 a 800</option>
                <option value="801 a 900">801 a 900</option>
                <option value="901 a 1000">901 a 1000</option>
                <option value="1001 o más">1001 o más</option>
              </select>
            </div>
            <div class="col-12 form-group py-2">
              <textarea id="comen" name="comen" class="form-control" rows="3"
                placeholder="Características adicionales"></textarea>
            </div>

            <div class="col-12 text-center form-group py-2">
              <button type="submit" class="btn btn-primary text-light fw-bold">
                ENVIAR CITA
              </button>
            </div>
          </form>
        </div>
        <style>
          #appointment-img {

            height: 100%;
            object-fit: cover;

          }
        </style>
        <div class="col-12 col-lg-6 px-0">
          <img id="appointment-img" class="appointment-img img-fluid position-start"
            src="{{asset('assets/images/cita.jpg')}}" alt="Soporte 24/7">
        </div>
      </div>
    </div>
  </div>
</section>