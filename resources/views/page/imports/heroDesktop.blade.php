<div id="intro" class="bg-image d-none d-lg-block">
  <div class="container d-flex align-items-center justify-content-start h-100">
    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="text-white">
          <h1 class="display-5 fw-bold mb-5 mb-lg-1">
            {{$banners->slogan}}
          </h1>
        </div>
      </div>
    </div>
  </div>

</div>