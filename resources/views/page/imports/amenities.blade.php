<div class="card-body">
  <p class="fw-bold mb-2">Comodidades</p>
  <div class="list-grid">
    <!-- item list -->


    <div class="row justify-content g-3">
      @if($property_details->category->nombre)
      <div class="col-6 col-lg-4">
        <img class="list-grid-ico" src="{{ asset('assets/icons/casa.svg') }}" width="28px" alt="categoria" />
        <span class="fs-5">
          Tipo: {{($property_details->category->nombre)}}
        </span>
      </div>
      @endif


      @if($property_details->datos_construccion['area_terreno'])
      <div class="col-6 col-lg-4">
        <img class="list-grid-ico" src="{{
                        asset(
                            'assets/icons/area-de-terreno.svg'
                        )
                    }}" width="28px" alt="area-de-terreno" />
        <span class="fs-5">
          Area de terreno:


          {{ $property_details->datos_construccion['area_terreno'] }}
          m2
        </span>
      </div>
      @endif

      <div class="col-6 col-lg-4">
        <img class="list-grid-ico" src="{{
                        asset(
                            'assets/icons/area-de-construccion.svg'
                        )
                    }}" width="28px" alt="area-de-construccion" />
        <span class="fs-5">
          Area de construccion:

          {{ $property_details->datos_construccion['area_construccion'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">

        <img class="list-grid-ico" src="{{ asset('assets/icons/wifi.svg') }}" width="28px" alt="wifi" />
        <span class="fs-5">Wi-fi:

          {{ $property_details->datos_construccion['wifi'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img class="list-grid-ico" src="{{ asset('assets/icons/rooms.svg') }}" width="28px" alt="cuartos" />
        <span class="fs-5">Habitaciones:


          {{ $property_details->datos_construccion['habit'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img class="list-grid-ico" src="{{
                        asset('assets/icons/spatula.svg')
                    }}" width="28px" alt="cocina" />
        <span class="fs-5">Cocina:

          {{ $property_details->datos_construccion['cocina'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img class="list-grid-ico" src="{{
                        asset('assets/icons/bathtub.svg')
                    }}" width="28px" alt="baños" />
        <span class="fs-5">Baños:

          {{ $property_details->datos_construccion['banos'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{ asset('assets/icons/toilet.svg') }}" width="28px" alt="medio_baño" />
        <span class="fs-5">Medio baño

          {{ $property_details->datos_construccion['banos_s'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{ asset('assets/icons/sala.svg') }} " width="28px" alt="Sala" />
        <span class="fs-5">Sala:

          {{ $property_details->datos_construccion['sala'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{
                        asset('assets/icons/parking.svg')
                    }} " width="28px" alt="Estacionamiento" />
        <span class="fs-5">Estacionamiento:

          {{ $property_details->datos_construccion['estacionamiento'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{
                        asset(
                            'assets/icons/air-conditioner.svg'
                        )
                    }} " width="28px" alt="Aire acondicionado" />
        <span class="fs-5">Aire Acondicionado:

          {{ $property_details->datos_construccion['aire_a'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{ asset('assets/icons/bbq.svg') }} " alt="parrillera" width="28px" />
        <span class="fs-5">Parrillera:

          {{ $property_details->datos_construccion['parrillera'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{ asset('assets/icons/bed.svg') }} " alt="habitación de servicio" width="28px" />
        <span class="fs-5">Hab. Servicio

          {{ $property_details->datos_construccion['habit_s'] }}

        </span>
      </div>
      @if($property_details->datos_construccion['tv'])

      <div class="col-6 col-lg-4">
        <img src="{{
                        asset('assets/icons/television.svg')
                    }} " alt="TV por cable" width="28px" />
        <span class="fs-5">TV por cable:

          {{ $property_details->datos_construccion['tv'] }}

        </span>
      </div>
      @endif

      <div class="col-6 col-lg-4">
        <img src="{{
                        asset('assets/icons/locker.svg')
                    }} " alt="Maletero" width="28px" />
        <span class="fs-5">Maletero:

          {{ $property_details->datos_construccion['maletero'] }}

        </span>
      </div>
      <div class="col-6 col-lg-4">
        <img src="{{
                        asset('assets/icons/dishwasher.svg')
                    }} " alt="Lavadero" width="28px" />
        <span class="fs-5">Lavadero:

          {{ $property_details->datos_construccion['lavadero'] }}

        </span>
      </div>
    </div>
  </div>
</div>
