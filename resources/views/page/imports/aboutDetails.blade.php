<div class="container-fluid">
  <div class="row featurette bg-dark">
    <div class="col-md-6 align-self-center ">
      <div class="p-lg-5 m-4 text-light lead">
        <p> <span class="text-primary">Habita Inmueble</span> representa un concepto novedoso e integral en atención y
          comodidad para el sector inmobiliario.</p>
        <p>Nuestro esquema y filosofía de trabajo nos ha permitido responder de forma positiva a una gran cantidad de
          clientes en Venezuela.</p>
      </div>
    </div>
    <div class="col-md-6 px-0">
      <img src="{{asset('assets/images/aboutus/nosotros1.jpg')}}" alt="" class="img-fluid">

    </div>
  </div>
  <div class="row featurette">
    <div class="col-md-6 align-self-center order-md-2">
      <div class="p-lg-5 m-4 lead">

        <p>Conozca las mejores opciones que existen en el mercado, con detalles específicos que lo ayudaran a
          encontrar el lugar que más se adapta a sus expectativas y necesidades.</p>
      </div>
    </div>
    <div class="col-md-6 px-0 order-md-1">
      <img src="{{asset('assets/images/aboutus/nosotros2.jpg')}}" alt="" class="img-fluid">
    </div>
  </div>
</div>