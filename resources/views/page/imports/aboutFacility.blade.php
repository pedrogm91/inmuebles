<div class="container-fluid bg-dark">

  <div class="container pt-5 pb-3">
    <div class="text-center">
      <h2 class="text-primary fw-bold">Nuestras instalaciones</h2>
      <p class="lead text-light">Visítanos y conoce nuestra oficina y equipo de trabajo</p>
    </div>
    <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4 py-5">
      <img src="{{asset('assets/images/aboutus/oficina1.jpg')}} " alt="">
      <img src="{{asset('assets/images/aboutus/oficina2.jpg')}} " alt="">
      <img src="{{asset('assets/images/aboutus/oficina3.jpg')}} " alt="">
      <img src="{{asset('assets/images/aboutus/oficina4.jpg')}} " alt="">
      <img src="{{asset('assets/images/aboutus/oficina5.jpg')}} " alt="">
      <img src="{{asset('assets/images/aboutus/oficina6.jpg')}} " alt="">

    </div>
  </div>
</div>