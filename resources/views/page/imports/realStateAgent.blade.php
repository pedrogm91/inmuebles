<div class="col-12 col-lg-3 mb-5">
  <div class="card mb-3">
    <div class="card-body text-center">
      <h3 class="mb-3">Agente Inmobiliario</h3>
      <figure class="widget-img-agente text-center">
        <img src="{{ asset('storage/'.$property_details->adviser->imagen_perfil) }}" alt="agente" class="img-fluid"
          style="width: 128px; height: 128px; border-radius: 100%; object-fit: cover;" />
      </figure>
      <p class="text-center">
        <span class="fw-bold fs-5 mb-2">{{($property_details->adviser->nombres_apellidos)}}</span>
        <br />

        {{-- 1 --}}
        <div class="row">
          <div class="col-12">
            <img class="" src="{{ asset('assets/icons/email.svg') }} " alt="email" />
            <span class="fs-6">Correo:</span>
            <p class="fs-6">
              {{($property_details->adviser->correo)}}
            </p>

          </div>
        </div>
        {{-- 2 --}}
        <div class="row">
          <div class="col-12">
            <img class="pr-1" src="{{
                                    asset('assets/icons/whatsapp-color.svg')
                                }}" />
            <span class="fs-6">Whatsapp:</span>
            <p class="fs-6">
              <a href="https://wa.me/{{($property_details->adviser->telefono_movil)}}?text=¡Hola!">
                {{($property_details->adviser->telefono_movil)}}
              </a>
            </p>

          </div>
        </div>
      </p>
      <hr />
      <p class="text-center">
        <span class="fw-bold fs-5 mb-2">Número de la agencia</span>
        <br />
        <a href="tel:{{$property_details->adviser->telefono}}">{{($property_details->adviser->telefono)}}</a>
      </p>
      <p></p>
    </div>
  </div>

  <div class="card">
    <div class="card-body">
      <h4 class="title-secondary mb-3">
        Quiero saber mas sobre esta propiedad
      </h4>
      <form class="row" action="{{route('leadactivity.store')}}" method="POST">
        @csrf
        <input type="hidden" name="interes" value="Comprar">
        <div class="form-group mb-3">
          <input id="name" name="nombres_apellidos" type="text" class="form-control" placeholder="Tu nombre*" />
        </div>
        <div class="form-group mb-3">
          <input id="email" name="correo" type="text" class="form-control" placeholder="Tu email*" />
        </div>
        <div class="form-group mb-3">
          <input id="phone" name="telefono" type="text" class="form-control" placeholder="Teléfono*" />
        </div>
        <div class="form-group mb-2">
          <textarea id="comen" name="comen" class="form-control" placeholder="">{{($property_details->category->nombre)}},Estoy interesado en la propiedad código {{($property_details->id)}}. {{($property_details->ubicacion)}}{{($property_details->datos_construccion['area_terreno'])}} metros cuadrados,
                    </textarea>
        </div>
        <div class="text-center mt-4 mb-3">
          <button type="submit" class="btn fw-bold text-light btn-block btn-primary align-center">
            ENVIA TU SOLICITUD
          </button>
        </div>
      </form>
    </div>
  </div>
</div>