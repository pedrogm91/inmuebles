<div class="container">
  <div class="row">
    <div class="col-12">
      <h4>
        {{$property_details->nombre_propiedad}}
        <a class="btn btn-primary text-light fw-bold mx-4" href="javascript:void(0)"
          role="button">{{$property_details->negocio()}}</a>
      </h4>
    </div>
    <!-- location -->
    <div class="col-12 col-lg-7">
      <p>
        <img src="{{ asset('assets/icons/location.svg') }}" alt="location" height="18px" />
        Estado {{($property_details->state->nombre)}}
      </p>
      <!-- meta -->
      <p>
        Categoría: {{($property_details->category->nombre)}}
      </p>
    </div>
    <div class="col-12 col-lg-5">
      <h3 class="text-end">
        <span class=""> Ref {{ number_format($property_details->precio, 0, ',', '.') }}</span>
      </h3>
    </div>
  </div>
</div>