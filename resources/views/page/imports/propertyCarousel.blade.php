<div class="container">

  <div class="row">
    <div class="col-12 mb-3">
      <div id="carouselProperties" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
          @foreach($property_details->documents() as $element)
          <div class="carousel-item {{ $element->order==1 ? 'active':'' }}">
            <div>

              <img class="d-block w-100 " src=" {{ asset('storage/'.$element->documento_url) }}"
                alt="fotos de la propiedad">
            </div>
          </div>
          @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselProperties" role="button" data-bs-slide="prev">
          <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        </a>
        <a class="carousel-control-next" href="#carouselProperties" role="button" data-bs-slide="next">
          <span class="carousel-control-next-icon" aria-hidden="true"></span>
        </a>
      </div>
    </div>


  </div>
  {{-- <div class="row mb-4">
    <div class="col-6">
      <img src="{{ asset('assets/icons/camera.svg') }}" alt="camara" />{{ count($property_details->documents()) }}
    </div>
    <div class="col-6 text-end">
      <a href="admin/properties/">
        Pantalla completa
        <img src="{{ asset('assets/icons/fullscreen.svg') }}" alt="fullscreen" />
      </a>
    </div>
  </div> --}}
</div>