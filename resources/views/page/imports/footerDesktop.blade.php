<div class="d-none d-lg-block">
  <div class="row">
    <div class="col-12 col-lg-3">
      <a href="index.php" class="d-block mb-2">
        <img class="fluid" src="{{ asset('storage/'.$company->logo) }}" alt="habitainmueble" width="240px">
      </a>
      <span class="text-white font-weight-bold">
        Llámanos ahora:
      </span>
      <h5 class="text-white">
        <a href="https://wa.me/{{$company->celular}}" class="text-white">{{$company->celular}}</a><br>
        <a href="https://wa.me/{{$company->celular}}" class="text-white">{{$company->celular}}</a>
      </h5>
      <a href="mailto:{{$company->correo}}" class="text-white  text-decoration-none">
        <img src="{{asset('assets/icons/email.svg')}}" alt="email">&nbsp;
        {{$company->correo}}
      </a>
    </div>

    <div class="col-6 col-lg">
      <h4 class="text-white title-footer">CATEGORÍAS</h4>
      <ul class="nav flex-column text-white ">
        @foreach ($enlaces['links_categories'] as $link)
        <li><a href="{{$link->route}}" class="text-white text-decoration-none">{{$link->nombre}}</a></li>
        @endforeach
      </ul>
    </div>
    <div class="col-6 col-lg">
      <h4 class="text-white title-footer">Enlaces</h4>
      <ul class="nav flex-column text-white">
        @foreach ($enlaces['links_type'] as $link)
        <li><a href="{{$link->route}}" class="text-white text-decoration-none">{{$link->nombre}}</a></li>
        @endforeach
    </div>
    <div class="col-12 col-lg">
      <h4 class="text-white title-footer">Link de Interés</h4>
      <ul class="nav flex-column">
        @foreach ($enlaces['links_footer'] as $link)
        <li><a href="{{$link->route}}" class="text-white text-decoration-none">{{$link->nombre}}</a></li>
        @endforeach
      </ul>
    </div>

    <div class="col-12 col-lg-3">
      <h4 class="text-white title-footer">{{$company->nombre}}</h4>
      <p class="text-white small">
        <img src="{{asset('assets/icons/alarm_24px.svg')}} " alt="hora">&nbsp;
        {{$company->horario}}
      </p>
      <p class="small">
        <a href="https://goo.gl/maps/7t68yqiUFMfKQFTK8" class="text-white">
          <img src="{{asset('assets/icons/map-2.svg')}}" alt="direccion">&nbsp;
          {{$company->direccion}}
        </a>
      </p>
      <span class="rrss">

        <a href="{{$company->instagram}}"><img src="{{asset('assets/icons/instagram.svg')}}" alt="instagram"></a>
        <a href="{{$company->facebook}}"><img src="{{asset('assets/icons/facebook.svg')}}" alt="facebook"></a>
      </span>

    </div>
  </div>
</div>