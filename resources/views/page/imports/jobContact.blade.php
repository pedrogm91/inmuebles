<section id="jobsContact" class="bg-white-light py-5">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-5 offset-lg-1">
        <h4 class="text-primary fw-bold">Completa el formulario</h4>
        <p>Si eres apasionado y deseas incorporarte a un equipo de profesionales que cree en Venezuela y que hace que
          las cosas sucedan, esta oferta es para ti.</p>
        <form action="{{route('leadactivity.store')}}" method="POST" class="row">
          @csrf
          <input type="hidden" name="interes" value="contacto">
          <div class="col-6 form-group py-2">
            <input id="name" name="nombres_apellidos" type="text" class="form-control" placeholder="Nombre" required="">
          </div>
          <div class="col-6 form-group py-2">
            <input id="lname" name="lname" type="text" class="form-control" placeholder="Apellido" required="">
          </div>
          <div class="col-6 form-group py-2">
            <input id="email" name="correo" type="text" class="form-control" placeholder="Correo" required="">
          </div>
          <div class="col-6 form-group py-2">
            <input id="phone" name="telefono" type="text" class="form-control" placeholder="Teléfono" required="">
          </div>
          <div class="col-12 form-group py-2">
            <textarea class="form-control" id="comen" name="comen" rows="3" placeholder="Comentario"
              required=""></textarea>
          </div>
          <div class="my-3">
            <h6 class="fw-bold">Adjunte su currículo(PDF)</h6>
            <input type="file" class="form-control form-control-sm" aria-label="Small file input example">
          </div>
          <div class="col-12 text-center form-group py-2">
            <button type="submit" class="btn btn-primary text-light fw-bold">
              ENVIA TU SOLICITUD
            </button>
          </div>
        </form>
        <div class="row py-2">
          <div class="col-12 col-md-7 mb-3">
            <a class=" text-decoration-none" href="#">
              <img src="{{asset('assets/icons/email.svg')}}" alt="email">&nbsp;
              <span class="text-dark">atencion@habitainmueble.com</span>
            </a>
          </div>
          <div class="col-12 col-md-5 mb-3">
            <a class=" text-decoration-none" href="https://wa.me/584141514151">
              <img src="{{asset('assets/icons/Whatsapp-color.svg')}}" alt="email">&nbsp;
              <span class="text-dark">(+58) 414 151 4151</span>
            </a>
          </div>
          <div class="col-12 col-md-12 mb-3">
            <a class=" text-decoration-none" href="#" class="d-flex align-items-start">
              <img src="{{asset('assets/icons/map-2.svg')}}" class="pr-2" alt="email">
              <span class="text-dark">Centro de Servicio Plaza La Boyera, Urbanización La Boyera, Municipio El
                Hatillo</span>
            </a>
          </div>
          <div class="col-12 col-md-12 mb-3">
            <a class=" text-decoration-none" href="#" class="d-flex">
              <img src="{{asset('assets/icons/alarm_24px.svg')}}" alt="email" class="">&nbsp;
              <span class="text-dark">Lun - Vie 8:30 AM - 6:00 PM</span>
            </a>
          </div>
        </div>
      </div>
      <div class="col-12 col-lg-6">
        <iframe style="width:100%; height:450px;"
          src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15693.034015537689!2d-66.88380283950764!3d10.480276480365019!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8c2a58e66afa7fa3%3A0xdca0a1f50ea3b281!2sColinas%20de%20Bello%20Monte%2C%20Caracas%2C%20Miranda!5e0!3m2!1ses-419!2sve!4v1587504332107!5m2!1ses-419!2sve"
          frameborder="0" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
      </div>
    </div>
  </div>
</section>