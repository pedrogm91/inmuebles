<div class="search-filter bg-dark text-white py-4 ">
    <form class="container" action="{{ route('front.showProperty') }}" method="get">
        <div class="row">
            <div class="col-12 mb-3">
                <h2 class="fw-bold text-center">Busca tu Inmueble</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-6 col-lg-3 mb-3">            
                <select id="tipo_negocio" name="tipo_negocio" class="form-select">
                    <option disabled selected="">Negociación</option>
                    <option value="2">Venta</option>
                    <option value="1">Alquiler</option>
                </select>
            </div>
            <div class="col-6 col-lg-3 mb-3">
                <select id="id_categoria" name="id_categoria" class="form-select">
                    <option disabled selected="">Tipología</option>
                    @foreach($cats as $cat)
                        <option value="{{$cat->id}}">{{$cat->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-6 col-lg-3 mb-3">
                <select id="id_estado" name="id_estado" class="form-select" onchange="getJsonRelationState(this.value);">
                    <option disabled selected="">Estado</option>
                    @foreach($ests as $est)
                        <option value="{{$est->id}}">{{$est->nombre}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-6 col-lg-3 mb-3">
                <select id="id_municipio" name="id_municipio" class="form-select">
                    <option disabled selected="">Municipio</option>
                </select>
            </div>
        </div>
        <div class="row">
            <!-- SUBMIT HIDDEN xs sm md  -->
            <div class="col-12 col-lg d-none d-lg-block mb-3 text-center">
                <button type="submit" class="btn btn-block btn-primary text-light fw-bold">
                    BUSCAR
                </button>
            </div>
        </div>
        <!-- SUBMIT VISIBLE xs sm md -->
        <div class="row">
            <div class="col-12 d-lg-none mb-3 text-center">
                <div class="d-grid gap-2">
                    <button type="submit" class="btn btn-primary text-light fw-bold" >BUSCAR</button>
                </div>
            </div>
        </div>
    </form>
</div>

@section('scripts')
<script>
    function getJsonRelationState(id){
        // Get Municipaloties
        $.ajax({
            url: '/municipalities/'+id+'/json',
            headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
            method: 'GET',
            success: function(response) {
                $("#id_municipio").empty();
                $("#id_municipio").append(`<option selected disabled>Municipio</option>`);
                $.each(response.municipalities, function(index, value) {
                    $("#id_municipio").append(`<option value=` + value.id + `>` + value.nombre + `</option>`);
                });
            }
        });
    }
</script>
@endsection