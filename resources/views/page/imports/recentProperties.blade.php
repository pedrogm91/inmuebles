<section class="section-estate py-5">
    <div class="container">
        <div class="row">
            @foreach($properties as $property)
            <div class="{{$column ?$column : col-lg-4 }}">
                <article class="card mb-4 shadow rounded">
                    <!-- card image -->
                    @include('page.imports.recentPropertiesImage')
                    <!-- card-description -->
                    @include('page.imports.recentPropertiesDescription')
                </article>

            </div>
            @endforeach
        </div>
        @if (Route::currentRouteName() != 'front.inicio' )
        <div>
            {{ $properties->links() }}
        </div>
        @endif

    </div>
</section>