<div id="intro-mobile" class="d-lg-none">
  <div class="container d-flex align-items-center justify-content-start h-100">
    <div class="row">
      <div class="col-12 col-lg-6">
        <div class="text-white px-5">
          <h1 class="fw-bold display-2 mb-5 mb-lg-1 shadow-sm">

            {{$banners->slogan}}
          </h1>
        </div>
      </div>
    </div>
  </div>
</div>