<div class="card-body">
  <p class="fw-bold mb-2">Compartir</p>
  <div class="row justify-content g-3">
    <div class="col-12">

      <a href="https://www.instagram.com/habitainmueble/"><img src="{{asset('assets/icons/instagram.svg')}} "
          alt="instagram" />
      </a>
      <a href="https://www.facebook.com/habitainmueble/"><img src="{{asset('assets/icons/facebook.svg')}} "
          alt="facebook" />
      </a>
    </div>
  </div>
</div>