
<style>
    #testimonies {
      background-image:url('{{ asset('assets/images/testimonios.jpg')}}');
      height: 60vh;
      background-repeat: no-repeat; background-size: cover; background-position: center center;
      
    }
    </style>
<section id="testimonies" class="py-5">
   
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto">
        <figure class="text-center mb-4">
          <img src="{{asset('assets/icons/comillas.svg')}}" alt="comillas">
        </figure>
        <p class="text-center fs-5 mb-5 pt-2">
          Gracias por realmente tomarse en serio el trabajo de venta,
          llevando muchos clientes y ofreciendo el apartamento de una manera
          novedosa y llamativa. Estoy muy contento con el proceso que
          llevamos y con el acompañamiento que me dieron.
        </p>
        <p>
          </p><h4 class="text-primary text-center fst-italic fw-bold">Juan Camilo, Caracas</h4>
        <p></p>
      </div>
    </div>
  </div>
</section>