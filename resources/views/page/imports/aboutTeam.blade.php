<div class="container pt-5 pb-3">
  <div class="text-center">
    <h2 class="text-primary fw-bold">Nuestro Equipo</h2>
    <p class="lead">Listos para apoyarte a conseguir el inmueble de tus sueños</p>
  </div>
  <div class="row row-cols-1 row-cols-md-2 row-cols-lg-3 g-4 py-5 text-center">


    @foreach ($advisers as $adviser)
    @if ($adviser->estatus == 'activo')

    <div class="align-self-center">

      <img src="{{asset('storage/'.$adviser->imagen_perfil)}}" alt="" class="rounded-circle img-top img-fluid mw-25"
        style="width: 128px; height: 128px; border-radius: 100%; object-fit: cover;" />
      <div class="py-2">
        <h4>{{$adviser->nombres_apellidos}}</h4>
        <div>
          <img class="pr-1" src="{{
              asset('assets/icons/whatsapp-color.svg')
          }}" />
          <span class="fs-6">Whatsapp:</span>
          <p class="lh-0 p-0 m-0">{{$adviser->telefono}}</p>
          <img class="" src="{{ asset('assets/icons/email.svg') }} " alt="email" />
          <span class="fs-6">Correo:</span>
          <p class="lh-0 m-0 p-0">{{$adviser->correo}}</p>

        </div>
      </div>
    </div>
    @endif
    @endforeach

  </div>
</div>