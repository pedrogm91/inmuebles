<div id="experiences" class="container mt-5">
    <h2 class="text-primary fw-bold text-center mb-4">Experiencias</h2>
    <p class="fs-5 text-center mb-4">Disfruta de fabulosas experiencias y actividades cerca de tu inmueble</p>
    <div class="row g-4 text-center">

        <div class="col-md-12 col-lg-6 position-relative">
            <figure class="">
                <a href="">
                    <img class="img-fluid" src="{{asset('assets/images/Cayo_de_Agua_in_Los_Roques.jpg')}}" alt=""></a>
            </figure>
            <div class="w-100 text-start position-absolute bottom-0 start-0 p-4
                              opacity-75 bg-light ">
                <a class="text-decoration-none" href="">
                    <h4 class="text-dark">Playas</h4>
                </a>
            </div>
        </div>

        <div class="col-md-6 col-lg-6 position-relative">
            <figure class="card-experience-img">
                <a href=""><img class="img-fluid"
                        src="{{asset('assets/images/salto-angel.jpg')}}" alt=""></a>
            </figure>
            <div class="w-100 text-start position-absolute bottom-0 start-0 p-4
                              opacity-75 bg-light ">
                <a class="text-decoration-none" href="">
                    <h4 class="text-dark">Parques</h4>
                </a>
            </div>
        </div>


        <div class="col-md-6 col-lg-4 position-relative">
            <figure class="card-experience-img">
                <a href=""><img class="img-fluid" src="{{asset('assets/images/picacho.jpg')}}"
                        alt=""></a>
            </figure>
            <div class="w-100 text-start position-absolute bottom-0 start-0 p-4
                              opacity-75 bg-light ">
                <a class="text-decoration-none" href="">
                    <h4 class="text-dark">Excursiones</h4>
                </a>
            </div>

        </div>
        <div class="col-md-6 col-lg-4 position-relative">
            <figure class="card-experience-img">
                <a href=""><img class="img-fluid"
                        src="{{asset('assets/images/Cachapa-Venezolana-2-1-1280x720.jpg')}}" alt=""></a>
            </figure>
            <div class="w-100 text-start position-absolute bottom-0 start-0 p-4
                              opacity-75 bg-light ">
                <a class="text-decoration-none" href="">
                    <h4 class="text-dark">Gastronomía</h4>
                </a>
            </div>

        </div>
        <div class="col-md-6 col-lg-4 position-relative">
            <figure class="card-experience-img">
                <a href=""><img class="img-fluid"
                        src="{{asset('assets/images/surf-classes.jpg')}}" alt=""></a>
            </figure>
            <div class="w-100 text-start position-absolute bottom-0 start-0 p-4
                  opacity-75 bg-light ">
                <a class="text-decoration-none" href="">
                    <h4 class="text-dark">Actividades</h4>
                </a>
            </div>

        </div>

    </div>
</div>