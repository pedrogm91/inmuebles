<section class="newsletter bg-primary py-5">
    <div class="container">
      <div class="row">
        <div class="col-12">
          <h2 class="text-light text-center fw-bold mb-4">Regístrate pare recibir nuestras novedades</h2>
        </div>
        <div class="col-12 col-md-6 mx-auto">
           <form id="newsform" class="form-row" action="php/form-post.php" method="post">
            <div class="col-lg-12 col-12">
              <div class="input-group mb-3">
                <input type="email" class="form-control" name="txtnews" placeholder="Email" required="">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-secondary btn-block px-5">Enviar</button>
                </div>
              </div>                
            </div>
            
            <div class="col-auto input-group justify-content-center">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="terms" id="terms" required="">
                <label class="form-check-label" for="gridRadios2">
                  <a href="#" class="text-white small
                  ">Acepto los términos y condiciones del sitio web.</a>
                </label>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
</section>