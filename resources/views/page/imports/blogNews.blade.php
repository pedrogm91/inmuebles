<section id="blog" class="blog py-5 bg-dark">
  <div class="container">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto">
        <h2 class="text-primary text-center fw-bold mb-2">
          Últimas noticias en nuestro blog
        </h2>
        <p class="text-center text-light fs-5">
          Lo último en noticias y actividades del mundo inmobiliario
        </p>
      </div>
    </div>
    <div class="row">
      <!-- 1 -->

    </div>
    <div class="row">
      <div class="col-12 text-center mt-4">
        <a href="blog.php" class="btn btn-primary text-light fw-bold">IR AL BLOG</a>
      </div>
    </div>
  </div>
</section>