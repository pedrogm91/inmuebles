<div class="container py-5">
    <div class="row">
        <div class="col-12 col-lg-8 mx-auto mb-3">
          <h2 class="fw-bold text-primary text-center">
            ¿Cómo lo hacemos?
          </h2>
          <p class="text-center fs-5">
            Te mostramos los pasos para que juntos logremos el éxito de
            encontrar o vender el inmueble
          </p>
        </div>
      </div>
  <nav class="nav nav-pills text-center mb-5" id="pills-tab" role="tablist">
    <a class="nav-link active col-6 fw-bold" id="pills-home-tab" data-bs-toggle="pill" href="#pills-home" role="tab" aria-controls="pills-home" aria-selected="true">COMPRAR</a>
    <a class="nav-link col-6 fw-bold"  id="pills-profile-tab" data-bs-toggle="pill" href="#pills-profile" role="tab" aria-controls="pills-profile" aria-selected="false">VENDER</a>
   
  </nav>
  <div class="tab-content" id="pills-tabContent">
    {{-- active --}}
    <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
    <div class="row text-primary">
        <!-- 1 -->
        <div class="col-12 col-lg-4">
          <figure class="text-center">
            <img src="{{asset('assets/icons/indentificar-necesidades.svg')}}" alt="identificar" height="120px">
          </figure>
          <h4 class="text-center fw-bold">
            Identifica tus necesidades
          </h4>
        </div>
        <!-- 2 -->
        <div class="col-12 col-lg-4">
          <figure class="text-center">
            <img src="{{asset('assets/icons/buscala-en-la-web.svg')}}" alt="buscar en la web" height="120px">
          </figure>
          <h4 class="text-center fw-bold">
            Haz la búsqueda en Internet 
          </h4>
        </div>
        <!-- 3 -->
        <div class="col-12 col-lg-4">
          <figure class="text-center">
            <img src="{{asset('assets/icons/contacta-a-un-agente.svg')}}" alt="Contacta a un Agente" height="120px">
          </figure>
          <h4 class="text-center fw-bold">
            Contacta nuestro agente inmobiliario
          </h4>
        </div>
      </div>
    </div>
    {{-- no active --}}
    <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
    <div class="row text-primary">
        <!-- 1 -->
        <div class="col-12 col-lg-4">
          <figure class="text-center">
            <img src="{{asset('assets/icons/buscala-en-la-web.svg')}}" alt="identificar" height="120px">
          </figure>
          <h4 class="text-center fw-bold">
            Agendamos una cita contigo
          </h4>
        </div>
        <!-- 2 -->
        <div class="col-12 col-lg-4">
          <figure class="text-center">
            <img src="{{asset('assets/icons/indentificar-necesidades.svg')}}" alt="buscar en la web" height="120px">
          </figure>
          <h4 class="text-center fw-bold">
            Empezamos la búsqueda de un comprador
          </h4>
        </div>
        <!-- 3 -->
        <div class="col-12 col-lg-4">
          <figure class="text-center">
            <img src="{{asset('assets/icons/contacta-a-un-agente.svg')}}" alt="Contacta a un Agente" height="120px">
          </figure>
          <h4 class="text-center fw-bold">
            Te asignamos un agente inmobiliario
          </h4>
        </div>
      </div> 
  </div>
</div>
</div>