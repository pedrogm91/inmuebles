<div>
  <style scoped>
    .nav-link{
      font-size: 1.1rem;
      font-weight: 700;
    }
  </style>
<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container">
    {{-- Botón mobil --}}
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    {{-- Logo --}}
    <a class="navbar-brand" href="/">
      <img src="{{ asset('storage/'.$company->logo) }}" alt="Habita inmueble" width="203px">
    </a>
    
    <div class="collapse navbar-collapse" id="navbarTogglerDemo01">
      {{-- Menú --}}
      <ul class="navbar-nav m-auto mb-2 mb-lg-0">
        @foreach ($enlaces['links_navbar'] as $link)
          <li class="nav-item">
            <a @class(['nav-link', 'active' => Request::routeIs($link->route)]) 
              @if ( Request::routeIs($link->route) || url()->current() == $link->route)
                aria-current="page"
              @endif
              href="{{ Route::has($link->route) ? route($link->route) : $link->route  }}" >
              
              {{-- texto enlace --}}
              {{$link->nombre}} 
            </a>
          </li>
        @endforeach

      </ul>
      {{-- Menú --}}
      <div class="d-flex">
        @auth()
          <a href="{{ url('/dashboard') }}" class="btn btn-outline-primary me-2 fw-bold">ADMINISTRACIÓN</a>
        @endauth
        
        @guest()
          <a href="{{ route('login') }}" class="btn btn-outline-primary me-2 fw-bold">INGRESA</a>
        @endguest
        
        {{-- @if (Route::has('register'))
            <a href="{{ route('register') }}" class="btn btn-primary text-light fw-bold">REGÍSTRATE</a>
        @endif --}}

      </div>
    </div>
  </div>
</nav>
</div>
