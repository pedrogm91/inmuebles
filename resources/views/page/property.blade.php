@extends('page.layout.themes') @section('content')

<div class="container">
    <div class="row">
        <div class="col-12">
            <a class="breadcrumb" href="{{ URL::previous() }}">
                <img src="{{ asset('assets/icons/arrow-left.svg') }}" alt="arrow-left"
                    class="p-1 text-decoration-none" />

                Regresar a la búsqueda
            </a>
        </div>
    </div>
</div>

@include('page.imports.propertyHeader')


@include('page.imports.propertyCarousel')

<div class="container">
    <div class="row">
        <div class="col-12 col-lg-9">
            <div class="card mb-5">



                @include('page.imports.propertyDescription')


                @include('page.imports.amenities')

                @include('page.imports.nearbyFacilities')
                @include('page.imports.propertySocials')
            </div>
        </div>
        @include('page.imports.realStateAgent')
    </div>
</div>
<div class="container">

    @if ($similar_property)

    <div class="row">
        <div class="col-12 text-center">
            <h3 class="title-section-secondary font-weight-bold mb-4">
                Propiedades Similares
            </h3>
        </div>
        @include('page.imports.similarProperty',['properties'=>$similar_property,'column' =>'col-lg-4'])
    </div>
    @endif

    <div class="row">
        <div class="col-12 text-center mt-3 mb-5">
            <a href="/propertylist" class="btn fw-bold text-light btn-primary text-uppercase">Buscar
                Inmuebles</a>
        </div>
    </div>
</div>
@endsection