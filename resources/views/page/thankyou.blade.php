@extends('page.layout.themes')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-6 py-5 text-center" style="min-height: 100vh;">
                <h1 class="text-center">!Gracias!</h1>
                <p class="text-center">¡Tus datos han sido registrados correctamente, pronto nos pondremos en contacto contigo!</p>
                <a href="{{route('front.showProperty')}}" class="btn btn-primary">Ver propiedades</a>
            </div>
        </div>
    </div>
@endsection

