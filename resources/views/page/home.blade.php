@extends('page.layout.themes')
@section('content')

@include('page.imports.hero')

@include('page.imports.searchProperty')
<div class="container">

  <div class="row pt-4">
    <div class="col-12">
      <h2 class="fw-bold text-center">
        Propiedades Recientes
      </h2>
    </div>
  </div>
</div>
@include('page.imports.recentProperties',['column' =>'col-lg-4'])

<div class="container">
  <div class="col-12 text-center pb-5">
    <a href="propertylist" class="btn btn-primary text-light fw-bold">VER MÁS</a>
  </div>

</div>

@include('page.imports.chooseUs')

@include('page.imports.appoinment')

@include('page.imports.youtube')

@include('page.imports.howWeDoIt')

{{-- @include('page.imports.blogNews') --}}

{{-- @include('page.imports.experiences') --}}

@include('page.imports.testimonies')

{{-- @include('page.imports.newsletter') --}}

@include('page.imports.contactUs')





@endsection