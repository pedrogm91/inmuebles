@extends('page.layout.themes')
@section('content')
<section class="container py-5">
  <div class="row">
    <div class="col-12 col-lg-12 mb-3">
      <div class="card mb-5">
        <div class="card-header bg-primary">
          <h4 class="fw-bold">{{$job->cargo}}</h4>
        </div>
        <div class="card-header px-4">
          <p class="mb-0">Publicado: <strong>{{Carbon\Carbon::parse($job->updated_at)->diffForHumans()}}</strong></p>
          <p class="mb-0">
            Lugar de Trabajo: <strong>{{$job->state->nombre}}</strong>
          </p>
          <p class="mb-0">Área: <strong>{{$job->area->nombre}}</strong></p>
        </div>
        <div class="card-body px-4">
          <h6 class="card-title">
            <strong>Descripción del Cargo:</strong>
          </h6>
          <p>
            {{$job->describelo}}
          </p>
          <h6 class="card-title">
            <strong>Requisitos del Cargo:</strong>
          </h6>
          <p>{{$job->requisitos}}</p>

          <strong>Ofrecemos:</strong>
          <p>{{$job->brief}}</p>
        </div>

      </div>


    </div>
  </div>

</section>
@include('page.imports.contactUs')

@endsection