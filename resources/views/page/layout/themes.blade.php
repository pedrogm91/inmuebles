<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{$company->nombre ?? 'Sin título'}}</title>
    <link href="{{asset('/css/app.css')}}" rel="stylesheet">
    <link rel="icon" type="image/x-icon" href="/favicon.ico">
    <script type="text/javascript">

        function callbackThen(response){

        // read HTTP status

        console.log(response.status);

        // read Promise object

        response.json().then(function(data){

        console.log(data);

        });

        }

        function callbackCatch(error){

        console.error('Error:', error)

        }

        </script>

        {!! htmlScriptTagJsApi([

        'callback_then' => 'callbackThen',

        'callback_catch' => 'callbackCatch'

        ]) !!}

</head>

<body>
    @include('page.imports.navbar')
    @yield('content')
    @include('page.imports.footer')

    <script src="{{asset('/js/app.js')}}">
    </script>
    <script src="{{ asset('libs/jquery/jquery.min.js') }}"></script>
    @yield('scripts')
</body>

</html>