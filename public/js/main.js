$(document).ready(function() {
    $('.DataTable').DataTable({
        responsive: true,
        "pageLength": 25,
        "lengthMenu": [25, 50, 100],
        "language": {
            "zeroRecords": "Disculpe, No existen registros",
            "info": "Mostrando páginas _PAGE_ de _PAGES_",
            "infoEmpty": " ",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "decimal": ",",
            "thousands": ".",
            "lengthMenu": "Mostrar _MENU_ Registros por página",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
    });

    var menu = document.getElementById('menu').value;
    if(menu){
        var menub = document.getElementById(menu);
        menub.classList.add('active');
    }

    var seccion = document.getElementById('seccion').value;
    if(seccion){
        var menu = document.getElementById(seccion);
        menu.classList.add('active');
        menu.classList.remove('collapsed');
        menu.setAttribute('aria-expanded', true);
    }

    var subseccion = document.getElementById('subseccion').value;
    if(subseccion){
        var menua = document.getElementById(subseccion);
        menua.classList.add('collapse');
        menua.classList.add('show');
    }

    var enlace = document.getElementById('link').value;
    if(enlace){
        var menua = document.getElementById(enlace);
        menua.classList.add('active');
    }

    $('[data-toggle="popover"]').popover();
    $('[data-toggle="tooltip"]').tooltip();
    $('select').selectpicker();
    $('.datepicker').datepicker({
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
        format: 'dd-mm-yyyy',
        iconsLibrary: 'fontawesome',
    });
    $('.datepickerinput').datepicker({
        locale: 'es-es',
        uiLibrary: 'bootstrap4',
        format: 'dd-mm-yyyy',
    });
    $( "#sortable" ).sortable();
    $( "#sortable" ).disableSelection();
})

function confirmar(form, title, text) {
    Swal.fire({
        title: title,
        text: text,
        icon: 'question',
        showCancelButton: true,
        confirmButtonColor: '#1cc88a',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Aceptar',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            document.getElementById(form).submit();
        }
    })
}