<?php
 
namespace App\View\Composers;
 
use App\Models\State;
use Illuminate\View\View;
 
class StateComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('ests', State::orderBy('nombre', 'asc')->get());
    }
}