<?php
 
namespace App\View\Composers;
 
use App\Models\Company;
use Illuminate\View\View;
 
class CompanyComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('company', Company::first());
    }
}