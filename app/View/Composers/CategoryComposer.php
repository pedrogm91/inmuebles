<?php
 
namespace App\View\Composers;
 
use App\Models\CategoryProperty;
use Illuminate\View\View;
 
class CategoryComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('cats', CategoryProperty::orderBy('nombre', 'asc')->get());
    }
}