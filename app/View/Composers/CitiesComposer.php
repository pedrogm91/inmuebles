<?php
 
namespace App\View\Composers;
 
use App\Models\Location;
use Illuminate\View\View;
 
class CitiesComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('cius', Location::orderBy('nombre', 'asc')->get());
    }
}