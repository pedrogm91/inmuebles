<?php
 
namespace App\View\Composers;
 
use App\Models\Banners;
use Illuminate\View\View;
use Illuminate\Support\Facades\Storage;

 
class BannersComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $banners = Banners::first();
        if ($banners->banner_principal) {
            $banners->banner_principal = Storage::url($banners->banner_principal);
        } else{
            $banners->banner_principal = url('assets/images/1594226549_banner.jpg');
        }
        
        if ($banners->banner_mobile) {
            $banners->banner_mobile = Storage::url($banners->banner_mobile);
        } else{
            $banners->banner_mobile = url('assets/images/1592224948_habita-avila1-mobil.jpg');
        }
        
        $view->with('banners', $banners);
    }
}