<?php
 
namespace App\View\Composers;
 
use App\Models\Menus;
use Illuminate\View\View;
 
class MenusComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('menus', Menus::all());
    }
}