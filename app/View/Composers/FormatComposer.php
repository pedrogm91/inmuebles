<?php
 
namespace App\View\Composers;
 
use App\Models\Format;
use Illuminate\View\View;
 
class FormatComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('formats', Format::where('status', 'Habilitado')->get());
    }
}