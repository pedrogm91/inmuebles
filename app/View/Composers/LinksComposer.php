<?php
 
namespace App\View\Composers;

use Illuminate\Database\Eloquent\Builder;
use App\Models\Links;
use App\Models\Menus;
use Illuminate\View\View;
 
class LinksComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $navbar = Links::where('status', 'Habilitado')->wherehas('menu', function(Builder $query){
            return $query->where('nombre', 'Navbar');
        })->orderBy('ordering', 'asc')->get();
        
        $footer = Links::where('status', 'Habilitado')->wherehas('menu', function(Builder $query){
            return $query->where('nombre', 'footer');
        })->orderBy('ordering', 'asc')->get();
        
        $categories = Links::where('status', 'Habilitado')->wherehas('menu', function(Builder $query){
            return $query->where('nombre', 'categorias');
        })->orderBy('ordering', 'asc')->get();
        
        $type = Links::where('status', 'Habilitado')->wherehas('menu', function(Builder $query){
            return $query->where('nombre', 'tipo');
        })->orderBy('ordering', 'asc')->get();

        // Array
        $enlaces = [
            'links_navbar' => $navbar,
            'links_footer' => $footer,
            'links_categories' => $categories,
            'links_type' => $type,
        ];
        
        $view->with('enlaces', $enlaces);
    }
}