<?php

namespace App\Observers;
include_once('main_functions.php');
use App\Models\Citas;


class CitasObserver
{
    /**
     * Handle the Citas "created" event.
     *
     * @param  \App\Models\Citas  $citas
     * @return void
     */
    public function created(Citas $citas)
    {
        $module= 'Citas';
        $activity = 'Crear';
        observe_create($citas,$activity,$module);
    }

    /**
     * Handle the Citas "updated" event.
     *
     * @param  \App\Models\Citas  $citas
     * @return void
     */
    public function updated(Citas $citas)
    {
        $module= 'Citas';
        $activity = 'Actualizar';
        observe_update($citas,$activity,$module);
    }

    /**
     * Handle the Citas "deleted" event.
     *
     * @param  \App\Models\Citas  $citas
     * @return void
     */
    public function deleted(Citas $citas)
    {
        $module= 'Citas';
        $activity = 'Borrar';
       observe_delete($citas,$activity,$module);
    }

    /**
     * Handle the Citas "restored" event.
     *
     * @param  \App\Models\Citas  $citas
     * @return void
     */
    public function restored(Citas $citas)
    {
        //
    }

    /**
     * Handle the Citas "force deleted" event.
     *
     * @param  \App\Models\Citas  $citas
     * @return void
     */
    public function forceDeleted(Citas $citas)
    {
        //
    }
}
