<?php
use App\Models\Timeline;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;

function id($model)
{
    return $model->id_propiedad?$model->id_propiedad:$model->id;
}

function observe_create($model,$activity,$module)
{
    Timeline::create([
        'propiedades_id' => id($model),
        'user_id' =>Auth::user()->id,
        'modulo' => $module,
        'accion' => $activity,
        'modificaciones' => 'Creacion de '.$module,
        'fecha_modificacion' => now(),

    ]);
}

function observe_update($model,$activity,$module)
{
    $modificaciones ='';
    $valores = array_values($model->getChanges());
    $campos = array_keys($model->getChanges());
    
    for ($i=0; $i <count($campos) ; $i++) { 
       $modificaciones .= ''.$campos[$i].': '.$valores[$i].' , ';
    }

     Timeline::create([
        'propiedades_id' => id($model),
        'user_id' =>Auth::user()->id,
        'modulo' => $module,
        'accion' => $activity,
        'modificaciones' => $modificaciones,
        'fecha_modificacion' => Carbon::now(),

    ]);
}

function observe_delete($model,$activity,$module)
{
    Timeline::create([
        'propiedades_id' => id($model),
        'user_id' =>Auth::user()->id,
        'modulo' => $module,
        'accion' => $activity,
        'modificaciones' => 'Eliminacion de '.$module,
        'fecha_modificacion' => now(),
    ]);
}