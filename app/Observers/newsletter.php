<?php

namespace App\Observers;

class newsletter
{
    public function created(Ofertas $ofertas)
    {
        $module="Newsletter";
        $activity="Registrado Nuevo Usuario";
        observe_create($ofertas,$activity,$module);
    }
}
