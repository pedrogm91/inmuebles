<?php

namespace App\Observers;

include_once('main_functions.php');

use App\Models\Ofertas;

class OfertasObserver
{
    /**
     * Handle the Ofertas "created" event.
     *
     * @param  \App\Models\Ofertas  $ofertas
     * @return void
     */
    public function created(Ofertas $ofertas)
    {
        $module="Ofertas";
        $activity="Creacion de Oferta";
        observe_create($ofertas,$activity,$module);
    }

    /**
     * Handle the Ofertas "updated" event.
     *
     * @param  \App\Models\Ofertas  $ofertas
     * @return void
     */
    public function updated(Ofertas $ofertas)
    {
        $module="Ofertas";
        $activity="Actualizacion";
        observe_update($ofertas,$activity,$module);
    }

    /**
     * Handle the Ofertas "deleted" event.
     *
     * @param  \App\Models\Ofertas  $ofertas
     * @return void
     */
    public function deleted(Ofertas $ofertas)
    {
        $module="Ofertas";
        $activity="Eliminacion de la Oferta";
        observe_delete($ofertas,$activity,$module);
    }

    /**
     * Handle the Ofertas "restored" event.
     *
     * @param  \App\Models\Ofertas  $ofertas
     * @return void
     */
    public function restored(Ofertas $ofertas)
    {
        //
    }

    /**
     * Handle the Ofertas "force deleted" event.
     *
     * @param  \App\Models\Ofertas  $ofertas
     * @return void
     */
    public function forceDeleted(Ofertas $ofertas)
    {
        //
    }
}
