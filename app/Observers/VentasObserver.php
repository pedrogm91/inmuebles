<?php

namespace App\Observers;

include_once('main_functions.php');

use App\Models\Ventas;

class VentasObserver
{
    /**
     * Handle the Ventas "created" event.
     *
     * @param  \App\Models\Ventas  $ventas
     * @return void
     */
    public function created(Ventas $ventas)
    {
        observe_create($ventas);
    }

    /**
     * Handle the Ventas "updated" event.
     *
     * @param  \App\Models\Ventas  $ventas
     * @return void
     */
    public function updated(Ventas $ventas)
    {
        observe_update($ventas);
    }

    /**
     * Handle the Ventas "deleted" event.
     *
     * @param  \App\Models\Ventas  $ventas
     * @return void
     */
    public function deleted(Ventas $ventas)
    {
        observe_delete($ventas);
    }

    /**
     * Handle the Ventas "restored" event.
     *
     * @param  \App\Models\Ventas  $ventas
     * @return void
     */
    public function restored(Ventas $ventas)
    {
        //
    }

    /**
     * Handle the Ventas "force deleted" event.
     *
     * @param  \App\Models\Ventas  $ventas
     * @return void
     */
    public function forceDeleted(Ventas $ventas)
    {
        //
    }
}
