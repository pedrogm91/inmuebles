<?php

namespace App\Observers;

include_once('main_functions.php');

use App\Models\Properties;

class PropertyObserver
{
    /**
     * Handle the Properties "created" event.
     *
     * @param  \App\Models\Properties  $properties
     * @return void
     */
    public function created(Properties $properties)
    {
        $activity = 'Creacion';
        $module = 'Propiedades';
        observe_create($properties,$activity,$module);
    }

    /**
     * Handle the Properties "updated" event.
     *
     * @param  \App\Models\Properties  $properties
     * @return void
     */
    public function updated(Properties $properties)
    {
        $activity = 'Actualizacion';
        $module = 'Propiedades';
        observe_update($properties,$activity,$module);
    }

    /**
     * Handle the Properties "deleted" event.
     *
     * @param  \App\Models\Properties  $properties
     * @return void
     */
    public function deleted(Properties $properties)
    {
        $activity = 'Eliminacion';
        $module = 'Propiedades';
        observe_delete($properties,$activity,$module);
    }

    /**
     * Handle the Properties "restored" event.
     *
     * @param  \App\Models\Properties  $properties
     * @return void
     */
    public function restored(Properties $properties)
    {
        //
    }

    /**
     * Handle the Properties "force deleted" event.
     *
     * @param  \App\Models\Properties  $properties
     * @return void
     */
    public function forceDeleted(Properties $properties)
    {
        //
    }
}
