<?php

namespace App\Observers;

use App\Models\Negotiation;

class NegotiationObserver
{
    /**
     * Handle the Negotiation "created" event.
     *
     * @param  \App\Models\Negotiation  $negotiation
     * @return void
     */
    public function created(Negotiation $negotiation)
    {
        $module= 'Negociacion';
        $activity = 'Crear';
        observe_create($negotiation,$activity,$module);
    }

    /**
     * Handle the Negotiation "updated" event.
     *
     * @param  \App\Models\Negotiation  $negotiation
     * @return void
     */
    public function updated(Negotiation $negotiation)
    {
        $module= 'Negociacion';
        $activity = 'Actualizar';
        observe_create($negotiation,$activity,$module);
    }

    /**
     * Handle the Negotiation "deleted" event.
     *
     * @param  \App\Models\Negotiation  $negotiation
     * @return void
     */
    public function deleted(Negotiation $negotiation)
    {
        $module= 'Negociacion';
        $activity = 'Actualizar';
        observe_create($negotiation,$activity,$module);
    }

    /**
     * Handle the Negotiation "restored" event.
     *
     * @param  \App\Models\Negotiation  $negotiation
     * @return void
     */
    public function restored(Negotiation $negotiation)
    {
        $module= 'Negociacion';
        $activity = 'Restaurar';
        observe_create($negotiation,$activity,$module);
    }

    /**
     * Handle the Negotiation "force deleted" event.
     *
     * @param  \App\Models\Negotiation  $negotiation
     * @return void
     */
    public function forceDeleted(Negotiation $negotiation)
    {
        $module= 'Negociacion';
        $activity = 'Eliminar';
        observe_create($negotiation,$activity,$module);
    }
}
