<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProspectUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres_apellidos' => 'required | string',
            'cedula' => 'required | string',
            'telefono' => 'string',
            'correo' =>  'required', Rule::unique('prospects')->ignore($this->id),
        ];
    }
}
