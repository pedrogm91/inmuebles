<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadActivityCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombres_apellidos' => 'required | string',
            'telefono' => 'required',
            'correo' => 'required | email',
        ];
    }
}
