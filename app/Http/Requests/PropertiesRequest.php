<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PropertiesRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre_propiedad'     => 'required',
            'estatus_publicacion'  => 'required',
            'tipo_negocio'         => 'required',
            'id_categoria'         => 'required',
            'precio'               => 'required', 'numeric',
            'id_estado'            => 'required',
            'id_municipio'         => 'required',
            //'id_parroquia'         => 'required',
            // 'id_ciudad'            => 'required',
            //'ubicacion'            => 'required',

            'area_terreno'         => 'required',
            'area_construccion'    => 'required',
            'antiguedad'           => 'required',
            'cocina'               => 'required',
            'sala'                 => 'required',
            'habit'                => 'required',
            'habit_s'              => 'required',
            'banos'                => 'required',
            'banos_s'              => 'required',
            'bano_medio'           => 'required',
            'aire_a'               => 'required',
            'tv'                   => 'required',
            'wifi'                 => 'required',
            'parrillera'           => 'required',
            'piscina'              => 'required',
            'maletero'             => 'required',
            'lavadero'             => 'required',
            'gimnasio'             => 'required',
            'estacionamiento'      => 'required',
            'tipo_estacionamiento' => 'required',
            'descripcion'          => 'required',

            'supermercado'         => 'required',
            'clinica'              => 'required',
            'farmacia'             => 'required',
            'colegios'             => 'required',
            'estacion_metro'       => 'required',
            'autobus'              => 'required',
            'centro_comercial'     => 'required',

            'nombre_propietario'   => 'required',
            'tel_propietario'      => 'required',
            'email_propietario'    => 'required',

            'id_asesor_asignado'   => 'required',
            'destacado'            => 'required'
        ];
    }
}
