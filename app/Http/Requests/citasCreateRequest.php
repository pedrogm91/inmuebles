<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class citasCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'fecha_cita' => 'required | date',
            'hora_cita' => 'required | string',
            'nombre_cliente' => 'required | string',
            'telefono_cliente' => 'required | string',
            'correo_cliente' => 'required | string',

            // 'id_prospect' => 'required',
            // 'telefono_prospect' => 'required | string',
            // 'correo_prospect' => 'required | string',

            'id_asesor' => 'required',
            'id_propiedad' => 'required',
            'asunto' => 'required | string',
            'descripcion' => 'required | string',
            'estatus' => 'required | string',
            'prioridad' => 'string',
            'observaciones' => 'string'
        ];
    }
}
