<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class adviserCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id_usuario' => 'required',
            'nombres_apellidos' => 'required|string|min:2|max:100' ,
            'cedula' => 'required|string|min:6|max:9' ,
            'telefono' => 'required',
            'telefono_movil' => 'required',
            'correo' => 'required|email', Rule::unique('users'),
            'direccion' => 'required',
            'area_de_trabajo' => 'required',
            'tipo_de_contrato' => 'required',
            'vehiculo' => 'required',
            'lugar_de_trabajo' => 'required',
            'estatus' => 'required',
            'porcentaje_ganancia' => 'required',
            'experiencia' => 'required',
            'recomendado' => 'required',
            'total_percibido' => 'required',
            'imagen_perfil' => 'required',
        ];
    }
}
