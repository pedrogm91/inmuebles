<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class LeadActivityUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'prospect_id'=> 'numeric',
            'interes' => 'required | string',
            'datos_adicionales' => 'required | string',
        ];
    }
}
