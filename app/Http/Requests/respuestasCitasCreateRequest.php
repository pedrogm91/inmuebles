<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class respuestasCitasCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'citas_id' => 'required',
            'asesores_inmobiliarios_id' => 'required',
            'texto' => 'required',
        ];
    }
}
