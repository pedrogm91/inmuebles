<?php
 
namespace App\Http\Middleware;
use Closure;
use Illuminate\Support\Facades\Auth;
 
class checkRoles
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        if (Auth::check()) {
            if(in_array($request->user()->role, explode("-", $role))){
                return $next($request);
            }
            return redirect()->back()->with('error', 'No tiene los privilegios suficientes para realizar esta acción o ingresar al módulo.');
        }
        
        return redirect()->route('login');
    }
}