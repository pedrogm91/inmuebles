<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;
use App\Http\Requests\FormatRequest;
use App\Models\Format;

class FormatsController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Formatos',
            'modulo'     => 'administracion',
            'seccion'    => 'formats',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.formats.index',
            [
                'formats' => Format::latest()->filter(request(['search']))->paginate(10)
            ]
        );
    }

    public function list()
    {
        view()->share([
            'title'      => 'Formatos',
            'modulo'     => 'asesores',
            'seccion'    => 'formats_asesores',
        ]);
        return view('admin.formats.list',
            [
                'formats' => Format::where('status', 'Habilitado')->get()
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.formats.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(FormatRequest $request)
    {
        $format = Format::create([
            'name'   => $request->name,
            'route'  => $request->route->store('formatos','public'),
            'status' => $request->status
        ]);

        if($format){
            return redirect()->route('formats.index')->with('success', 'Formato registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Format $format)
    {
        return view('admin.formats.show', [
            'format' => $format
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Format $format)
    {
        return view('admin.formats.edit', [
            'format' => $format
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(FormatRequest $request, $format)
    {
        $query = Format::where('id', $format)->update([
            'name'   => $request->name,
            'status' => $request->status
        ]);

        if($request->hasFile('route')){
            $query = Format::find($format);
            $query->route = $request->route->store('formatos','public');
            $query->save();
        }

        if($query){
            return redirect()->route('formats.index')->with('success', 'Formato actualizado.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Format $format)
    {
        $format->delete();
        return redirect()->route('formats.index')->with('success', 'Formato Eliminado.');
    }
}
