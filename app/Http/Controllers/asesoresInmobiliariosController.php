<?php

namespace App\Http\Controllers;

use App\Http\Requests\adviserCreateRequest;
use App\Http\Requests\adviserUpdateRequest;

use App\Models\asesoresInmobiliarios;
use App\Models\propiedades;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

class asesoresInmobiliariosController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Asesores',
            'modulo'     => 'asesores',
            'seccion'    => 'asesores',
        ]);
    }
    
        /**
     * devuelve vista con un listado de todos los asesores registrados
     */
    public function index()
    {
        return view('admin/advisers/index',
        [ 'asesores' => asesoresInmobiliarios::latest()->filter(request(['search']))->paginate(5)]);
    }

    /**
     * devuelve vista con los detalles del asesor
     */
    public function show(asesoresInmobiliarios $id)
    {
        return view('admin/advisers/show',['asesor'=> $id]);
    }

    /**
     * verificamos que el id ingresado sea valido y no este en uso
     * devuelve el vista del formulario para registrar a un asesor
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function create($user_id = false)
    {
        // comprobamos que los usuarios disponibles no tengan una relación con asesores
        $available_users = User::doesntHave('asesoresInmobiliarios')->get();
        
        if($user_id)
        {
            
            //verificamos si el usuario es valido y si no tiene perfil de asesor, para evitar errores fuera del flujo normal.
            if(!($user=User::where('id',$user_id)->first()) || (asesoresInmobiliarios::where('id_usuario',$user_id)->first()))
            {
                //de cumplirse alguna de las 2 regresamos con error
                return redirect()->route('advisers.index')->with('error', 'Usuario inexistente o Ya esta registrado como asesor.');
            }
            else
            {
                return view('admin/advisers/create', ['user' =>$user,'users' => $available_users]);
            }
        }
        else
        {
                return view('admin/advisers/create', ['users' => $available_users]);
        }
    
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(adviserCreateRequest $request)
    {
        /**filtros para datos obligatorios
         * guardar datos
         * regresar al index
         */

 
        //asignamos los datos  del nuevo asesor
        $asesor = asesoresInmobiliarios::create([
            'id_usuario' => $request->id_usuario,
            'nombres_apellidos' => $request->nombres_apellidos ,
            'cedula' => $request->cedula ,
            'telefono' => $request->telefono,
            'telefono_movil' => $request->telefono_movil,
            'correo' => $request->correo,
            'fecha_nacimiento' => $request->fecha_nacimiento,
            'direccion' => $request->direccion,
            'area_de_trabajo' => $request->area_de_trabajo,
            'tipo_de_contrato' => $request->tipo_de_contrato,
            'vehiculo' => $request->vehiculo,
            'lugar_de_trabajo' => $request->lugar_de_trabajo,
            'porcentaje_ganancia' => $request->porcentaje_ganancia,
            'total_percibido' => $request->total_percibido,
            'recomendado' => $request->recomendado,
            'estatus' => $request->estatus,
            'experiencia' => $request->experiencia,
            'imagen_perfil' => null,
        ]);
        //verificamos si subieron imagen de perfil
        if($request->hasFile('imagen_perfil')){
            $asesor = asesoresInmobiliarios::find($asesor->id);
            $asesor->imagen_perfil = $request->imagen_perfil->store('imagen_perfil','public');
            $asesor->save();
        }
        $RegisteredUser = User::find($asesor->id_usuario);
        if(!$RegisteredUser->role=='admin' || !$RegisteredUser->role=='director')
        {
            $RegisteredUser->update(['role'=>'adviser']);
            //retornamos al index
            return redirect()->route('advisers.index')->with('success', 'Asesor registrado.');
        }
        return redirect()->route('advisers.index')->with('success', 'Asesor registrado.');
    }
    
    public function edit(asesoresInmobiliarios $id)
    {
        return view('admin/advisers/edit',['asesor'=> $id]);
    }

    public function update(adviserUpdateRequest $request, $id)
    {
        $data = $request->only([
            'nombres_apellidos',
            'cedula',
            'telefono',
            'telefono_movil',
            'correo',
            'direccion',
            'area_de_trabajo',
            'tipo_de_contrato',
            'vehiculo',
            'lugar_de_trabajo',
            'porcentaje_ganancia',
            'total_percibido',
            'recomendado',
            'estatus',
            'experiencia'
        ]);
        asesoresInmobiliarios::where('id',$id)->update($data);

        //verificamos si subieron imagen de perfil
        if($request->hasFile('imagen_perfil')){
            $asesor = asesoresInmobiliarios::find($id);
            $asesor->imagen_perfil = $request->imagen_perfil->store('imagen_perfil','public');
            $asesor->save();
        }
        return redirect()->route('advisers.index')->with('success', 'Asesor Actualizado.');
    }

        /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asesor = asesoresInmobiliarios::find($id);
        // No eliminar asesor si tiene propiedades
        if ($asesor->propiedades->isNotEmpty()) {
            return redirect()->route('advisers.show', $id)->with('warning', 'No puedes eliminar una asesor antes de Re-asignar las propiedades a otro asesor.');
        }
        //Cambiamos el rol al usuario a 'user'
        User::where('id', $asesor->id_usuario)->update(['role' => 'user']);

        $asesor->delete();
        return redirect()->route('advisers.index', $id)->with('success', 'Perfil de asesor Eliminado.');
    }


    /**
     * toma el id del asesor y devuelve las propiedades relacionadas
     * @param int $id
     * @return \Illuminate\Http\Response 
     */
    public function propiedades(asesoresInmobiliarios $adviser)
    {
        /**
         * tomar el id ingresado y
         * devolver propiedades relacionadas
         */
        $propiedades_asociadas = propiedades::find()->where('id_asesor',$adviser);
        return $propiedades_asociadas;
    }

   
}
