<?php

namespace App\Http\Controllers;

use App\Http\Requests\citasCreateRequest;
use App\Http\Requests\citasUpdateRequest;
use Illuminate\Http\Request;
use App\Models\asesoresInmobiliarios;
use App\Models\citas;
use App\Models\Properties;
use App\Models\propiedades;
use App\Models\Prospect;

class CitasController extends Controller
{
    protected $citas;
    public function __construct(Citas $citas)
    {
        $this->citas = $citas;
        view()->share([
            'title'      => 'Meetings',
            'modulo'     => 'inmuebles',
            'seccion'    => 'meetings',
        ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role=='adviser')
        {
            return view('admin/meetings/index',[
                'citas_programadas' => citas::where('id_asesor',auth()->user()->asesoresInmobiliarios->id)->orderByDesc('fecha_cita')->paginate(10)
            ]);
        }
        if(auth()->user()->role=='admin' || auth()->user()->role=='director')
        {
            return view('admin/meetings/index',[
                'citas_programadas' => $this->citas->citasProgramadas()->orderByDesc('fecha_cita')->paginate(10)
            ]);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('/admin/meetings/create', [
            // 'clientes' => User::all(),
            'asesores' => asesoresInmobiliarios::where('estatus','activo')->get(),
            'propiedades' => propiedades::get(),

        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(citasCreateRequest $request)
    {
        $fecha_hora = $request->fecha_cita . $request->hora_cita;
        $request->fecha_cita = date('Y-m-d H:i:s', strtotime($fecha_hora));
        $data = citas::create([
            'fecha_cita' => $request->fecha_cita,
            'hora_cita' => $request->hora_cita,
            'nombre_cliente' => $request->nombre_cliente,
            'telefono_cliente' => $request->telefono_cliente,
            'correo_cliente' => $request->correo_cliente,
            'id_asesor' => $request->id_asesor,
            'id_propiedad' => $request->id_propiedad,
            'asunto' => $request->asunto,
            'descripcion' => $request->descripcion,
            'estatus' => $request->estatus,
            'prioridad' => $request->prioridad,
            'observaciones' => $request->observaciones,
        ]);
        $data->url = '/meetings/' . $data->id;
        $data->update();
        return redirect()->route('meetings.index')->with('success', 'cita registrada con exito');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cita = citas::find($id)->load(['asesor', 'respuestasCitas.asesores', 'property']);
        $current_adviser = null;

        // obtenemos el id del asesor auth() para usarlo al momento de crear una nueva respuesta.
        if (auth()->user()->asesoresInmobiliarios) {
            $current_adviser = auth()->user()->asesoresInmobiliarios->id;
        }

        return view('/admin/meetings/show',
        [
            'datos_cita' => $cita,
            'current_adviser' => $current_adviser,
        ]);

        // return view(
        //     '/admin/meetings/show',
        //     [
        //         'datos_cita' => $this->citas->datos($id),
        //         'respuestas' => $this->citas::find($id->id)->respuestasCitas,
        //     ]
        // );

    }

    public function calendar_view()
    {
        return view('/admin/meetings/calendar');
    }
    public function calendardata(Request $request)
    {
        return $this->citas->calendario($request);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(citas $id)
    {
        $asesores = asesoresInmobiliarios::get();
        $prospect = Prospect::get();
        $propiedades = Properties::get();

        return view(
            '/admin/meetings/edit',
            [
                'datos_cita' => $id,
                'datos_asesores' => $asesores,
                'datos_propiedades' => $propiedades,
                'datos_prospect' => $prospect
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(citasUpdateRequest $request, citas $id)
    {

        $id->update($request->all());
        return redirect()->route('meetings.index')->with('success', 'Cita Actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $cita = citas::find($id);
        $cita->delete();
        return redirect()->route('meetings.index')->with('success', 'Cita Eliminada.');
    }
}
