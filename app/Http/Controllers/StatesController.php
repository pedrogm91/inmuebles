<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StateRequest;

use App\Models\State;

class StatesController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Estados',
            'modulo'     => 'administracion',
            'seccion'    => 'localidades',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.states.index',
        ['states' => State::latest()->orderBy('nombre','asc')->filter(request(['search']))->paginate(25)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.states.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StateRequest $request)
    {
        $state = State::create([
            'nombre'    => $request->nombre,
            'capital'    => $request->capital,
        ]);

        if($state){
            return redirect()->route('states.index')->with('success', 'Estado registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function municipalities(State $state)
    {
        return view('admin.states.municipalities', ['state'=> $state]);
    }

    public function state_cities(State $state)
    {
        return view('admin.states.cities', ['state' => $state]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function edit(State $state)
    {
        return view('admin.states.edit', ['state'=> $state]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function update(StateRequest $request, $state)
    {
        $data = array_filter($request->only(['nombre', 'capital']));
        State::where('id', $state)->update($data);
        return redirect()->route('states.index')->with('success', 'Estado actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function destroy(State $state)
    {
        if ($state->municipalities->isNotEmpty() && $state->cities->isNotEmpty())
        {
            return redirect()->back()->with('warning', 'No puedes eliminar un Estado que tiene Municipios o Ciudades.');
        }

        $state->delete();
        return redirect()->route('states.index')->with('success', 'Estado eliminado.');
    }
}
