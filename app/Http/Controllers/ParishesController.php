<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ParishRequest;

use App\Models\Parish;
use App\Models\Municipality;

class ParishesController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Parroquias',
            'modulo'     => 'administracion',
            'seccion'    => 'localidades',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.parishes.index',
        ['parishes' => Parish::latest()->orderBy('nombre','asc')->filter(request(['search']))->paginate(5)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Municipality $municipality)
    {
        return view('admin.parishes.create', ['municipality' => $municipality]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ParishRequest $request)
    {
        $parish = Parish::create([
            'nombre'       => $request->nombre,
            'id_municipio' => $request->id_municipio
        ]);

        if($parish){
            return redirect()->route('municipalities.show', $request->id_municipio)->with('success', 'Parroquia o Urbanizacion registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function show(Parish $parish)
    {
        return view('admin.parishes.show', ['parish'=> $parish]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function edit(Parish $parish)
    {
        return view('admin.parishes.edit', ['parish'=> $parish]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function update(ParishRequest $request, $parish)
    {
        $data = array_filter($request->only(['nombre']));
        Parish::where('id', $parish)->update($data);
        return redirect()->route('municipalities.show', Parish::find($parish)->id_municipio)->with('success', 'Parroquia o Urbanización actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function destroy(Parish $parish)
    {
        $municipality = $parish->id_municipio;
        $parish->delete();
        return redirect()->route('municipalities.show', $municipality)->with('success', 'Parroquia o Urbanización eliminada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function json($municipality)
    {
        return response()->json(['success' => true, 'parishes' => Parish::where('id_municipio', $municipality)->get()]);
    }
}
