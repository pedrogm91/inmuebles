<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\respuestasCitasCreateRequest;
use App\Models\respuestasCitas;

class respuestasCitasController extends Controller
{
    public function store(respuestasCitasCreateRequest $request)
    {
        respuestasCitas::create([
            'citas_id' => $request->citas_id,
            'asesores_inmobiliarios_id' => $request->asesores_inmobiliarios_id,
            'texto' => $request->texto,
        ]);

        return redirect()->route('meetings.show',$request->citas_id)->with('success','Respuesta Agregada');
    }
}
