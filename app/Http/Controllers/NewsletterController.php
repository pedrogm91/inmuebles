<?php

namespace App\Http\Controllers;

use App\Http\Requests\newsletterCreateRequest;
use App\Models\Newsletter as ModelsNewsletter;
use App\Observers\newsletter;
use Illuminate\Http\Request;

class NewsletterController extends Controller
{
    public function index()
    {
        return 'funciona';
    }


    public function store(newsletterCreateRequest $request)
    {
        ModelsNewsletter::create([
            'nombre' => $request->nombre,
            'correo_cliente' => $request->correo_cliente,
        ]);
        return back()->with('success','Cliente agregado al Newsletter');
    }
}
