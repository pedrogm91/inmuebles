<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Requests\CategoryPropertyRequest;
use App\Models\CategoryProperty;

class CategoryPropertyController extends Controller
{

    public function __construct()
    {
        view()->share([
            'title'      => 'Tipos de Inmuebles',
            'modulo'     => 'inmuebles',
            'seccion'    => 'types_property',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.category_property.index',
        ['categories' => CategoryProperty::latest()->orderBy('nombre','asc')->filter(request(['search']))->paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category_property.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryPropertyRequest $request, CategoryProperty $category)
    {
        $category->create([
            'nombre' => $request->nombre,
        ]);

        if($category){
            return redirect()->route('types-property.index')->with('success', 'Tipo de Inmueble Registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CategoryProperty  $category
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryProperty $category)
    {
        return view('admin.category_property.show', [
            'category' => $category
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CategoryProperty  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryProperty $category)
    {
        return view('admin.category_property.edit', [
            'category' => $category
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CategoryProperty  $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryPropertyRequest $request, CategoryProperty $category)
    {
        $category->update([
            'nombre' => $request->nombre
        ]);

        if($category){
            return redirect()->route('types-property.index')->with('success', 'Tipo de Inmueble Actualizado.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CategoryProperty  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryProperty $category)
    {
        $category->delete();
        return redirect()->route('types-property.index')->with('success', 'Tipo de Inmueble Eliminado.');
    }
}
