<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CompanyRequest;
use File;
use Illuminate\Support\Facades\Storage;
use DB;
use App\Models\Company;
use App\Models\Banners;
use App\Models\Menus;
use App\Models\Links;

class ContentsController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'   => 'Gestor de Contenido',
            'modulo'  => 'administracion',
            'seccion' => 'contents'
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($opt = 'index')
    {
        switch ($opt) {
            case 'company':
                view()->share(['title' => 'Empresa']);
                return view('admin.contents.'.$opt,
                    ['company' => Company::first()]);
                break;
            case 'logos':
                view()->share(['title' => 'Logos']);
                return view('admin.contents.'.$opt,
                    ['company' => Company::first()]);
                break;
            case 'banners':
                view()->share(['title' => 'Banners']);
                return view('admin.contents.'.$opt,
                    ['company' => Banners::first()]);
                break;
            case 'menus':
                view()->share(['title' => 'Menús']);
                return view('admin.contents.'.$opt,
                    ['menus' => Menus::get()]);
                break;
            case 'links':
                view()->share(['title' => 'Enlaces']);
                return view('admin.contents.'.$opt,
                    ['links' => Links::all()]);
                break;
            default:
                return view('admin.contents.'.$opt);
            break;
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_company(CompanyRequest $request)
    {
        $empresa = Company::where('id', $request->id)->update([
            'nombre'    => $request->nombre,
            'rif'       => $request->rif,
            'telefono'  => $request->telefono,
            'celular'   => $request->celular,
            'fax'       => $request->fax,
            'direccion' => $request->direccion,
            'correo'    => $request->correo,
            'horario'   => $request->horario,
            'whatsapp'  => $request->whatsapp,
            'twitter'   => $request->twitter,
            'facebook'  => $request->facebook,
            'instagram' => $request->instagram,
            'youtube'   => $request->youtube,
            'linkedin'  => $request->linkedin,
        ]);

        if($request->hasFile('logo')){
            $empresa = Company::find($request->id);
            $empresa->logo = $request->logo->store('company','public');
            $empresa->save();
        }

        if($empresa){
            return redirect()->route('contents.index')->with('success', 'Información de la Empresa Actualizada.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_logos(Request $request)
    {
        if($request->hasFile('logo')){
            $empresa = Company::find($request->id);
            $empresa->logo = $request->logo->store('company','public');
            $empresa->save();
        }else{
            return redirect()->route('contents.index')->with('danger', 'No ha cargado ningún archivo de imagen.');
        }

        if($empresa){
            return redirect()->route('contents.index')->with('success', 'Logo de la Empresa Actualizado.');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update_banners(Request $request)
    {
        $empresa = Banners::find($request->id);


        if($request->hasFile('banner_principal')){
            $empresa->banner_principal = $request->banner_principal->store('company','public');
        }

        if($request->hasFile('banner_mobile')){
            $empresa->banner_mobile = $request->banner_mobile->store('company','public');
        }

        if($request->slogan){
            $empresa->slogan = $request->slogan;
        }else{
            $empresa->slogan = null;
        }

        $empresa->save();

        if($empresa){
            return redirect()->route('contents.index')->with('success', 'Banners de la Empresa Actualizados.');
        }
    }

    ## MENUS ##

    public function create_menus()
    {
        return view('admin.contents.create_menu');
    }

    public function store_menus(Request $request, Menus $menu)
    {
        $menu->create([
            'nombre' => $request->nombre,
            'status' => $request->status,
        ]);

        if($menu){
            return redirect()->route('contents.index', 'menus')->with('success', 'Menú Registrado.');
        }
    }

    public function edit_menus(Menus $menu)
    {
        return view('admin.contents.edit_menu', [
            'menu' => $menu
        ]);
    }

    public function show_menus(Menus $menu)
    {
        return view('admin.contents.show_menu', [
            'menu' => $menu,
            'links' => Links::where('id_menu', $menu->id)->orderBy('ordering', 'asc')->get()
        ]);
    }

    public function update_menus(Request $request, Menus $menu)
    {
        $menu->update([
            'nombre' => $request->nombre,
            'status' => $request->status,
        ]);

        if($menu){
            return redirect()->route('contents.index', 'menus')->with('success', 'Menú Actualizado.');
        }
    }

    public function destroy_menus(Menus $menu)
    {
        $menu->delete();
        return redirect()->route('contents.index', 'menus')->with('success', 'Menú Eliminado.');
    }

    ## ENLACES ##

    public function create_links()
    {
        return view('admin.contents.create_link',[
            'menus' => Menus::where('status', 'Habilitado')->get()
        ]);
    }

    public function store_links(Request $request, Links $link)
    {
        $link->create([
            'id_menu'  => $request->id_menu,
            'nombre'   => $request->nombre,
            'route'    => $request->route,
            'ordering' => $request->ordering,
            'status'   => $request->status,
        ]);

        if($link){
            return redirect('contents/menus/'.$link->get()->last()->id_menu)->with('success', 'Enlace Registrado.');
        }
    }

    public function edit_links(Links $link)
    {
        return view('admin.contents.edit_link', [
            'link'  => $link,
            'menus' => Menus::where('status', 'Habilitado')->get()
        ]);
    }

    public function update_links(Request $request, Links $link)
    {
        $link->update([
            'id_menu'  => $request->id_menu,
            'nombre'   => $request->nombre,
            'route'    => $request->route,
            'ordering' => $request->ordering,
            'status'   => $request->status,
        ]);

        if($link){
            return redirect('contents/menus/'.$link->id_menu)->with('success', 'Enlace Actualizado.');
        }
    }

    public function destroy_links(Links $link)
    {
        $id_menu = $link->id_menu;
        $link->delete();
        return redirect('contents/menus/'.$id_menu)->with('success', 'Enlace Eliminado.');
    }
}
