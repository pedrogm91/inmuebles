<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Requests\PropertiesRequest;
use DB;
use Illuminate\Support\Facades\Storage;

use App\Models\Properties;
use App\Models\State;
use App\Models\Municipality;
use App\Models\Parish;
use App\Models\Location;
use App\Models\CategoryProperty;
use App\Models\asesoresInmobiliarios;

class PropertiesController extends Controller
{

    protected $properties;

    public function __construct(Properties $properties)
    {
        $this->properties = $properties;

        view()->share([
            'title'      => 'Inmuebles',
            'modulo'     => 'inmuebles',
            'seccion'    => 'inmuebles',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        if (auth()->user()->role == 'admin' || auth()->user()->role == 'director') {
            $properties = Properties::latest()->filterAdmin(request(['search']))->paginate(10);
        }else {
            $current_adviser = auth()->user()->asesoresInmobiliarios->id;
            $properties = Properties::where('id_asesor_asignado', $current_adviser)->latest()->filterAdmin(request(['search']))->paginate(10);
        }

        return view('admin/properties/index',
            [
                'properties' =>  $properties,
                'advisers' => asesoresInmobiliarios::where('estatus', 'activo')->orderBy('nombres_apellidos', 'ASC')->get(),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.properties.create', [
            'advisers'=> asesoresInmobiliarios::where('estatus', 'activo')->get(),
            'states'=> State::orderBy('nombre')->get(),
            'categories'=> CategoryProperty::orderBy('nombre')->get(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PropertiesRequest $request, Properties $property)
    {
        $datos_propietario = [];
        $datos_propietario = Arr::add($datos_propietario, 'nombre_propietario', $request->nombre_propietario);
        $datos_propietario = Arr::add($datos_propietario, 'tel_propietario', $request->tel_propietario);
        $datos_propietario = Arr::add($datos_propietario, 'email_propietario', $request->email_propietario);

        $datos_apoderado = [];
        $datos_apoderado = Arr::add($datos_apoderado, 'nombre_apoderado', $request->nombre_apoderado);
        $datos_apoderado = Arr::add($datos_apoderado, 'tel_apoderado', $request->tel_apoderado);
        $datos_apoderado = Arr::add($datos_apoderado, 'email_apoderado', $request->email_apoderado);

        $instalaciones_cercanas = [];
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'supermercado', $request->supermercado);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'clinica', $request->clinica);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'farmacia', $request->farmacia);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'colegios', $request->colegios);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'estacion_metro', $request->estacion_metro);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'autobus', $request->autobus);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'centro_comercial', $request->centro_comercial);

        $datos_construccion = [];
        $datos_construccion = Arr::add($datos_construccion, 'area_terreno', $request->area_terreno);
        $datos_construccion = Arr::add($datos_construccion, 'area_construccion', $request->area_construccion);
        $datos_construccion = Arr::add($datos_construccion, 'antiguedad', $request->antiguedad);
        $datos_construccion = Arr::add($datos_construccion, 'cocina', $request->cocina);
        $datos_construccion = Arr::add($datos_construccion, 'sala', $request->sala);
        $datos_construccion = Arr::add($datos_construccion, 'habit', $request->habit);
        $datos_construccion = Arr::add($datos_construccion, 'habit_s', $request->habit_s);
        $datos_construccion = Arr::add($datos_construccion, 'banos', $request->banos);
        $datos_construccion = Arr::add($datos_construccion, 'banos_s', $request->banos_s);
        $datos_construccion = Arr::add($datos_construccion, 'bano_medio', $request->bano_medio);
        $datos_construccion = Arr::add($datos_construccion, 'aire_a', $request->aire_a);
        $datos_construccion = Arr::add($datos_construccion, 'tv', $request->tv);
        $datos_construccion = Arr::add($datos_construccion, 'wifi', $request->wifi);
        $datos_construccion = Arr::add($datos_construccion, 'parrillera', $request->parrillera);
        $datos_construccion = Arr::add($datos_construccion, 'piscina', $request->piscina);
        $datos_construccion = Arr::add($datos_construccion, 'maletero', $request->maletero);
        $datos_construccion = Arr::add($datos_construccion, 'lavadero', $request->lavadero);
        $datos_construccion = Arr::add($datos_construccion, 'gimnasio', $request->gimnasio);
        $datos_construccion = Arr::add($datos_construccion, 'estacionamiento', $request->estacionamiento);
        $datos_construccion = Arr::add($datos_construccion, 'tipo_estacionamiento', $request->tipo_estacionamiento);
        $datos_construccion = Arr::add($datos_construccion, 'descripcion', $request->descripcion);

        $property->create([
            'nombre_propiedad'       => $request->nombre_propiedad,
            'estatus_publicacion'    => $request->estatus_publicacion,
            'tipo_negocio'           => $request->tipo_negocio,
            'id_categoria'           => $request->id_categoria,
            'precio'                 => $request->precio,
            'id_estado'              => $request->id_estado,
            'id_municipio'           => $request->id_municipio,
            'id_parroquia'           => $request->id_parroquia,
            'id_ciudad'              => $request->id_ciudad,
            'ubicacion'              => $request->ubicacion,
            'id_asesor_asignado'     => $request->id_asesor_asignado,
            'destacado'              => $request->destacado,
            'oferta'                 => $request->precio,
            'datos_propietario'      => $datos_propietario,
            'datos_apoderado'        => $datos_apoderado,
            'instalaciones_cercanas' => $instalaciones_cercanas,
            'datos_construccion'     => $datos_construccion
        ]);

        if($property){
            return redirect()->route('properties.index')->with('success', 'Inmueble registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function show(Properties $property)
    {
        $state = State::find($property->id_estado);
        $municipalities = Municipality::find($property->id_municipio);
        $parroquias = Parish::find($property->id_parroquia);
        $locations = Location::find($property->id_ciudad);


        return view('admin.properties.show', [
            'property'       => $property,
            'advisers'       => asesoresInmobiliarios::all(),
            'categories'     => CategoryProperty::orderBy('nombre')->get(),
            'states'         => $state ? $state : null,
            'municipalities' => $municipalities ? $municipalities : null,
            'parishes'       =>  $parroquias ? $parroquias : null,
            'locations'      => $locations ? $locations : null,
            'documents'      => DB::table('documentos')->where('id_propiedad', $property->id)->where('tipo_documento', 'documentos')->get(),
            'photos'      => DB::table('documentos')->where('id_propiedad', $property->id)->where('tipo_documento', 'fotografias')->orderBy('order', 'ASC')->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function edit(Properties $property)
    {
        return view('admin.properties.edit', [
            'property'       => $property,
            'advisers'       => asesoresInmobiliarios::all(),
            'categories'     => CategoryProperty::orderBy('nombre')->get(),
            'states'         => State::orderBy('nombre')->get(),
            'municipalities' => Municipality::where('id_estado', $property->id_estado)->orderBy('nombre')->get(),
            'parishes'       => Parish::where('id_municipio', $property->id_municipio)->orderBy('nombre')->get(),
            'locations'      => Location::where('id_estado', $property->id_estado)->orderBy('nombre')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function update(PropertiesRequest $request,Properties $property)
    {
        $datos_propietario = [];
        $datos_propietario = Arr::add($datos_propietario, 'nombre_propietario', $request->nombre_propietario);
        $datos_propietario = Arr::add($datos_propietario, 'tel_propietario', $request->tel_propietario);
        $datos_propietario = Arr::add($datos_propietario, 'email_propietario', $request->email_propietario);

        $datos_apoderado = [];
        $datos_apoderado = Arr::add($datos_apoderado, 'nombre_apoderado', $request->nombre_apoderado);
        $datos_apoderado = Arr::add($datos_apoderado, 'tel_apoderado', $request->tel_apoderado);
        $datos_apoderado = Arr::add($datos_apoderado, 'email_apoderado', $request->email_apoderado);

        $instalaciones_cercanas = [];
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'supermercado', $request->supermercado);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'clinica', $request->clinica);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'farmacia', $request->farmacia);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'colegios', $request->colegios);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'estacion_metro', $request->estacion_metro);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'autobus', $request->autobus);
        $instalaciones_cercanas = Arr::add($instalaciones_cercanas, 'centro_comercial', $request->centro_comercial);

        $datos_construccion = [];
        $datos_construccion = Arr::add($datos_construccion, 'area_terreno', $request->area_terreno);
        $datos_construccion = Arr::add($datos_construccion, 'area_construccion', $request->area_construccion);
        $datos_construccion = Arr::add($datos_construccion, 'antiguedad', $request->antiguedad);
        $datos_construccion = Arr::add($datos_construccion, 'cocina', $request->cocina);
        $datos_construccion = Arr::add($datos_construccion, 'sala', $request->sala);
        $datos_construccion = Arr::add($datos_construccion, 'habit', $request->habit);
        $datos_construccion = Arr::add($datos_construccion, 'habit_s', $request->habit_s);
        $datos_construccion = Arr::add($datos_construccion, 'banos', $request->banos);
        $datos_construccion = Arr::add($datos_construccion, 'banos_s', $request->banos_s);
        $datos_construccion = Arr::add($datos_construccion, 'bano_medio', $request->bano_medio);
        $datos_construccion = Arr::add($datos_construccion, 'aire_a', $request->aire_a);
        $datos_construccion = Arr::add($datos_construccion, 'tv', $request->tv);
        $datos_construccion = Arr::add($datos_construccion, 'wifi', $request->wifi);
        $datos_construccion = Arr::add($datos_construccion, 'parrillera', $request->parrillera);
        $datos_construccion = Arr::add($datos_construccion, 'piscina', $request->piscina);
        $datos_construccion = Arr::add($datos_construccion, 'maletero', $request->maletero);
        $datos_construccion = Arr::add($datos_construccion, 'lavadero', $request->lavadero);
        $datos_construccion = Arr::add($datos_construccion, 'gimnasio', $request->gimnasio);
        $datos_construccion = Arr::add($datos_construccion, 'estacionamiento', $request->estacionamiento);
        $datos_construccion = Arr::add($datos_construccion, 'tipo_estacionamiento', $request->tipo_estacionamiento);
        $datos_construccion = Arr::add($datos_construccion, 'descripcion', $request->descripcion);

        $property->update([
            'nombre_propiedad'       => $request->nombre_propiedad,
            'estatus_publicacion'    => $request->estatus_publicacion,
            'tipo_negocio'           => $request->tipo_negocio,
            'id_categoria'           => $request->id_categoria,
            'precio'                 => $request->precio,
            'id_estado'              => $request->id_estado,
            'id_municipio'           => $request->id_municipio,
            'id_parroquia'           => $request->id_parroquia,
            'id_ciudad'              => $request->id_ciudad,
            'ubicacion'              => $request->ubicacion,
            'id_asesor_asignado'     => $request->id_asesor_asignado,
            'destacado'              => $request->destacado,
            'oferta'                 => $request->precio,
            'datos_propietario'      => $datos_propietario,
            'datos_apoderado'        => $datos_apoderado,
            'instalaciones_cercanas' => $instalaciones_cercanas,
            'datos_construccion'     => $datos_construccion
        ]);

        if($property){
            return redirect()->route('properties.index')->with('success', 'Inmueble actualizado.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function destroy(Properties $property)
    {
        $property->delete();
        return redirect()->route('properties.index')->with('success', 'Inmueble Eliminado.');
    }

    public function documents(Properties $property)
    {
        return view('admin.properties.documents', [
            'property'  => $property,
            'documents' => DB::table('documentos')->where('id_propiedad', $property->id)->where('tipo_documento', 'documentos')->get()
        ]);
    }

    public function photos(Properties $property)
    {
        return view('admin.properties.photos', [
            'property'  => $property,
            'documents' => DB::table('documentos')->where('id_propiedad', $property->id)->where('tipo_documento', 'fotografias')->get()
        ]);
    }

    public function upload_documents(Request $request)
    {
        for ($i=0; $i < count($request->documento) ; $i++) {
            if(!empty($request->documento[$i])){
                $url = $request->documento[$i]->store($request->tipo_documento,'public'); //documentos y fotos

                $order = (DB::table('documentos')->where('id_propiedad', $request->id_propiedad)->where('tipo_documento', $request->tipo_documento)->get());
                if (count($order)>0){
                    $order = ($order->last())->order+1;
                }else{
                    $order = 1;
                }

                DB::table('documentos')->insert([
                    'documento_url'    => $url,
                    'nombre_documento' => ($request->nombre[$i]) ? $request->nombre[$i] : strtoupper($request->tipo_documento),
                    'tipo_documento'   => $request->tipo_documento,
                    'id_propiedad'     => $request->id_propiedad,
                    'id_asesor'        => $request->id_asesor,
                    'order'            => $order
                ]);
            }
        }

        $mjs = ($request->tipo_documento == 'documentos') ? 'Documento(s) del inmueble cargados.' : 'Fotografía(s) del inmueble cargadas.';
        return redirect()->route('properties.show', $request->id_propiedad)->with('success', $mjs);
    }

    public function destroy_documents($id)
    {
        $doc = DB::table('documentos')->where('id', $id)->first();
        if($doc){
            Storage::disk('public')->delete($doc->documento_url);
            DB::table('documentos')->where('id', $id)->delete();
            return back()->with('success', 'Documento eliminado');
        }

        return redirect()->route('properties.index')->with('danger', 'Documento no elimiado.');
    }

    public function order_documents(Request $request)
    {
        $order = 0;
        for ($i=0; $i < count($request->order) ; $i++) {
            $order++;
            $doc = DB::table('documentos')->where('id', $request->order[$i])->update([
                'order' => $order
            ]);
        }
        return back()->with('success', 'Fotografías organizadas');
    }

    public function timeline($id)
    {
        // array de property y timelineGroup
        $timeline_data = $this->properties->timeline($id);

        // asignamos a variables
        $property = $timeline_data['property'];
        $timelineGroup = $timeline_data['timeline']->groupBy('fecha_modificacion');

        return view('/admin/properties/timeline',
        [
            'property' => $property,
            'timeline' => $timelineGroup
        ]);
    }
}
