<?php

namespace App\Http\Controllers;

use App\Http\Requests\LeadActivityCreateRequest;
use App\Http\Requests\LeadActivityUpdateRequest;
use App\Models\LeadActivity;
use App\Models\Prospect;

class LeadActivityController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Meetings',
            'modulo'     => 'inmuebles',
            'seccion'    => 'meetings',
        ]);
    }

    public function index()
    {
        return view('/admin/leadactivity/index', [
            'leads' => LeadActivity::LSearch(request(['search']))->paginate(10),
        ]);
    }

    public function create()
    {
        return view('/admin/leadactivity/create');
    }

    public function store(LeadActivityCreateRequest $request)
    {

        $prospectExist = Prospect::where('correo', $request->correo)->first();
        if ($prospectExist) {
            $datos = json_encode($request->except(['_token']));
            LeadActivity::create([
                'prospects_id' => $prospectExist->id,
                'nombres_apellidos' => $request->nombres_apellidos,
                'interes' => $request->interes,
                'datos_adicionales' => $datos,
            ]);
            return redirect('/thankyou')->with('success', 'Lead generado correctamente');
        } else {
            $prospect = Prospect::create([
                'nombres_apellidos' => $request->nombres_apellidos,
                'telefono' => $request->telefono,
                'correo' => $request->correo,
            ]);
            $datos = json_encode($request->except(['_token']));
            LeadActivity::create([
                'prospects_id' => $prospect->id,
                'nombres_apellidos' => $request->nombres_apellidos,
                'interes' => $request->interes,
                'datos_adicionales' => $datos,
            ]);
            return redirect('/thankyou')->with('success', 'Lead generado correctamente');
        }
    }

    public function show(LeadActivity $leadactivity)
    {
        return view('/admin/leadactivity/show', [
            'leadactivity' => $leadactivity,
            'prospect' => Prospect::where('id',$leadactivity->prospects_id)->first(),
        ]);
    }

    public function edit(LeadActivity $leadactivity)
    {
        return view('/admin/leadactivity/edit', [
            'leadactivity' => $leadactivity,
            'prospects' => Prospect::get(),
        ]);
    }

    public function update(LeadActivityUpdateRequest $request, $id)
    {
        if (LeadActivity::find($id)->update($request->all())) {
            return redirect()->route('leadactivity.index')->with('success', 'lead actualizado correctamente');
        }
    }

    public function destroy($id)
    {

        LeadActivity::find($id)->delete();
        return redirect()->route('leadactivity.index')->with('success', 'lead eliminado correctamente');
    }
}
