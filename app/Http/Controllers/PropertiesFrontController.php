<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Properties;
use App\Models\asesoresInmobiliarios;


use App\Models\propertyfront;



class PropertiesFrontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view(
            'page.home',
            [
                'properties' => Properties::where('estatus_publicacion', 1)->latest()->paginate(3),

            ]
        );
    }
    public function showProperty(Request $request)
    {
        //
        return view(
            'page.propertylist',
            [
                'properties' => Properties::latest()->filter($request->all())->paginate(10),
                'request'    => $request->all()

            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $property = Properties::find($id);
        $similarProperties = Properties::where('id_categoria', $property->id_categoria)->whereNotIn('id', [$property->id])->get();
        return view('page.property', [
            'property_details' => $property,
            'similar_property' => empty($similarProperties) ? $similarProperties->random(3) : null,

        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function about()
    {
        return view('page.about', [
            'advisers' => asesoresInmobiliarios::get()
        ]);
    }
}
