<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use \App\Models\Negotiation;
use \App\Models\asesoresInmobiliarios;
use \App\Models\Properties;
use \App\Models\citas;
use Illuminate\Support\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        view()->share([
            'title'      => 'Dashboard',
            'modulo'     => '',
            'seccion'    => '',
        ]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        if (auth()->user()->role != 'user') {
            
            // Seleccionamos las [negociaciones] con [fecha_oferta, monto_mediacion, RAW(month)]
            // del año actual [Carbon::now()->year]
            $query = DB::table('negociaciones')
            ->select('fecha_oferta', 'monto_mediacion', DB::raw('MONTH(fecha_oferta) as month'))
            ->whereYear('fecha_oferta', Carbon::now()->year)
            ->orderBy('fecha_oferta', 'ASC')
            ->get();

            // Agrupamos nuestra consulta por mes [month]
            $group_month = $query->groupBy('month');
            // creamos una nueva colleccion para enviar a la vista
            $total_month_negotiation = collect();            
            
            // Reorganizamos la collección para asignarla a $arreglo2
            foreach ($group_month as $key => $value) {
                $item = ['month' => $key, 'total' => $value->sum('monto_mediacion')];
                $total_month_negotiation->push($item);
            }
            
            return view('home', [
                'properties' => Properties::count(),
                'advisers' => asesoresInmobiliarios::where('estatus', 'Activo')->count(),
                'negotiation' => Negotiation::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->count(),
                'meeting' => citas::whereYear('created_at', Carbon::now()->year)->whereMonth('created_at', Carbon::now()->month)->count(),
                'total_month_negotiation' => $total_month_negotiation,
            ]);
        }
        return redirect()->route('users.perfil');
    }
}
