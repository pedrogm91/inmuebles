<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Reports;
use App\Models\asesoresInmobiliarios;
use App\Models\Negotiation;
use App\Models\Properties;

class ReportsController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Reportes',
            'modulo'     => 'reportes',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.reports.index');
    }

    public function profitability()
    {
        view()->share(['title' => 'Reporte de Rentabilidad', 'seccion' => 'profitability']);
        return view('admin.reports.profitability.index');
    }

    public function profitability_advisers()
    {
        view()->share(['title' => 'Reporte de Rentabilidad por Asesor', 'seccion' => 'profitability_advisers']);
        return view('admin.reports.profitability.advisers',
            [
                'advisers' => asesoresInmobiliarios::orderBy('nombres_apellidos', 'ASC')->get(),
            ]);
    }

    public function queryProfitability(Request $request)
    {
        if($request->asesor){
            $asesor = asesoresInmobiliarios::find($request->asesor);
            view()->share(['title' => 'Reporte de Rentabilidad '.$request->tipo_negocio.' - '.$asesor->nombres_apellidos, 'seccion' => 'profitability']);
            return view('admin.reports.profitability.reports',
                [
                    'reports' => Negotiation::where('tipo_negocio', $request->tipo_negocio)->where('id_asesor', $request->asesor)->whereBetween('fecha_oferta', [Carbon::parse($request->desde)->format('Y-m-d'), Carbon::parse($request->hasta)->format('Y-m-d')])->get(),
                ]);
        }

        view()->share(['title' => 'Reporte de Rentabilidad '.$request->tipo_negocio, 'seccion' => 'profitability']);
        return view('admin.reports.profitability.reports',
            [
                'reports' => Negotiation::where('tipo_negocio', $request->tipo_negocio)->whereBetween('fecha_oferta', [Carbon::parse($request->desde)->format('Y-m-d'), Carbon::parse($request->hasta)->format('Y-m-d')])->get(),
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function show(Reports $reports)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function edit(Reports $reports)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Reports $reports)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Reports  $reports
     * @return \Illuminate\Http\Response
     */
    public function destroy(Reports $reports)
    {
        //
    }
}
