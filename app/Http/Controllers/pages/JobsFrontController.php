<?php

namespace App\Http\Controllers\pages;

use App\Http\Controllers\Controller;

use Illuminate\Support\Carbon;
use DB;
use App\Http\Requests\JobRequest;

use App\Models\Job;
use App\Models\JobArea;
use App\Models\JobPlace;
use App\Models\State;

class JobsFrontController extends Controller
{
    public function index()
    {
        return view('page.jobs',
            [
                'jobs' => Job::latest()->where('publicado', 'Si')->paginate(9)
            ]
        );
    }

    public function show(Job $job)
    {
        return view('page.jobDetails', [
            'job' => $job,
        ]);
    }
}
