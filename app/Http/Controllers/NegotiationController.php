<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;
use App\Http\Requests\NegotiationRequest;

use App\Models\Negotiation;
use App\Models\Properties;
use App\Models\asesoresInmobiliarios;


class NegotiationController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Negociaciones',
            'modulo'     => 'inmuebles',
            'seccion'    => 'negotiations',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.negotiations.index',
            ['negotiations' => Negotiation::latest()->orderBy('negociaciones.fecha_oferta','asc')->filter(request(['search']))->paginate(10)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.negotiations.create',
            [
                'properties' => Properties::where('estatus_publicacion', 1)->get(),
                'advisers' => asesoresInmobiliarios::where('estatus', 'activo')->orderBy('nombres_apellidos', 'ASC')->get(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(NegotiationRequest $request)
    {
        $negotiation = Negotiation::create([
            'id_propiedad'          => $request->id_propiedad,
            'id_asesor'             => $request->id_asesor,
            'nombre_cliente'        => $request->nombre_cliente,
            'tel_cliente'           => $request->tel_cliente,
            'tipo_negocio'          => $request->tipo_negocio,
            'monto_mediacion'       => $request->monto_mediacion,
            'porcentaje_honorarios' => $request->porcentaje_honorarios,
            'fecha_oferta'          => Carbon::now()->format('Y-m-d'),
            'sistema_de_pago'       => $request->sistema_de_pago,
            'fecha_inicial'         => $request->fecha_inicial,
            'fecha_final'           => $request->fecha_final,
        ]);

        if($negotiation){
            return redirect()->route('negotiations.index')->with('success', 'Negociación registrada.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Negotiation $negotiation)
    {
        return view('admin.negotiations.show', [
            'negotiation' => $negotiation,
            'properties'  => Properties::all(),
            'advisers'    => asesoresInmobiliarios::all(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Negotiation $negotiation)
    {
        return view('admin.negotiations.edit', [
            'negotiation' => $negotiation,
            'properties'  => Properties::where('estatus_publicacion', 1)->get(),
            'advisers'    => asesoresInmobiliarios::where('estatus', 'activo')->orderBy('nombres_apellidos', 'ASC')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(NegotiationRequest $request, Negotiation $negotiation)
    {
        $negotiation->update([
            'id_propiedad'          => $request->id_propiedad,
            'id_asesor'             => $request->id_asesor,
            'nombre_cliente'        => $request->nombre_cliente,
            'tel_cliente'           => $request->tel_cliente,
            'tipo_negocio'          => $request->tipo_negocio,
            'monto_mediacion'       => $request->monto_mediacion,
            'porcentaje_honorarios' => $request->porcentaje_honorarios,
            'sistema_de_pago'       => $request->sistema_de_pago,
            'fecha_inicial'         => $request->fecha_inicial,
            'fecha_final'           => $request->fecha_final,
        ]);

        if($negotiation){
            return redirect()->route('negotiations.index')->with('success', 'Negociación actualizada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Negotiation $negotiation)
    {
        $negotiation->delete();
        return redirect()->route('negotiations.index')->with('success', 'Negociación Eliminada.');
    }
}
