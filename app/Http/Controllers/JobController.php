<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use DB;
use App\Http\Requests\JobRequest;

use App\Models\Job;
use App\Models\JobArea;
use App\Models\JobPlace;
use App\Models\State;


class JobController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Vacantes',
            'modulo'     => 'vacantes',
            'seccion'    => 'vacantes',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.jobs.index',
            [
                'jobs' => Job::latest()->filter(request(['search']))->filter(request(['id_area']))->filter(request(['id_puesto']))->paginate(10),
                'states' => State::orderBy('nombre', 'asc')->get(),
                'areas' => JobArea::orderBy('nombre', 'asc')->get(),
                'puestos' => JobPlace::orderBy('nombre', 'asc')->get(),
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jobs.create',
            [
                'states' => State::orderBy('nombre', 'asc')->get(),
                'areas' => JobArea::orderBy('nombre', 'asc')->get(),
                'puestos' => JobPlace::orderBy('nombre', 'asc')->get(),
            ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(JobRequest $request)
    {
        $job = Job::create([
            'cargo'      => $request->cargo,
            'id_area'    => $request->id_area,
            'id_puesto'  => $request->id_puesto,
            'id_estado'  => $request->id_estado,
            'salario'    => $request->salario,
            'publicado'  => $request->publicado,
            'brief'      => $request->brief,
            'describelo' => $request->describelo,
            'requisitos' => $request->requisitos,
            'ofrecemos'  => $request->ofrecemos,
        ]);

        if($job){
            return redirect()->route('jobs.index')->with('success', 'Vacante registrada.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Job $job)
    {
        return view('admin.jobs.show', [
            'job' => $job,
            'states' => State::orderBy('nombre', 'asc')->get(),
            'areas' => JobArea::orderBy('nombre', 'asc')->get(),
            'puestos' => JobPlace::orderBy('nombre', 'asc')->get(),
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Job $job)
    {
        return view('admin.jobs.edit', [
            'job' => $job,
            'states' => State::orderBy('nombre', 'asc')->get(),
            'areas' => JobArea::orderBy('nombre', 'asc')->get(),
            'puestos' => JobPlace::orderBy('nombre', 'asc')->get(),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(JobRequest $request, $job)
    {
        $query = Job::where('id', $job)->update([
            'cargo'      => $request->cargo,
            'id_area'    => $request->id_area,
            'id_puesto'  => $request->id_puesto,
            'id_estado'  => $request->id_estado,
            'salario'    => $request->salario,
            'publicado'  => $request->publicado,
            'brief'      => $request->brief,
            'describelo' => $request->describelo,
            'requisitos' => $request->requisitos,
            'ofrecemos'  => $request->ofrecemos,
        ]);

        if($query){
            return redirect()->route('jobs.index')->with('success', 'Vacante actualizada.');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Job $job)
    {
        $job->delete();
        return redirect()->route('jobs.index')->with('success', 'Vacante Eliminada.');
    }
}
