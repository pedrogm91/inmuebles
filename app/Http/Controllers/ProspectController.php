<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProspectCreateRequest;
use App\Http\Requests\ProspectUpdateRequest;
use App\Models\Prospect;

class ProspectController extends Controller
{
    public function __construct()
    {

        view()->share([
            'title'      => '',
            'modulo'     => '',
            'seccion'    => '',
        ]);
    }


    public function index()
    {
        return view('/admin/prospect/index', [
            'prospects' => Prospect::BuildList(request(['search']))->latest()->paginate(10),
        ]);
    }

    public function create()
    {

        return view('/admin/prospect/create');
    }

    public function show(Prospect $prospect)
    {

        return view('/admin/prospect/show', [
            'prospect' =>  $prospect,
        ]);
    }

    public function store(ProspectCreateRequest $request)
    {
        if (Prospect::create($request->all())) {
            return redirect()->route('prospect.index')->with('success', 'Prospecto Registrado Con exito');
        }
    }

    public function edit(Prospect $prospect)
    {

        return view('/admin/prospect/edit', [
            'prospect' => $prospect,
        ]);
    }

    public function update(ProspectUpdateRequest $request, $id)
    {
        if (Prospect::find($id)->update($request->all())) {
            return redirect()->route('prospect.index')->with('success', 'Prospecto Actualizado');
        };
    }

    public function destroy($id)
    {
        $prospect = Prospect::find($id);
        $prospect->delete();
        return redirect()->route('prospect.index')->with('success', 'Prospecto Eliminado.');
    }
}
