<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use App\Models\Favorite;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;
use App\Http\Requests\PerfilUpdateRequest;
use App\Models\asesoresInmobiliarios;

class UserController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Usuarios',
            'modulo'     => 'administracion',
            'seccion'    => 'usuarios',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Request $request)
    {   
        /*$users = User::when($request->has('column'), function($query) use ($request){
            $query->ColumnSearch($request->column,$request->search);
        })->paginate(25);
        
        $users->appends(request()->query());

        return view('admin.users.index', [
            'users' => $users
        ]);*/
        
         return view('admin.users.index', 
            [ 'users' => User::latest()->filter(request(['search']))->paginate(5)]);
                
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserCreateRequest $request)
    {
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
            'status' => $request->status,
            'role' => $request->role
        ]);
        return redirect()->route('users.index')->with('success', 'Usuario registrado.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(User $id)
    {
        return view('admin.users.show', [
           'user' => $id,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(User $id)
    {
        return view('admin.users.edit', [
            'user' => $id,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $data = array_filter($request->only(['name', 'email', 'status', 'role']));
        User::where('id', $id)->update($data);
        return redirect()->route('users.index')->with('success', 'Usuario actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        if (collect($user->adviser)->isEmpty()){
            return redirect()->back()->with('warning', 'No puedes eliminar este usuario porque tiene un perfil de asesor. Ve a la sección de asesor y elimina el asesor antes de eliminar su cuenta de usuario.');
        }
        $user->delete();
        return redirect()->route('users.index')->with('success', 'Usuario eliminado');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function perfil()
    {
        $favorites = Favorite::where('user_id', Auth::user()->id)->get();
        return view('admin.users.perfil', [
            'user' => User::find(Auth::user()->id),
            'favorites' => $favorites->load(['property']),
        ]);
    }

    public function perfil_update(PerfilUpdateRequest $request, $id)
    {
        $user = array_filter($request->only(['name', 'email']));
        User::where('id', $id)->update($user);

        return redirect()->route('users.perfil')->with('success', 'Usuario actualizado.');
    }
}
