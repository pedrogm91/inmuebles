<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\LocationRequest;

use App\Models\Location;
use App\Models\State;

class LocationsController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Localidades',
            'modulo'     => 'administracion',
            'seccion'    => 'localidades',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.locations.index',
        ['locations' => Location::latest()->orderBy('nombre','asc')->filter(request(['search']))->paginate(5)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(State $state)
    {
        return view('admin.locations.create', ['state' => $state]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LocationRequest $request)
    {
        $location = Location::create([
            'nombre'       => $request->nombre,
            'id_estado' => $request->id_estado
        ]);

        if($location){
            return redirect()->route('states.cities', $request->id_estado)->with('success', 'Ciudad registrada.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function show(Location $location)
    {
        return view('admin.locations.show', ['location'=> $location]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function edit(Location $location)
    {
        return view('admin.locations.edit', ['location'=> $location]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function update(LocationRequest $request, $location)
    {
        $data = array_filter($request->only(['nombre']));
        Location::where('id', $location)->update($data);
        return redirect()->route('states.cities', Location::find($location)->id_estado)->with('success', 'Ciudad actualizada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function destroy(Location $location)
    {
        $state = $location->id_estado;
        $location->delete();
        return redirect()->route('states.cities', $state)->with('success', 'Ciudad eliminada.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function json($state)
    {
        return response()->json(['success' => true, 'locations' => Location::where('id_estado', $state)->get()]);
    }
}
