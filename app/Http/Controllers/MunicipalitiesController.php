<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\MunicipalityRequest;

use App\Models\Municipality;
use App\Models\State;

class MunicipalitiesController extends Controller
{
    public function __construct()
    {
        view()->share([
            'title'      => 'Municipios',
            'modulo'     => 'administracion',
            'seccion'    => 'localidades',
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.municipalities.index',
        ['municipalities' => Municipality::latest()->orderBy('nombre','asc')->filter(request(['search']))->paginate(5)]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(State $state)
    {
        //return view('admin.municipalities.create');
        return view('admin.municipalities.create', ['state' => $state]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MunicipalityRequest $request)
    {
        $municipality = Municipality::create([
            'nombre'    => $request->nombre,
            'id_estado' => $request->id_estado
        ]);

        if($municipality){
            return redirect()->route('states.municipalities', $request->id_estado)->with('success', 'Municipio registrado.');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function show(Municipality $municipality)
    {
        return view('admin.municipalities.show', ['municipality'=> $municipality]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function edit(Municipality $municipality)
    {
        return view('admin.municipalities.edit', ['municipality'=> $municipality]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function update(MunicipalityRequest $request, $municipality)
    {
        $data = array_filter($request->only(['nombre']));
        Municipality::where('id', $municipality)->update($data);
        return redirect()->route('states.municipalities', Municipality::find($municipality)->id_estado)->with('success', 'Municipio actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function destroy(Municipality $municipality)
    {
        if ($municipality->parishes->isNotEmpty())
        {
            return redirect()->back()->with('warning', 'No puedes eliminar un municipio que tiene parroquias o Urbanizaciones.');
        }
        $state = $municipality->id_estado;
        $municipality->delete();
        return redirect()->route('states.municipalities', $state)->with('info', 'Municipio eliminado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\properties  $properties
     * @return \Illuminate\Http\Response
     */
    public function json($state)
    {
        return response()->json(['success' => true, 'municipalities' => Municipality::where('id_estado', $state)->get()]);
    }
}
