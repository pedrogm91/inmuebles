<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Banners extends Model
{
    use HasFactory;

    protected $table = 'contenido_banners';

    protected $fillable = [
        'slogan', 'banner_principal', 'banner_mobile'
    ];
}