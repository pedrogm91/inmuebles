<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class asesoresInmobiliarios extends Model
{
    use HasFactory;

    protected $table = 'asesores_inmobiliarios';

    protected $fillable=[
        'id_usuario',
        'nombres_apellidos',
        'cedula','telefono',
        'telefono_movil',
        'correo',
        'fecha_nacimiento',
        'id_area',
        'id_puesto',
        'direccion',
        'area_de_trabajo',
        'tipo_de_contrato',
        'vehiculo',
        'lugar_de_trabajo',
        'porcentaje_ganancia',
        'total_percibido',
        'recomendado',
        'estatus',
        'imagen_perfil',
        'experiencia'
    ];

   /**
    * relaciones:
    * propiedades
    * citas
    * comisiones
    */

    //busqueda en el index
    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombres_apellidos', 'like', '%'.request('search').'%')
            ->orWhere('area_de_trabajo', 'like', '%'.request('search').'%')
            ->orWhere('vehiculo', 'like', '%'.request('search').'%')
            ->orWhere('estatus', 'like', '%'.request('search').'%');
        }
    }

    public function asesor($query)
    { //busca el agente en base a su id
       return asesoresInmobiliarios::where('id',$query)->first();
    }

    public function propiedades()
    {
        return $this->hasMany(Properties::class, 'id_asesor_asignado');
        //hasMany->propiedades
    }

    public function citas()
    {
        return $this->hasMany(citas::class, 'id_asesor');
    }

    public function respuestasCitas()
    {
        return $this->hasMany(respuestasCitas::class, 'asesores_inmobiliarios_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_usuario');
    }

    public function comisiones()
    {
        //hasMany->comisiones
        return $this->hasMany(comisiones::class);
    }

    public function documentos()
    {
        return $this->hasMany(documentos::class);
    }

    public function area_trabajo()
    {
        if($this->area_de_trabajo == 1){
            return 'Administración';
        }elseif($this->area_de_trabajo == 2){
            return 'Mantenimiento';
        }elseif($this->area_de_trabajo == 3){
            return 'Asesor Inmobiliario';
        }elseif($this->area_de_trabajo == 4){
            return 'Asesor Legal';
        }elseif($this->area_de_trabajo == 5){
            return 'Directora';
        }elseif($this->area_de_trabajo == 6){
            return 'Gerente General';
        }
    }
}
