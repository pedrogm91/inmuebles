<?php

namespace App\Models;

use Illuminate\Http\Request;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'status', // ['active', 'inactive']
        'role',  // ['admin', 'director', 'adviser', 'user']
    ];

    public function scopefilter($query, array $filters)
    {
      if($filters['search'] ?? false)
      {
         $query->where('name', 'like', '%'.request('search').'%')
         ->orWhere('email', 'like', '%'.request('search').'%')
         ->orWhere('status', 'like', '%'.request('search').'%')
         ->orWhere('role', 'like', '%'.request('search').'%');
      }

    }

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    public function asesoresInmobiliarios()
    {
        return $this->hasOne(asesoresInmobiliarios::class, 'id_usuario');
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class, 'user_id');
    }

}
