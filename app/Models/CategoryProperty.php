<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use App\Models\Properties;

class CategoryProperty extends Model
{
    use HasFactory;

    protected $table = 'categorias_propiedades';

    protected $fillable = [
        'nombre'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombre', 'like', '%'.request('search').'%');
        }
    }

    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    public function isUsed(){
        $cont = 0;
        $cont += Properties::where('id_categoria', $this->id)->count();
        return $cont;
    }
}
