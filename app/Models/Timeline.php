<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Timeline extends Model
{
    use HasFactory;

    protected $fillable = [
        'propiedades_id',
        'user_id',
        'accion',
        'modulo',
        'modificaciones',
        'fecha_modificacion',
    ];

    protected $table = 'timeline';

    public function Properties()
    {
        return $this->belongsTo(Properties::class);
    }
}
