<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\Links;

class Menus extends Model
{
    use HasFactory;

    protected $table = 'contenido_menus';

    protected $fillable = [
        'nombre', 'status'
    ];

    public function isUsed(){
        $cont = 0;
        $cont += Links::where('id_menu', $this->id)->count();
        return $cont;
    }

    public function links()
    {
        return $this->hasMany(Links::class, 'id_menu');
    }
}