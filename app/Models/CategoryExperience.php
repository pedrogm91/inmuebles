<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CategoryExperience extends Model
{
    use HasFactory;

    protected $table = "categorias_experiencias";

    protected $fillable = [
        'nombre', 'estatus'
    ];

    public function experiences()
    {
        return $this->hasMany(Experience::class, 'id_categoria');
    }
}
