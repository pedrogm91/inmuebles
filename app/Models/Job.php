<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Models\JobPostulations;

class Job extends Model
{
    use HasFactory;

    protected $table = 'vacantes';

    protected $fillable = [
        'cargo', 
        'id_area', 
        'id_puesto', 
        'id_estado', 
        'salario', 
        'publicado', 
        'brief', 
        'describelo', 
        'requisitos', 
        'ofrecemos'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['id_area'] ?? false){
            $query->orWhere('id_area',request('id_area'));
        }
        if($filters['id_puesto'] ?? false){
            $query->orWhere('id_puesto',request('id_puesto'));
        }
        if($filters['search'] ?? false){
            $query->orWhere('cargo','like', '%'.request('search').'%');
        }
    }

    public function area()
    {
        return $this->belongsTo(JobArea::class, 'id_area');
    }

    public function puesto()
    {
        return $this->belongsTo(JobPlace::class, 'id_puesto');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'id_estado');
    }

    public function isUsed()
    {
        $cont = 0;
        $cont += JobPostulations::where('id_vacante', $this->id)->count();
        return $cont;
    }
}