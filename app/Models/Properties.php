<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use App\Observers\PropertyObserver;
use App\Models\Negotiation;

class Properties extends Model
{
    use HasFactory;

    protected $table = 'propiedades';

    protected $fillable = [
        'nombre_propiedad', 'estatus_publicacion', 'tipo_negocio', 'id_categoria', 'precio', 'id_estado', 'id_municipio', 'id_parroquia', 'id_ciudad', 'ubicacion', 'nombre_propietario', 'tel_propietario', 'email_propietario', 'id_asesor_asignado', 'destacado', 'datos_propietario', 'datos_apoderado', 'instalaciones_cercanas', 'datos_construccion', 'servicios'
    ];

    protected $casts = [
        'datos_propietario'      => 'array',
        'datos_apoderado'        => 'array',
        'instalaciones_cercanas' => 'array',
        'datos_construccion'     => 'array',
        'servicios'              => 'array',
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */


    //busqueda en el index
    public function scopefilterAdmin($query, array $filters)
    {  
        if($filters ?? false){
            if($filters['nombre_propiedad'] ?? false){
                $query->where('nombre_propiedad', 'like', '%'.$filters['nombre_propiedad'].'%');
            }
        }
    }
    
    // Filter for Pages front
    public function scopefilter($query, array $filters)
    {
        $query->where('estatus_publicacion', 1);
        
        if($filters ?? false){
            if($filters['nombre_propiedad'] ?? false){
                $query->where('nombre_propiedad', 'like', '%'.$filters['nombre_propiedad'].'%');
            }
            if($filters['tipo_negocio'] ?? false){
                $query->where('tipo_negocio', $filters['tipo_negocio']);
            }
            if($filters['id_categoria'] ?? false){
                $query->where('id_categoria', $filters['id_categoria']);
            }
            if($filters['id_estado'] ?? false){
                $query->where('id_estado', $filters['id_estado']);
            }
            if($filters['id_ciudad'] ?? false){
                $query->where('id_ciudad', $filters['id_ciudad']);
            }
            if($filters['id_municipio'] ?? false){
                $query->where('id_municipio', $filters['id_municipio']);
            }
            if($filters['precio'] ?? false){
                $query->where('precio', '<=', $filters['precio']);
            }
        }
    }

    public function adviser()
    {
        return $this->belongsTo(asesoresInmobiliarios::class, 'id_asesor_asignado');
        // return $this->hasOne(asesoresInmobiliarios::class, 'id', 'id_asesor_asignado');
    }

    /**
     *
     */
    public function category()
    {
        return $this->belongsTo(CategoryProperty::class, 'id_categoria');
    }

    /**
     *
     */
    public function location()
    {
        return $this->belongsTo(Location::class, 'id_ciudad');
    }

    /**
     *
     */
    public function state()
    {
        return $this->belongsTo(State::class, 'id_estado');
    }

    public function municipality()
    {
        return $this->belongsTo(Municipality::class, 'id_municipio');
    }

    public function parish()
    {
        return $this->belongsTo(Parish::class, 'id_parroquia');
    }

    /**
     *
     */
    public function citas()
    {
        return $this->hasMany(citas::class, 'id_propiedad');
    }
    

    /**
     *
     */
    public function estatus()
    {
        if($this->estatus_publicacion == 1){
            return 'Publicada';
        }elseif($this->estatus_publicacion == 2){
            return 'Terminada';
        }elseif($this->estatus_publicacion == 3){
            return 'Pausada';
        }
    }

    /**
     *
     */
    public function negocio()
    {
        if($this->tipo_negocio == 1){
            return 'Alquiler';
        }elseif($this->tipo_negocio == 2){
            return 'Venta';
        }
    }

    /**
     * recibe un id
     * regresa todas las operaciones relacionadas con esa propiedad.
     * operaciones: registro/actualizacion de propiedad, venta/alquiler y citas.
     * 
     */
    public function timeline($id)
    {
         #Buscamos la propiedad
         //luego buscamos en la tabla de ventas/alquiler si esta listada y si se registro alguna cita con ese id
         $timeline = DB::table('timeline')
            ->join('propiedades', 'timeline.propiedades_id','=','propiedades.id')
            ->select('timeline.*','propiedades.id AS propiedades_id','propiedades.nombre_propiedad AS propiedad','propiedades.ubicacion AS ubicacion')
            ->where('timeline.propiedades_id','=',$id)
            ->get();
         $propiedad = DB::table('propiedades')
            ->select('*')
            ->where('id','=',$id)
            ->first();

        return ['property' => $propiedad, 'timeline' => $timeline];
    }

    public function documents()
    {
        $documento = DB::table('documentos')->where('id_propiedad', $this->id)->where('tipo_documento', 'fotografias')->get();
        
        if($documento){
            return $documento;
        }else{
            return '#';
        }
    }

    public function isUsed(){
        $cont = 0;
        $cont += Negotiation::where('id_propiedad', $this->id)->count();
        return $cont;
    }
}
