<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class citas extends Model
{
    use HasFactory;

    protected $fillable =
    [
        'fecha_cita',
        'hora_cita',
        'id_asesor',
        'id_propiedad',
        'nombre_cliente',
        'telefono_cliente',
        'correo_cliente',
        'asunto',
        'descripcion',
        'estatus',
        'observaciones',
        'url',
    ];

    // Relations

    public function asesor()
    {
        return $this->belongsTo(asesoresInmobiliarios::class, 'id_asesor');
    }

    public function property()
    {
        return $this->belongsTo(Properties::class, 'id_propiedad');
    }

    public function respuestasCitas()
    {
        return $this->hasMany(respuestasCitas::class, 'citas_id');
    }

    // public function clientes()
    // {
    //     return $this->belongsTo(clientes::class);
    // }

    public function citasProgramadas($query = False)
    {
        if (isset($query)) {
            $meetings_data = DB::table('citas')
                ->Join('asesores_inmobiliarios', 'citas.id_asesor', '=', 'asesores_inmobiliarios.id')
                ->Join('propiedades', 'citas.id_propiedad', '=', 'propiedades.id')
                ->select('citas.*', 'asesores_inmobiliarios.id AS id_asesor', 'nombres_apellidos AS nombres_apellidos')
                ->where('fecha_cita', 'like', '%' . request('search') . '%')
                ->orWhere('nombres_apellidos', 'like', '%' . request('search') . '%')
                ->orWhere('id_asesor', 'like', '%' . request('search') . '%')
                ->orWhere('asunto', 'like', '%' . request('search') . '%')
                ->orWhere('id_propiedad', 'like', '%' . request('search') . '%');
        } else {
            $meetings_data = DB::table('citas')
                ->Join('asesores_inmobiliarios', 'citas.id_asesor', '=', 'asesores_inmobiliarios.id')
                ->Join('propiedades', 'citas.id_propiedad', '=', 'propiedades.id')
                ->select('citas.*', 'asesores_inmobiliarios.id AS id_asesor', 'nombres_apellidos AS nombres_apellidos');
        }
        return $meetings_data;
    }

    public function calendario($request)
    {
        $start = explode('T', $request->start);
        $end = explode('T', $request->end);
        if(auth()->user()->role=="admin" || auth()->user()->role=="director")
        {
            $calendario = DB::table('citas')
            ->select('citas.id AS id', 'citas.asunto AS title', 'citas.fecha_cita AS start', 'citas.url AS url', 'citas.prioridad AS className')
            ->where('citas.fecha_cita', '>', date($start[0]))
            ->get();
        return response()->json($calendario);
        }
        if(auth()->user()->role=="adviser")
        {
            $calendario = DB::table('citas')
            ->select('citas.id AS id', 'citas.asunto AS title', 'citas.fecha_cita AS start', 'citas.url AS url', 'citas.prioridad AS className')
            ->where('citas.fecha_cita', '>', date($start[0]))
            ->where('citas.id_asesor','=',auth()->user()->asesoresInmobiliarios->id)
            ->get();
        return response()->json($calendario);
        }
    }
}
