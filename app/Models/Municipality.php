<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Municipality extends Model
{
    use HasFactory;

    protected $table = 'municipios';

    protected $fillable = [
        'id_estado', 'nombre'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombre', 'like', '%'.request('search').'%');
        }
    }

    public function parishes()
    {
        return $this->hasMany(Parish::class, 'id_municipio');
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'id_estado');
    }
}
