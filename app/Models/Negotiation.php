<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Negotiation extends Model
{
    use HasFactory;

    protected $table = 'negociaciones';

    protected $fillable = [
        'id_propiedad', 
        'id_asesor', 
        'nombre_cliente',  
        'tel_cliente', 
        'tipo_negocio', 
        'fecha_oferta', 
        'monto_mediacion', 
        'porcentaje_honorarios', 
        'sistema_de_pago', 
        'fecha_inicial', 
        'fecha_final'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query
            ->where('nombre_cliente', 'like', '%'.request('search').'%')
            ->orWhere('tel_cliente', 'like', '%'.request('search').'%')
            ->orWhere('tipo_negocio', 'like', '%'.request('search').'%')
            ->orWhere('fecha_oferta', 'like', '%'.request('search').'%')
            ->orWhere('sistema_de_pago', 'like', '%'.request('search').'%')
            ->orWhere('fecha_inicial', 'like', '%'.request('search').'%')
            ->orWhere('fecha_final', 'like', '%'.request('search').'%')
            ->orWhere('porcentaje_honorarios', 'like', '%'.request('search').'%')
            ->orWhere('monto_mediacion', 'like', '%'.request('search').'%');
        }
    }

    public function property()
    {
        return $this->hasOne(Properties::class, 'id', 'id_propiedad');
    }

    public function adviser()
    {
        return $this->hasOne(asesoresInmobiliarios::class, 'id', 'id_asesor');
    }
}
