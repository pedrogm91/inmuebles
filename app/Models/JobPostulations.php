<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class JobPostulations extends Model
{
    use HasFactory;

    protected $table = 'vacantes_postulaciones';

    protected $fillable = [
        'id_vacante', 
        'nombre', 
        'telefono', 
        'correo', 
        'comentario', 
        'adjunto'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false){
            $query->orWhere('nombre','like', '%'.request('search').'%')
            ->orWhere('telefono','like', '%'.request('search').'%')
            ->orWhere('correo','like', '%'.request('search').'%')
            ->orWhere('comentario','like', '%'.request('search').'%');
        }
    }

    public function vacante()
    {
        return $this->belongsTo(Job::class, 'id_vacante');
    }
}