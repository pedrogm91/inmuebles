<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Experience extends Model
{
    use HasFactory;

    protected $table = "experiencias";

    protected $fillable = [
        'id_categoria', 'fecha_experiencia', 'redes_sociales', 'horario', 'sitio_web', 'mapa', 'descripcion', 'portada', 'fecha_publicacion'
    ];

    public function category()
    {
        return $this->belongsTo(CategoryExperience::class, 'id_categoria');
    }
}
