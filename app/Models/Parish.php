<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Parish extends Model
{
    use HasFactory;

    protected $table = 'parroquias';

    protected $fillable = [
        'id_municipio', 'nombre'
    ];


    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombre', 'like', '%'.request('search').'%');
        }
    }

    public function municipalities()
    {
        return $this->belongsTo(Municipality::class);
        
    }
}