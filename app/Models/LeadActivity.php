<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class LeadActivity extends Model
{
    use HasFactory;

    protected $table = "leads_activities";
    protected $fillable = [
        'prospects_id',
        'nombres_apellidos',
        'interes',
        'datos_adicionales',
    ];

    public function Prospect()
    {
        return $this->belongsTo(Prospect::class,'id','prospects_id');
    }

    public function scopeLSearch($query, array $filters)
    {
        if ($filters['search'] ?? false) {
            return $query->where('nombres_apellidos', 'like', '%' . request('search') . '%')
                ->orWhere('interes', 'like', '%' . request('search') . '%');
        }
    }
}
