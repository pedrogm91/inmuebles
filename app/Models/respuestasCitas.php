<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class respuestasCitas extends Model
{
    use HasFactory;
    protected $fillable = [
        'citas_id',
        'asesores_inmobiliarios_id',
        'texto',
    ];
    protected $table = 'respuestas_citas';

    public function citas()
    {
        return $this->belongsTo(Citas::class, 'citas_id');
    }
    public function asesores()
    {
        return $this->belongsTo(asesoresInmobiliarios::class, 'asesores_inmobiliarios_id');
    }
    
}
