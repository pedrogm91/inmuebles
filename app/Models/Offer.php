<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Offer extends Model
{
    use HasFactory;

    protected $table = 'ofertas';

    protected $fillable = [
        'precio', 'fecha_oferta', 'id_propiedad', 'id_asesor', 'fecha_caducidad', 'margen_de_ganancia'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombres_apellidos', 'like', '%'.request('search').'%')
            ->orWhere('area_de_trabajo', 'like', '%'.request('search').'%')
            ->orWhere('vehiculo', 'like', '%'.request('search').'%')
            ->orWhere('estatus', 'like', '%'.request('search').'%');
        }
    }

    public function adviser()
    {
        return $this->hasOne(asesoresInmobiliarios::class, 'id', 'id_asesor');
    }

    public function property()
    {
        return $this->hasOne(Properties::class, 'id', 'id_propiedad');
    }
}
