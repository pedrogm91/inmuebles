<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Links extends Model
{
    use HasFactory;

    protected $table = 'contenido_enlaces';

    protected $fillable = [
        'id_menu', 'nombre', 'route' , 'ordering', 'status'
    ];

    public function menu()
    {
        return $this->belongsTo(Menus::class, 'id_menu', 'id');
    }
}