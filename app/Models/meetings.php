<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class meetings extends Model
{
    use HasFactory;

    public function scopeFilter($query, array $filters){
        //buscamos por dia, por (id) asesor y propiedad

        if($filters['search'] ?? false)
        {
            $query->where('fecha_cita','like', '%'.request('search').'%')
            ->orWhere('id_cliente','like','%'.request('search').'%')
            ->orWhere('id_asesor','like','%'.request('search').'%')
            ->orWhere('id_propiedad','like','%'.request('search').'%');
        }
    }

    public function asesor()
    {
        return $this->belongsTo(asesoresInmobiliarios::class);
    }
}
