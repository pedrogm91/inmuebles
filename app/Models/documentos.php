<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class documentos extends Model
{
    use HasFactory;

    public function asesor()
    {
        return $this->belongsTo(asesorInmobiliario::class);
    }

}
