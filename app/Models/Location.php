<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{
    use HasFactory;

    protected $table = 'ciudades';

    protected $fillable = [
        'id', 'id_estado', 'nombre'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombre', 'like', '%'.request('search').'%');
        }
    }

    public function state()
    {
        return $this->belongsTo(State::class, 'id_estado');
    }

}