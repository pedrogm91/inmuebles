<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profiles';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'second_name',
        'surname',
        'second_surname',
        'dni',
        'date_birth',
        'phone',
        'second_phone',
        'percentage',
        'job',
        'work_address',
        'house_address',
        'type_contract',
        'years_experience',
        'referred',
        'vehicle',
        'email',
    ];
/*
 $profile = new Profile(
        [
            'first_name' => 'Pedro',
            'second_name' => 'Elias',
            'surname' => 'Gonzalez',
            'second_surname' => 'Matos',
            'dni' => '20261862',
            'date_birth' => '1991-08-20',
            'phone' => '+58 4141044175',
            'second_phone' => 'sin descripción',
            'percentage' => '50',
            'work_address' => 'lorem',
            'job' => 'Gerente',
            'house_address' => 'dirección',
            'type_contract' => 'Fijo',
            'years_experience' => '1 - 5',
            'referred' => 'si',
            'vehicle' => 'si',
            'email' => 'pedro@gianchinnici.com'
        ]);
*/

    /**
     * 
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * 
    */
    public function properties()
    {
        return $this->hasMany(Property::class);
    }

    
}
