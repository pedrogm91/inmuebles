<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Prospect extends Model
{
    use HasFactory;
    protected $fillable = [
        'nombres_apellidos',
        'cedula',
        'telefono',
        'telefono_movil',
        'correo',
        'direccion'
    ];

    public function LeadActivity()
    {
        return $this->hasMany(LeadActivity::class, 'prospects_id', 'id');
    }

    public function scopeBuildList($query, array $filters)
    {
        if ($filters['search'] ?? false) {
            $query->where('nombres_apellidos', 'like', '%' . request('search') . '%')
                ->orWhere('correo', 'like', '%' . request('search') . '%')
                ->orWhere('cedula', 'like', '%' . request('search') . '%')
                ->orWhere('telefono', 'like', '%' . request('search') . '%')
                ->orWhereHas('LeadActivity', function($query2) { $query2->where('interes', 'like','%' . request('search') . '%'); });
        }
    }
}
