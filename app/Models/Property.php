<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'status',
        'featured',
        'price',
        'address',
        'description',
        'attributes',
        'surroundings',
        'property_owner',
    ];


    /**
     * 
     */
    public function profile()
    {
        return $this->belognsTo(Profile::class);
    }

    /**
     * 
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * 
     */
    public function location()
    {
        return $this->belongsTo(Location::class);
    }

}
