<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobPlace extends Model
{
    use HasFactory;

    protected $table = 'vacantes_puestos';

    protected $fillable = [
        'nombre', 'created_at', 'updated_at'
    ];
}
