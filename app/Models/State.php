<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    use HasFactory;

    protected $table = 'estados';

    protected $fillable = [
        'id',
        'nombre',
        'capital'
    ];

    public function scopefilter($query, array $filters)
    {
        if($filters['search'] ?? false)
        {
            $query->where('nombre', 'like', '%'.request('search').'%');
        }
    }

    public function municipalities()
    {
        return $this->hasMany(Municipality::class, 'id_estado');
    }
    public function cities()
    {
        return $this->hasMany(Location::class, 'id_estado');
    }
}
