<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JobArea extends Model
{
    use HasFactory;

    protected $table = 'vacantes_areas';

    protected $fillable = [
        'nombre', 'created_at', 'updated_at'
    ];

    public function area()
    {
        return $this->hasMany(Job::class, 'id_area');
    }
}
