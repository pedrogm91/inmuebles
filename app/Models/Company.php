<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Company extends Model
{
    use HasFactory;

    protected $table = 'contenido_empresa';

    protected $fillable = [
        'nombre', 'rif', 'telefono', 'celular', 'fax', 'direccion', 'correo', 'horario', 'whatsapp', 'twitter', 'facebook', 'instagram', 'youtube', 'linkedin', 'logo'
    ];
}