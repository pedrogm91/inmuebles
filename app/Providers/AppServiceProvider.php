<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\View;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Facades\Schema;

use App\View\Composers\BannersComposer;
use App\View\Composers\CategoryComposer;
use App\View\Composers\CompanyComposer;
use App\View\Composers\FormatComposer;
use App\View\Composers\LinksComposer;
use App\View\Composers\MenusComposer;
use App\View\Composers\StateComposer;
use App\View\Composers\CitiesComposer;


class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Paginator::useBootstrapFive();
        View::composer('*', BannersComposer::class);
        View::composer('*', CategoryComposer::class);
        View::composer('*', CompanyComposer::class);
        View::composer('*', FormatComposer::class);
        View::composer('*', LinksComposer::class);
        View::composer('*', MenusComposer::class);
        View::composer('*', StateComposer::class);
        View::composer('*', CitiesComposer::class);
    }
   
}
