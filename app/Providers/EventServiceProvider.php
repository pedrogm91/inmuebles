<?php

namespace App\Providers;

use App\Models\citas;
use App\Models\Negotiation;
use App\Models\Ofertas;
use App\Models\Properties;
use App\Models\Ventas;
use App\Observers\CitasObserver;
use App\Observers\NegotiationObserver;
use App\Observers\OfertasObserver;
use App\Observers\PropertyObserver;
use App\Observers\VentasObserver;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array<class-string, array<int, class-string>>
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Determine if events and listeners should be automatically discovered.
     *
     * @return bool
     */
    public function shouldDiscoverEvents()
    {
        return false;
    }

    /**
     * The model observers for your application.
     *
     * @var array
     */
    protected $observers = [
        Properties::class => [PropertyObserver::class],
        citas::class => [CitasObserver::class],
        Ventas::class => [VentasObserver::class],
        Ofertas::class => [OfertasObserver::class],
        Negotiation::class => [NegotiationObserver::class]
    ];
}
